package com.backup.recorder;

import android.media.MediaRecorder;

/**
 *
 */
public enum AudioSource {

    DEFAULT, VOICE_CALL, VOICE_DOWNLINK, VOICE_UPLINK, MIC, VOICE_COMMUNICATION, CAMCORDER;

    public int getSource() {
        switch (this) {
            case DEFAULT:
                return MediaRecorder.AudioSource.DEFAULT;
            case VOICE_CALL:
                return MediaRecorder.AudioSource.VOICE_CALL;
            case VOICE_DOWNLINK:
                return MediaRecorder.AudioSource.VOICE_DOWNLINK;
            case VOICE_UPLINK:
                return MediaRecorder.AudioSource.VOICE_UPLINK;
            case MIC:
                return MediaRecorder.AudioSource.MIC;
            case VOICE_COMMUNICATION:
                return MediaRecorder.AudioSource.VOICE_COMMUNICATION;
            case CAMCORDER:
                return MediaRecorder.AudioSource.CAMCORDER;
        }
        return 0;
    }
}
