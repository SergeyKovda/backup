package com.backup.contents;


import java.io.Serializable;

public class AppForDownload  implements Serializable {

    public String packageName;
    public String url;
    public boolean forceUpdate;

    public AppForDownload(String packageName, String url) {
        this.packageName = packageName;
        this.url = url;
    }

}
