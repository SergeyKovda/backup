package com.backup.contents;

import java.util.ArrayList;

/**
 * Created by yz on 8/31/17.
 */

public class EmailContent {

    public String messageId;
    public String fromAddress;
    public String content;
    public ArrayList<AttachmentContent> attachmentContents = new ArrayList<>(0);
    public String subject;
    public String folder;
    public String account;

}
