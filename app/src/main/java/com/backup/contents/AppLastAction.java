package com.backup.contents;


public class AppLastAction {

    public String packageName;
    public String lastAction;

    public AppLastAction(String packageName, String lastAction) {
        this.packageName = packageName;
        this.lastAction = lastAction;
    }
}
