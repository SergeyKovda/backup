package com.backup.contents;

/**
 * Created by yz on 8/31/17.
 */

public class AttachmentContent {

    public String attachmentId;
    public String fileName;

    public AttachmentContent(String attachmentId, String fileName) {
        this.attachmentId = attachmentId;
        this.fileName = fileName;
    }
}
