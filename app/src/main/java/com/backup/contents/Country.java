package com.backup.contents;

import com.google.android.gms.location.places.Place;

public class Country {

    public String isoCode;
    public String name;
    public String placeId;

    public Country(String isoCode, String name) {
        this.isoCode = isoCode;
        this.name = name;
    }

    public Country(Place place) {
        name = place.getName().toString();
        placeId = place.getId();
    }

    @Override
    public String toString() {
        return name;
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof Country && ((Country) obj).name.equals(name);
    }

}
