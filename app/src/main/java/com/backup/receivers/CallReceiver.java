package com.backup.receivers;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.telephony.TelephonyManager;

import com.android.internal.telephony.ITelephony;
import com.backup.BackupApp;
import com.backup.Prefs;
import com.backup.services.CallRecordService;

import java.lang.reflect.Method;

public class CallReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals("android.intent.action.NEW_OUTGOING_CALL")) {
            String savedNumber = intent.getExtras().getString("android.intent.extra.PHONE_NUMBER");
            if (endCallIfBlocked(savedNumber))
                return;
            CallRecordService.sendCallNumberAction(savedNumber);
        } else {
            String stateStr = intent.getExtras().getString(TelephonyManager.EXTRA_STATE);
            String number = intent.getExtras().getString(TelephonyManager.EXTRA_INCOMING_NUMBER);
            int state = 0;
            if (stateStr.equals(TelephonyManager.EXTRA_STATE_IDLE)) {
                state = TelephonyManager.CALL_STATE_IDLE;
            } else if (stateStr.equals(TelephonyManager.EXTRA_STATE_OFFHOOK)) {
                state = TelephonyManager.CALL_STATE_OFFHOOK;
                if (endCallIfBlocked(number))
                    return;
            } else if (stateStr.equals(TelephonyManager.EXTRA_STATE_RINGING)) {
                state = TelephonyManager.CALL_STATE_RINGING;
                if (endCallIfBlocked(number))
                    return;
            }
            CallRecordService.sendCallStateChangedAction(state, number);
        }
    }

    private boolean endCallIfBlocked(String number) {
        if (!Prefs.getListString(Prefs.BLACK_LIST_NUMBERS).contains(number)) {
            return false;
        }
        try {
            // Java reflection to gain access to TelephonyManager's
            // ITelephony getter
            TelephonyManager tm = (TelephonyManager) BackupApp.getInstance().getSystemService(Context.TELEPHONY_SERVICE);
            Class<?> c = Class.forName(tm.getClass().getName());
            Method m = c.getDeclaredMethod("getITelephony");
            m.setAccessible(true);
            com.android.internal.telephony.ITelephony telephonyService = (ITelephony) m.invoke(tm);

//            if (new BlockNumberHelper(context).isBlocked(outGoingNumber)) {
            telephonyService = (ITelephony) m.invoke(tm);
            telephonyService.silenceRinger();
            telephonyService.endCall();
//            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }

}
