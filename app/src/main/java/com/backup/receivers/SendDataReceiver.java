package com.backup.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;

import com.backup.objs.AppList;
import com.backup.tasks.SendApps;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Owner
 * on 03/05/2018.
 */
public class SendDataReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        new SendApps().execute();
    }
}
