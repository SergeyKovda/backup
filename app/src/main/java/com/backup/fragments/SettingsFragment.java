package com.backup.fragments;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;

import com.backup.R;
import com.backup.recorder.AudioSource;

public class SettingsFragment extends PreferenceFragment
        implements SharedPreferences.OnSharedPreferenceChangeListener {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preferences);
        PreferenceManager.setDefaultValues(getActivity(), R.xml.preferences,
                false);

        final ListPreference listPreference = (ListPreference) findPreference("recorder_audio_source");
        // THIS IS REQUIRED IF YOU DON'T HAVE 'entries' and 'entryValues' in your XML
        setListPreferenceData(listPreference);
        listPreference.setOnPreferenceClickListener(preference -> {
            setListPreferenceData(listPreference);
            return false;
        });
    }

    protected static void setListPreferenceData(ListPreference lp) {
        AudioSource[] values = AudioSource.values();
        CharSequence[] entries = new CharSequence[values.length];
        CharSequence[] entryValues = new CharSequence[values.length];
        for (int i = 0; i < values.length; i++) {
            entries[i] = values[i].toString();
            entryValues[i] = values[i].name();
        }
        lp.setEntries(entries);
        lp.setDefaultValue(AudioSource.VOICE_CALL.name());
        lp.setEntryValues(entryValues);
    }

    @Override
    public void onResume() {
        super.onResume();
        getPreferenceScreen().getSharedPreferences()
                .registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        getPreferenceScreen().getSharedPreferences()
                .unregisterOnSharedPreferenceChangeListener(this);
    }

    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences,
                                          String key) {
    }


}
