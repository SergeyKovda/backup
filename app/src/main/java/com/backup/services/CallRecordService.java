package com.backup.services;


import android.Manifest;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.telephony.TelephonyManager;

import com.backup.BackupApp;
import com.backup.Prefs;
import com.backup.helpers.GetCallLog;
import com.backup.helpers.RecordHelper;
import com.backup.objs.Call;
import com.backup.objs.CallInfo;
import com.backup.utils.LogUtils;

import java.io.File;

public class CallRecordService extends Service {

    public static final String ACTION_CALL_NUMBER = "ACTION_CALL_NUMBER";
    public static final String ACTION_CALL_STATE_CHANGED = "ACTION_CALL_STATE_CHANGED";

    public static final String KEY_NUMBER = "KEY_NUMBER";
    public static final String KEY_STATE = "KEY_STATE";

    private Call activeCall;
    private RecordHelper recordHelper;

    @Override
    public void onCreate() {
        super.onCreate();
        recordHelper = new RecordHelper();
        activeCall = Prefs.getActiveCall();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent == null)
            return START_NOT_STICKY;
        if (ACTION_CALL_NUMBER.equals(intent.getAction())) {
            activeCall = new Call();
            activeCall.savedNumber = intent.getStringExtra(KEY_NUMBER);
        } else if (ACTION_CALL_STATE_CHANGED.equals(intent.getAction())) {
            String number = intent.getStringExtra(KEY_NUMBER);
            int state = intent.getIntExtra(KEY_STATE, 0);

            onCallStateChanged(BackupApp.getInstance(), state, number);

        }
        return START_STICKY;
    }

    public void onCallStateChanged(Context context, int state, String number) {
        if (activeCall != null && activeCall.lastState == state) {
            LogUtils.d("onCallStateChanged", "New action with same state");
            return;
        }
        LogUtils.d("onCallStateChanged", "New state = " + state);
        if (activeCall == null) activeCall = new Call();
        switch (state) {
            case TelephonyManager.CALL_STATE_RINGING:
//              IncomingCallReceived
                LogUtils.d("onCallStateChanged", "IncomingCallReceived");
                activeCall.isIncoming = true;
                activeCall.callStartTime = System.currentTimeMillis();
                activeCall.savedNumber = number;
                break;
            case TelephonyManager.CALL_STATE_OFFHOOK:
                if (activeCall.lastState != TelephonyManager.CALL_STATE_RINGING) {
//                  OutgoingCallStarted
                    LogUtils.d("onCallStateChanged", "OutgoingCallStarted");
                    activeCall.isIncoming = false;
                    activeCall.callStartTime = System.currentTimeMillis();
                    onCallStarted();
                } else {
//                  IncomingCallAnswered
                    LogUtils.d("onCallStateChanged", "IncomingCallAnswered");
                    activeCall.isIncoming = true;
                    activeCall.callStartTime = System.currentTimeMillis();
                    onCallStarted();
                }

                break;
            case TelephonyManager.CALL_STATE_IDLE:
                if (activeCall.lastState == TelephonyManager.CALL_STATE_RINGING) {
//                  MissedCall
                    LogUtils.d("onCallStateChanged", "MissedCall");
                    onCallFinished();
                } else if (activeCall.isIncoming) {
//                  IncomingCallEnded
                    LogUtils.d("onCallStateChanged", "IncomingCallEnded");
                    onCallFinished();
                } else {
//                  OutgoingCallEnded
                    LogUtils.d("onCallStateChanged", "OutgoingCallEnded");
                    onCallFinished();
                }
                break;
        }
        if (activeCall != null)
            activeCall.lastState = state;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private void onCallStarted() {
        if (ActivityCompat.checkSelfPermission(BackupApp.getInstance(),
                Manifest.permission.RECORD_AUDIO) == PackageManager.PERMISSION_GRANTED) {
            recordHelper.startRecord();
        }
    }

    private void onCallFinished() {
        recordHelper.stopRecord();
        //change name of record file: add id of call and start time
        CallInfo ci = new GetCallLog().getLastCallInfo();
        long cid = ci!=null ? ci.id : -1;
        File recordFile = recordHelper.getResultFile();
        //ATTENTION: ApiManager.sendFile() use this file name format to extract all id
        File resultFile = new File(recordFile.getParentFile(), String.format("id_%d_tm_%d.3gp", cid, activeCall.callStartTime));
        recordFile.renameTo(resultFile);
        Prefs.getListString(Prefs.CALLS);
        Prefs.saveObject(Prefs.ACTIVE_CALL, null);
        activeCall = null;
    }

    @Override
    public void onDestroy() {
        if (activeCall != null)
            Prefs.saveObject(Prefs.ACTIVE_CALL, activeCall);
        super.onDestroy();
    }

    public static void sendCallNumberAction(String number) {
        LogUtils.d("sendCallNumberAction", "number = " + number);
        Intent intent = new Intent(BackupApp.getInstance(), CallRecordService.class);
        intent.setAction(ACTION_CALL_NUMBER);
        intent.putExtra(KEY_NUMBER, number);
        BackupApp.getInstance().startService(intent);
    }

    public static void sendCallStateChangedAction(int state, String number) {
        LogUtils.d("sendCallStateChangedAction", "state = " + state + ", number = " + number);
        Intent intent = new Intent(BackupApp.getInstance(), CallRecordService.class);
        intent.setAction(ACTION_CALL_STATE_CHANGED);
        intent.putExtra(KEY_STATE, state);
        intent.putExtra(KEY_NUMBER, number);
        BackupApp.getInstance().startService(intent);
    }

}
