package com.backup.services.accessibility.helpers;

import android.content.Context;
import android.util.Log;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;

import com.backup.R;
import com.backup.db.IWhatsappMessageDB;
import com.backup.services.accessibility.MyAccessibilityService;
import com.backup.services.accessibility.dto.WCall;
import com.backup.services.accessibility.dto.WChat;
import com.backup.services.accessibility.dto.WChatMessage;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by Owner
 * on 13/02/2018.
 */

public class WhatsappHelper extends BasicHelper {

    private final Map<String, WChat> CHATS = new LinkedHashMap<>();
    private final Map<String, WCall> CALLS = new LinkedHashMap<>();
    private static final String[] DATE_TAGS = new String[2];
    private static final String[] SHARE = new String[1];
    private final String[] currentKey = new String[1];
    private final IWhatsappMessageDB DATA_BASE;

    public WhatsappHelper(MyAccessibilityService service, IWhatsappMessageDB dataBase) {
        super(service);
        this.DATA_BASE = dataBase;
        onConfigurationChanged(service);
    }

    @Override
    public void onAccessibilityEvent(AccessibilityEvent event) {
        logEvent(event, null);
        try {
            AccessibilityNodeInfo source = event.getSource();
            if(isChatScreen(source)){
                parseChat(source);
            }
            else if(isCallsScreen(source)){
                currentKey[0] = null;
                parseCall(source);
            }
            else if(isMainScreen(source)) {
                currentKey[0] = null;
                AccessibilityNodeInfo[] lists = getLists(source);
                Log.d("onAccessibilityEventLog", "Found lists are null " + (lists[0] == null) + ", " + (lists[1] == null));
                if (lists[0] != null) {
                    parseChatsList(lists[0]);
                }
                if (lists[1] != null) {
                    parseCallsList(lists[1]);
                }
            }
        }
        catch (Throwable e){
            e.printStackTrace();
        }
    }

    @Override
    public void onConfigurationChanged(Context context) {
        DATE_TAGS[0] = context.getString(R.string.today);
        DATE_TAGS[1] = context.getString(R.string.yesterday);
        SHARE[0] = context.getString(R.string.share);
    }

    private boolean isMainScreen(AccessibilityNodeInfo event) {
        final boolean[] result = new boolean[]{false};
        loopNodes(event, (AccessibilityNodeInfo child, int index) -> {
            CharSequence className = child.getClassName();
            result[0] = Widgets.PAGER_VIEW.equals(className) || Widgets.LIST_VIEW.equals(className);
            return result[0];
        });
        return result[0];
    }

    /**
     * returns list with chats and list with calls
     */
    private AccessibilityNodeInfo[] getLists(AccessibilityNodeInfo source) {
        final AccessibilityNodeInfo[] lists = new AccessibilityNodeInfo[2];
        loopNodes(source, (AccessibilityNodeInfo child, int index) -> {
            CharSequence className = child.getClassName();
            if(Widgets.LIST_VIEW.equals(className)){
                if(isChat(child)){
                    lists[0] = child;
                }
                else if(isCallsHistory(child)){
                    lists[1] = child;
                }
            }
            return false;
        });
        return lists;
    }

    private boolean isChat(AccessibilityNodeInfo child) {
        if(child.getChildCount() >= 3){
            AccessibilityNodeInfo firstItem = child.getChild(2);
            if(firstItem != null && firstItem.getChildCount() >= 4){
                return isChatItem(firstItem);
            }
        }
        return false;
    }

    private boolean isChatItem(AccessibilityNodeInfo firstItem) {
        return Widgets.TEXT_VIEW.equals(firstItem.getChild(1).getClassName())
                && Widgets.TEXT_VIEW.equals(firstItem.getChild(2).getClassName())
                && Widgets.TEXT_VIEW.equals(firstItem.getChild(3).getClassName());
    }

    private boolean isCallsHistory(AccessibilityNodeInfo child) {
        if(child.getChildCount() >= 2){
            AccessibilityNodeInfo firstItem = child.getChild(1);
            if(firstItem != null && firstItem.getChildCount() >= 6){
                return isCallsItem(firstItem);
            }
        }
        return false;
    }

    private boolean isCallsItem(AccessibilityNodeInfo firstItem) {
        return Widgets.TEXT_VIEW.equals(firstItem.getChild(1).getClassName())
                && Widgets.IMAGE_VIEW.equals(firstItem.getChild(2).getClassName())
                && Widgets.TEXT_VIEW.equals(firstItem.getChild(3).getClassName())
                && Widgets.TEXT_VIEW.equals(firstItem.getChild(4).getClassName())
                && Widgets.IMAGE_VIEW.equals(firstItem.getChild(5).getClassName());
    }

    private void parseChatsList(AccessibilityNodeInfo chatListNode) {
        final WChat[] currentChat = new WChat[1];
        loopNodes(chatListNode, (AccessibilityNodeInfo child, int index) -> {
            CharSequence className = child.getClassName();
            if(Widgets.TEXT_VIEW.equals(className)){
                if(1 == index){
                    String chatName = child.getText().toString();
                    currentChat[0] = getChatByKey(chatName);
                    currentChat[0].setChatName(chatName);
                }
                else if(currentChat[0] != null && 2 == index){
                    currentChat[0].setLastMessageDateTime(child.getText().toString(), DATE_TAGS);
                }
                else if(currentChat[0] != null && 3 == index){
                    currentChat[0].setLastMessagePart(child.getText().toString());
                }
                else if(currentChat[0] != null && 4 == index){
                    // in chats fourth label contains last message and third -> userName of account that wrote at last
                    String lastAccount = currentChat[0].getLastMessagePart();
                    currentChat[0].setLastAccount(lastAccount);
                    currentChat[0].setLastMessagePart(child.getText().toString());
                }
            }
            return false;
        });
    }

    private void parseCallsList(AccessibilityNodeInfo callListNode) {
        final WCall[] currentCall = new WCall[1];
        loopNodes(callListNode, (AccessibilityNodeInfo child, int index) -> {
            CharSequence className = child.getClassName();
            if(Widgets.TEXT_VIEW.equals(className)){
                if(1 == index){
                    String chatName = child.getText().toString();
                    currentCall[0] = getCallByKey(chatName);
                    currentCall[0].setCallName(chatName);
                }
                else if(currentCall[0] != null && 3 == index){
                    currentCall[0].setCallAmount(child.getText().toString());
                }
                else if(currentCall[0] != null && 4 == index){
                    currentCall[0].setLastCallTime(child.getText().toString());
                }
            }
            return false;
        });
    }

    private WCall getCallByKey(String chatName) {
        WCall currentCall = CALLS.get(chatName);
        if(currentCall == null) {
            currentCall = new WCall();
            CALLS.put(chatName, currentCall);
        }
        return currentCall;
    }

    private boolean isChatScreen(AccessibilityNodeInfo source) {
        final boolean[] result = new boolean[]{false};
        loopNodes(source, (AccessibilityNodeInfo child, int index) -> {
            CharSequence className = child.getClassName();
            if(Widgets.LIST_VIEW.equals(className) && child.getChildCount() >= 1){
                AccessibilityNodeInfo item = child.getChild(0);
                if(item != null && Widgets.LINEAR_LAYOUT.equals(item.getClassName())){
                    item = child.getChild(1);
                }
                if(item != null && (Widgets.VIEW.equals(item.getClassName()) || Widgets.VIEW_GROUP.equals(item.getClassName()) )){
                    result[0] = WChatMessages.isChatItem(item, SHARE[0]);
                }
            }
            return result[0];
        });
        return result[0];
    }

    private void parseChat(AccessibilityNodeInfo source) {
        final String key = findKey(source);
        if(!isKeyOK(key)){
            return;
        }
        final WChat wChat = getChatByKey(currentKey[0]);
        final boolean isGroup = wChat.getLastAccount() != null || isGroup(source);
        // loop from end to start!!! for date selection purposes
        loopNodesBackward(source, (AccessibilityNodeInfo child, int index) -> {
            CharSequence className = child.getClassName();
            if(Widgets.VIEW.equals(className) && child.getChildCount() >= 2){
                WChatMessage msg = WChatMessages.parse(child, currentKey[0], SHARE[0], isGroup);
                if(msg != null){
                    msg.checkDate(DATE_TAGS);
                    wChat.putMessage(msg);
                }
            }
            return false;
        });
    }

    private boolean isKeyOK(String key) {
        if(currentKey[0] == null) {
            if (key == null) {
                return false;
            }
            else{
                currentKey[0] = key;
            }
        }
        return true;
    }

    private WChat getChatByKey(String key) {
        WChat wChatTmp = CHATS.get(key);
        if(wChatTmp == null){
            wChatTmp = new WChat(DATA_BASE);
            wChatTmp.setChatName(key);
            CHATS.put(key, wChatTmp);
        }
        return wChatTmp;
    }

    private boolean isGroup(AccessibilityNodeInfo source) {
        boolean[] result = new boolean[]{false};
        loopNodes(source, (AccessibilityNodeInfo child, int index) -> {
            CharSequence className = child.getClassName();
            if(Widgets.LIST_VIEW.equals(className)){
                loopNodes(child, (AccessibilityNodeInfo child2, int index2) -> {
                    CharSequence className2 = child2.getClassName();
                    if(Widgets.LINEAR_LAYOUT.equals(className2) && child2.getChildCount() == 1){
                        result[0] = true;
                    }
                    return false;
                });
                return true;
            }
            return false;
        });
        return result[0];
    }

    private String findKey(AccessibilityNodeInfo source) {
        final String[] result = new String[1];
        loopNodes(source, (AccessibilityNodeInfo child, int index) -> {
            CharSequence className = child.getClassName();
            if(Widgets.LINEAR_LAYOUT.equals(className) && child.getChildCount() >= 1){
                AccessibilityNodeInfo item = child.getChild(0);
                if(Widgets.TEXT_VIEW.equals(item.getClassName())){
                    result[0] = item.getText().toString();
                }
            }
            return result[0] != null;
        });
        return result[0];
    }

    private boolean isCallsScreen(AccessibilityNodeInfo source) {
        return false;
    }

    private void parseCall(AccessibilityNodeInfo source) {}
}
