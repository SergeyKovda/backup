package com.backup.services.accessibility.helpers.chat;

import android.view.accessibility.AccessibilityNodeInfo;

import com.backup.services.accessibility.dto.WChatMessage;
import com.backup.services.accessibility.helpers.Widgets;
import com.backup.services.accessibility.helpers.chat.templates.Text;

/**
 * Created by Owner
 * on 26/02/2018.
 */

public class TextMsg extends Msg {

    public static boolean isTypeMessage(int childCount, AccessibilityNodeInfo item){
        if(childCount < 2 || childCount > 4){
            return false;
        }
        if(childCount == 4){
            return checkSelectedSequence(Text.MSG, item, 0, 3);
        }
        else if(childCount == 3){
            return checkSelectedSequence(Text.MSG, item, 0, 2) || checkSelectedSequence(Text.MSG, item, 1, 3);
        }
        else if(childCount == 2){
            return checkSelectedSequence(Text.MSG, item, 1, 2);
        }
        return false;
    }

    public static WChatMessage parseMessage(AccessibilityNodeInfo child, String key){
        int childCount = child.getChildCount();
        String author = DEF_AUTHOR;
        AccessibilityNodeInfo date = null;
        AccessibilityNodeInfo content = null;
        AccessibilityNodeInfo time = null;
        AccessibilityNodeInfo status = null;

        if(childCount == 4){
            date = child.getChild(0);
            content = child.getChild(1);
            time = child.getChild(2);
            status = child.getChild(3);
        }
        else if(childCount == 3){
            AccessibilityNodeInfo last = child.getChild(childCount - 1);
            if(Widgets.IMAGE_VIEW.equals(last.getClassName())){
                content = child.getChild(0);
                time = child.getChild(1);
                status = child.getChild(2);
            }
            else{
                date = child.getChild(0);
                content = child.getChild(1);
                time = child.getChild(2);
            }
        }
        else if(childCount == 2){
            content = child.getChild(0);
            time = child.getChild(1);
        }

        if(status == null){
            author = key;
        }
        return WChatMessage.createTextMsg(date, content, time, author);
    }

    public static boolean isGroupTypeMessage(int childCount, AccessibilityNodeInfo item){
        return isGroupTypeMessageI(childCount, item).isTypedMsg;
    }

    private static Wrapper isGroupTypeMessageI(int childCount, AccessibilityNodeInfo item){
        if(childCount < 2 || childCount > 4){
            return MISTAKE_WRAPPER;
        }
        if(childCount == 4){
            if(Widgets.LINEAR_LAYOUT.equals(item.getChild(1).getClassName())) {
                return new Wrapper(checkSelectedSequence(Text.MSG_GROUP_INCOME, item, 0, 3), Text.MSG_GROUP_INCOME);
            }
            else if(Widgets.TEXT_VIEW.equals(item.getChild(1).getClassName())) {
                return new Wrapper(checkSelectedSequence(Text.MSG, item, 0, 3), Text.MSG);
            }
        }
        else if(childCount == 3){
            if(Widgets.LINEAR_LAYOUT.equals(item.getChild(0).getClassName())) {
                return new Wrapper(checkSelectedSequence(Text.MSG_GROUP_INCOME, item, 1, 3), Text.MSG_GROUP_INCOME);
            }
            else if(Widgets.TEXT_VIEW.equals(item.getChild(0).getClassName())) {
                return new Wrapper(checkSelectedSequence(Text.MSG, item, 1, 3), Text.MSG);
            }
        }
        else if(childCount == 2){
            return new Wrapper(checkSelectedSequence(Text.MSG, item, 1, 2), Text.MSG);
        }
        return MISTAKE_WRAPPER;
    }

    public static WChatMessage parseGroupMessage(AccessibilityNodeInfo child) {
        int childCount = child.getChildCount();
        Wrapper type = isGroupTypeMessageI(childCount, child);
        if(!type.isTypedMsg){
            return null;
        }
        CharSequence author;
        AccessibilityNodeInfo date = null;
        AccessibilityNodeInfo authorNode = null;
        AccessibilityNodeInfo content = null;
        AccessibilityNodeInfo time = null;
        AccessibilityNodeInfo status = null;

        if(childCount == 4){
            if(Text.MSG_GROUP_INCOME == type.Seq){
                date = child.getChild(0);
                authorNode = child.getChild(1).getChild(0);
                content = child.getChild(2);
                time = child.getChild(3);
            }
            else if(Text.MSG == type.Seq){
                date = child.getChild(0);
                content = child.getChild(1);
                time = child.getChild(2);
                status = child.getChild(3);
            }
        }
        else if(childCount == 3){
            if(Text.MSG_GROUP_INCOME == type.Seq){
                authorNode = child.getChild(0).getChild(0);
                content = child.getChild(1);
                time = child.getChild(2);
            }
            else if(Text.MSG == type.Seq){
                content = child.getChild(0);
                time = child.getChild(1);
                status = child.getChild(2);
            }
        }
        else if(childCount == 2 && Text.MSG == type.Seq){
            content = child.getChild(0);
            time = child.getChild(1);
        }
        if(status != null){
            author = DEF_AUTHOR;
        }
        else if(authorNode != null){
            author = authorNode.getText();
        }
        else {
            author = PREV_AUTHOR;
        }
        return WChatMessage.createTextMsg(date, content, time, author);
    }
}
