package com.backup.services.accessibility.helpers;

import android.graphics.Rect;
import android.text.TextUtils;
import android.util.Log;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;

import com.backup.services.accessibility.IAccessibilityHelper;
import com.backup.services.accessibility.IAction;
import com.backup.services.accessibility.MyAccessibilityService;

/**
 * Created by Owner
 * on 13/02/2018.
 */

public abstract class BasicHelper implements IAccessibilityHelper {
    protected final MyAccessibilityService service;

    BasicHelper(MyAccessibilityService service) {
        this.service = service;
    }

    void loopNodes(AccessibilityNodeInfo source, IAction action){
        loopNodes(source, true/*check root node or not*/, action);
    }

    private void loopNodes(AccessibilityNodeInfo source, boolean checkSource, IAction action){
        if(source == null || action == null || (checkSource && action.execute(source, 0))){
            return;
        }
        int count = source.getChildCount();
        for(int i = 0; i < count; i++){
            AccessibilityNodeInfo child = source.getChild(i);
            if(child == null || action.execute(child, i)){
                return;
            }
            loopNodes(child, false, action);
        }
    }

    void loopNodesBackward(AccessibilityNodeInfo source, IAction action){
        loopNodesBackward(source, true, action);
    }

    private void loopNodesBackward(AccessibilityNodeInfo source, boolean checkSource, IAction action) {
        if(source == null || action == null || (checkSource && action.execute(source, 0))){
            return;
        }
        int count = source.getChildCount() - 1;
        for(int i = count; i >= 0; i--){
            AccessibilityNodeInfo child = source.getChild(i);
            if(child == null || action.execute(child, i)){
                return;
            }
            loopNodesBackward(child, false, action);
        }
    }



    // <!-- debug functions
    public static void logEvent(AccessibilityEvent event, String classFilter){
        if(event == null) {
            return;
        }
        showChildInfo(event.getSource(), new Rect(), new Rect(), 0, classFilter);
    }

    private static void showChildInfo(AccessibilityNodeInfo sourceX, Rect rS, Rect rP, int lvl, String filter) {
        if(sourceX == null){
            return;
        }
        StringBuilder sb = new StringBuilder();
        for(int i = 0; i < lvl; i++){
            sb.append(" ");
        }
        String padding = sb.toString();
        sb = null;
        if(lvl == 0){
            logNode(sourceX, filter, rS, rP, padding);
        }
        int count = sourceX.getChildCount();
        for(int i = 0; i < count; i++){
            AccessibilityNodeInfo child = sourceX.getChild(i);
            logNode(child, filter, rS, rP, padding);
            showChildInfo(child, rS, new Rect(), lvl + 1, filter);
        }
        System.out.println();
    }

    private static void logNode(AccessibilityNodeInfo child, String filter, Rect rS, Rect rP, String padding) {
        if(child != null) {
            if(child.getClassName() == null){
                Log.d("onAccessibilityEventLog", "child " + child.toString());
            }
            else {
                String cls = child.getClassName().toString();
                if (TextUtils.isEmpty(filter) || cls.contains(filter)) {
                    child.getBoundsInScreen(rS);
                    child.getBoundsInParent(rP);
                    Log.d("onAccessibilityEventLog", padding + "cls: " + cls
                            + ", posS " + rS.toShortString() + ", posP " + rP.toShortString() + ", desc " + child.getContentDescription() + ", txt " + child.getText());
                }
            }
        }
    }
    // debug functions -->
}
