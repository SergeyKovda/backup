package com.backup.services.accessibility;

import android.view.accessibility.AccessibilityNodeInfo;

/**
 * Created by Owner
 * on 20/12/2017.
 */

@FunctionalInterface
public interface IAction {

    /**
     * returns true if wanted object is found (stop looping), false - otherwise (continue looping)
     */
    boolean execute(AccessibilityNodeInfo child, int index);
}
