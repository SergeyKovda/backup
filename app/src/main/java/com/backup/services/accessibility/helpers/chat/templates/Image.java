package com.backup.services.accessibility.helpers.chat.templates;

import com.backup.services.accessibility.helpers.Widgets;

/**
 * Created by Owner
 * on 08/03/2018.
 */

public class Image {

    // also MSG_GROUP_OUTCOME_TXT
    public static final CharSequence[] MSG_OUTCOME_TXT = new CharSequence[]{ // index max 5, min 4
            Widgets.TEXT_VIEW, // -> date, opt
            Widgets.IMAGE_VIEW, // -> share
            Widgets.IMAGE_VIEW, // -> content
            Widgets.TEXT_VIEW, // -> text
            Widgets.TEXT_VIEW, // -> time
            Widgets.IMAGE_VIEW // -> state
    };

    public static final CharSequence[] MSG_INCOME_TXT = new CharSequence[]{ // index max 4, min 3
            Widgets.TEXT_VIEW, // -> date, opt
            Widgets.IMAGE_VIEW, // -> content
            Widgets.TEXT_VIEW, // -> text
            Widgets.TEXT_VIEW, // -> time
            Widgets.IMAGE_VIEW // -> share
    };

    // also MSG_GROUP_OUTCOME
    public static final CharSequence[] MSG_OUTCOME = new CharSequence[]{ // index max 4, min 3
            Widgets.TEXT_VIEW, // -> date, opt
            Widgets.IMAGE_VIEW, // -> share
            Widgets.IMAGE_VIEW, // -> content
            Widgets.TEXT_VIEW, // -> time
            Widgets.IMAGE_VIEW // -> state
    };

    public static final CharSequence[] MSG_INCOME = new CharSequence[]{ // index max 3, min 2
            Widgets.TEXT_VIEW, // -> date, opt
            Widgets.IMAGE_VIEW, // -> content
            Widgets.TEXT_VIEW, // -> time
            Widgets.IMAGE_VIEW // -> share
    };

    public static final CharSequence[] MSG_GROUP_INCOME_TXT = new CharSequence[]{ // index max 5, min 3
            Widgets.TEXT_VIEW, // -> date, opt
            Widgets.LINEAR_LAYOUT, // -> in inner tv username, opt
            Widgets.IMAGE_VIEW, // -> content
            Widgets.TEXT_VIEW, // -> text
            Widgets.TEXT_VIEW, // -> time
            Widgets.IMAGE_VIEW // -> share
    };

    public static final CharSequence[] MSG_GROUP_INCOME = new CharSequence[]{ // index max 4, min 2
            Widgets.TEXT_VIEW, // -> date, opt
            Widgets.LINEAR_LAYOUT, // -> in inner tv username, opt
            Widgets.IMAGE_VIEW, // -> content
            Widgets.TEXT_VIEW, // -> time
            Widgets.IMAGE_VIEW // -> share
    };

    public static final CharSequence[] MSG_GROUP_INCOME_D = new CharSequence[]{ // index max 4, min 2
            Widgets.TEXT_VIEW, // -> date, opt
            Widgets.LINEAR_LAYOUT, // -> in inner tv username, opt
            Widgets.IMAGE_VIEW, // -> content
            Widgets.BUTTON, // -> download btn
            Widgets.TEXT_VIEW // -> time
    };

    public static final CharSequence[] MSG_GROUP_INCOME_TXT_D = new CharSequence[]{ // index max 5, min 3
            Widgets.TEXT_VIEW, // -> date, opt
            Widgets.LINEAR_LAYOUT, // -> in inner tv username, opt
            Widgets.IMAGE_VIEW, // -> content
            Widgets.BUTTON, // -> download btn
            Widgets.TEXT_VIEW, // -> text
            Widgets.TEXT_VIEW // -> time
    };

    public static final CharSequence[] MSG_INCOME_D = new CharSequence[]{ // index max 3, min 2
            Widgets.TEXT_VIEW, // -> date, opt
            Widgets.IMAGE_VIEW, // -> content
            Widgets.BUTTON, // -> download btn
            Widgets.TEXT_VIEW // -> time
    };

    public static final CharSequence[] MSG_INCOME_TXT_D = new CharSequence[]{ // index max 4, min 3
            Widgets.TEXT_VIEW, // -> date, opt
            Widgets.IMAGE_VIEW, // -> content
            Widgets.BUTTON, // -> download btn
            Widgets.TEXT_VIEW, // -> text
            Widgets.TEXT_VIEW // -> time
    };

    // MSG_GROUP_OUTCOME_D
    public static final CharSequence[] MSG_OUTCOME_P = new CharSequence[]{ // index max 4, min 3
            Widgets.TEXT_VIEW, // -> date, opt
            Widgets.IMAGE_VIEW, // -> content
            Widgets.PROGRESS_BAR, // progress
            Widgets.TEXT_VIEW, // -> time
            Widgets.IMAGE_VIEW // status
    };

    // MSG_GROUP_OUTCOME_TXT_D
    public static final CharSequence[] MSG_OUTCOME_TXT_P = new CharSequence[]{ // index max 5, min 4
            Widgets.TEXT_VIEW, // -> date, opt
            Widgets.IMAGE_VIEW, // -> content
            Widgets.PROGRESS_BAR, // progress
            Widgets.TEXT_VIEW, // -> text
            Widgets.TEXT_VIEW, // -> time
            Widgets.IMAGE_VIEW // status
    };
}
