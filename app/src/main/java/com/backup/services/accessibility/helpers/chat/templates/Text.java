package com.backup.services.accessibility.helpers.chat.templates;


import com.backup.services.accessibility.helpers.Widgets;

/**
 * Created by Owner
 * on 08/03/2018.
 */

public class Text {

    // individual chat msg(in/out), group chat outcome message
    public static final CharSequence[] MSG = new CharSequence[]{ // index max 3 items, min 2
            Widgets.TEXT_VIEW, // -> date, opt
            Widgets.TEXT_VIEW, // content
            Widgets.TEXT_VIEW, // time
            Widgets.IMAGE_VIEW // status, opt (only for outcome msg)
    };

    // group chat income message
    public static final CharSequence[] MSG_GROUP_INCOME = new CharSequence[]{ // index max 3 items, min 2
            Widgets.TEXT_VIEW, // -> date, opt
            Widgets.LINEAR_LAYOUT, // -> author in inner tv, opt
            Widgets.TEXT_VIEW, // content
            Widgets.TEXT_VIEW // time
    };
}
