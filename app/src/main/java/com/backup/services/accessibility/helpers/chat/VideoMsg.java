package com.backup.services.accessibility.helpers.chat;

import android.view.accessibility.AccessibilityNodeInfo;

import com.backup.services.accessibility.dto.WChatMessage;
import com.backup.services.accessibility.helpers.Widgets;
import com.backup.services.accessibility.helpers.chat.templates.Video;


/**
 * Created by Owner
 * on 26/02/2018.
 */

public class VideoMsg extends Msg {

    public static boolean isTypeMessage(int childCount, AccessibilityNodeInfo item){
        return isTypeMessageI(childCount, item).isTypedMsg;
    }

    private static Wrapper isTypeMessageI(int childCount, AccessibilityNodeInfo item){
        if(childCount < 5 || childCount > 8){
            return MISTAKE_WRAPPER;
        }
        if(childCount == 8){
            return new Wrapper(checkSelectedSequence(Video.MSG_OUTCOME_TXT, item, 0, 7), Video.MSG_OUTCOME_TXT);
        }
        if(childCount == 7) {
            if(Widgets.TEXT_VIEW.equals(item.getChild(0).getClassName())){
                if(Widgets.FRAME_LAYOUT.equals(item.getChild(3).getClassName())) {
                    if (Widgets.FRAME_LAYOUT.equals(item.getChild(2).getClassName())) {
                        return new Wrapper(checkSelectedSequence(Video.MSG_OUTCOME_TXT_P, item, 0, 6), Video.MSG_OUTCOME_TXT_P);
                    }
                    else {
                        return new Wrapper(checkSelectedSequence(Video.MSG_OUTCOME, item, 0, 6), Video.MSG_OUTCOME);
                    }
                }
                else if(Widgets.TEXT_VIEW.equals(item.getChild(3).getClassName())){
                    return new Wrapper(checkSelectedSequence(Video.MSG_INCOME_TXT, item, 0, 6), Video.MSG_INCOME_TXT);
                }
            }
            else if(Widgets.IMAGE_VIEW.equals(item.getChild(0).getClassName())){
                return new Wrapper(checkSelectedSequence(Video.MSG_OUTCOME_TXT, item, 1, 7), Video.MSG_OUTCOME_TXT);
            }
        }
        else if(childCount == 6){
            CharSequence first = item.getChild(0).getClassName();
            CharSequence third = item.getChild(2).getClassName();
            if(Widgets.IMAGE_VIEW.equals(first)){
                if (Widgets.FRAME_LAYOUT.equals(item.getChild(1).getClassName())) {
                    return new Wrapper(checkSelectedSequence(Video.MSG_OUTCOME_TXT_P, item, 1, 6), Video.MSG_OUTCOME_TXT_P);
                }
                else if(Widgets.FRAME_LAYOUT.equals(third)) {
                    return new Wrapper(checkSelectedSequence(Video.MSG_OUTCOME, item, 1, 4), Video.MSG_OUTCOME);
                }
                else if(Widgets.TEXT_VIEW.equals(third)){
                    return new Wrapper(checkSelectedSequence(Video.MSG_INCOME_TXT, item, 1, 4), Video.MSG_INCOME_TXT);
                }
            }
            else if(Widgets.TEXT_VIEW.equals(first)){
                if(Widgets.FRAME_LAYOUT.equals(third)) {
                    if(Widgets.TEXT_VIEW.equals(item.getChild(5).getClassName())){
                        return new Wrapper(checkSelectedSequence(Video.MSG_INCOME_TXT_D, item, 0, 5), Video.MSG_INCOME_TXT_D);
                    }
                    else {
                        return new Wrapper(checkSelectedSequence(Video.MSG_OUTCOME_P, item, 0, 5), Video.MSG_OUTCOME_P);
                    }
                }
                else {
                    return new Wrapper(checkSelectedSequence(Video.MSG_INCOME, item, 0, 5), Video.MSG_INCOME);
                }
            }
        }
        else if(childCount == 5){
            if(Widgets.FRAME_LAYOUT.equals(item.getChild(1).getClassName())) {
                if(Widgets.TEXT_VIEW.equals(item.getChild(4).getClassName())){
                    return new Wrapper(checkSelectedSequence(Video.MSG_INCOME_TXT_D, item, 1, 5), Video.MSG_INCOME_TXT_D);
                }
                else {
                    return new Wrapper(checkSelectedSequence(Video.MSG_OUTCOME_P, item, 1, 5), Video.MSG_OUTCOME_P);
                }
            }
            else if(Widgets.TEXT_VIEW.equals(item.getChild(4).getClassName())){
                return new Wrapper(checkSelectedSequence(Video.MSG_INCOME_D, item, 0, 4), Video.MSG_INCOME_D);
            }
            else {
                return new Wrapper(checkSelectedSequence(Video.MSG_INCOME, item, 1, 5), Video.MSG_INCOME);
            }
        }
        else if(childCount == 4){
            return new Wrapper(checkSelectedSequence(Video.MSG_INCOME_D, item, 1, 4), Video.MSG_INCOME_D);
        }
        return MISTAKE_WRAPPER;
    }

    public static WChatMessage parseMessage(AccessibilityNodeInfo child, String key){
        int childCount = child.getChildCount();
        Wrapper type = isTypeMessageI(childCount, child);
        if(!type.isTypedMsg){
            return null;
        }
        String author = DEF_AUTHOR;
        AccessibilityNodeInfo date = null;
        AccessibilityNodeInfo text = null;
        AccessibilityNodeInfo length = null;
        AccessibilityNodeInfo time = null;
        AccessibilityNodeInfo status = null;
        boolean isLoading = false;

        if(childCount == 8 && Video.MSG_OUTCOME_TXT.equals(type.Seq)){ // MSG_OUTCOME_TXT
            date = child.getChild(0);
            text = child.getChild(4);
            length = child.getChild(5);
            time = child.getChild(6);
            status = child.getChild(7);
        }
        if(childCount == 7) {
            if(Video.MSG_OUTCOME.equals(type.Seq)) { // MSG_OUTCOME
                date = child.getChild(0);
                length = child.getChild(4);
                time = child.getChild(5);
                status = child.getChild(6);
            }
            else if(Video.MSG_INCOME_TXT.equals(type.Seq)){ // MSG_INCOME_TXT
                date = child.getChild(0);
                text = child.getChild(3);
                length = child.getChild(4);
                time = child.getChild(5);
            }
            else if(Video.MSG_OUTCOME_TXT.equals(type.Seq)){ // MSG_OUTCOME_TXT
                text = child.getChild(3);
                length = child.getChild(4);
                time = child.getChild(5);
                status = child.getChild(6);
            }
            if(Video.MSG_OUTCOME_TXT_P.equals(type.Seq)){
                date = child.getChild(0);
                text = child.getChild(4);
                time = child.getChild(5);
                status = child.getChild(6);
                isLoading = true;
            }
        }
        else if(childCount == 6){
            if(Video.MSG_OUTCOME.equals(type.Seq)) { // MSG_OUTCOME
                length = child.getChild(3);
                time = child.getChild(4);
                status = child.getChild(5);
            }
            else if(Video.MSG_INCOME_TXT.equals(type.Seq)){ // MSG_INCOME_TXT
                text = child.getChild(2);
                length = child.getChild(3);
                time = child.getChild(4);
            }
            else if(Video.MSG_INCOME.equals(type.Seq)){ // MSG_INCOME
                date = child.getChild(0);
                length = child.getChild(3);
                time = child.getChild(4);
            }
            else if(Video.MSG_OUTCOME_P.equals(type.Seq)){
                date = child.getChild(0);
                time = child.getChild(4);
                status = child.getChild(5);
                isLoading = true;
            }
            else if(Video.MSG_OUTCOME_TXT_P.equals(type.Seq)){
                text = child.getChild(3);
                time = child.getChild(4);
                status = child.getChild(5);
                isLoading = true;
            }
            else if(Video.MSG_INCOME_TXT_D.equals(type.Seq)){
                date = child.getChild(0);
                text = child.getChild(4);
                time = child.getChild(5);
                isLoading = true;
            }
        }
        else if(childCount == 5) {
            if (Video.MSG_INCOME.equals(type.Seq)) { // MSG_INCOME
                length = child.getChild(2);
                time = child.getChild(3);
            }
            else if(Video.MSG_OUTCOME_P.equals(type.Seq)){
                time = child.getChild(3);
                status = child.getChild(4);
                isLoading = true;
            }
            else if(Video.MSG_INCOME_D.equals(type.Seq)){
                date = child.getChild(0);
                time = child.getChild(4);
                isLoading = true;
            }
            else if(Video.MSG_INCOME_TXT_D.equals(type.Seq)){
                text = child.getChild(3);
                time = child.getChild(4);
                isLoading = true;
            }
        }
        else if(childCount == 4) {
            time = child.getChild(3);
            isLoading = true;
        }
        if(status == null){
            author = key;
        }
        return WChatMessage.createVideoMsg(date, text, length, time, author, isLoading);
    }

    public static boolean isGroupTypeMessage(int childCount, AccessibilityNodeInfo item, CharSequence shareLabel){
        return isGroupTypeMessageI(childCount, item, shareLabel).isTypedMsg;
    }

    private static Wrapper isGroupTypeMessageI(int childCount, AccessibilityNodeInfo item, CharSequence shareLabel){
        if(childCount < 5 || childCount > 8){
            return MISTAKE_WRAPPER;
        }
        if(childCount == 8){
            if(Widgets.LINEAR_LAYOUT.equals(item.getChild(1).getClassName())){
                if(Widgets.FRAME_LAYOUT.equals(item.getChild(3).getClassName())
                        && Widgets.FRAME_LAYOUT.equals(item.getChild(4).getClassName())){
                    return new Wrapper(checkSelectedSequence(Video.MSG_GROUP_INCOME_TXT_P, item, 0, 7), Video.MSG_GROUP_INCOME_TXT_P);
                }
                else {
                    return new Wrapper(checkSelectedSequence(Video.MSG_GROUP_INCOME_TXT, item, 0, 7), Video.MSG_GROUP_INCOME_TXT);
                }
            }
            else {
                return new Wrapper(checkSelectedSequence(Video.MSG_GROUP_OUTCOME_TXT, item, 0, 7), Video.MSG_GROUP_OUTCOME_TXT);
            }
        }
        else if(childCount == 7) {
            if(Widgets.IMAGE_VIEW.equals(item.getChild(0).getClassName())){
                return new Wrapper(checkSelectedSequence(Video.MSG_GROUP_OUTCOME_TXT, item, 1, 7), Video.MSG_GROUP_OUTCOME_TXT);
            }
            else if(Widgets.LINEAR_LAYOUT.equals(item.getChild(0).getClassName())){
                if(Widgets.FRAME_LAYOUT.equals(item.getChild(2).getClassName())
                        && Widgets.FRAME_LAYOUT.equals(item.getChild(3).getClassName())){
                    return new Wrapper(checkSelectedSequence(Video.MSG_GROUP_INCOME_TXT_P, item, 1, 7), Video.MSG_GROUP_INCOME_TXT_P);
                }
                else {
                    return new Wrapper(checkSelectedSequence(Video.MSG_GROUP_INCOME_TXT, item, 1, 7), Video.MSG_GROUP_INCOME_TXT);
                }
            }
            else if(Widgets.TEXT_VIEW.equals(item.getChild(0).getClassName())){
                if(Widgets.LINEAR_LAYOUT.equals(item.getChild(1).getClassName())){
                    if(Widgets.FRAME_LAYOUT.equals(item.getChild(3).getClassName())
                            && Widgets.FRAME_LAYOUT.equals(item.getChild(4).getClassName())){
                        if(Widgets.IMAGE_VIEW.equals(item.getChild(6).getClassName())) {
                            return new Wrapper(checkSelectedSequence(Video.MSG_GROUP_INCOME_P, item, 0, 6), Video.MSG_GROUP_INCOME_P);
                        }
                        else{
                            return new Wrapper(checkSelectedSequence(Video.MSG_GROUP_INCOME_TXT_D, item, 0, 6), Video.MSG_GROUP_INCOME_TXT_D);
                        }
                    }
                    else {
                        return new Wrapper(checkSelectedSequence(Video.MSG_GROUP_INCOME, item, 0, 6), Video.MSG_GROUP_INCOME);
                    }
                }
                else if(Widgets.IMAGE_VIEW.equals(item.getChild(1).getClassName())
                        && Widgets.IMAGE_VIEW.equals(item.getChild(6).getClassName())) {
                    if(Widgets.FRAME_LAYOUT.equals(item.getChild(2).getClassName())
                            && Widgets.FRAME_LAYOUT.equals(item.getChild(3).getClassName())) {
                        return new Wrapper(checkSelectedSequence(Video.MSG_OUTCOME_TXT_P, item, 0, 6), Video.MSG_OUTCOME_TXT_P);
                    }
                    else {
                        return new Wrapper(checkSelectedSequence(Video.MSG_GROUP_OUTCOME, item, 0, 6), Video.MSG_GROUP_OUTCOME);
                    }
                }
            }
        }
        else if(childCount == 6){
            CharSequence first = item.getChild(0).getClassName();
            if(Widgets.TEXT_VIEW.equals(first)) {
                if(Widgets.IMAGE_VIEW.equals(item.getChild(1).getClassName())
                        && Widgets.FRAME_LAYOUT.equals(item.getChild(2).getClassName())
                        && Widgets.FRAME_LAYOUT.equals(item.getChild(3).getClassName())) {
                    return new Wrapper(checkSelectedSequence(Video.MSG_OUTCOME_P, item, 0, 5), Video.MSG_OUTCOME_P);
                }
                else if(Widgets.LINEAR_LAYOUT.equals(item.getChild(1).getClassName())){
                    return new Wrapper(checkSelectedSequence(Video.MSG_GROUP_INCOME_D, item, 0, 5), Video.MSG_GROUP_INCOME_D);
                }
            }
            else if(Widgets.IMAGE_VIEW.equals(first)
                    && Widgets.FRAME_LAYOUT.equals(item.getChild(1).getClassName())) {
                if(Widgets.FRAME_LAYOUT.equals(item.getChild(2).getClassName())) {
                    if(shareLabel.equals(item.getChild(5).getContentDescription())) {
                        return new Wrapper(checkSelectedSequence(Video.MSG_OUTCOME_TXT_P, item, 1, 6), Video.MSG_OUTCOME_TXT_P);
                    }
                    else {
                        // same as
                        return new Wrapper(checkSelectedSequence(Video.MSG_GROUP_INCOME_TXT_P, item, 2, 7), Video.MSG_GROUP_INCOME_TXT_P);
                    }
                }
                else {
                    return new Wrapper(checkSelectedSequence(Video.MSG_GROUP_INCOME_TXT, item, 2, 7), Video.MSG_GROUP_INCOME_TXT);
                }
            }
            else if(Widgets.IMAGE_VIEW.equals(first)
                    && Widgets.IMAGE_VIEW.equals(item.getChild(5).getClassName())) {
                return new Wrapper(checkSelectedSequence(Video.MSG_GROUP_OUTCOME, item, 1, 6), Video.MSG_GROUP_OUTCOME);
            }
            else if(Widgets.LINEAR_LAYOUT.equals(first)){
                if(Widgets.FRAME_LAYOUT.equals(item.getChild(2).getClassName())
                        && Widgets.FRAME_LAYOUT.equals(item.getChild(3).getClassName())){
                    if(Widgets.IMAGE_VIEW.equals(item.getChild(5).getClassName())) {
                        return new Wrapper(checkSelectedSequence(Video.MSG_GROUP_INCOME_P, item, 1, 6), Video.MSG_GROUP_INCOME_P);
                    }
                    else{
                        return new Wrapper(checkSelectedSequence(Video.MSG_GROUP_INCOME_TXT_D, item, 1, 6), Video.MSG_GROUP_INCOME_TXT_D);
                    }
                }
                else {
                    return new Wrapper(checkSelectedSequence(Video.MSG_GROUP_INCOME, item, 1, 6), Video.MSG_GROUP_INCOME);
                }
            }
        }
        else if(childCount == 5){
            if(Widgets.IMAGE_VIEW.equals(item.getChild(0).getClassName())
                    && Widgets.FRAME_LAYOUT.equals(item.getChild(1).getClassName())) {
                if(Widgets.FRAME_LAYOUT.equals(item.getChild(2).getClassName())
                        && Widgets.IMAGE_VIEW.equals(item.getChild(4).getClassName())) {

                    return new Wrapper(checkSelectedSequence(Video.MSG_OUTCOME_P, item, 1, 5), Video.MSG_OUTCOME_P);
                }
                else if(Widgets.FRAME_LAYOUT.equals(item.getChild(2).getClassName())){
                    return new Wrapper(checkSelectedSequence(Video.MSG_GROUP_INCOME_P, item, 1, 6), Video.MSG_GROUP_INCOME_P);
                }
                else {
                    return new Wrapper(checkSelectedSequence(Video.MSG_GROUP_INCOME, item, 2, 6), Video.MSG_GROUP_INCOME);
                }
            }
            else if(Widgets.LINEAR_LAYOUT.equals(item.getChild(0).getClassName())){
                return new Wrapper(checkSelectedSequence(Video.MSG_GROUP_INCOME_D, item, 1, 5), Video.MSG_GROUP_INCOME_D);
            }
        }
        return MISTAKE_WRAPPER;
    }

    public static WChatMessage parseGroupMessage(AccessibilityNodeInfo child, CharSequence shareLabel) {
        int childCount = child.getChildCount();
        Wrapper type = isGroupTypeMessageI(childCount, child, shareLabel);
        if(!type.isTypedMsg){
            return null;
        }
        CharSequence author;
        AccessibilityNodeInfo authorNode = null;
        AccessibilityNodeInfo date = null;
        AccessibilityNodeInfo text = null;
        AccessibilityNodeInfo time = null;
        AccessibilityNodeInfo length = null;
        AccessibilityNodeInfo status = null;
        boolean isLoading = false;

        if(childCount == 8){
            if(Video.MSG_GROUP_INCOME_TXT_P.equals(type.Seq)){
                date = child.getChild(0);
                authorNode = child.getChild(1).getChild(0);
                text = child.getChild(5);
                time = child.getChild(6);
                isLoading = true;
            }
            else if(Video.MSG_GROUP_INCOME_TXT.equals(type.Seq)){
                date = child.getChild(0);
                authorNode = child.getChild(1).getChild(0);
                length = child.getChild(4);
                text = child.getChild(5);
                time = child.getChild(6);
            }
            else if(Video.MSG_GROUP_OUTCOME_TXT.equals(type.Seq)){
                date = child.getChild(0);
                length = child.getChild(4);
                text = child.getChild(5);
                time = child.getChild(6);
                status = child.getChild(7);
            }
        }
        else if(childCount == 7) {
            if(Video.MSG_GROUP_OUTCOME_TXT.equals(type.Seq)){
                length = child.getChild(3);
                text = child.getChild(4);
                time = child.getChild(5);
                status = child.getChild(6);
            }
            else if(Video.MSG_GROUP_INCOME_TXT_D.equals(type.Seq)){
                date = child.getChild(0);
                authorNode = child.getChild(1).getChild(0);
                text = child.getChild(5);
                time = child.getChild(6);
                isLoading = true;
            }
            else if(Video.MSG_GROUP_INCOME_TXT_P.equals(type.Seq)){
                authorNode = child.getChild(0).getChild(0);
                text = child.getChild(4);
                time = child.getChild(5);
                isLoading = true;
            }
            else if(Video.MSG_GROUP_INCOME_TXT.equals(type.Seq)){
                authorNode = child.getChild(0).getChild(0);
                length = child.getChild(3);
                text = child.getChild(4);
                time = child.getChild(5);
            }
            else if(Video.MSG_GROUP_INCOME_P.equals(type.Seq)){
                date = child.getChild(0);
                authorNode = child.getChild(1).getChild(0);
                time = child.getChild(5);
                isLoading = true;
            }
            else if(Video.MSG_GROUP_INCOME.equals(type.Seq)){
                date = child.getChild(0);
                authorNode = child.getChild(1).getChild(0);
                length = child.getChild(4);
                time = child.getChild(5);
            }
            else if(Video.MSG_GROUP_OUTCOME.equals(type.Seq)){
                date = child.getChild(0);
                length = child.getChild(4);
                time = child.getChild(5);
                status = child.getChild(6);
            }
            if(Video.MSG_OUTCOME_TXT_P.equals(type.Seq)){
                date = child.getChild(0);
                text = child.getChild(4);
                time = child.getChild(5);
                status = child.getChild(6);
                isLoading = true;
            }
        }
        else if(childCount == 6){
            if(Video.MSG_OUTCOME_TXT_P.equals(type.Seq)){
                text = child.getChild(3);
                time = child.getChild(4);
                status = child.getChild(5);
                isLoading = true;
            }
            else if(Video.MSG_GROUP_INCOME_TXT_D.equals(type.Seq)){
                authorNode = child.getChild(0).getChild(0);
                text = child.getChild(4);
                time = child.getChild(5);
                isLoading = true;
            }
            else if(Video.MSG_GROUP_INCOME_TXT_P.equals(type.Seq)){
                text = child.getChild(3);
                time = child.getChild(4);
                isLoading = true;
            }
            else if(Video.MSG_GROUP_INCOME_D.equals(type.Seq)){
                date = child.getChild(0);
                authorNode = child.getChild(1).getChild(0);
                time = child.getChild(5);
                isLoading = true;
            }
            else if(Video.MSG_GROUP_INCOME_TXT.equals(type.Seq)){
                length = child.getChild(2);
                text = child.getChild(3);
                time = child.getChild(4);
            }
            else if(Video.MSG_GROUP_OUTCOME.equals(type.Seq)){
                length = child.getChild(3);
                time = child.getChild(4);
                status = child.getChild(5);
            }
            else if(Video.MSG_GROUP_INCOME_P.equals(type.Seq)){
                authorNode = child.getChild(0).getChild(0);
                time = child.getChild(4);
                isLoading = true;
            }
            else if(Video.MSG_GROUP_INCOME.equals(type.Seq)){
                authorNode = child.getChild(0).getChild(0);
                length = child.getChild(3);
                time = child.getChild(4);
            }
            else if(Video.MSG_OUTCOME_P.equals(type.Seq)){
                date = child.getChild(0);
                time = child.getChild(4);
                status = child.getChild(5);
                isLoading = true;
            }
        }
        else if(childCount == 5){
            if(Video.MSG_GROUP_INCOME_P.equals(type.Seq)){
                time = child.getChild(5);
                isLoading = true;
            }
            else if(Video.MSG_GROUP_INCOME.equals(type.Seq)){
                length = child.getChild(2);
                time = child.getChild(3);
            }
            else if(Video.MSG_OUTCOME_P.equals(type.Seq)){
                time = child.getChild(3);
                status = child.getChild(4);
                isLoading = true;
            }
            else if(Video.MSG_GROUP_INCOME_D.equals(type.Seq)){
                authorNode = child.getChild(0).getChild(0);
                time = child.getChild(4);
                isLoading = true;
            }
        }
        if(status != null){
            author = DEF_AUTHOR;
        }
        else if(authorNode != null){
            author = authorNode.getText();
        }
        else {
            author = PREV_AUTHOR;
        }
        return WChatMessage.createVideoMsg(date, text, length, time, author, isLoading);
    }
}
