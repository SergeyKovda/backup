package com.backup.services.accessibility.helpers;

/**
 * Created by Owner
 * on 15/02/2018.
 */

public class Widgets {
   public static final CharSequence LIST_VIEW = "android.widget.ListView";
   public static final CharSequence TEXT_VIEW = "android.widget.TextView";
   public static final CharSequence IMAGE_VIEW = "android.widget.ImageView";
   public static final CharSequence PAGER_VIEW = "android.support.v4.view.ViewPager";
   public static final CharSequence VIEW_GROUP = "android.view.ViewGroup";
   public static final CharSequence LINEAR_LAYOUT = "android.widget.LinearLayout";
   public static final CharSequence IMAGE_BTN = "android.widget.ImageButton";
   public static final CharSequence VIEW = "android.view.View";
   public static final CharSequence FRAME_LAYOUT = "android.widget.FrameLayout";
   public static final CharSequence BUTTON = "android.widget.Button";
   public static final CharSequence PROGRESS_BAR = "android.widget.ProgressBar";
}
