package com.backup.services.accessibility.helpers.chat.templates;


import com.backup.services.accessibility.helpers.Widgets;

/**
 * Created by Owner
 * on 08/03/2018.
 */

public class Audio {

    public static final CharSequence[] MSG_INCOME = new CharSequence[]{ // index max 6, min 5
            Widgets.TEXT_VIEW, // -> date, opt
            Widgets.IMAGE_BTN, // -> play
            Widgets.VIEW,
            Widgets.TEXT_VIEW,  // -> length
            Widgets.TEXT_VIEW,  // -> time
            Widgets.IMAGE_VIEW,  // -> user icon
            Widgets.IMAGE_VIEW // -> share
    };

    // also MSG_GROUP_OUTCOME
    public static final CharSequence[] MSG_OUTCOME = new CharSequence[]{ // index max 7, min 6
            Widgets.TEXT_VIEW, // -> date, opt
            Widgets.IMAGE_VIEW, // -> share
            Widgets.IMAGE_VIEW, // -> user icon
            Widgets.IMAGE_BTN, // -> play
            Widgets.VIEW,
            Widgets.TEXT_VIEW, // -> length
            Widgets.TEXT_VIEW, // -> time
            Widgets.IMAGE_VIEW // -> status
    };

    public static final CharSequence[] MSG_GROUP_INCOME = new CharSequence[]{ // index max 7, min 6
            Widgets.TEXT_VIEW, // -> date, opt
            Widgets.IMAGE_BTN, // -> play
            Widgets.VIEW,
            Widgets.LINEAR_LAYOUT, // -> author node
            Widgets.TEXT_VIEW,  // -> length
            Widgets.TEXT_VIEW,  // -> time
            Widgets.IMAGE_VIEW,  // -> user icon
            Widgets.IMAGE_VIEW // -> share
    };

    public static final CharSequence[] MSG_GROUP_INCOME_NO_AUTHOR = new CharSequence[]{ // index max 6, min 5; does it exists?
            Widgets.TEXT_VIEW, // -> date, opt
            Widgets.IMAGE_BTN, // -> play
            Widgets.VIEW,
            Widgets.TEXT_VIEW,  // -> length
            Widgets.TEXT_VIEW,  // -> time
            Widgets.IMAGE_VIEW,  // -> user icon
            Widgets.IMAGE_VIEW // -> share
    };

    public static final CharSequence[] MSG_INCOME_D = new CharSequence[]{ // index max 7, min 6
            Widgets.TEXT_VIEW, // -> date, opt
            Widgets.IMAGE_BTN, // -> play
            Widgets.PROGRESS_BAR, // progress bar
            Widgets.VIEW,
            Widgets.TEXT_VIEW,  // -> file size
            Widgets.TEXT_VIEW,  // -> length
            Widgets.TEXT_VIEW,  // -> time
            Widgets.IMAGE_VIEW  // -> user icon
    };

    public static final CharSequence[] MSG_OUTCOME_D = new CharSequence[]{ // index max 9, min 8
            Widgets.TEXT_VIEW, // -> date, opt
            Widgets.IMAGE_VIEW, // -> user icon
            Widgets.IMAGE_BTN, // -> play
            Widgets.PROGRESS_BAR, // progress bar
            Widgets.VIEW,
            Widgets.TEXT_VIEW, // -> file size
            Widgets.TEXT_VIEW, // -> length
            Widgets.TEXT_VIEW, // -> time
            Widgets.IMAGE_VIEW // -> status
    };

    public static final CharSequence[] MSG_GROUP_OUTCOME_D = new CharSequence[]{ // index max 8, min 7
            Widgets.TEXT_VIEW, // -> date, opt
            Widgets.IMAGE_VIEW, // -> user icon
            Widgets.IMAGE_BTN, // -> play
            Widgets.PROGRESS_BAR, // progress bar
            Widgets.VIEW,
            Widgets.TEXT_VIEW,  // -> size
            Widgets.TEXT_VIEW,  // -> length
            Widgets.TEXT_VIEW,  // -> time
            Widgets.IMAGE_VIEW // -> status
    };

    public static final CharSequence[] MSG_GROUP_INCOME_D = new CharSequence[]{ // index max 8, min 7
            Widgets.TEXT_VIEW, // -> date, opt
            Widgets.IMAGE_BTN, // -> play
            Widgets.PROGRESS_BAR, // progress bar
            Widgets.VIEW,
            Widgets.LINEAR_LAYOUT, // -> author node
            Widgets.TEXT_VIEW,  // -> size
            Widgets.TEXT_VIEW,  // -> length
            Widgets.TEXT_VIEW,  // -> time
            Widgets.IMAGE_VIEW  // -> user icon
    };
}
