package com.backup.services.accessibility;

import android.content.Context;
import android.view.accessibility.AccessibilityEvent;

/**
 * Created by Owner
 * on 13/02/2018.
 */

public interface IAccessibilityHelper {

    void onAccessibilityEvent(AccessibilityEvent event);

    void onConfigurationChanged(Context context);
}
