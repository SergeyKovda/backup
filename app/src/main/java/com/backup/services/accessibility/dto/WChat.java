package com.backup.services.accessibility.dto;

import com.backup.services.accessibility.helpers.IWChatUpdateObserver;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;

/**
 * Created by Owner
 * on 15/02/2018.
 */

public class WChat {
    private static final Pattern PATTERN_DATE = Pattern.compile("^\\d{1,2}\\.\\d{2}\\.\\d{2,4}]*");
    private static final Pattern PATTERN_TIME = Pattern.compile("^\\d{1,2}:\\d{2}]*");
    private String chatName;
    private String lastMessageTime; // 08:05, Yesterday, 12.02.17
    private String lastMessageDate; // 12.02.17
    private String lastMessagePart; // can be a full message or only starting part of message
    private String lastAccount; // only for group chats
    private final List<WChatMessage> MESSAGES = new ArrayList<>();
    private boolean isGroup = false;
    private IWChatUpdateObserver observer;

    public WChat(IWChatUpdateObserver observer) {
        this.observer = observer;
    }

    public void setChatName(String chatName) {
        this.chatName = chatName;
    }

    public void setLastMessageDateTime(String lastMessageTime, String[] dateTags) {
        this.lastMessageTime = lastMessageTime;
        if(lastMessageTime == null){
            return;
        }
        if(PATTERN_DATE.matcher(lastMessageTime).matches()){
            lastMessageDate = lastMessageTime;
        }
        else if(PATTERN_TIME.matcher(lastMessageTime).matches()){
            lastMessageDate = checkDate(dateTags[0], dateTags);
        }
        else{
            lastMessageDate = checkDate(dateTags[1], dateTags);
        }
    }

    private String checkDate(String date, String[] DATE_TAGS) {
        if(date == null){
            return null;
        }
        int index = -1;
        for(int i = 0; i < DATE_TAGS.length; i++){
            String s = DATE_TAGS[i];
            if(s.equalsIgnoreCase(date)){
                index = i;
                break;
            }
        }
        if(index == -1){
            return null;
        }
        if(index == 0){
            return SDFWrapper.formatDate(new Date());
        }
        else if(index == 1){
            Calendar c = Calendar.getInstance();
            c.add(Calendar.DATE, -1);
            return SDFWrapper.formatDate(c.getTime());
        }
        return null;
    }

    public void setLastMessagePart(String lastMessage) {
        this.lastMessagePart = lastMessage;
    }

    public String getLastMessagePart() {
        return lastMessagePart;
    }

    public void setLastAccount(String lastAccount) {
        if(lastAccount != null){
            isGroup = true;
        }
        this.lastAccount = lastAccount;
    }

    public String getLastAccount() {
        return lastAccount;
    }

    public boolean isGroup() {
        return isGroup;
    }

    public void putMessage(WChatMessage message) {
        if(message != null){
            boolean isAdded = false;
            int lastItemIndex = MESSAGES.size() - 1;
            int index = checkForUpdate(message);
            if(index != -1){
                WChatMessage msg = MESSAGES.get(index);
                message.updateDateFrom(msg);
                MESSAGES.set(index, message);
                isAdded = true;
            }
            else if(!MESSAGES.contains(message)) {
                boolean addToStart = checkIfAddToStart(message, MESSAGES);
                if(addToStart){
                    WChatMessage item = MESSAGES.get(0);
                    if(!message.hasDateLabel() && !item.hasDateLabel()){
                        message.updateDateFrom(item);
                    }
                    MESSAGES.add(0, message);
                }
                else {
                    if(MESSAGES.isEmpty()){
                        message.setDate(getLastMessageDate());
                    }
                    MESSAGES.add(message);
                }
                isAdded = true;
            }
            if(isAdded){
                message.setChat(chatName);
                fillDate(message, lastItemIndex);
                fillPrevDates(lastItemIndex, message);
                notifyObserver(message);
            }
        }
    }

    private void notifyObserver(WChatMessage message) {
        if(observer != null) {
            observer.onChatUpdated(message);
        }
    }

    private boolean checkIfAddToStart(WChatMessage message, List<WChatMessage> messages) {
        boolean addToStart = false;
        if(messages != null && !messages.isEmpty()) {
            long dateMs = messages.get(0).getMs();
            long newItemDateMs = message.getMs();
            if(dateMs > newItemDateMs){
                addToStart = true;
            }
        }
        return addToStart;
    }

    /**
     * check if contains same message in another state(downloading or uploading)
     */
    private int checkForUpdate(WChatMessage message) {
        int index = -1;
        if(message.isVideoOrAudio()){
            for(int i = 0 ; i < MESSAGES.size(); i++){
                WChatMessage msg = MESSAGES.get(i);
                if(msg.isLoading() && msg.sameAs(message)){
                    index = i;
                    break;
                }
            }
        }
        return index;
    }

    private void fillDate(WChatMessage message, int lastItemIndex) {
        if(message.getDate() == null) {
            if (lastItemIndex < 0) {
                return;
            }
            WChatMessage msg = MESSAGES.get(lastItemIndex);
            if(msg.getDate() != null) {
                message.updateDateFrom(msg);
            }
        }
    }

    private void fillPrevDates(int lastItemIndex, WChatMessage message) {
        if (message.getDate() == null) {
            return;
        }
        for(int i = lastItemIndex; i >= 0; i--){
            WChatMessage msg = MESSAGES.get(i);
            CharSequence date = msg.getDate();
            if(date == null){
                if(msg.before(message)){
                    msg.updateDateFrom(message);
                    notifyObserver(msg);
                }
                else {
                    break;
                }
            }
            else {
                break;
            }
        }
    }

    private String getLastMessageDate() {
        return lastMessageDate;
    }
}
