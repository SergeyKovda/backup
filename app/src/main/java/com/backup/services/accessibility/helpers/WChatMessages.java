package com.backup.services.accessibility.helpers;

import android.util.Log;
import android.view.accessibility.AccessibilityNodeInfo;

import com.backup.services.accessibility.dto.WChatMessage;
import com.backup.services.accessibility.helpers.chat.AudioMsg;
import com.backup.services.accessibility.helpers.chat.ImageMsg;
import com.backup.services.accessibility.helpers.chat.TextMsg;
import com.backup.services.accessibility.helpers.chat.VideoMsg;

/**
 * Created by Owner
 * on 15/02/2018.
 */

class WChatMessages {

    static boolean isChatItem(AccessibilityNodeInfo item, CharSequence shareLabel) {
        boolean result;
        int childCount = item.getChildCount();
        result = AudioMsg.isTypeMessage(childCount, item) || AudioMsg.isGroupTypeMessage(childCount, item);
        if(!result){
            result = VideoMsg.isTypeMessage(childCount, item) || VideoMsg.isGroupTypeMessage(childCount, item, shareLabel);
        }
        if(!result){
            result = ImageMsg.isTypeMessage(childCount, item) || ImageMsg.isGroupTypeMessage(childCount, item);
        }
        if(!result) {
            result = TextMsg.isTypeMessage(childCount, item) || TextMsg.isGroupTypeMessage(childCount, item);
        }
        return result;
    }

    static WChatMessage parse(AccessibilityNodeInfo child, String key, String shareLabel, boolean isGroup) {
        if(isGroup){
            return WChatMessages.parseMessageInGroup(child, shareLabel);
        }
        else{
            return WChatMessages.parseMessageInChat(child, key);
        }
    }

    private static WChatMessage parseMessageInGroup(AccessibilityNodeInfo child, CharSequence shareLabel) {
        WChatMessage result;
        result = AudioMsg.parseGroupMessage(child);
        if(result == null){
            result = VideoMsg.parseGroupMessage(child, shareLabel);
        }
        if(result == null){
            result = ImageMsg.parseGroupMessage(child);
        }
        if(result == null) {
            result = TextMsg.parseGroupMessage(child);
        }
        if(result == null) {
            Log.e("onAccessibilityEventLog", "CAN'T PARSE MSG!!");
        }
        return result;
    }

    private static WChatMessage parseMessageInChat(AccessibilityNodeInfo child, String key) {
        WChatMessage result;
        result = AudioMsg.parseMessage(child, key);
        if(result == null){
            result = VideoMsg.parseMessage(child, key);
        }
        if(result == null){
            result = ImageMsg.parseMessage(child, key);
        }
        if(result == null) {
            result = TextMsg.parseMessage(child, key);
        }
        if(result == null) {
            Log.e("onAccessibilityEventLog", "CAN'T PARSE MSG!!");
        }
        return result;
    }
}
