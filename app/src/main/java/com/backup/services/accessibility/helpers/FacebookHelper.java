package com.backup.services.accessibility.helpers;

import android.content.Context;
import android.view.accessibility.AccessibilityEvent;

import com.backup.services.accessibility.MyAccessibilityService;

/**
 * Created by Owner
 * on 13/02/2018.
 */

public class FacebookHelper extends BasicHelper {

    public FacebookHelper(MyAccessibilityService service) {
        super(service);
    }

    @Override
    public void onAccessibilityEvent(AccessibilityEvent event) {
    }

    @Override
    public void onConfigurationChanged(Context context) {

    }
}
