package com.backup.services.accessibility.dto;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.regex.Pattern;

/**
 * Created by Owner
 * on 13/03/2018.
 */

public class SDFWrapper {
    public static final Pattern PATTERN_DATE = Pattern.compile("^\\d{1,2}\\.\\d{2}\\.\\d{2,4}]*");
    public static final Pattern PATTERN_TIME = Pattern.compile("^\\d{1,2}:\\d{2}]*");
    private static final String SDF_PATTERN_DATE_MONTH_NAME = "dd MMM y";
    private static final String SDF_PATTERN_DATE = "dd.MM.y";
    private static final String SDF_PATTERN_TIME = "HH:mm";
    private static final SimpleDateFormat SDF = new SimpleDateFormat(SDF_PATTERN_DATE, Locale.getDefault());

    public static String formatDate(Date d){
        if(!SDF_PATTERN_DATE.equals(SDF.toPattern())){
            SDF.applyPattern(SDF_PATTERN_DATE);
        }
        return SDF.format(d);
    }

    static long parseTime(CharSequence time) {
        try {
            if (!SDF_PATTERN_TIME.equals(SDF.toPattern())) {
                SDF.applyPattern(SDF_PATTERN_TIME);
            }
            return SDF.parse((String) time).getTime();
        }
        catch (Exception e){
            e.printStackTrace();
        }
        return -1;
    }

    static long parseDate(CharSequence time) {
        try {
            if (!SDF_PATTERN_DATE.equals(SDF.toPattern())) {
                SDF.applyPattern(SDF_PATTERN_DATE);
            }
            return SDF.parse((String) time).getTime();
        }
        catch (Exception e){
            e.printStackTrace();
        }
        return -1;
    }

    static long parseDateWithMonthName(CharSequence time) {
        try {
            if (!SDF_PATTERN_DATE_MONTH_NAME.equals(SDF.toPattern())) {
                SDF.applyPattern(SDF_PATTERN_DATE_MONTH_NAME);
            }
            return SDF.parse((String) time).getTime();
        }
        catch (Exception e){
            e.printStackTrace();
        }
        return -1;
    }

    static long parseWMsgDate(CharSequence date) {
        if(PATTERN_DATE.matcher(date).matches()){
            return SDFWrapper.parseDate(date);
        }
        else{
            return SDFWrapper.parseDateWithMonthName(date);
        }
    }
}
