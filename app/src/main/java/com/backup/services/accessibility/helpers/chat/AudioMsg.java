package com.backup.services.accessibility.helpers.chat;

import android.view.accessibility.AccessibilityNodeInfo;

import com.backup.services.accessibility.dto.WChatMessage;
import com.backup.services.accessibility.helpers.Widgets;
import com.backup.services.accessibility.helpers.chat.templates.Audio;

/**
 * Created by Owner
 * on 26/02/2018.
 */

public class AudioMsg extends Msg {

    public static boolean isTypeMessage(int childCount, AccessibilityNodeInfo item){
        return isTypeMessageI(childCount, item).isTypedMsg;
    }

    private static Wrapper isTypeMessageI(int childCount, AccessibilityNodeInfo item){
        if(childCount < 6 || childCount > 9){
            return MISTAKE_WRAPPER;
        }
        if(childCount == 9) {
            return new Wrapper(checkSelectedSequence(Audio.MSG_OUTCOME_D, item, 0, 8), Audio.MSG_OUTCOME_D);
        }
        else if(childCount == 8){
            if(Widgets.TEXT_VIEW.equals(item.getChild(0).getClassName())) {
                if (Widgets.PROGRESS_BAR.equals(item.getChild(2).getClassName())) {
                    return new Wrapper(checkSelectedSequence(Audio.MSG_INCOME_D, item, 0, 7), Audio.MSG_INCOME_D);
                }
                else {
                    return new Wrapper(checkSelectedSequence(Audio.MSG_OUTCOME, item, 0, 7), Audio.MSG_OUTCOME);
                }
            }
            else if(Widgets.PROGRESS_BAR.equals(item.getChild(2).getClassName())) {
                return new Wrapper(checkSelectedSequence(Audio.MSG_OUTCOME_D, item, 1, 8), Audio.MSG_OUTCOME_D);
            }
        }
        else if(childCount == 7) {
            CharSequence first = item.getChild(0).getClassName();
            if(Widgets.TEXT_VIEW.equals(first)
                    && Widgets.IMAGE_BTN.equals(item.getChild(1).getClassName())){
                return new Wrapper(checkSelectedSequence(Audio.MSG_INCOME, item, 0, 6), Audio.MSG_INCOME);
            }
            else if(Widgets.IMAGE_VIEW.equals(first)){
                return new Wrapper(checkSelectedSequence(Audio.MSG_OUTCOME, item, 1, 7), Audio.MSG_OUTCOME);
            }
            else if(Widgets.PROGRESS_BAR.equals(item.getChild(1).getClassName())){
                return new Wrapper(checkSelectedSequence(Audio.MSG_INCOME_D, item, 1, 7), Audio.MSG_INCOME_D);
            }
        }
        else if(childCount == 6){
            return new Wrapper(checkSelectedSequence(Audio.MSG_INCOME, item, 1, 6), Audio.MSG_INCOME);
        }
        return MISTAKE_WRAPPER;
    }

    public static WChatMessage parseMessage(AccessibilityNodeInfo child, String key){
        int childCount = child.getChildCount();
        Wrapper type = isTypeMessageI(childCount, child);
        if(!type.isTypedMsg){
            return null;
        }

        String author = DEF_AUTHOR;
        AccessibilityNodeInfo date = null;
        AccessibilityNodeInfo length = null;
        AccessibilityNodeInfo time = null;
        AccessibilityNodeInfo status = null;

        if(childCount == 9){
            if(Audio.MSG_OUTCOME_D.equals(type.Seq)) { // MSG_OUTCOME
                date = child.getChild(0);
                length = child.getChild(6);
                time = child.getChild(7);
                status = child.getChild(8);
            }
        }
        else if(childCount == 8){
            if(Audio.MSG_OUTCOME.equals(type.Seq)) { // MSG_OUTCOME
                date = child.getChild(0);
                length = child.getChild(5);
                time = child.getChild(6);
                status = child.getChild(7);
            }
            else if(Audio.MSG_INCOME_D.equals(type.Seq)){
                date = child.getChild(0);
                length = child.getChild(5);
                time = child.getChild(6);
            }
            else if(Audio.MSG_OUTCOME_D.equals(type.Seq)) { // MSG_OUTCOME
                length = child.getChild(5);
                time = child.getChild(6);
                status = child.getChild(7);
            }
        }
        else if(childCount == 7){
            if(Audio.MSG_INCOME.equals(type.Seq)) {
                date = child.getChild(0);
                length = child.getChild(3);
                time = child.getChild(4);
            }
            else if(Audio.MSG_OUTCOME.equals(type.Seq)){
                length = child.getChild(4);
                time = child.getChild(5);
                status = child.getChild(6);
            }
            else if(Audio.MSG_INCOME_D.equals(type.Seq)){
                length = child.getChild(4);
                time = child.getChild(5);
            }
        }
        else if(childCount == 6 && Audio.MSG_INCOME.equals(type.Seq)){
            length = child.getChild(2);
            time = child.getChild(3);
        }

        if(status == null){
            author = key;
        }
        return WChatMessage.createAudioMsg(date, length, time, author);
    }

    public static boolean isGroupTypeMessage(int childCount, AccessibilityNodeInfo item) {
        return isGroupTypeMessageI(childCount, item).isTypedMsg;
    }

    private static Wrapper isGroupTypeMessageI(int childCount, AccessibilityNodeInfo item){
        if(childCount < 6 || childCount > 9){
            return MISTAKE_WRAPPER;
        }
        if(childCount == 9){
            if(Widgets.LINEAR_LAYOUT.equals(item.getChild(4).getClassName())){
                return new Wrapper(checkSelectedSequence(Audio.MSG_GROUP_INCOME_D, item, 0, 8), Audio.MSG_GROUP_INCOME_D);
            }
            else {
                return new Wrapper(checkSelectedSequence(Audio.MSG_GROUP_OUTCOME_D, item, 0, 8), Audio.MSG_GROUP_OUTCOME_D);
            }
        }
        else if(childCount == 8){
            CharSequence cls = item.getChild(1).getClassName();
            if(Widgets.IMAGE_VIEW.equals(cls)){
                return new Wrapper(checkSelectedSequence(Audio.MSG_OUTCOME, item, 0, 7), Audio.MSG_OUTCOME);
            }
            else if(Widgets.IMAGE_BTN.equals(cls)){
                if(Widgets.PROGRESS_BAR.equals(item.getChild(2).getClassName())){
                    return new Wrapper(checkSelectedSequence(Audio.MSG_GROUP_OUTCOME_D, item, 1, 8), Audio.MSG_GROUP_OUTCOME_D);
                }
                else {
                    return new Wrapper(checkSelectedSequence(Audio.MSG_GROUP_INCOME, item, 0, 7), Audio.MSG_GROUP_INCOME);
                }
            }
            else if(Widgets.PROGRESS_BAR.equals(cls)){
                return new Wrapper(checkSelectedSequence(Audio.MSG_GROUP_INCOME_D, item, 1, 8), Audio.MSG_GROUP_INCOME_D);
            }
        }
        else if(childCount == 7) {
            CharSequence cls = item.getChild(0).getClassName();
            if(Widgets.IMAGE_VIEW.equals(cls)){
                return new Wrapper(checkSelectedSequence(Audio.MSG_OUTCOME, item, 1, 7), Audio.MSG_OUTCOME);
            }
            else if(Widgets.IMAGE_BTN.equals(cls)){
                return new Wrapper(checkSelectedSequence(Audio.MSG_GROUP_INCOME, item, 1, 7), Audio.MSG_GROUP_INCOME);
            }
            else if(Widgets.TEXT_VIEW.equals(cls)){
                return new Wrapper(checkSelectedSequence(Audio.MSG_GROUP_INCOME_NO_AUTHOR, item, 0, 6), Audio.MSG_GROUP_INCOME_NO_AUTHOR);
            }
        }
        else if(childCount == 6){
            return new Wrapper(checkSelectedSequence(Audio.MSG_GROUP_INCOME_NO_AUTHOR, item, 1, 6), Audio.MSG_GROUP_INCOME_NO_AUTHOR);
        }
        return MISTAKE_WRAPPER;
    }

    public static WChatMessage parseGroupMessage(AccessibilityNodeInfo child) {
        int childCount = child.getChildCount();
        Wrapper type = isGroupTypeMessageI(childCount, child);
        if(!type.isTypedMsg){
            return null;
        }

        CharSequence author;
        AccessibilityNodeInfo date = null;
        AccessibilityNodeInfo authorNode = null;
        AccessibilityNodeInfo length = null;
        AccessibilityNodeInfo time = null;
        AccessibilityNodeInfo status = null;

        if(childCount == 9) {
            if(Audio.MSG_GROUP_OUTCOME_D.equals(type.Seq)){
                date = child.getChild(0);
                length = child.getChild(6);
                time = child.getChild(7);
                status = child.getChild(8);
            }
            else if(Audio.MSG_GROUP_INCOME_D.equals(type.Seq)){
                date = child.getChild(0);
                authorNode = child.getChild(4).getChild(0);
                length = child.getChild(6);
                time = child.getChild(7);
            }
        }
        else if(childCount == 8){
            if(Audio.MSG_OUTCOME.equals(type.Seq)){
                date = child.getChild(0);
                length = child.getChild(5);
                time = child.getChild(6);
                status = child.getChild(7);
            }
            else if(Audio.MSG_GROUP_INCOME.equals(type.Seq)){
                date = child.getChild(0);
                authorNode = child.getChild(3).getChild(0);
                length = child.getChild(4);
                time = child.getChild(5);
            }
            else if(Audio.MSG_GROUP_OUTCOME_D.equals(type.Seq)){
                length = child.getChild(5);
                time = child.getChild(6);
                status = child.getChild(7);
            }
            else if(Audio.MSG_GROUP_INCOME_D.equals(type.Seq)){
                authorNode = child.getChild(3).getChild(0);
                length = child.getChild(5);
                time = child.getChild(6);
            }
        }
        else if(childCount == 7) {
            if(Audio.MSG_OUTCOME.equals(type.Seq)){
                length = child.getChild(4);
                time = child.getChild(5);
                status = child.getChild(6);
            }
            else if(Audio.MSG_GROUP_INCOME.equals(type.Seq)){
                authorNode = child.getChild(2).getChild(0);
                length = child.getChild(3);
                time = child.getChild(4);
            }
            else if(Audio.MSG_GROUP_INCOME_NO_AUTHOR.equals(type.Seq)){
                date = child.getChild(0);
                length = child.getChild(3);
                time = child.getChild(4);
            }
        }
        else if(childCount == 6 && Audio.MSG_GROUP_INCOME_NO_AUTHOR.equals(type.Seq)){
            length = child.getChild(2);
            time = child.getChild(3);
        }

        if(status != null){
            author = DEF_AUTHOR;
        }
        else if(authorNode != null){
            author = authorNode.getText();
        }
        else {
            author = PREV_AUTHOR;
        }
        return WChatMessage.createAudioMsg(date, length, time, author);
    }
}
