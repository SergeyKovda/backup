package com.backup.services.accessibility.helpers;

import com.backup.services.accessibility.dto.WChatMessage;

/**
 * Created by Owner
 * on 13/03/2018.
 */

public interface IWChatUpdateObserver {

    void onChatUpdated(WChatMessage message);
}
