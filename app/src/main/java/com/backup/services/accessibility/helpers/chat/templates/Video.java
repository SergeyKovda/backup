package com.backup.services.accessibility.helpers.chat.templates;


import com.backup.services.accessibility.helpers.Widgets;

/**
 * Created by Owner
 * on 08/03/2018.
 */

public class Video {

    public static final CharSequence[] MSG_OUTCOME_TXT = new CharSequence[]{ // index max 7, min 6
            Widgets.TEXT_VIEW, // -> date, opt
            Widgets.IMAGE_VIEW, // -> share
            Widgets.IMAGE_VIEW, // -> camera icon?
            Widgets.FRAME_LAYOUT, // -> play
            Widgets.TEXT_VIEW, // -> text
            Widgets.TEXT_VIEW, // -> length
            Widgets.TEXT_VIEW, // -> time
            Widgets.IMAGE_VIEW // -> state
    };

    public static final CharSequence[] MSG_INCOME_TXT = new CharSequence[]{ // index max 6, min 5
            Widgets.TEXT_VIEW, // -> date, opt
            Widgets.IMAGE_VIEW, // -> icon
            Widgets.IMAGE_VIEW, // -> play
            Widgets.TEXT_VIEW, // -> text
            Widgets.TEXT_VIEW, // -> length
            Widgets.TEXT_VIEW, // -> time
            Widgets.IMAGE_VIEW // -> share
    };

    public static final CharSequence[] MSG_OUTCOME = new CharSequence[]{ // index max 6, min 5
            Widgets.TEXT_VIEW, // -> date, opt
            Widgets.IMAGE_VIEW, // -> share
            Widgets.IMAGE_VIEW, // -> camera icon?
            Widgets.FRAME_LAYOUT, // -> play
            Widgets.TEXT_VIEW, // -> length
            Widgets.TEXT_VIEW, // -> time
            Widgets.IMAGE_VIEW // -> state
    };

    public static final CharSequence[] MSG_INCOME = new CharSequence[]{ // index max 5, min 4
            Widgets.TEXT_VIEW, // -> date, opt
            Widgets.IMAGE_VIEW, // -> icon
            Widgets.IMAGE_VIEW, // -> play
            Widgets.TEXT_VIEW, // -> length
            Widgets.TEXT_VIEW, // -> time
            Widgets.IMAGE_VIEW // -> share
    };

    public static final CharSequence[] MSG_GROUP_OUTCOME = new CharSequence[]{ // index max 6, min 5
            Widgets.TEXT_VIEW, // -> date, opt
            Widgets.IMAGE_VIEW, // -> share
            Widgets.IMAGE_VIEW, // -> content
            Widgets.FRAME_LAYOUT, // -> play, IV
            Widgets.TEXT_VIEW, // -> length
            Widgets.TEXT_VIEW, // -> time
            Widgets.IMAGE_VIEW // -> status
    };

    public static final CharSequence[] MSG_GROUP_OUTCOME_TXT = new CharSequence[]{ // index max 7, min 6
            Widgets.TEXT_VIEW, // -> date, opt
            Widgets.IMAGE_VIEW, // -> share
            Widgets.IMAGE_VIEW, // -> content
            Widgets.FRAME_LAYOUT, // -> play, IV
            Widgets.TEXT_VIEW, // -> length
            Widgets.TEXT_VIEW, // -> text
            Widgets.TEXT_VIEW, // -> time
            Widgets.IMAGE_VIEW // -> status
    };

    public static final CharSequence[] MSG_GROUP_INCOME_TXT = new CharSequence[]{ // index max 8, min 7
            Widgets.TEXT_VIEW, // -> date, opt
            Widgets.LINEAR_LAYOUT, // -> author, opt
            Widgets.IMAGE_VIEW, // -> content
            Widgets.FRAME_LAYOUT, // -> play, IV
            Widgets.TEXT_VIEW, // -> length
            Widgets.TEXT_VIEW, // -> text
            Widgets.TEXT_VIEW, // -> time
            Widgets.IMAGE_VIEW // -> share
    };

    public static final CharSequence[] MSG_GROUP_INCOME = new CharSequence[]{ // index max 6, min 5
            Widgets.TEXT_VIEW, // -> date, opt
            Widgets.LINEAR_LAYOUT, // -> author, opt
            Widgets.IMAGE_VIEW, // -> content
            Widgets.FRAME_LAYOUT, // -> play, IV
            Widgets.TEXT_VIEW, // -> length
            Widgets.TEXT_VIEW, // -> time
            Widgets.IMAGE_VIEW // -> share
    };

    public static final CharSequence[] MSG_GROUP_INCOME_TXT_P = new CharSequence[]{ // index max 8, min 6
            Widgets.TEXT_VIEW, // -> date, opt
            Widgets.LINEAR_LAYOUT, // -> author, opt
            Widgets.IMAGE_VIEW, // -> content
            Widgets.FRAME_LAYOUT, // -> play, IV
            Widgets.FRAME_LAYOUT, // -> progress or btn
            Widgets.TEXT_VIEW, // -> text
            Widgets.TEXT_VIEW, // -> time
            Widgets.IMAGE_VIEW // -> share
    };

    public static final CharSequence[] MSG_GROUP_INCOME_TXT_D = new CharSequence[]{ // index max 8, min 6
            Widgets.TEXT_VIEW, // -> date, opt
            Widgets.LINEAR_LAYOUT, // -> author, opt
            Widgets.IMAGE_VIEW, // -> content
            Widgets.FRAME_LAYOUT, // -> play, IV
            Widgets.FRAME_LAYOUT, // -> progress or btn
            Widgets.TEXT_VIEW, // -> text
            Widgets.TEXT_VIEW // -> time
    };

    public static final CharSequence[] MSG_GROUP_INCOME_P = new CharSequence[]{ // index max 7, min 5
            Widgets.TEXT_VIEW, // -> date, opt
            Widgets.LINEAR_LAYOUT, // -> author, opt
            Widgets.IMAGE_VIEW, // -> content
            Widgets.FRAME_LAYOUT, // -> play, IV
            Widgets.FRAME_LAYOUT, // -> progress or btn
            Widgets.TEXT_VIEW, // -> time
            Widgets.IMAGE_VIEW // -> share
    };

    public static final CharSequence[] MSG_GROUP_INCOME_D = new CharSequence[]{ // index max 7, min 5
            Widgets.TEXT_VIEW, // -> date, opt
            Widgets.LINEAR_LAYOUT, // -> author, opt
            Widgets.IMAGE_VIEW, // -> content
            Widgets.FRAME_LAYOUT, // -> play, IV
            Widgets.FRAME_LAYOUT, // -> progress or btn
            Widgets.TEXT_VIEW // -> time
    };

    // MSG_GROUP_OUTCOME_TXT_D
    public static final CharSequence[] MSG_OUTCOME_TXT_P = new CharSequence[]{ // index max 6, min 5
            Widgets.TEXT_VIEW, // -> date, opt
            Widgets.IMAGE_VIEW, // -> content
            Widgets.FRAME_LAYOUT, // -> play, IV
            Widgets.FRAME_LAYOUT, // -> progress
            Widgets.TEXT_VIEW, // -> text
            Widgets.TEXT_VIEW, // -> time
            Widgets.IMAGE_VIEW // -> status
    };

    // MSG_GROUP_OUTCOME_D
    public static final CharSequence[] MSG_OUTCOME_P = new CharSequence[]{ // index max 5, min 4
            Widgets.TEXT_VIEW, // -> date, opt
            Widgets.IMAGE_VIEW, // -> content
            Widgets.FRAME_LAYOUT, // -> play, IV
            Widgets.FRAME_LAYOUT, // -> progress
            Widgets.TEXT_VIEW, // -> time
            Widgets.IMAGE_VIEW // -> status
    };

    public static final CharSequence[] MSG_INCOME_D = new CharSequence[]{ // index max 4, min 3
            Widgets.TEXT_VIEW, // -> date, opt
            Widgets.IMAGE_VIEW, // -> content
            Widgets.FRAME_LAYOUT, // -> play, IV
            Widgets.FRAME_LAYOUT, // -> progress or btn
            Widgets.TEXT_VIEW // -> time
    };

    public static final CharSequence[] MSG_INCOME_TXT_D = new CharSequence[]{ // index max 5, min 4
            Widgets.TEXT_VIEW, // -> date, opt
            Widgets.IMAGE_VIEW, // -> content
            Widgets.FRAME_LAYOUT, // -> play, IV
            Widgets.FRAME_LAYOUT, // -> progress or btn
            Widgets.TEXT_VIEW, // -> text
            Widgets.TEXT_VIEW // -> time
    };
}















//    cls: android.view.ViewGroup, posS [0,812][1080,1756], posP [0,0][1080,944], desc null, txt null
//      cls: android.widget.LinearLayout, posS [52,823][829,890], posP [0,0][777,67], desc null, txt null
//        cls: android.widget.TextView, posS [76,832][180,890], posP [1048472,0][1048576,58], desc null, txt ע אייל
//      cls: android.widget.ImageView, posS [64,902][817,1655], posP [0,0][753,753], desc null, txt null
//      cls: android.widget.FrameLayout, posS [344,1182][536,1374], posP [0,0][192,192], desc null, txt null
//        cls: android.widget.ImageView, posS [389,1227][491,1329], posP [0,0][102,102], desc Воспр., txt null
//      cls: android.widget.FrameLayout, posS [64,1499][220,1655], posP [0,0][156,156], desc null, txt null
//        cls: android.widget.ProgressBar, posS [79,1544][175,1640], posP [0,0][96,96], desc null, txt null
//      cls: android.widget.TextView, posS [52,1667][255,1742], posP [0,0][203,77], desc null, txt נוחהחה
//      cls: android.widget.TextView, posS [719,1684][808,1733], posP [0,0][89,49], desc null, txt 13:44

//    cls: android.view.ViewGroup, posS [0,819][1080,1756], posP [0,0][1080,937], desc null, txt null
//      cls: android.widget.LinearLayout, posS [52,823][829,890], posP [0,0][777,67], desc null, txt null
//        cls: android.widget.TextView, posS [76,832][180,890], posP [1048472,0][1048576,58], desc null, txt ע אייל
//      cls: android.widget.ImageView, posS [64,902][817,1655], posP [0,0][753,753], desc null, txt null
//      cls: android.widget.FrameLayout, posS [344,1182][536,1374], posP [0,0][192,192], desc null, txt null
//        cls: android.widget.ImageView, posS [389,1227][491,1329], posP [0,0][102,102], desc Воспр., txt null
//      cls: android.widget.TextView, posS [64,1591][287,1655], posP [0,0][223,64], desc null, txt 01:14
//      cls: android.widget.TextView, posS [52,1667][255,1742], posP [0,0][203,77], desc null, txt נוחהחה
//      cls: android.widget.TextView, posS [719,1684][808,1733], posP [0,0][89,49], desc null, txt 13:44
//      cls: android.widget.ImageView, posS [829,1210][973,1354], posP [0,0][144,144], desc Переслать..., txt null