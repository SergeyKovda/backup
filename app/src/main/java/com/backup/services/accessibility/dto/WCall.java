package com.backup.services.accessibility.dto;

/**
 * Created by Owner
 * on 15/02/2018.
 */

public class WCall {
    private String callName;
    private String callAmount; // "" or (3)
    private String lastCallTime; // 12 february, 19:42 or 06.11.16, 20:24

    public void setCallName(String callName) {
        this.callName = callName;
    }

    public String getCallName() {
        return callName;
    }

    public void setCallAmount(String callAmount) {
        this.callAmount = callAmount;
    }

    public String getCallAmount() {
        return callAmount;
    }

    public void setLastCallTime(String lastCallTime) {
        this.lastCallTime = lastCallTime;
    }

    public String getLastCallTime() {
        return lastCallTime;
    }
}
