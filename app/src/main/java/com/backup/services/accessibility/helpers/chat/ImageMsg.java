package com.backup.services.accessibility.helpers.chat;

import android.view.accessibility.AccessibilityNodeInfo;

import com.backup.services.accessibility.dto.WChatMessage;
import com.backup.services.accessibility.helpers.Widgets;
import com.backup.services.accessibility.helpers.chat.templates.Image;

/**
 * Created by Owner
 * on 26/02/2018.
 */

public class ImageMsg extends Msg {

    public static boolean isTypeMessage(int childCount, AccessibilityNodeInfo item){
        return isTypeMessageI(childCount, item).isTypedMsg;
    }

    private static Wrapper isTypeMessageI(int childCount, AccessibilityNodeInfo item){
        if(childCount < 3 || childCount > 6){
            return MISTAKE_WRAPPER;
        }
        if(childCount == 6){
            if(Widgets.PROGRESS_BAR.equals(item.getChild(2).getClassName())){
                return new Wrapper(checkSelectedSequence(Image.MSG_OUTCOME_TXT_P, item, 0, 5), Image.MSG_OUTCOME_TXT_P);
            }
            else {
                return new Wrapper(checkSelectedSequence(Image.MSG_OUTCOME_TXT, item, 0, 5), Image.MSG_OUTCOME_TXT);
            }
        }
        else if(childCount == 5) {
            CharSequence second = item.getChild(0).getClassName();
            if(Widgets.TEXT_VIEW.equals(second)){
                CharSequence third = item.getChild(2).getClassName();
                if(Widgets.IMAGE_VIEW.equals(third)) {
                    return new Wrapper(checkSelectedSequence(Image.MSG_OUTCOME, item, 0, 4), Image.MSG_OUTCOME);
                }
                else if(Widgets.TEXT_VIEW.equals(third)){
                    return new Wrapper(checkSelectedSequence(Image.MSG_INCOME_TXT, item, 0, 4), Image.MSG_INCOME_TXT);
                }
                else if(Widgets.BUTTON.equals(third)){
                    return new Wrapper(checkSelectedSequence(Image.MSG_INCOME_TXT_D, item, 0, 4), Image.MSG_INCOME_TXT_D);
                }
                else if(Widgets.PROGRESS_BAR.equals(third)){
                    return new Wrapper(checkSelectedSequence(Image.MSG_OUTCOME_P, item, 0, 4), Image.MSG_OUTCOME_P);
                }
            }
            else if(Widgets.IMAGE_VIEW.equals(second)){
                if(Widgets.PROGRESS_BAR.equals(item.getChild(1).getClassName())){
                    return new Wrapper(checkSelectedSequence(Image.MSG_OUTCOME_TXT_P, item, 1, 5), Image.MSG_OUTCOME_TXT_P);
                }
                else{
                    return new Wrapper(checkSelectedSequence(Image.MSG_OUTCOME_TXT, item, 1, 5), Image.MSG_OUTCOME_TXT);
                }
            }
        }
        else if(childCount == 4){
            if(Widgets.IMAGE_VIEW.equals(item.getChild(0).getClassName())){
                CharSequence second = item.getChild(1).getClassName();
                if(Widgets.IMAGE_VIEW.equals(second)) {
                    return new Wrapper(checkSelectedSequence(Image.MSG_OUTCOME, item, 1, 4), Image.MSG_OUTCOME);
                }
                else if(Widgets.TEXT_VIEW.equals(second)){
                    return new Wrapper(checkSelectedSequence(Image.MSG_INCOME_TXT, item, 1, 4), Image.MSG_INCOME_TXT);
                }
                else if(Widgets.BUTTON.equals(second)){
                    return new Wrapper(checkSelectedSequence(Image.MSG_INCOME_TXT_D, item, 1, 4), Image.MSG_INCOME_TXT_D);
                }
                else if(Widgets.PROGRESS_BAR.equals(second)){
                    return new Wrapper(checkSelectedSequence(Image.MSG_OUTCOME_P, item, 1, 4), Image.MSG_OUTCOME_P);
                }
            }
            else if(Widgets.TEXT_VIEW.equals(item.getChild(0).getClassName())){
                if(Widgets.BUTTON.equals(item.getChild(2).getClassName())){
                    return new Wrapper(checkSelectedSequence(Image.MSG_INCOME_D, item, 0, 3), Image.MSG_INCOME_D);
                }
                else {
                    return new Wrapper(checkSelectedSequence(Image.MSG_INCOME, item, 0, 3), Image.MSG_INCOME);
                }
            }
        }
        else if(childCount == 3){
            if(Widgets.BUTTON.equals(item.getChild(1).getClassName())){
                return new Wrapper(checkSelectedSequence(Image.MSG_INCOME_D, item, 1, 3), Image.MSG_INCOME_D);
            }
            else {
                return new Wrapper(checkSelectedSequence(Image.MSG_INCOME, item, 1, 3), Image.MSG_INCOME);
            }
        }
        return MISTAKE_WRAPPER;
    }

    public static WChatMessage parseMessage(AccessibilityNodeInfo child, String key){
        int childCount = child.getChildCount();
        Wrapper type = isTypeMessageI(childCount, child);
        if(!type.isTypedMsg){
            return null;
        }

        String author = DEF_AUTHOR;
        AccessibilityNodeInfo date = null;
        AccessibilityNodeInfo text = null;
        AccessibilityNodeInfo time = null;
        AccessibilityNodeInfo status = null;

        if(childCount == 6) {
            if (Image.MSG_OUTCOME_TXT.equals(type.Seq)) { // MSG_OUTCOME_TXT
                date = child.getChild(0);
                text = child.getChild(3);
                time = child.getChild(4);
                status = child.getChild(5);
            }
            else if(Image.MSG_OUTCOME_TXT_P.equals(type.Seq)){
                date = child.getChild(0);
                text = child.getChild(3);
                time = child.getChild(4);
                status = child.getChild(5);
            }
        }
        else if(childCount == 5) {
            if(Image.MSG_OUTCOME.equals(type.Seq)) { // MSG_OUTCOME
                date = child.getChild(0);
                time = child.getChild(3);
                status = child.getChild(4);
            }
            else if(Image.MSG_INCOME_TXT.equals(type.Seq)){ // MSG_INCOME_TXT
                date = child.getChild(0);
                text = child.getChild(2);
                time = child.getChild(3);
            }
            else if(Image.MSG_OUTCOME_TXT.equals(type.Seq)){ // MSG_OUTCOME_TXT
                text = child.getChild(2);
                time = child.getChild(3);
                status = child.getChild(4);
            }
            else if(Image.MSG_INCOME_TXT_D.equals(type.Seq)){ // MSG_INCOME_TXT_D
                date = child.getChild(0);
                text = child.getChild(3);
                time = child.getChild(4);
            }
            else if(Image.MSG_OUTCOME_TXT_P.equals(type.Seq)){
                text = child.getChild(2);
                time = child.getChild(3);
                status = child.getChild(4);
            }
            else if(Image.MSG_OUTCOME_P.equals(type.Seq)){ // MSG_OUTCOME_P
                date = child.getChild(0);
                time = child.getChild(3);
                status = child.getChild(4);
            }
        }
        else if(childCount == 4){
            if(Image.MSG_OUTCOME.equals(type.Seq)) { // MSG_OUTCOME
                time = child.getChild(2);
                status = child.getChild(3);
            }
            else if(Image.MSG_INCOME_TXT.equals(type.Seq)){ // MSG_INCOME_TXT
                text = child.getChild(1);
                time = child.getChild(2);
            }
            else if(Image.MSG_INCOME.equals(type.Seq)){ // MSG_INCOME
                date = child.getChild(0);
                time = child.getChild(2);
            }
            else if(Image.MSG_INCOME_TXT_D.equals(type.Seq)){ // MSG_INCOME_TXT_D
                text = child.getChild(2);
                time = child.getChild(3);
            }
            else if(Image.MSG_INCOME_D.equals(type.Seq)){ // MSG_INCOME_D
                date = child.getChild(0);
                time = child.getChild(3);
            }
            else if(Image.MSG_OUTCOME_P.equals(type.Seq)){ // MSG_OUTCOME_P
                time = child.getChild(2);
                status = child.getChild(3);
            }
        }
        else if(childCount == 3){
            if(Image.MSG_INCOME.equals(type.Seq)) { // MSG_INCOME
                time = child.getChild(1);
            }
            else if(Image.MSG_INCOME_D.equals(type.Seq)){ // MSG_INCOME_D
                time = child.getChild(2);
            }
        }

        if(status == null){
            author = key;
        }
        return WChatMessage.createImageMsg(date, text, time, author);
    }

    public static boolean isGroupTypeMessage(int childCount, AccessibilityNodeInfo item){
        return isGroupTypeMessageI(childCount, item).isTypedMsg;
    }

    private static Wrapper isGroupTypeMessageI(int childCount, AccessibilityNodeInfo item){
        if(childCount < 3 || childCount > 6){
            return MISTAKE_WRAPPER;
        }
        if(childCount == 6){
            CharSequence second = item.getChild(1).getClassName();
            if(Widgets.LINEAR_LAYOUT.equals(second)){
                if(Widgets.BUTTON.equals(item.getChild(3).getClassName())){
                    return new Wrapper(checkSelectedSequence(Image.MSG_GROUP_INCOME_TXT_D, item, 0, 5), Image.MSG_GROUP_INCOME_TXT_D);
                }
                else {
                    return new Wrapper(checkSelectedSequence(Image.MSG_GROUP_INCOME_TXT, item, 0, 5), Image.MSG_GROUP_INCOME_TXT);
                }
            }
            else if(Widgets.IMAGE_VIEW.equals(second)){
                if(Widgets.PROGRESS_BAR.equals(item.getChild(2).getClassName())){
                    return new Wrapper(checkSelectedSequence(Image.MSG_OUTCOME_TXT_P, item, 0, 5), Image.MSG_OUTCOME_TXT_P);
                }
                else {
                    return new Wrapper(checkSelectedSequence(Image.MSG_OUTCOME_TXT, item, 0, 5), Image.MSG_OUTCOME_TXT);
                }
            }
        }
        else if(childCount == 5) {
            CharSequence first = item.getChild(0).getClassName();
            if(Widgets.TEXT_VIEW.equals(first)){
                CharSequence second = item.getChild(1).getClassName();
                if(Widgets.IMAGE_VIEW.equals(second)) {
                    if(Widgets.PROGRESS_BAR.equals(item.getChild(2).getClassName())){
                        return new Wrapper(checkSelectedSequence(Image.MSG_OUTCOME_P, item, 0, 4), Image.MSG_OUTCOME_P);
                    }
                    else {
                        return new Wrapper(checkSelectedSequence(Image.MSG_OUTCOME, item, 0, 4), Image.MSG_OUTCOME);
                    }
                }
                else if(Widgets.LINEAR_LAYOUT.equals(second)){
                    if(Widgets.BUTTON.equals(item.getChild(3).getClassName())){
                        return new Wrapper(checkSelectedSequence(Image.MSG_GROUP_INCOME_D, item, 0, 4), Image.MSG_GROUP_INCOME_D);
                    }
                    else {
                        return new Wrapper(checkSelectedSequence(Image.MSG_GROUP_INCOME, item, 0, 4), Image.MSG_GROUP_INCOME);
                    }
                }
            }
            else if(Widgets.LINEAR_LAYOUT.equals(first)){
                if(Widgets.BUTTON.equals(item.getChild(2).getClassName())){
                    return new Wrapper(checkSelectedSequence(Image.MSG_GROUP_INCOME_TXT_D, item, 1, 5), Image.MSG_GROUP_INCOME_TXT_D);
                }
                else {
                    return new Wrapper(checkSelectedSequence(Image.MSG_GROUP_INCOME_TXT, item, 1, 5), Image.MSG_GROUP_INCOME_TXT);
                }
            }
            else if(Widgets.IMAGE_VIEW.equals(first)){
                if(Widgets.PROGRESS_BAR.equals(item.getChild(1).getClassName())){
                    return new Wrapper(checkSelectedSequence(Image.MSG_OUTCOME_TXT_P, item, 1, 5), Image.MSG_OUTCOME_TXT_P);
                }
                else {
                    return new Wrapper(checkSelectedSequence(Image.MSG_OUTCOME_TXT, item, 1, 5), Image.MSG_OUTCOME_TXT);
                }
            }
        }
        else if(childCount == 4){
            CharSequence first = item.getChild(0).getClassName();
            if(Widgets.LINEAR_LAYOUT.equals(first)){
                if(Widgets.BUTTON.equals(item.getChild(2).getClassName())){
                    return new Wrapper(checkSelectedSequence(Image.MSG_GROUP_INCOME_D, item, 1, 4), Image.MSG_GROUP_INCOME_D);
                }
                else {
                    return new Wrapper(checkSelectedSequence(Image.MSG_GROUP_INCOME, item, 1, 4), Image.MSG_GROUP_INCOME);
                }
            }
            else if(Widgets.IMAGE_VIEW.equals(first)){
                CharSequence second = item.getChild(1).getClassName();
                if(Widgets.IMAGE_VIEW.equals(second)) {
                    return new Wrapper(checkSelectedSequence(Image.MSG_OUTCOME, item, 1, 4), Image.MSG_OUTCOME);
                }
                else if(Widgets.TEXT_VIEW.equals(second)){ // MSG_OUTCOME
                    return new Wrapper(checkSelectedSequence(Image.MSG_GROUP_INCOME_TXT, item, 2, 5), Image.MSG_GROUP_INCOME_TXT);
                }
                else if(Widgets.BUTTON.equals(second)){
                    return new Wrapper(checkSelectedSequence(Image.MSG_GROUP_INCOME_TXT_D, item, 2, 5), Image.MSG_GROUP_INCOME_TXT_D);
                }
                else if(Widgets.PROGRESS_BAR.equals(second)){
                    return new Wrapper(checkSelectedSequence(Image.MSG_OUTCOME_P, item, 1, 4), Image.MSG_OUTCOME_P);
                }
            }
        }
        else if(childCount == 3){
            if(Widgets.BUTTON.equals(item.getChild(1).getClassName())){
                return new Wrapper(checkSelectedSequence(Image.MSG_GROUP_INCOME_D, item, 2, 4), Image.MSG_GROUP_INCOME_D);
            }
            else {
                return new Wrapper(checkSelectedSequence(Image.MSG_GROUP_INCOME, item, 2, 4), Image.MSG_GROUP_INCOME);
            }
        }
        return MISTAKE_WRAPPER;
    }

    public static WChatMessage parseGroupMessage(AccessibilityNodeInfo child) {
        int childCount = child.getChildCount();
        Wrapper type = isGroupTypeMessageI(childCount, child);
        if(!type.isTypedMsg){
            return null;
        }

        CharSequence author;
        AccessibilityNodeInfo authorNode = null;
        AccessibilityNodeInfo date = null;
        AccessibilityNodeInfo text = null;
        AccessibilityNodeInfo time = null;
        AccessibilityNodeInfo status = null;

        if(childCount == 6){
            if(Image.MSG_GROUP_INCOME_TXT.equals(type.Seq)){
                date = child.getChild(0);
                authorNode = child.getChild(1).getChild(0);
                text = child.getChild(3);
                time = child.getChild(4);
            }
            else if(Image.MSG_OUTCOME_TXT.equals(type.Seq)){
                date = child.getChild(0);
                text = child.getChild(3);
                time = child.getChild(4);
                status = child.getChild(5);
            }
            else if(Image.MSG_GROUP_INCOME_TXT_D.equals(type.Seq)){
                date = child.getChild(0);
                authorNode = child.getChild(1).getChild(0);
                text = child.getChild(4);
                time = child.getChild(5);
            }
            else if(Image.MSG_OUTCOME_TXT_P.equals(type.Seq)){
                date = child.getChild(0);
                text = child.getChild(3);
                time = child.getChild(4);
                status = child.getChild(5);
            }
        }
        if(childCount == 5) {
            if(Image.MSG_OUTCOME.equals(type.Seq)){
                date = child.getChild(0);
                time = child.getChild(3);
                status = child.getChild(4);
            }
            else if(Image.MSG_GROUP_INCOME.equals(type.Seq)){
                date = child.getChild(0);
                authorNode = child.getChild(1).getChild(0);
                time = child.getChild(3);
            }
            else if(Image.MSG_GROUP_INCOME_TXT.equals(type.Seq)){
                authorNode = child.getChild(0).getChild(0);
                text = child.getChild(2);
                time = child.getChild(3);
            }
            else if(Image.MSG_OUTCOME_TXT.equals(type.Seq)){
                text = child.getChild(2);
                time = child.getChild(3);
                status = child.getChild(4);
            }
            else if(Image.MSG_GROUP_INCOME_TXT_D.equals(type.Seq)){
                authorNode = child.getChild(0).getChild(0);
                text = child.getChild(3);
                time = child.getChild(4);
            }
            else if(Image.MSG_GROUP_INCOME_D.equals(type.Seq)){
                date = child.getChild(0);
                authorNode = child.getChild(1).getChild(0);
                time = child.getChild(4);
            }
            else if(Image.MSG_OUTCOME_TXT_P.equals(type.Seq)){
                text = child.getChild(2);
                time = child.getChild(3);
                status = child.getChild(4);
            }
            else if(Image.MSG_OUTCOME_P.equals(type.Seq)){ // MSG_OUTCOME_P
                date = child.getChild(0);
                time = child.getChild(3);
                status = child.getChild(4);
            }
        }
        else if(childCount == 4){
            if(Image.MSG_GROUP_INCOME.equals(type.Seq)){
                authorNode = child.getChild(0).getChild(0);
                time = child.getChild(2);
            }
            else if(Image.MSG_GROUP_INCOME_TXT.equals(type.Seq)){
                text = child.getChild(1);
                time = child.getChild(2);
            }
            else if(Image.MSG_OUTCOME.equals(type.Seq)){
                time = child.getChild(2);
                status = child.getChild(3);
            }
            else if(Image.MSG_GROUP_INCOME_TXT_D.equals(type.Seq)){
                text = child.getChild(2);
                time = child.getChild(3);
            }
            else if(Image.MSG_GROUP_INCOME_D.equals(type.Seq)){
                authorNode = child.getChild(0).getChild(0);
                time = child.getChild(3);
            }
            else if(Image.MSG_OUTCOME_P.equals(type.Seq)){ // MSG_OUTCOME_P
                time = child.getChild(2);
                status = child.getChild(3);
            }
        }
        else if(childCount == 3){
            if(Image.MSG_GROUP_INCOME.equals(type.Seq)) {
                time = child.getChild(1);
            }
            else if(Image.MSG_GROUP_INCOME_D.equals(type.Seq)){
                time = child.getChild(2);
            }
        }

        if(status != null){
            author = DEF_AUTHOR;
        }
        else if(authorNode != null){
            author = authorNode.getText();
        }
        else {
            author = PREV_AUTHOR;
        }
        return WChatMessage.createImageMsg(date, text, time, author);
    }
}
