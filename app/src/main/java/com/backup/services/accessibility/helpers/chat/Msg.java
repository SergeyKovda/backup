package com.backup.services.accessibility.helpers.chat;

import android.view.accessibility.AccessibilityNodeInfo;

/**
 * Created by Owner
 * on 26/02/2018.
 */

public class Msg {
    static final CharSequence PREV_AUTHOR = "prev_author";
    public static final String DEF_AUTHOR = "user";
    static final Wrapper MISTAKE_WRAPPER = new Wrapper(false, null);

    static boolean checkSelectedSequence(CharSequence[] checkSequence, AccessibilityNodeInfo item, int start, int end) {
        int j = 0;
        for(int i = start; i <= end; i++, j++){
            CharSequence cls = checkSequence[i];
            if (!cls.equals(item.getChild(j).getClassName())) {
                return false;
            }
        }
        return true;
    }

    static class Wrapper{
        final boolean isTypedMsg;
        final CharSequence[] Seq;

        Wrapper(boolean isTypedMsg, CharSequence[] seq) {
            this.isTypedMsg = isTypedMsg;
            Seq = seq;
        }
    }
}
