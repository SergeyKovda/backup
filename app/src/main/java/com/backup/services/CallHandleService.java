package com.backup.services;


import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;

import com.backup.utils.LogUtils;

@Deprecated
public class CallHandleService extends Service {

    private static final String TAG = CallHandleService.class.getSimpleName();

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        StateListener phoneStateListener = new StateListener();
        TelephonyManager telephonyManager = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
        telephonyManager.listen(phoneStateListener, PhoneStateListener.LISTEN_CALL_STATE);
        return START_STICKY;
    }


    class StateListener extends PhoneStateListener {
        @Override
        public void onCallStateChanged(int state, String incomingNumber) {
            super.onCallStateChanged(state, incomingNumber);
            switch (state) {
                case TelephonyManager.CALL_STATE_RINGING:
                    LogUtils.d(TAG, "onCallStateChanged CALL_STATE_RINGING");
                    break;
                case TelephonyManager.CALL_STATE_OFFHOOK:
                    LogUtils.d(TAG, "onCallStateChanged CALL_STATE_OFFHOOK");
                    break;
                case TelephonyManager.CALL_STATE_IDLE:
                    LogUtils.d(TAG, "onCallStateChanged CALL_STATE_IDLE");
                    break;
            }
        }
    }

    ;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

}
