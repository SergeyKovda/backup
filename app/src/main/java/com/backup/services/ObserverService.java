package com.backup.services;


import android.app.Service;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Environment;
import android.os.FileObserver;
import android.os.IBinder;
import android.os.ParcelFileDescriptor;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.util.Log;

import com.backup.BackupApp;
import com.backup.Prefs;
import com.backup.helpers.RecursiveFileObserver;

import java.io.File;
import java.net.URLConnection;
import java.util.ArrayList;

public class ObserverService extends Service {

    public static final String ACTION_START_OBSERVER = "ACTION_START_OBSERVER";
    public static final String ACTION_STOP_OBSERVER = "ACTION_STOP_OBSERVER";

    private RecursiveFileObserver fileObserver;

    private ArrayList<String> zeroLengthFiles = new ArrayList<>();

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d("ObserverService", "onStartCommand");
        String action = isObservSholudBeActive() ?
                ObserverService.ACTION_START_OBSERVER : ObserverService.ACTION_STOP_OBSERVER;

        if (ACTION_STOP_OBSERVER.equals(action)) {
            if (fileObserver != null) {
                fileObserver.stopWatching();
            }
            return START_NOT_STICKY;
        } else if (ACTION_START_OBSERVER.equals(action)) {
            if (isObservSholudBeActive()) {
                Log.d("ObserverService", "fileObserver created");
                String path = Environment.getExternalStorageDirectory().getPath();
                if (fileObserver != null) {
                    fileObserver.stopWatching();
                    fileObserver = null;
                }
                fileObserver = new RecursiveFileObserver(path, new RecursiveFileObserver.EventListener() {
                    @Override
                    public void onEvent(int event, File file) {
                        boolean isApkNotAppStore = false;
                        if ((event == FileObserver.MOVED_TO || event == FileObserver.CREATE
                                || event == FileObserver.CLOSE_NOWRITE)
                                &&

                                ((isObserVideoChanges()
                                        && isVideoFile(file))

                                        ||

                                        (isObservImagesChanges()
                                                && isImageFile(file)))
                                ) {
                            Log.d("onEvent " + event, file.getAbsolutePath());
                            long length = -1;
                            if (file.length() == 0) {
                                addZeroFile(file.getPath());
                            }
                            Log.d("length", file.length() + "");
                            if (event != FileObserver.CLOSE_NOWRITE)
                                while (length != file.length()) {
                                    length = file.length();
                                    try {
                                        Thread.sleep(2000);
                                    } catch (InterruptedException e) {
                                        e.printStackTrace();
                                    }
                                }
                            Log.d("length 2", file.length() + "");
                            String absolutePath = file.getAbsolutePath();
                            if (isWasZeroFile(file.getPath())) {
                                // Continue for delete;
                            } else if (System.currentTimeMillis() - file.lastModified() > 5000 && !isApkNotAppStore) {
                                return;
                            } else if (event == FileObserver.CLOSE_NOWRITE) {
                                // Continue for delete;
                            } else {
                                return;
                            }
                            fileChanges(file);
//                            Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
//                            Uri contentUri = Uri.fromFile(file);
//                            mediaScanIntent.setData(contentUri);
//                            getApplicationContext().sendBroadcast(mediaScanIntent);
                            Log.d("ObserverService", "File created or changed: " + file.getPath());
//                        Toast.makeText(BootApp.getInstance(), "File created " + file.getPath(), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                fileObserver.startWatching();
            }
        }
        return START_STICKY;
    }

    private void fileChanges(File file) {
        if (isVideoFile(file)) {
            onVideosChanged();
        } else if (isImageFile(file)) {
            onImagesChanged();
        }
    }

    private void onImagesChanged() {

    }

    private void onVideosChanged() {

    }

    private boolean isObservSholudBeActive() {
        return isObservImagesChanges() || isObserVideoChanges();
    }

    private boolean isObservImagesChanges() {
        return Prefs.getPreferenceBoolean(Prefs.FLAG_OBSERV_IMAGES_CHANGES, true);
    }

    private boolean isObserVideoChanges() {
        return Prefs.getPreferenceBoolean(Prefs.FLAG_OBSERV_VIDEO_CHANGES, true);
    }

    private boolean isApkFileNotAppStore(File file) {
        if (file.getName().indexOf(".apk") != -1) {
            boolean appStoreMockExist = false;
//                File mockDirAppStore = new File(
//                        BootApp.getInstance().getPackageManager().getPackageInfo("com.appstore", 0).applicationInfo.dataDir, "FolderName");
//                File mockFileAppStore = new File(mockDirAppStore, file.getName());
            Uri parse = Uri.parse("content://com.appstore.fileprovider/FolderName/" + file.getName());
            ParcelFileDescriptor mInputFile = null;
            try {
                mInputFile = getContentResolver().openFileDescriptor(parse, "r");
            } catch (Throwable e) {
                e.printStackTrace();
            }
            if (mInputFile != null && mInputFile.getFileDescriptor().valid()) {
                Log.d("isApkFileAppStore", file.getPath() + " true");
                return false;
            } else {
                Log.d("isApkFileNotAppStore", file.getPath() + " true");
                return true;
            }
        }
        Log.d("isApk", file.getPath() + " false");
        return false;
    }

    private String getRealPathFromURI(Uri contentURI) {
        String result;
        Cursor cursor = getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) { // Source is Dropbox or other similar local file path
            result = contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Files.FileColumns.DATA);
            result = cursor.getString(idx);
            cursor.close();
        }
        return result;
    }

    private boolean isVideoFile(File file) {
        String mimeType = URLConnection.guessContentTypeFromName(file.getPath());
        if (mimeType != null)
            Log.d("mimeType", mimeType);
        return mimeType != null && mimeType.startsWith("video");
    }

    private boolean isImageFile(File file) {
        String mimeType = URLConnection.guessContentTypeFromName(file.getPath());
        if (mimeType != null)
            Log.d("mimeType", mimeType);
        return mimeType != null && mimeType.startsWith("image");
    }


//    public static boolean isImageFile(File file) {
//        String mimeType = URLConnection.guessContentTypeFromName(file.getPath());
//        return mimeType != null && mimeType.startsWith("image");
//    }

    @Override
    public void onDestroy() {
        if (fileObserver != null) {
            fileObserver.stopWatching();
            fileObserver = null;
        }
        Log.d("ObserverService", "onDestroy");
        super.onDestroy();
    }

    public void addZeroFile(String path) {
        if (!isWasZeroFile(path)) {
            zeroLengthFiles.add(path);
        }
    }

    public boolean isWasZeroFile(String path) {
        for (String str : zeroLengthFiles) {
            if (path.equals(str)) {
                return true;
            }
        }
        return false;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    public static void modifyObserverService() {
        Intent service = new Intent(BackupApp.getInstance(), ObserverService.class);
        BackupApp.getInstance().startService(service);
    }

}
