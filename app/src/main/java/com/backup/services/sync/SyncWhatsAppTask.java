package com.backup.services.sync;

import android.app.AlarmManager;
import android.app.IntentService;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.backup.Prefs;
import com.backup.services.SendService;


public class SyncWhatsAppTask extends IntentService {

    public static final int ALARM_INTENT_REQUEST_ID = 1506;
    private static final String TAG = "SyncWhatsAppTask";

    public SyncWhatsAppTask() {
        super("SyncWhatsAppTask");
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        Log.d(TAG, "SyncWhatsAppTask handle intent");
        startUpdateService(getApplicationContext());
        schedule(this);
    }

    public void startUpdateService(Context context) {
        context.startService(SendService.getIntentForWhatsapp(this));
    }

    public static void schedule(@NonNull Context context) {
        Log.d(TAG, "SyncWhatsAppTask schedule");
        int periodInSec = Prefs.getPeriodSyncWhatsAppInMin() * 60;
        AlarmManager alarmMgr = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(context, SyncWhatsAppTask.class);
        PendingIntent alarmIntent = PendingIntent.getService(context, ALARM_INTENT_REQUEST_ID, intent, 0);
        alarmMgr.set(AlarmManager.ELAPSED_REALTIME_WAKEUP, SystemClock.elapsedRealtime() + periodInSec * 1000, alarmIntent);
    }
}
