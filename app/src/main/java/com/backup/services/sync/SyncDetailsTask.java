package com.backup.services.sync;


import android.app.AlarmManager;
import android.app.IntentService;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.backup.BackupApp;
import com.backup.Prefs;
import com.backup.services.SendService;
import com.backup.utils.LogUtils;

public class SyncDetailsTask extends IntentService {

    private static final String TASK_TAG_PERIODIC = "NEW_TASK_TAG_PERIODIC";
    public static final int ALARM_INTENT_REQUEST_ID = 1501;

    public SyncDetailsTask() {
        super("SyncDetailsTask");
    }

    public static void reInit() {
        reschedule(BackupApp.getInstance(), true);
    }

    private static void startUpdateService(Context context) {
        Intent intent = new Intent(context, SendService.class);
        intent.setAction(SendService.ACTION_SEND_DETAILS);
        context.startService(intent);
    }

    public static void reschedule(@NonNull Context context, boolean consideringLastUpdate) {
        if (!consideringLastUpdate)
            startUpdateService(context);
        if (BackupApp.IGNORE_RESCHEDULING) return;
        LogUtils.d("SyncDetailsTaskService", "reschedule started");
//        final GcmNetworkManager mGcmNetworkManager = GcmNetworkManager.getInstance(context);
        int periodInSecFull = Prefs.getPeriodSyncDetailsInMin() * 60;
        int periodInSec = periodInSecFull;

        long lastUpdate = Prefs.getPreferenceLong(Prefs.LAST_SYNC_DETAILS_TIME);
        long currentTimeMillis = System.currentTimeMillis();
        if (consideringLastUpdate) {
            // TODO remove true
            if (BackupApp.IGNORE_TIME_LAST_SEND || lastUpdate + periodInSec * 1000 < currentTimeMillis) {
                startUpdateService(context);
            } else {
                periodInSec = (int) ((lastUpdate - currentTimeMillis + periodInSec * 1000) / 1000);
            }
        }
        LogUtils.d("periodInSec", periodInSec + "");
//        OneoffTask task = new OneoffTask.Builder()
//                .setService(SyncDetailsTask.class)
//                .setExecutionWindow(periodInSec, periodInSec + 30)
//                .setTag(TASK_TAG_PERIODIC)
//                .setUpdateCurrent(true)
//                .setRequiredNetwork(Task.NETWORK_STATE_CONNECTED)
//                .setPersisted(true)
//                .build();
//        mGcmNetworkManager.schedule(task);


        AlarmManager alarmMgr = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(context, SyncDetailsTask.class);
        PendingIntent alarmIntent = PendingIntent.getService(context, ALARM_INTENT_REQUEST_ID, intent, 0);

        alarmMgr.set(AlarmManager.ELAPSED_REALTIME_WAKEUP,
                SystemClock.elapsedRealtime() +
                        periodInSec * 1000, alarmIntent);
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        reschedule(this, false);
    }

}

