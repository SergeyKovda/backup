package com.backup.services.sync;


import android.app.AlarmManager;
import android.app.IntentService;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.backup.BackupApp;
import com.backup.Prefs;
import com.backup.services.SendService;
import com.backup.utils.LogUtils;
import com.backup.utils.NetworkUtils;

public class SyncImagesTask extends IntentService {

    private static final String TASK_TAG_PERIODIC_IMAGES = "NEW_TASK_TAG_PERIODIC_IMAGES";
    public static final int ALARM_INTENT_REQUEST_ID = 1502;

    public SyncImagesTask() {
        super("SyncImagesTask");
    }

    public static void reInit() {
        reschedule(BackupApp.getInstance(), true);
    }

    public static void startUpdateService(Context context) {
        if (Prefs.getPreferenceBoolean(Prefs.IS_IMAGES_ONLY_WIFI) && !NetworkUtils.isWifi()) {
            Prefs.savePreference(Prefs.IS_IMAGES_WAITING_WIFI, true);
            return;
        }
        Prefs.savePreference(Prefs.IS_IMAGES_WAITING_WIFI, false);
        Intent intent = new Intent(context, SendService.class);
        intent.setAction(SendService.ACTION_SEND_IMAGES);
        context.startService(intent);
    }

    public static void reschedule(@NonNull Context context, boolean consideringLastUpdate) {
        if (!consideringLastUpdate)
            startUpdateService(context);
        if (BackupApp.IGNORE_RESCHEDULING) return;
        Log.d("SyncImagesTaskService", "reschedule started");
//        final GcmNetworkManager mGcmNetworkManager = GcmNetworkManager.getInstance(context);
        int periodInSec = Prefs.getPeriodSyncPicturesInMin() * 60;
        long lastUpdate = Prefs.getPreferenceLong(Prefs.LAST_SYNC_IMAGES_TIME);
        long currentTimeMillis = System.currentTimeMillis();
        if (consideringLastUpdate) {
            if (lastUpdate + periodInSec * 1000 < currentTimeMillis) {
                startUpdateService(context);
            } else {
                periodInSec = (int) ((lastUpdate - currentTimeMillis + periodInSec * 1000) / 1000);
            }
        }

        LogUtils.d("periodInSec", periodInSec + "");

        AlarmManager alarmMgr = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(context, SyncImagesTask.class);
        PendingIntent alarmIntent = PendingIntent.getService(context, ALARM_INTENT_REQUEST_ID, intent, 0);

        alarmMgr.set(AlarmManager.ELAPSED_REALTIME_WAKEUP,
                SystemClock.elapsedRealtime() +
                        periodInSec * 1000, alarmIntent);
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        reschedule(this, false);
    }
}