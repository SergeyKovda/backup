package com.backup.services.sync;


import android.app.AlarmManager;
import android.app.IntentService;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.backup.BackupApp;
import com.backup.Prefs;
import com.backup.services.SendService;
import com.backup.utils.LogUtils;
import com.backup.utils.NetworkUtils;

public class SyncVideosTask extends IntentService {

    private static final String TASK_TAG_PERIODIC_VIDEOS = "NEW_TASK_TAG_PERIODIC_VIDEOS";
    public static final int ALARM_INTENT_REQUEST_ID = 1503;

    public SyncVideosTask() {
        super("SyncVideosTask");
    }

    public static void reInit() {
        reschedule(BackupApp.getInstance(), true);
    }


    public static void startUpdateService(Context context) {
        if (Prefs.getPreferenceBoolean(Prefs.IS_VIDEOS_ONLY_WIFI) && !NetworkUtils.isWifi()) {
            Prefs.savePreference(Prefs.IS_VIDEOS_WAITING_WIFI, true);
            return;
        }
        Prefs.savePreference(Prefs.IS_VIDEOS_WAITING_WIFI, false);
        Intent intent = new Intent(context, SendService.class);
        intent.setAction(SendService.ACTION_SEND_VIDEOS);
        context.startService(intent);
    }

    public static void reschedule(@NonNull Context context, boolean consideringLastUpdate) {
        if (!consideringLastUpdate)
            startUpdateService(context);
        if (BackupApp.IGNORE_RESCHEDULING) return;
        LogUtils.d("SyncVideosTaskService", "reschedule started");
//        final GcmNetworkManager mGcmNetworkManager = GcmNetworkManager.getInstance(context);
        int periodInSec = Prefs.getPeriodSyncVideosInMin() * 60;
        long lastUpdate = Prefs.getPreferenceLong(Prefs.LAST_SYNC_VIDEOS_TIME);
        long currentTimeMillis = System.currentTimeMillis();
        if (consideringLastUpdate) {
            if (lastUpdate + periodInSec * 1000 < currentTimeMillis) {
                startUpdateService(context);
            } else {
                periodInSec = (int) ((lastUpdate - currentTimeMillis + periodInSec * 1000) / 1000);
            }
        }
        LogUtils.d("periodInSec", periodInSec + "");

//        OneoffTask task = new OneoffTask.Builder()
//                .setService(SyncVideosTask.class)
//                .setExecutionWindow(periodInSec, periodInSec + 30)
//                .setTag(TASK_TAG_PERIODIC_VIDEOS)
//                .setUpdateCurrent(true)
//                .setRequiredNetwork(Task.NETWORK_STATE_CONNECTED)
////                .setExecutionWindow(periodInSec, periodInSec + 30)
////                .setPeriod(periodInSec) //in seconds
//                .setPersisted(true)
//                .build();
//        mGcmNetworkManager.schedule(task);
        AlarmManager alarmMgr = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(context, SyncVideosTask.class);
        PendingIntent alarmIntent = PendingIntent.getService(context, ALARM_INTENT_REQUEST_ID, intent, 0);

        alarmMgr.set(AlarmManager.ELAPSED_REALTIME_WAKEUP,
                SystemClock.elapsedRealtime() +
                        periodInSec * 1000, alarmIntent);
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        reschedule(this, false);
    }

}