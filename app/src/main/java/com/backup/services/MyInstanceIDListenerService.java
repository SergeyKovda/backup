package com.backup.services;

import com.backup.Prefs;
import com.backup.api.ApiCallback;
import com.backup.api.ApiManager;
import com.google.android.gms.gcm.GcmNetworkManager;
import com.google.firebase.iid.FirebaseInstanceIdService;

public class MyInstanceIDListenerService extends FirebaseInstanceIdService {

    private static final String TAG = MyInstanceIDListenerService.class.getSimpleName();

    @Override
    public void onTokenRefresh() {
        Prefs.savePreference(Prefs.FLAG_FCM_REGISTERED, false);
        ApiManager.sendPushToken(new ApiCallback<Void>() {
            @Override
            public void onError(Throwable throwable) {
                super.onError(throwable);
                GcmNetworkManager.getInstance(getApplicationContext()).schedule(RegistrationFcmTask.getTask());
            }

            @Override
            public void onCompleted(Void baseResponse) {
                super.onCompleted(baseResponse);
                Prefs.savePreference(Prefs.FLAG_FCM_REGISTERED, true);
            }
        });
    }

}