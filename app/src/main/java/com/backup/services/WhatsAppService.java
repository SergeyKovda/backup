package com.backup.services;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;

import com.backup.activities.WhatsappMessage;

/**
 * Created by tomas
 * on 12/20/2017.
 */
@Deprecated
public class WhatsAppService extends Service {

    Context context;

    public WhatsAppService(Context context){
        this.context=context;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId){
        return flags;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    public void whatsappcall(){
        WhatsappMessage whatsappMessage = new WhatsappMessage(this.context);
        whatsappMessage.Ready();
    }
}
