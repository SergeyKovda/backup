package com.backup.services;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.Nullable;

import com.backup.BackupApp;
import com.backup.Prefs;
import com.backup.services.sync.SyncImagesTask;
import com.backup.services.sync.SyncVideosTask;
import com.backup.utils.LogUtils;
import com.backup.utils.NetworkUtils;

public class DummyService extends Service {

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        LogUtils.d("DummyService", "Backup DummyService onCreate");
        IntentFilter filter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        BackupApp.getInstance().registerReceiver(connectivityReceiver,
                filter);
    }

    private BroadcastReceiver connectivityReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            LogUtils.d("DummyService", "Backup DummyService onReceive");
            if (ConnectivityManager.CONNECTIVITY_ACTION.equals(intent.getAction())) {
                if (intent.getExtras() != null) {
                    NetworkInfo ni = (NetworkInfo) intent.getExtras().get(ConnectivityManager.EXTRA_NETWORK_INFO);
                    if (ni != null && ni.getState() == NetworkInfo.State.CONNECTED)
                        checkOnlineAndStart(0);
                }
            }
        }
    };

    private void checkOnlineAndStart(int waitSec) {
        if (waitSec >= 10) {
            return;
        } else if (waitSec == 0 || isOnline()) {
            startAction();
        } else {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    checkOnlineAndStart(waitSec + 1);
                }
            }, waitSec * 1000);
        }
    }

    private void startAction() {
        if (NetworkUtils.isWifi()) {
            if (Prefs.getPreferenceBoolean(Prefs.IS_VIDEOS_WAITING_WIFI)) {
                SyncVideosTask.startUpdateService(BackupApp.getInstance());
            }
            if (Prefs.getPreferenceBoolean(Prefs.IS_IMAGES_WAITING_WIFI)) {
                SyncImagesTask.startUpdateService(BackupApp.getInstance());
            }
        }
    }

    public static boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) BackupApp.getInstance().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnected();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        LogUtils.d("DummyService", "Backup DummyService onStartCommand " + (intent != null ? intent.toString() : ""));
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        BackupApp.getInstance().unregisterReceiver(connectivityReceiver);
        super.onDestroy();
    }

}
