package com.backup.services;

import android.content.Intent;

import com.backup.Prefs;
import com.backup.api.ApiCallback;
import com.backup.api.ApiManager;
import com.google.android.gms.gcm.GcmNetworkManager;
import com.google.android.gms.gcm.GcmTaskService;
import com.google.android.gms.gcm.OneoffTask;
import com.google.android.gms.gcm.Task;
import com.google.android.gms.gcm.TaskParams;

public class RegistrationFcmTask extends GcmTaskService {

    public static final String ACTION_RUN = "ACTION_RUN";

    private static final String TASK_TAG_WIFI = "NEW_TASK_TAG_WIFI";

    @Override
    public int onStartCommand(Intent intent, int i, int i1) {
        if (ACTION_RUN.equals(intent.getAction())) {
            return GcmTaskService.START_NOT_STICKY;
        } else {
            return super.onStartCommand(intent, i, i1);
        }
    }

    public static OneoffTask getTask() {
        OneoffTask task = new OneoffTask.Builder()
                .setService(RegistrationFcmTask.class)
                .setTag(TASK_TAG_WIFI)
                .setExecutionWindow(0L, 3600L)
                .setRequiredNetwork(Task.NETWORK_STATE_UNMETERED)
                .setUpdateCurrent(true)
                .build();
        return task;
    }


    @Override
    public int onRunTask(TaskParams taskParams) {
        ApiManager.sendPushToken(new ApiCallback<Void>() {
            @Override
            public void onCompleted(Void baseResponse) {
                super.onCompleted(baseResponse);
                Prefs.savePreference(Prefs.FLAG_FCM_REGISTERED, true);
            }

            @Override
            public void onError(Throwable throwable) {
                super.onError(throwable);
                GcmNetworkManager.getInstance(getApplicationContext()).schedule(RegistrationFcmTask.getTask());
            }
        });
        return GcmNetworkManager.RESULT_SUCCESS;
    }

    @Override
    public void onInitializeTasks() {
        super.onInitializeTasks();
        GcmNetworkManager.getInstance(this).schedule(getTask());
    }

}
