package com.backup.services;


import android.Manifest;
import android.app.Service;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.ContentObserver;
import android.database.Cursor;
import android.os.IBinder;
import android.provider.ContactsContract;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import com.backup.BackupApp;
import com.backup.helpers.GetContactsHelper;
import com.backup.objs.Contact;
import com.backup.utils.LogUtils;
import com.backup.utils.SendUtils;

public class ContactsService extends Service {

    public static final String TAG = "ContactsService";

    public static final String ACTION_START = "";
    private MyContentObserver myContentObserver;


    @Override
    public void onCreate() {
        super.onCreate();
        myContentObserver = new MyContentObserver();
        getContentResolver()
                .registerContentObserver(
                        ContactsContract.Contacts.CONTENT_URI, true,
                        myContentObserver);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent == null || ACTION_START.equals(intent.getAction())) {
            LogUtils.d(TAG, "Started");
        }
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        getContentResolver().unregisterContentObserver(myContentObserver);
        super.onDestroy();
    }

    public class MyContentObserver extends ContentObserver {
        public MyContentObserver() {
            super(null);
        }

        @Override
        public void onChange(boolean selfChange) {
            super.onChange(selfChange);
            if (ActivityCompat.checkSelfPermission(BackupApp.getInstance(),
                    Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED)
                return;
            Cursor cursor = BackupApp.getInstance().getContentResolver().query(
                    ContactsContract.Contacts.CONTENT_URI, null, null, null,
                    ContactsContract.Contacts.CONTACT_LAST_UPDATED_TIMESTAMP + " Desc");
            if (cursor.moveToNext()) {
                Contact contact = GetContactsHelper.getObjectWithMainDataByCursor(cursor);
                new GetContactsHelper().fillContactInfo(contact);
                SendUtils.sendContact(contact);
            }
            Log.e("", "~~~~~~" + selfChange);
        }

        @Override
        public boolean deliverSelfNotifications() {
            return true;
        }
    }

    public static void startService() {
        Intent intent = new Intent(BackupApp.getInstance(), ContactsService.class);
        intent.setAction(ContactsService.ACTION_START);
        BackupApp.getInstance().startService(intent);

    }
}
