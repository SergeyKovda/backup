package com.backup.services;


import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;

import com.backup.api.ApiCallback;
import com.backup.api.ApiManager;
import com.backup.api.ProgressListener;
import com.backup.contents.AppForDownload;
import com.backup.helpers.ApkHelper;
import com.backup.manufacturer.DeviceActionsHelper;
import com.backup.utils.BroadcastUtils;
import com.backup.utils.LogUtils;
import com.backup.utils.Utils;

import java.io.File;
import java.util.ArrayList;

public class DownloadApksService extends Service {

    private static final String TAG = "DownloadApkService";



    public ArrayList<AppForDownload> appsForDownload = new ArrayList<>();

    public static final String ACTION_DOWNLOAD = "ACTION_DOWNLOAD";
    public static final String KEY_APP = "KEY_APP";
    public static final String KEY_ONLY_APP = "KEY_ONLY_APP";
    public static final String FLAG_ADD_DEF_APPS = "FLAG_ADD_DEF_APPS";
    private boolean downloading = false;

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (ACTION_DOWNLOAD.equals(intent.getAction())) {
            LogUtils.d("DownloadApkService", "onStartCommand ACTION_DOWNLOAD");
            AppForDownload appForDownload = (AppForDownload) intent.getSerializableExtra(KEY_APP);
            AppForDownload onlyAppForDownload = (AppForDownload) intent.getSerializableExtra(KEY_ONLY_APP);



            if (appForDownload != null) {
                boolean alreadyAdded = false;
                for (AppForDownload app : appsForDownload) {
                    if (app.packageName.equals(appForDownload.packageName)) {
                        alreadyAdded = true;
                        break;
                    }
                }
                if (!alreadyAdded)
                    appsForDownload.add(appForDownload);
            }
            if (onlyAppForDownload != null) {
                appsForDownload.clear();
                appsForDownload.add(onlyAppForDownload);
            }
            startDowтloading();
        }
        return START_NOT_STICKY;
    }

    private synchronized void startDowтloading() {
        LogUtils.d("startDowтloading",
                "starting downloading = " + downloading + ", appsForDownload.size() = " + appsForDownload.size());
        if (downloading && appsForDownload.size() > 0) {
            return;
        }
        if (appsForDownload.size() == 0) {
            BroadcastUtils.sendBroadcast(BroadcastUtils.ACTION_ALL_APPS_INSTALLED);
            return;
        }
        downloadApp(appsForDownload.get(0));
    }

    private void downloadApp(AppForDownload appForDownload) {
        LogUtils.d("downloadApp", " started for " + appForDownload);
        if (appForDownload.forceUpdate || !DeviceActionsHelper.getActionsObject().isApplicationInstalled(appForDownload.packageName)) {
            downloading = true;
            ApiManager.downloadApp(appForDownload.url, appForDownload.packageName, new ApiCallback<File>() {
                @Override
                public void onCompleted(File file) {
                    if (!appForDownload.forceUpdate)
                        BroadcastUtils.sendAppDownloadedBroadcast(appForDownload.packageName);
                    LogUtils.d("downloadApp", "File downloaded, packageName = " + appForDownload.packageName);
                    downloading = false;
                    ApkHelper.installApp(appForDownload.packageName, file.getPath(), () -> {
                        if (!appForDownload.forceUpdate)
                            BroadcastUtils.sendAppInstalledBroadcast(appForDownload.packageName);
                        LogUtils.d("installApp", "App installed, packageName = " + appForDownload.packageName);
                        appsForDownload.remove(appForDownload);
                        startDowтloading();
                    });
                }

                @Override
                public void onError(Throwable throwable) {
                    super.onError(throwable);
                    if (Utils.isOnline())
                        BroadcastUtils.sendBroadcast(BroadcastUtils.ACTION_APP_HTTP_FILE_NOT_FOUND);
                    else
                        BroadcastUtils.sendBroadcast(BroadcastUtils.ACTION_CHECK_INTERNET_CONNECTION);

                    LogUtils.d("downloadApp", "onError = " + throwable.getMessage() != null ? throwable.getMessage() :
                            throwable.getCause() != null && throwable.getCause().getMessage() != null ?
                                    throwable.getCause().getMessage().toString() :
                                    throwable.getClass().getSimpleName()
                    );
                    downloading = false;
                }
            }, new ProgressListener() {

                public long lastBytes = 0;

                @Override
                public void update(long bytesRead, long contentLength, boolean done) {
                    if (bytesRead > lastBytes + 50000) {
                        lastBytes = bytesRead;
                        LogUtils.d(TAG, "Downloading " + appForDownload.packageName + ". Downloaded  " + bytesRead + " bytes");
                    }
                }
            });
        } else {
            LogUtils.d("installApp", "App already installed, packageName = " + appForDownload.packageName);
            BroadcastUtils.sendAppInstalledBroadcast(appForDownload.packageName);
            appsForDownload.remove(appForDownload);
            startDowтloading();
        }
    }

}
