package com.backup.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Max
 * on 30-Apr-17.
 */
public class WChatMessageDBH extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "messages_wa.db";
    private static final int    DATABASE_VERSION = 1;

    static final String TABLE_WHATSAPP = "messages_wa";
    static final String COLUMN_ID = "_id";
    static final String COLUMN_DATE = "date";
    static final String COLUMN_TEXT = "text";
    static final String COLUMN_TIME = "time";
    static final String COLUMN_AUTHOR = "author";
    static final String COLUMN_TYPE = "type";
    static final String COLUMN_AUDIO = "audio";
    static final String COLUMN_VIDEO = "video";
    static final String COLUMN_CHAT = "chat";
    static final String COLUMN_SYNC = "sync";
    static final String COLUMN_LOADING = "loading";
    static final String COLUMN_TIME_MS = "time_ms";
    static final String COLUMN_DATE_MS = "date_ms";
    static final String COLUMN_HAS_DATE_LBL = "has_label";

    // Database creation sql statement
    private static final String WHATSAPP_TABLE_CREATE = "create table " +
            TABLE_WHATSAPP + "( " +
            COLUMN_ID           + " integer primary key autoincrement, " +
            COLUMN_DATE         + " text, " +
            COLUMN_TEXT         + " text not null, " +
            COLUMN_TIME         + " text not null, " +
            COLUMN_AUTHOR       + " text not null, " +
            COLUMN_TYPE         + " text not null, " +
            COLUMN_AUDIO        + " text, " +
            COLUMN_VIDEO        + " text, " +
            COLUMN_CHAT         + " text not null, " +
            COLUMN_SYNC         + " integer, " +
            COLUMN_LOADING      + " integer, " +
            COLUMN_TIME_MS      + " integer, " +
            COLUMN_DATE_MS      + " integer, " +
            COLUMN_HAS_DATE_LBL + " integer " +
            ");";


    WChatMessageDBH(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase database) {
        database.execSQL(WHATSAPP_TABLE_CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_WHATSAPP);
        onCreate(db);
    }
}

