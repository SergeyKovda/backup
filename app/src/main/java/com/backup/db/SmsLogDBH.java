package com.backup.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class SmsLogDBH extends SQLiteOpenHelper{

    private static final String DATABASE_NAME = "user_allsms.db";
    private static final int    DATABASE_VERSION = 1;

    static final String TABLE_USER_ALLSMS = "user_allsms";
    static final String COLUMN_ID = "_id";
    static final String COLUMN_MESSAGE = "message";
    static final String COLUMN_PHONE_NUMBER = "phoneNumber";
    static final String COLUMN_TYPE = "type";
    static final String COLUMN_TIME = "time";
    static final String COLUMN_SYNC = "sync";

    private static final String ALLSMS_USERS_TABLE_CREATE = "create table " +
            TABLE_USER_ALLSMS     + "( " +
            COLUMN_ID             + " integer primary key autoincrement, " +
            COLUMN_MESSAGE        + " text not null, " +
            COLUMN_PHONE_NUMBER   + " text not null, " +
            COLUMN_SYNC           + " integer, " +
            COLUMN_TYPE           + " text not null," +
            COLUMN_TIME           + " long " +
            ");";

    SmsLogDBH(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase database) {
        database.execSQL(ALLSMS_USERS_TABLE_CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase database, int oldVersion, int newVersion) {
        database.execSQL("DROP TABLE IF EXISTS " + TABLE_USER_ALLSMS);
        onCreate(database);
    }

}
