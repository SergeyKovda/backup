package com.backup.db;

import com.backup.services.accessibility.dto.WChatMessage;
import com.backup.services.accessibility.helpers.IWChatUpdateObserver;

import java.util.List;

public interface IWhatsapUsersDB {
    long getConversationIDByName(String username);

}
