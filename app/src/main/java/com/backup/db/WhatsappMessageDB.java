package com.backup.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.backup.BackupApp;
import com.backup.services.accessibility.dto.WChatMessage;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by max
 * on 3/1/18.
 */

public class WhatsappMessageDB implements IWhatsappMessageDB {
    private static WhatsappMessageDB mInstance = new WhatsappMessageDB(BackupApp.getInstance());
    private WChatMessageDBH mDbHelper;
    private SQLiteDatabase db;

    private final static long START_ID = 7000;


    private long maxID = -1;

    public static WhatsappMessageDB getInstance() {
        return mInstance;
    }

    private WhatsappMessageDB(Context context){
        mDbHelper = new WChatMessageDBH(context);
    }

    private void close() {
        mDbHelper.close();
    }

    private void openWritable() {
        checkIsOpen();
        checkIsWritable();
        if(db == null) {
            db = mDbHelper.getWritableDatabase();
        }
    }

    private void checkIsWritable() {
        if(db != null && db.isReadOnly()){
            db.close();
            db = null;
        }
    }

    private void openReadable() {
        checkIsOpen();
        if(db == null) {
            db = mDbHelper.getReadableDatabase();
    }
    }

    private void checkIsOpen() {
        if(db != null && !db.isOpen()){
            db.close();
            db = null;
        }
    }

    @Override
    protected void finalize() throws Throwable {
        close();
        super.finalize();
    }

    @Override
    public void save(List<WChatMessage> messages){
        if(messages == null || messages.isEmpty()){
            return;
        }
        ContentValues cv = new ContentValues();
        for(WChatMessage msg : messages){
            saveOrUpdate(msg, cv);
        }
    }


    private long getMaxID() {
        openWritable();
        Cursor cursor = db.rawQuery("SELECT MAX("+WChatMessageDBH.COLUMN_ID+") FROM "+WChatMessageDBH.TABLE_WHATSAPP, null);
        long maxid = (cursor.moveToFirst() ? cursor.getLong(0) : 0);
        maxid = maxid < START_ID ? START_ID : maxid;
        cursor.close();
        return maxid;
    }

    public void saveOrUpdate(WChatMessage msg) {
        saveOrUpdate(msg, null);
    }

    private void saveOrUpdate(WChatMessage msg, ContentValues cv) {
        openWritable();
        if(cv == null) {
            cv = new ContentValues();
        }
        if (maxID < START_ID) {
            maxID = getMaxID();
        }
        SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yy");

        long msgId = msg.getId() == WChatMessage.DEF_ID ? (++maxID) : msg.getId();
        cv.put(WChatMessageDBH.COLUMN_ID, msgId);
//        cv.put(WChatMessageDBH.COLUMN_DATE, (msg.getDate() == null ? "" : msg.getDate()).toString());
        cv.put(WChatMessageDBH.COLUMN_DATE, ((msg.getDate() == null || msg.getDate() == "") ? formatter.format(Calendar.getInstance().getTime()) : msg.getDate()).toString());
        cv.put(WChatMessageDBH.COLUMN_TEXT, (msg.getText() == null ? "" : msg.getText()).toString());
        cv.put(WChatMessageDBH.COLUMN_TIME, (msg.getTime() == null ? "" : msg.getTime()).toString());
        cv.put(WChatMessageDBH.COLUMN_AUTHOR, (msg.getAuthor() == null ? "" : msg.getAuthor()).toString());
        cv.put(WChatMessageDBH.COLUMN_TYPE, (msg.getType() == null ? "" : msg.getType()).toString());
        cv.put(WChatMessageDBH.COLUMN_AUDIO, (msg.getAudio() == null ? "" : msg.getAudio()).toString());
        cv.put(WChatMessageDBH.COLUMN_VIDEO, (msg.getVideo() == null ? "" : msg.getVideo()).toString());
        cv.put(WChatMessageDBH.COLUMN_CHAT, (msg.getChat() == null ? "" : msg.getChat()).toString());
        cv.put(WChatMessageDBH.COLUMN_SYNC, msg.isSynced() ? 1 : 0);
        cv.put(WChatMessageDBH.COLUMN_LOADING, msg.isLoading() ? 1 : 0);
        cv.put(WChatMessageDBH.COLUMN_TIME_MS, msg.getTimeMs());
        cv.put(WChatMessageDBH.COLUMN_DATE_MS, msg.getDateMs());
        cv.put(WChatMessageDBH.COLUMN_HAS_DATE_LBL, msg.hasDateLabel() ? 1 : 0);
        int result = msgId == WChatMessage.DEF_ID ?
                0 : db.update(WChatMessageDBH.TABLE_WHATSAPP, cv, WChatMessageDBH.COLUMN_ID + " = '" + msgId + "'", null);
        if(result == 0){
            db.insert(WChatMessageDBH.TABLE_WHATSAPP, null, cv);
        }
    }

    private WChatMessage createFromCursor(Cursor c) {
        return new WChatMessage(
                c.getLong(c.getColumnIndex(WChatMessageDBH.COLUMN_ID)),
                c.getString(c.getColumnIndex(WChatMessageDBH.COLUMN_DATE)),
                c.getString(c.getColumnIndex(WChatMessageDBH.COLUMN_TEXT)),
                c.getString(c.getColumnIndex(WChatMessageDBH.COLUMN_TIME)),
                c.getString(c.getColumnIndex(WChatMessageDBH.COLUMN_AUTHOR)),
                c.getString(c.getColumnIndex(WChatMessageDBH.COLUMN_TYPE)),
                c.getString(c.getColumnIndex(WChatMessageDBH.COLUMN_AUDIO)),
                c.getString(c.getColumnIndex(WChatMessageDBH.COLUMN_VIDEO)),
                c.getString(c.getColumnIndex(WChatMessageDBH.COLUMN_CHAT)),
        c.getInt(c.getColumnIndex(WChatMessageDBH.COLUMN_SYNC)) == 1,
        c.getInt(c.getColumnIndex(WChatMessageDBH.COLUMN_LOADING)) == 1,
                c.getLong(c.getColumnIndex(WChatMessageDBH.COLUMN_TIME_MS)),
                c.getLong(c.getColumnIndex(WChatMessageDBH.COLUMN_DATE_MS)),
    c.getInt(c.getColumnIndex(WChatMessageDBH.COLUMN_HAS_DATE_LBL)) == 1
        );
    }

    @Override
    public void onChatUpdated(WChatMessage message) {
        saveOrUpdate(message);
    }

    @Override
    public List<WChatMessage> getNotSyncedMessages() {
        String selection = WChatMessageDBH.COLUMN_SYNC + " = ?";
        return getMsg(selection, new String[]{"0"});
    }

    public List<WChatMessage> getAllMessages (){
        return getMsg(null, null);
    }


    private List<WChatMessage> getMsg (String selection, String[] mask) {
        openWritable();
        Cursor c = db.query(WChatMessageDBH.TABLE_WHATSAPP, null, selection, mask,
                null, null, null);

        List<WChatMessage> result = new ArrayList<>();
        if(c != null && c.moveToLast()) {
            do {
                result.add(createFromCursor(c));
            } while (c.moveToPrevious());
            c.close();
        }
        return result;
    }
}
