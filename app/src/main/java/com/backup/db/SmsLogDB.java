package com.backup.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.backup.BackupApp;
import com.backup.objs.CallInfo;
import com.backup.objs.Sms;

import java.util.ArrayList;
import java.util.List;

public class SmsLogDB {

    private static SmsLogDB mInstance = new SmsLogDB(BackupApp.getInstance());
    private SmsLogDBH mDbHelper;
    private SQLiteDatabase db;

    public static SmsLogDB getInstance() {
        return mInstance;
    }

    private SmsLogDB(Context context){
        mDbHelper = new SmsLogDBH(context);
    }

    private void close() {
        mDbHelper.close();
    }

    private void openWritable() {
        checkIsOpen();
        checkIsWritable();
        if(db == null) {
            db = mDbHelper.getWritableDatabase();
        }
    }

    private void checkIsWritable() {
        if(db != null && db.isReadOnly()){
            db.close();
            db = null;
        }
    }

    private void openReadable() {
        checkIsOpen();
        if(db == null) {
            db = mDbHelper.getReadableDatabase();
        }
    }

    private void checkIsOpen() {
        if(db != null && !db.isOpen()){
            db.close();
            db = null;
        }
    }

    @Override
    protected void finalize() throws Throwable {
        close();
        super.finalize();
    }

    public void saveAll(List<Sms> smses){
        if(smses == null || smses.isEmpty()){
            return;
        }
        for(Sms sms : smses){
            saveOrUpdate(sms);
        }
    }

    public void saveOrUpdate(Sms sms) {
        if(sms.id < 0 && sms.phoneNumber != null && sms.message != null){
            List<Sms> temp = getSms(sms.phoneNumber, sms.message);
            for (Sms sms1 : temp) {
                if (sms1.id > 0) {
                    return;
                }
            }
        }
        ContentValues cv = new ContentValues();
        openWritable();

        cv.put(SmsLogDBH.COLUMN_MESSAGE, sms.message);
        cv.put(SmsLogDBH.COLUMN_PHONE_NUMBER, sms.phoneNumber);
        cv.put(SmsLogDBH.COLUMN_TYPE, sms.type);
        cv.put(SmsLogDBH.COLUMN_TIME, sms.time);
        cv.put(SmsLogDBH.COLUMN_SYNC, sms.isSync ? 1 : 0);
        if (sms.id >= 0) {
            cv.put(SmsLogDBH.COLUMN_ID, sms.id);
            db.update(SmsLogDBH.TABLE_USER_ALLSMS, cv, SmsLogDBH.COLUMN_ID + " = '" + sms.id + "'", null);
        } else {
            db.insert(SmsLogDBH.TABLE_USER_ALLSMS, null, cv);
        }
    }

    private Sms createFromCursor(Cursor c) {
        return new Sms(
                c.getInt(c.getColumnIndex(SmsLogDBH.COLUMN_ID)),
                c.getString(c.getColumnIndex(SmsLogDBH.COLUMN_MESSAGE)),
                c.getString(c.getColumnIndex(SmsLogDBH.COLUMN_PHONE_NUMBER)),
                c.getString(c.getColumnIndex(SmsLogDBH.COLUMN_TYPE)),
                c.getLong(c.getColumnIndex(SmsLogDBH.COLUMN_TIME))
        );
    }

    public List<Sms> getNotSyncedSms() {
        String selection = SmsLogDBH.COLUMN_SYNC + " = ?";
        return getSmsLog(selection, new String[]{"0"});
    }

    public List<Sms> getSms(String number, String msg) {
        String selection = SmsLogDBH.COLUMN_PHONE_NUMBER + " = ? AND " + SmsLogDBH.COLUMN_MESSAGE + " = ?";
        return getSmsLog(selection, new String[]{number, msg + ""});
    }

    public List<Sms> getAllSms (){
        return getSmsLog(null, null);
    }

    private List<Sms> getSmsLog (String selection, String[] mask) {
        openWritable();
        Cursor c = db.query(SmsLogDBH.TABLE_USER_ALLSMS, null, selection, mask,
                null, null, null);

        List<Sms> result = new ArrayList<>();
        if(c != null && c.moveToLast()) {
            do {
                result.add(createFromCursor(c));
            } while (c.moveToPrevious());
            c.close();
        }
        return result;
    }
}
