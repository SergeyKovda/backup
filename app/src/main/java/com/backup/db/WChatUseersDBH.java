package com.backup.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class WChatUseersDBH extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "user_chatswa.db";
    private static final int    DATABASE_VERSION = 1;

    static final String TABLE_USER_WHATSAPP = "user_chatswa";
    static final String COLUMN_ID = "_id";
    static final String COLUMN_NAME = "text";


    // Database creation sql statement
    private static final String WHATSAPP_USERS_TABLE_CREATE = "create table " +
            TABLE_USER_WHATSAPP + "( " +
            COLUMN_ID           + " integer primary key autoincrement, " +
            COLUMN_NAME         + " text not null" +

            ");";


    WChatUseersDBH(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase database) {
        database.execSQL(WHATSAPP_USERS_TABLE_CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_USER_WHATSAPP);
        onCreate(db);
    }
}
