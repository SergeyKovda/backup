package com.backup.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.backup.BackupApp;
import com.backup.services.accessibility.dto.WChatMessage;

import java.util.ArrayList;
import java.util.List;

public class WhatsapUsersDB implements IWhatsapUsersDB {
    private static WhatsapUsersDB mInstance = new WhatsapUsersDB(BackupApp.getInstance());
    private WChatUseersDBH mDbHelper;
    private SQLiteDatabase db;

    private long max_ID = -1;

    public static WhatsapUsersDB getInstance() {
        return mInstance;
    }

    private WhatsapUsersDB(Context context){
        mDbHelper = new WChatUseersDBH(context);
    }

    private void close() {
        mDbHelper.close();
    }

    private void openWritable() {
        checkIsOpen();
        checkIsWritable();
        if(db == null) {
            db = mDbHelper.getWritableDatabase();
        }
    }

    private void checkIsWritable() {
        if(db != null && db.isReadOnly()){
            db.close();
            db = null;
        }
    }

    private void openReadable() {
        checkIsOpen();
        if(db == null) {
            db = mDbHelper.getReadableDatabase();
        }
    }

    private void checkIsOpen() {
        if(db != null && !db.isOpen()){
            db.close();
            db = null;
        }
    }

    @Override
    protected void finalize() throws Throwable {
        close();
        super.finalize();
    }

    @Override
    public long getConversationIDByName(String username) {
        openWritable();
        String selection = WChatUseersDBH.COLUMN_NAME + " = ?";
        Cursor c = db.query(WChatUseersDBH.TABLE_USER_WHATSAPP, null, selection, new String[]{username},
                null, null, null);

        long result = -1;
        if(c != null && c.moveToLast()) {
            do {
                result = c.getLong(c.getColumnIndex(WChatUseersDBH.COLUMN_ID));
            } while (c.moveToPrevious());
            c.close();
        }

        if (result == -1) {
            result = save(username);
        }
        return result;
    }


    private long save(String userName) {
        if(userName == null || userName.isEmpty()){
            long id = getConversationIDByName(userName);
            return id;
        }
        ContentValues cv = new ContentValues();
        openWritable();

        long msgId = (max_ID < 0 ? getMaxID() : max_ID) + 1;
        cv.put(WChatUseersDBH.COLUMN_ID, msgId);
        cv.put(WChatUseersDBH.COLUMN_NAME, userName);

        db.insert(WChatUseersDBH.TABLE_USER_WHATSAPP, null, cv);
        max_ID = msgId;
        return msgId;
    }


    public long getMaxID() {
        openWritable();
        Cursor cursor = db.rawQuery("SELECT MAX("+WChatUseersDBH.COLUMN_ID+") FROM "+WChatUseersDBH.TABLE_USER_WHATSAPP, null);
        max_ID = (cursor.moveToFirst() ? cursor.getLong(0) : 0);
        cursor.close();
        return max_ID;
    }

    private void getAll () {
        openWritable();
        Cursor c = db.query(WChatUseersDBH.TABLE_USER_WHATSAPP, null, null, null,
                null, null, null);

        if(c != null && c.moveToLast()) {
            do {
                Log.d("GETALL", c.toString());
            } while (c.moveToPrevious());
            c.close();
        }
    }


}
