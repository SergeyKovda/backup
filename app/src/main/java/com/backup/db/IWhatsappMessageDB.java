package com.backup.db;

import com.backup.services.accessibility.dto.WChatMessage;
import com.backup.services.accessibility.helpers.IWChatUpdateObserver;

import java.util.List;

/**
 * Created by Max
 * on 23-Jun-17.
 */
public interface IWhatsappMessageDB extends IWChatUpdateObserver {
    List<WChatMessage> getNotSyncedMessages();

    void save(List<WChatMessage> messages);
}
