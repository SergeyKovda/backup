package com.backup.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.CallLog;

import com.backup.BackupApp;
import com.backup.objs.Call;
import com.backup.objs.CallInfo;
import com.backup.services.accessibility.dto.WChatMessage;

import java.util.ArrayList;
import java.util.List;

public class CallLogDB {

    private static CallLogDB mInstance = new CallLogDB(BackupApp.getInstance());
    private CallLogDBH mDbHelper;
    private SQLiteDatabase db;

    public static CallLogDB getInstance() {
        return mInstance;
    }

    private CallLogDB(Context context){
        mDbHelper = new CallLogDBH(context);
    }

    private void close() {
        mDbHelper.close();
    }

    private void openWritable() {
        checkIsOpen();
        checkIsWritable();
        if(db == null) {
            db = mDbHelper.getWritableDatabase();
        }
    }

    private void checkIsWritable() {
        if(db != null && db.isReadOnly()){
            db.close();
            db = null;
        }
    }

    private void openReadable() {
        checkIsOpen();
        if(db == null) {
            db = mDbHelper.getReadableDatabase();
        }
    }

    private void checkIsOpen() {
        if(db != null && !db.isOpen()){
            db.close();
            db = null;
        }
    }

    @Override
    protected void finalize() throws Throwable {
        close();
        super.finalize();
    }

    public void saveAll(List<CallInfo> callInfos){
        if(callInfos == null || callInfos.isEmpty()){
            return;
        }
        for(CallInfo callInfo : callInfos){
            saveOrUpdate(callInfo);
        }
    }

    public void saveOrUpdate(CallInfo callInfo) {
        if(callInfo.id < 0 && callInfo.savedNumber != null && callInfo.callStartTime > 0){
            List<CallInfo> temp = getCalls(callInfo.savedNumber, callInfo.callStartTime);
            for (CallInfo callInfo1 : temp) {
                if (callInfo1.id > 0) {
                    return;
                }
            }
        }
        ContentValues cv = new ContentValues();
        openWritable();

        cv.put(CallLogDBH.COLUMN_CALL_START_TIME, callInfo.callStartTime);
        cv.put(CallLogDBH.COLUMN_IS_INCOMING, callInfo.isIncoming ? 1 : 0);
        cv.put(CallLogDBH.COLUMN_SAVED_NUMBER, callInfo.savedNumber);
        cv.put(CallLogDBH.COLUMN_SYNC, callInfo.isSync ? 1 : 0);
        if (callInfo.id >= 0) {
            cv.put(CallLogDBH.COLUMN_ID, callInfo.id);
            db.update(CallLogDBH.TABLE_USER_ALLCALLS, cv, CallLogDBH.COLUMN_ID + " = '" + callInfo.id + "'", null);
        } else {
            db.insert(CallLogDBH.TABLE_USER_ALLCALLS, null, cv);
        }
    }

    private CallInfo createFromCursor(Cursor c) {
        return new CallInfo(
            c.getInt(c.getColumnIndex(CallLogDBH.COLUMN_ID)),
            c.getLong(c.getColumnIndex(CallLogDBH.COLUMN_CALL_START_TIME)),
            c.getInt(c.getColumnIndex(CallLogDBH.COLUMN_IS_INCOMING)) == 1,
            c.getString(c.getColumnIndex(CallLogDBH.COLUMN_SAVED_NUMBER))
        );
    }

    public List<CallInfo> getNotSyncedCalls() {
        String selection = CallLogDBH.COLUMN_SYNC + " = ?";
        return getCallLog(selection, new String[]{"0"});
    }

    public List<CallInfo> getCalls(String number, long time) {
        String selection = CallLogDBH.COLUMN_SAVED_NUMBER + " = ? AND " + CallLogDBH.COLUMN_CALL_START_TIME + " = ?";
        return getCallLog(selection, new String[]{number, time+""});
    }

    public List<CallInfo> getAllCalls (){
        return getCallLog(null, null);
    }

    private List<CallInfo> getCallLog (String selection, String[] mask) {
        openWritable();
        Cursor c = db.query(CallLogDBH.TABLE_USER_ALLCALLS, null, selection, mask,
                null, null, null);

        List<CallInfo> result = new ArrayList<>();
        if(c != null && c.moveToLast()) {
            do {
                result.add(createFromCursor(c));
            } while (c.moveToPrevious());
            c.close();
        }
        return result;
    }

}
