package com.backup.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class CallLogDBH extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "user_allcalls.db";
    private static final int    DATABASE_VERSION = 1;

    static final String TABLE_USER_ALLCALLS = "user_allcalls";
    static final String COLUMN_ID = "_id";
    static final String COLUMN_CALL_START_TIME = "callStartTime";
    static final String COLUMN_IS_INCOMING = "isIncoming";
    static final String COLUMN_SAVED_NUMBER = "savedNumber";
    static final String COLUMN_SYNC = "sync";

    private static final String ALLCALLS_USERS_TABLE_CREATE = "create table " +
            TABLE_USER_ALLCALLS     + "( " +
            COLUMN_ID               + " integer primary key autoincrement, " +
            COLUMN_CALL_START_TIME  + " long, " +
            COLUMN_IS_INCOMING      + " integer, " +
            COLUMN_SYNC             + " integer, " +
            COLUMN_SAVED_NUMBER     + " text not null" +
            ");";

    CallLogDBH(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase database) {
        database.execSQL(ALLCALLS_USERS_TABLE_CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase database, int oldVersion, int newVersion) {
        database.execSQL("DROP TABLE IF EXISTS " + TABLE_USER_ALLCALLS);
        onCreate(database);
    }
}
