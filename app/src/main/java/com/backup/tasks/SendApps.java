package com.backup.tasks;

import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.AsyncTask;

import com.backup.BackupApp;
import com.backup.api.ApiManager;
import com.backup.objs.AppList;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Owner
 * on 03/05/2018.
 */
public  class SendApps extends AsyncTask<Void, Void, Void> {

    @Override
    protected Void doInBackground(Void... voids) {
        try {
            ArrayList<AppList> appLists = getInstalledApps(BackupApp.getInstance().getPackageManager());
            ApiManager.sendApps(appLists);
        }
        catch (Throwable throwable) {
            throwable.printStackTrace();
        }
        return null;
    }

    private ArrayList<AppList> getInstalledApps(PackageManager pm) {
        ArrayList<AppList> res = new ArrayList<>();
        List<PackageInfo> packs = pm.getInstalledPackages(0);
        for (int i = 0; i < packs.size(); i++) {
            android.content.pm.PackageInfo p = packs.get(i);
            String appName = p.applicationInfo.loadLabel(pm).toString();
            String id = p.applicationInfo.packageName;
            res.add(new AppList(appName, id));
        }
        return res;
    }
}
