package com.backup.objs;


public class Sms {

    public static final String TYPE_INBOX = "inbox";
    public static final String TYPE_SENT = "sent";

    public long id;
    public String message;
    public String phoneNumber;
    public String type;
    public long time;
    public boolean isSync;

    public Sms(long id) {
        this.id = id;
    }

    public Sms(long id,String message, String phoneNumber, String type, long time) {
        this.id = id;
        this.message = message;
        this.phoneNumber = phoneNumber;
        this.type = type;
        this.time = time;
    }
}
