package com.backup.objs;


import java.io.Serializable;

public class CallInfo implements Serializable {

    public long id;
    public long callStartTime;
    public boolean isIncoming;
    public String savedNumber;
    public boolean isSync;

    public CallInfo(long id) {
        this.id = id;
    }

    public CallInfo(long id, long callStartTime, boolean isIncoming, String savedNumber) {
        this.id = id;
        this.callStartTime = callStartTime;
        this.isIncoming = isIncoming;
        this.savedNumber = savedNumber;
    }
}
