package com.backup.objs;


public class Contact {

    public long id;
    public String name;
    public String phone;
    public String email;
    public int hasPhoneCounter;

    public Contact(long id) {
        this.id = id;
    }

    public Contact(long id, String name, String phone, String email, int hasPhoneCounter) {
        this.id = id;
        this.name = name;
        this.phone = phone;
        this.email = email;
        this.hasPhoneCounter = hasPhoneCounter;
    }

    @Override
    public String toString() {
        return "Contact{" +
                "name='" + name + '\'' +
                ", phone='" + phone + '\'' +
                ", email='" + email + '\'' +
                '}';
    }
}
