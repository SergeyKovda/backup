package com.backup.objs;


public class Event {

    public long id;
    public String accountName;
    public String title;

    public long dateStart;
    public long dateEnd;
    public String eventTimeZone;

    public Event(long id) {
        this.id = id;
    }

    public Event(long id,String accountName, String title, long dateStart, long dateEnd, String eventTimeZone) {
        this.id = id;
        this.accountName = accountName;
        this.title = title;
        this.dateStart = dateStart;
        this.dateEnd = dateEnd;
        this.eventTimeZone = eventTimeZone;
    }
}
