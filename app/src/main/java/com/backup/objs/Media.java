package com.backup.objs;

import android.net.Uri;

public class Media {

    public static final String TYPE_VIDEO = "video";
    public static final String TYPE_IMAGE = "image";

    public String type;
    public String path;
    public Uri uriExternal;
    public String fileName;

}
