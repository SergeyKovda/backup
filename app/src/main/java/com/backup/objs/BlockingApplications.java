package com.backup.objs;


import java.util.ArrayList;

/**
 * Created by tomas on 12/12/2017.
 */

public class BlockingApplications {


    public links links;
    public ArrayList<data> data;
    public meta meta;

    public class links{
        public String self;
        public String first;
        public String next;
        public String last;
    }


    public class data {

        public attributes attributes;
        public String id;
        public String type;

        public class attributid {
            public String $oid;
        }

        public  class attributes {
            public attributid _id;
            public long addedTime;
            public String deviceId;
            public Boolean enabled;
            public String packageId;
            public String packageName;

            public long timestamp;





        }

    }
    public class meta{
        public Integer total;
    }


}
