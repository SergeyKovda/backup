package com.backup.objs;

import java.util.ArrayList;

/**
 * Created by tomas on 12/12/2017.
 */

public class Recordings {


    public links links;
    public ArrayList<datar> data;
    public meta meta;

    public class links{
        public String self;
        public String first;
        public String last;
    }
    public class meta{
        public Integer total;
    }


    public class datar {

        public attributes attributes;
        public String id;
        public String type;

        public class attributid {
            public String $oid;
        }

        public  class attributes {
            public attributid _id;
            public long from;
            public long to;
            public String deviceId;
            public long addedTime;
            public long timestamp;

        }

    }

}