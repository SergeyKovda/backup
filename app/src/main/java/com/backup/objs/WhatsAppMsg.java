package com.backup.objs;

import com.backup.db.WhatsapUsersDB;
import com.backup.services.accessibility.dto.WChatMessage;

// {
//   "id": "7",
//   "username": "+97252284004",
//   "phoneNumber": "hello master",
//   "message": "test dan message",
//   "isIncoming": true,
//   "time": 1499161934031
// }

public class WhatsAppMsg {

    public final String id;
    public final String username;
//    public final String phoneNumber;
    public final String message;
    public final Boolean isIncoming;
    public final long time;
    public final long conversationId;

    public WhatsAppMsg(String id, String phoneNumber, String message, Boolean isIncoming, long time) {
        this.id = id;
        this.username = phoneNumber;
//        this.phoneNumber = phoneNumber;
        this.message = message;
        this.isIncoming = isIncoming;
        this.time = time;
        this.conversationId = WhatsapUsersDB.getInstance().getConversationIDByName(this.username);
    }

    //  {\r\n\t\"id\": \"123A\",\r\n \"phoneNumber\": \"test\",\r\n \"message\": \"test message\",\r\n \"isIncoming\": true,\r\n \"time\": 1499161934031\r\n}
    public WhatsAppMsg(WChatMessage msg) {
        id = String.valueOf(msg.getId());
        this.username = (msg.getAuthor().equals("user") || msg.getAuthor().equals("prev_author")) ? msg.getChat().toString() : msg.getAuthor().toString();
//        this.phoneNumber = (String) msg.getAuthor();
        this.message = (String) msg.getText();
        this.isIncoming = msg.isIncoming();
        this.time = msg.getMessageTimeMs();
        this.conversationId = WhatsapUsersDB.getInstance().getConversationIDByName(this.username);
    }
}
