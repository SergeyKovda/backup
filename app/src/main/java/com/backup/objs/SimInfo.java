package com.backup.objs;


import com.google.gson.annotations.SerializedName;

public class SimInfo {

    @SerializedName("sim_country")
    public String simCountry;
    @SerializedName("sim_operator_code")
    public String simOperatorCode;
    @SerializedName("sim_operator_name")
    public String simOperatorName;
    @SerializedName("sim_serial")
    public String simSerial;

}