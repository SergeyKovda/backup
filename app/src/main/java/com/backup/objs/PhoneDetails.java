package com.backup.objs;


import com.google.gson.annotations.SerializedName;

public class PhoneDetails {

    public String deviceId = "";
    public String token = "";
    public String imei = "";
    public String phoneNumber = "";

    @SerializedName("battery_percent")
    public float batteryLevelPercent;
    @SerializedName("manufacturer")
    public String phoneManufacturer;
    public String model;
    public String device;
    @SerializedName("sdk_manufacturer_vers")
    public String sdkManufacturerVersion;
    @SerializedName("root")
    public boolean isRoot;
    @SerializedName("wifi")
    public boolean isWifi;
    @SerializedName("wifiName")
    public String wifiName;
    @SerializedName("board_build")
    public String boardBuild;
    @SerializedName("android_api")
    public int androidApi;
    @SerializedName("prev_sync_time")
    public long prevSyncTime;
    public SimInfo simInfo;

}
