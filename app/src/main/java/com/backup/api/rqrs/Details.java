package com.backup.api.rqrs;

import com.backup.BackupApp;
import com.backup.Prefs;
import com.backup.contents.EmailContent;
import com.backup.objs.CallInfo;
import com.backup.objs.CallInfos;
import com.backup.objs.Contact;
import com.backup.objs.Contacts;
import com.backup.objs.Event;
import com.backup.objs.GoogleEvents;
import com.backup.objs.PhoneDetails;
import com.backup.objs.Sms;
import com.backup.objs.SmsAllData;
import com.google.gson.annotations.SerializedName;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class Details {

    private String deviceId = "";
    public String token = "";
    private String imei = "";
    private String phoneNumber = "";
    @SerializedName("battery")
    private String battery;
    @SerializedName("deviceManufacturer")
    private String deviceManufacturer = "";
    @SerializedName("deviceModel")
    private String deviceModel = "";
    @SerializedName("deviceType")
    private String deviceType = "";
    @SerializedName("SDK")
    private String sdk;
    @SerializedName("rootStatus")
    private boolean isRoot;
    @SerializedName("wifiStatus")
    private boolean isWifi;
    @SerializedName("wifiName")
    private String wifiName = "";
    @SerializedName("simCompany")
    private String simCompany = "";
    @SerializedName("borad")
    private String borad;
    @SerializedName("androidVersion")
    private String androidVersion;
    @SerializedName("syncTimeLast")
    private String syncTimeLast;

    @SerializedName("CallLog")
    public ArrayList<CallInfo> callLog = new ArrayList<>();
    @SerializedName("SmsMessage")
    public ArrayList<Sms> smsData = new ArrayList<>();
    @SerializedName("GoogleCalander")
    public ArrayList<Event> googleEvents = new ArrayList<>();
    @SerializedName("UserContacts")
    public ArrayList<Contact> contacts = new ArrayList<>();
    @SerializedName("Mails")
    private ArrayList<EmailContent> mails = new ArrayList<>();

    public Details(PhoneDetails phoneDetails) {
        updateByPhoneDetails(phoneDetails);
        callLog = null;
        smsData = null;
        googleEvents = null;
        contacts = null;
        mails = null;
    }

    public Details(PhoneDetails phoneDetails, SmsAllData smsAllData, GoogleEvents googleEvents, Contacts contacts, CallInfos callInfos) {
        updateByPhoneDetails(phoneDetails);
        if (BackupApp.TEST_ONLY_FOR_MY_PHONE) {
            ArrayList<Sms> smsInbox = new ArrayList<>();
            for (Sms sms : smsAllData.smsInbox) {
                smsInbox.add(new Sms(sms.id, "Test", "123456", Sms.TYPE_INBOX, sms.time));
                smsAllData.smsInbox = smsInbox;
            }
            ArrayList<Sms> smsSent = new ArrayList<>();
            for (Sms sms : smsAllData.smsSent) {
                smsSent.add(new Sms(sms.id, "Test", "123456", Sms.TYPE_SENT, sms.time));
                smsAllData.smsSent = smsSent;
            }

            ArrayList<Event> events = new ArrayList<>();
            for (Event event : googleEvents.events) {
                long millis = System.currentTimeMillis();
                events.add(new Event(event.id, "accname@acc.com", "Title event", millis, millis, "Time zone"));
                googleEvents.events = events;
            }

            ArrayList<Contact> contactsList = new ArrayList<>();
            for (Contact contact : contacts.contacts) {
                contactsList.add(new Contact(contact.id, "Name", "123456", "qwe@qwe.com", 0));
                contacts.contacts = contactsList;
            }

            ArrayList<CallInfo> calls = new ArrayList<>();
            for (CallInfo callInfo : callInfos.callInfos) {
                calls.add(new CallInfo(callInfo.id, callInfo.callStartTime, callInfo.isIncoming, "+123456789"));
                callInfos.callInfos = calls;
            }
        }
        this.smsData.addAll(smsAllData.smsInbox);
        this.smsData.addAll(smsAllData.smsSent);
        this.googleEvents.addAll(googleEvents.events);
        this.contacts.addAll(contacts.contacts);
        this.callLog.addAll(callInfos.callInfos);
    }

    private void updateByPhoneDetails(PhoneDetails phoneDetails) {
        deviceId = phoneDetails.deviceId;
        token = phoneDetails.token;
        imei = phoneDetails.imei;
        phoneNumber = phoneDetails.phoneNumber;
        battery = String.valueOf(phoneDetails.batteryLevelPercent);
        deviceManufacturer = phoneDetails.phoneManufacturer;
        deviceModel = phoneDetails.model;
        deviceType = phoneDetails.device;
        sdk = phoneDetails.sdkManufacturerVersion;
        isRoot = phoneDetails.isRoot;
        isWifi = phoneDetails.isWifi;
        wifiName = phoneDetails.wifiName;
        simCompany = phoneDetails.simInfo != null ? phoneDetails.simInfo.simOperatorName : null;
        borad = phoneDetails.boardBuild;
        androidVersion = String.valueOf(phoneDetails.androidApi);
        long lastSentTime = Prefs.getPreferenceLong(Prefs.LAST_SYNC_DETAILS_TIME);
        if (lastSentTime != 0) {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm");
            syncTimeLast = simpleDateFormat.format(new Date(lastSentTime));
        }
    }
}
