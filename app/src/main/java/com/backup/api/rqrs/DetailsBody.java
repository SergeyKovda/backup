package com.backup.api.rqrs;


import com.google.gson.annotations.SerializedName;

public class DetailsBody {

    public String key;
    public String imei;
    @SerializedName("data")
    public Details details;

}
