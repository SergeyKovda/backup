package com.backup.api.rqrs;


import com.google.gson.annotations.SerializedName;

public class BaseResponse {

    @SerializedName("error_description")
    public String errorDescription;

}
