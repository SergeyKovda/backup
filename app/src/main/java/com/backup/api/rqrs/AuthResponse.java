package com.backup.api.rqrs;


import com.google.gson.annotations.SerializedName;

public class AuthResponse extends BaseResponse {

    @SerializedName("access_token")
    public String accessToken;

    @SerializedName("token_type")
    public String tokenType;

    @SerializedName("refresh_token")
    public String refreshToken;
}
