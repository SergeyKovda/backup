package com.backup.api.rqrs;

/**
 * Created by Owner
 * on 15/03/2018.
 */

public interface Callback<T> {
    void onError(Throwable e);
    void onSuccess(T result);
}
