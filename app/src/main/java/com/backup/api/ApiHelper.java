package com.backup.api;

import com.backup.BackupApp;
import com.backup.utils.LogUtils;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiHelper {

    public static volatile ApiHelper instance;
    public static final String API_MAIN_URL = "http://el-eyes.com/api/backup/";
    public static final String API_EXTRA_URL = "http://el-eyes.com/api/";
    public static final String API_BASE_URL = "http://el-eyes.com/";


    private final ApiMethods service;
    private final ApiMethods serviceExtra;
    private final ApiMethods serviceBase;

    private boolean useCustomProgressListener = false;

    private ApiHelper() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor((String message) -> LogUtils.d("HTTP Logger", message));
        logging.setLevel(BackupApp.IS_FULL_LOG ? HttpLoggingInterceptor.Level.BODY : HttpLoggingInterceptor.Level.HEADERS);
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(logging);
        httpClient.addNetworkInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request request = chain.request();
                Response originalResponse = chain.proceed(request);
                ProgressListener listener = useCustomProgressListener ? customProgressListener
                        : progressListener;
                useCustomProgressListener = false;
                return originalResponse.newBuilder()
                        .body(new ProgressResponseBody(originalResponse.body(), listener))
                        .build();
            }
        });

        Retrofit retrofit = new Retrofit.Builder()
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
//                .addConverterFactory(new ToStringConverterFactory())
                .addConverterFactory(GsonConverterFactory.create())
                .client(httpClient.build())
                .baseUrl(API_MAIN_URL)
                .build();
        Retrofit retrofitExtra = new Retrofit.Builder()
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(httpClient.build())
                .baseUrl(API_EXTRA_URL)
                .build();
        Retrofit retrofitBase = new Retrofit.Builder()
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(httpClient.build())
                .baseUrl(API_BASE_URL)
                .build();
        service = retrofit.create(ApiMethods.class);
        serviceExtra = retrofitExtra.create(ApiMethods.class);
        serviceBase = retrofitBase.create(ApiMethods.class);
    }

    public static ApiHelper getInstance() {
        if (instance == null) {
            synchronized (ApiHelper.class) {
                if (instance == null) {
                    instance = new ApiHelper();
                }
            }
        }
        return instance;
    }

    public ProgressListener customProgressListener = null;

    public ProgressListener progressListener = (bytesRead, contentLength, done) -> {
    };

    public ApiMethods getService(ProgressListener progressListener) {
        if (progressListener != null) {
            useCustomProgressListener = true;
            this.customProgressListener = progressListener;
        }
        return service;
    }

    public ApiMethods getService() {
        return service;
    }

    public ApiMethods getExtraService() {
        return serviceExtra;
    }

    public ApiMethods getBaseService() {
        return serviceBase;
    }
}