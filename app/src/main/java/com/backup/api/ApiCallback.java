package com.backup.api;

import rx.functions.Action1;

public class ApiCallback<T> {

    public void onError(Throwable throwable) {
    }

    public void onCompleted(T t) {
    }

    public Action1 getOnErrorAction() {
        return (Object o) -> onError((Throwable) o);
    }

    public Action1 getSubscribeAction() {
        return (Object o) -> onCompleted((T) o);
    }
}
