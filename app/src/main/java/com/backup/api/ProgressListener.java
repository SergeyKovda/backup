package com.backup.api;

public interface ProgressListener {
    void update(long bytesRead, long contentLength, boolean done);
}