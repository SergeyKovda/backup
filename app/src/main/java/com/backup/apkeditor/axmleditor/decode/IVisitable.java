package com.backup.apkeditor.axmleditor.decode;

public interface IVisitable {
	public void accept(IVisitor v);  
}
