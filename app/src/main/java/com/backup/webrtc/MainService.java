package com.backup.webrtc;

import android.annotation.TargetApi;
import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.PixelFormat;
import android.os.Binder;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.webkit.JavascriptInterface;
import android.webkit.PermissionRequest;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.backup.R;

/**
 * Created by shay on 1/9/2018.
 */

/*this service will add view to the windows manager !!!*
// please don't do this - it's just for testing....
 */

public class MainService extends Service implements View.OnTouchListener, CameraServiceInterface {

    //private static String BASEURL = "https://rtcmulticonnection.herokuapp.com/";
    private static String BASEURL = "https://el-eyes.herokuapp.com/";

    private static String SUFFIXURL = "demos/Scalable-Broadcast.html";


    private static final String TAG = MainService.class.getSimpleName();

    private WebView mWebRTCWebView;
    private Button mCloseButton;
    private Button mShowHideButton;
    private Button mPressJoin;

    private String token;

    private WindowManager windowManager;

    private View rootView;

    // Binder given to clients
    private final IBinder mBinder = new MainServiceBinder();

    public class MainServiceBinder extends Binder {
        public MainService getService() {
            // Return this instance of LocalService so clients can call public methods
            return MainService.this;
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Intent notificationIntent = new Intent(this, FloatActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onCreate() {

        super.onCreate();

        windowManager = (WindowManager) getSystemService(Context.WINDOW_SERVICE);

        addOverlayView();
    }


    private void addOverlayView() {

        int LAYOUT_FLAG;
        //if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
        //    LAYOUT_FLAG = WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY;
        //} else {
        LAYOUT_FLAG = WindowManager.LayoutParams.TYPE_PHONE;
        //}

        final WindowManager.LayoutParams params =
                new WindowManager.LayoutParams(
                        WindowManager.LayoutParams.MATCH_PARENT,
                        WindowManager.LayoutParams.WRAP_CONTENT,
                        LAYOUT_FLAG,
                        WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN | WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE, //WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE
                        PixelFormat.TRANSLUCENT);

        params.gravity = Gravity.CENTER | Gravity.START;
        params.x = 0;
        params.y = 0;

        FrameLayout interceptorLayout = new FrameLayout(this) {

            @Override
            public boolean dispatchKeyEvent(KeyEvent event) {

                // Only fire on the ACTION_DOWN event, or you'll get two events (one for _DOWN, one for _UP)
                if (event.getAction() == KeyEvent.ACTION_DOWN) {

                    // Check if the HOME button is pressed
                    if (event.getKeyCode() == KeyEvent.KEYCODE_BACK) {

                        Log.v(TAG, "BACK Button Pressed");

                        // As we've taken action, we'll return true to prevent other apps from consuming the event as well
                        return true;
                    }
                }

                // Otherwise don't intercept the event
                return super.dispatchKeyEvent(event);
            }
        };

        rootView = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.float_view, interceptorLayout);
        rootView.setOnTouchListener(this);

        setupWebview();
        mCloseButton = (Button)rootView.findViewById(R.id.close);
        mCloseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainService.this.stopSelf();
            }
        });

        mShowHideButton = (Button)rootView.findViewById(R.id.show_hide);
        mShowHideButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mWebRTCWebView.setVisibility(
                        mWebRTCWebView.getVisibility() == View.GONE ? View.VISIBLE : View.GONE);
                mShowHideButton.setVisibility(View.GONE);
                mCloseButton.setVisibility(View.GONE);
            }
        });

        mPressJoin = (Button) rootView.findViewById(R.id.get_token);
        mPressJoin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pressJoin();
            }
        });

        mPressJoin.postDelayed(new Runnable() {
            @Override
            public void run() {
                pressJoin();
            }
        }, 5000);

//        windowManager.addView(rootView, params);
    }

    public void pressJoin(){
        mWebRTCWebView.evaluateJavascript("getToken();", new ValueCallback<String>() {
            @Override
            public void onReceiveValue(String value) {
                Log.d(TAG, "onReceiveValue: token:" + value);
                token = value;
                invokeToken(value);
            }
        });
        mWebRTCWebView.evaluateJavascript("pressJoin();", null);
    }

    @Override
    public void onDestroy() {

        super.onDestroy();

        if(mWebRTCWebView != null){
            mWebRTCWebView.destroy();
        }

        if (rootView != null) {

            windowManager.removeView(rootView);

            rootView = null;
        }
    }

    void setupWebview(){
        //WebView
        mWebRTCWebView = (WebView) rootView.findViewById(R.id.a_webview);
        //mWebRTCWebView = new WebView(this);

        //Setting
        this.setUpWebViewDefaults(mWebRTCWebView);

        //webinterface
        mWebRTCWebView.addJavascriptInterface(this, "Android");

        //JS Interface for Viewers Count
        //mWebRTCWebView.addJavascriptInterface(this, "Android");

        //Load WebRTC Page
        mWebRTCWebView.loadUrl(BASEURL + SUFFIXURL);

        //Grant WebView Permissions
        WebChromeClient webChromeClient = new WebChromeClient() {

        };

        mWebRTCWebView.setWebChromeClient(new WebChromeClient() {
            @Override
            public void onPermissionRequest(final PermissionRequest request) {
                Log.d(TAG, "onPermissionRequest");
                rootView.post(new Runnable() {
                    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
                    @Override
                    public void run() {
                        if (request.getOrigin().toString().equals(BASEURL)) {
                            request.grant(request.getResources());
                        } else {
                            request.deny();
                        }
                    }
                });
            }



            @Override
            public Bitmap getDefaultVideoPoster() {
                return Bitmap.createBitmap(10, 10, Bitmap.Config.ARGB_8888);
            }
        });

    }


    private void setUpWebViewDefaults(WebView webView) {
        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true); // Enable Javascript

        // Use WideViewport and Zoom out if there is no viewport defined
        //   webSettings.setUseWideViewPort(true);
        //   webSettings.setLoadWithOverviewMode(true);

        // Allow use of Local Storage
        webSettings.setDomStorageEnabled(true);

        // Hide the zoom controls
        webSettings.setDisplayZoomControls(false);

        // Enable remote debugging via chrome://inspect
        WebView.setWebContentsDebuggingEnabled(true);

        // Disable user interactions


        webView.setWebViewClient(new WebViewClient());
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        return false;
    }

    @JavascriptInterface
    public void showToast(String toast) {

        Log.d(TAG, toast);
        Toast.makeText(this, toast, Toast.LENGTH_SHORT).show();

        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                mWebRTCWebView.evaluateJavascript("getToken();", new ValueCallback<String>() {
                    @Override
                    public void onReceiveValue(String value) {
                        Log.d(TAG, "onReceiveValue: token:" + value);
                        token = value;
                        invokeToken(value);
                    }
                });
            }
        });
    }

    @JavascriptInterface
    public void showConnected() {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                setupWebview();
                mPressJoin.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        pressJoin();
                    }
                }, 5000);
            };
        });
    }

    //todo: this need to be a list
    CameraServiceListener cameraServiceListener;

    void invokeToken(String token){
        if(cameraServiceListener != null){
            cameraServiceListener.onTokenChanged(token);
        }
    }


    //////service interface /////
    @Override
    public void showHideChromeWindow() {
        mWebRTCWebView.setVisibility(mWebRTCWebView.getVisibility() == View.VISIBLE ? View.GONE: View.VISIBLE);
    }

    @Override
    public void resetBrowser() {
        setupWebview();
    }

    @Override
    public void registerListener(CameraServiceListener cameraServiceListener) {
        this.cameraServiceListener = cameraServiceListener;
    }

    @Override
    public void clearListener(CameraServiceListener cameraServiceListener) {
        cameraServiceListener = null;
    }

    @Override
    public String getCurrentToken() {
        return token;
    }

    public void stopService(){
        stopSelf();
    }

    /*@Override
    public boolean onTouch(View view, MotionEvent motionEvent) {

        Log.v(TAG, "onTouch...");

        // Kill service
        stopSelf();

        return true;
    }*/

}



