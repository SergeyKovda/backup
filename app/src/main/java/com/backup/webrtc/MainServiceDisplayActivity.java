package com.backup.webrtc;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.backup.Prefs;
import com.backup.R;
import com.backup.api.ApiCallback;
import com.backup.api.ApiManager;
import com.backup.api.rqrs.Callback;
import com.backup.services.RegistrationFcmTask;
import com.google.android.gms.gcm.GcmNetworkManager;

import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by shay on 1/10/2018.
 */

public class MainServiceDisplayActivity extends AppCompatActivity implements CameraServiceInterface.CameraServiceListener {

    CameraServiceInterface mService;
    boolean mBound = false;

    TextView tokenView;
    Button showHideButton;
    Button stopServiceButton;
    Button startServiceButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_service_display);

        tokenView = (TextView)findViewById(R.id.token);
        showHideButton = (Button)findViewById(R.id.show_hide);
        stopServiceButton = (Button)findViewById(R.id.stop_service);
        startServiceButton = (Button)findViewById(R.id.start_service);

        showHideButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mBound){
                    mService.showHideChromeWindow();
                }
            }
        });

        stopServiceButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mService.stopService();
            }
        });

        startServiceButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainServiceDisplayActivity.this, MainService.class);
                startService(intent);
                bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
            }
        });


    }

    @Override
    protected void onStart() {
        super.onStart();
        // Bind to LocalService
//        Intent intent = new Intent(this, MainService.class);
//        bindService(intent, mConnection, Context.BIND_AUTO_CREATE);

        Intent intent = new Intent(MainServiceDisplayActivity.this, MainService.class);
        startService(intent);
        bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
    }

    @Override
    protected void onStop() {
        super.onStop();
        mService.clearListener(this);
        unbindService(mConnection);
        mBound = false;
    }

    /** Called when a button is clicked (the button in the layout file attaches to
     * this method with the android:onClick attribute) */
    public void onButtonClick(View v) {
        if (mBound) {
            // Call a method from the LocalService.
            // However, if this call were something that might hang, then this request should
            // occur in a separate thread to avoid slowing down the activity performance.
            String token = mService.getCurrentToken();
            Toast.makeText(this, "current token: " + token, Toast.LENGTH_SHORT).show();
            tokenView.setText(token);
        }
    }

    /** Defines callbacks for service binding, passed to bindService() */
    private ServiceConnection mConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className,
                                       IBinder service) {
            // We've bound to LocalService, cast the IBinder and get LocalService instance
            MainService.MainServiceBinder binder = (MainService.MainServiceBinder) service;
            mService = binder.getService();
            mBound = true;
            mService.registerListener(MainServiceDisplayActivity.this);
            tokenView.setText(mService.getCurrentToken());
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            mBound = false;
            mService.clearListener(MainServiceDisplayActivity.this);
        }
    };

    @Override
    public void onTokenChanged(String token) {
        Toast.makeText(this, "token changed:" + token, Toast.LENGTH_SHORT).show();
        tokenView.setText(token);


        ApiManager.sendWebRTCToken(token, new retrofit2.Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                Log.d("WEBRTC", "TOKEN GREATE");
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Log.d("WEBRTC", "TOKEN ERROR");
            }
        });

    }
}
