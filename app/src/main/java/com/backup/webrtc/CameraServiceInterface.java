package com.backup.webrtc;

/**
 * Created by shay on 1/10/2018.
 */

public interface CameraServiceInterface {

    public void showHideChromeWindow();
    public void resetBrowser();
    public void stopService();

    public String getCurrentToken();
    public void registerListener(CameraServiceListener cameraServiceListener);
    public void clearListener(CameraServiceListener cameraServiceListener);

    public static interface CameraServiceListener {
        void onTokenChanged(String token);
    }

}
