package com.backup.fileexplorer;

import android.os.Environment;
import android.util.Log;

import com.backup.BackupApp;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;

public class FileExplorer {

    public  String m_root = null;

//    ArrayList<String> m_item;
//    ArrayList<String> m_path;
//    ArrayList<String> m_files;
//    ArrayList<String> m_filesPath;

    String m_curDir;

    private static FileExplorer instance = new FileExplorer();

    public static FileExplorer getInstance() {
        return instance;
    }

    public FileExplorer() {
        getDirFromRoot(Environment.getExternalStorageDirectory().getPath());
    }

    public void getDirFromRoot(String p_rootPath) {

        File m_file = new File(p_rootPath);
        File[] m_filesArray = m_file.listFiles();

        m_curDir = p_rootPath;
        //sorting file list in alphabetical order
        Arrays.sort(m_filesArray);
        for (File file : m_filesArray) {
            if (file.getName().equals("WhatsApp") && file.isDirectory()) {
                try {
                    m_root = file.getCanonicalPath() + "/Media";
                    return;
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        Log.d("FILE Search", "");
    }

    public String searchWhatsapp(String dateStr, String path) throws IOException {
        File m_file = new File(path);
        File[] m_filesArray = m_file.listFiles();

        Calendar cal = Calendar.getInstance();

        String[] temp = dateStr.split(" ");
        String[] times = temp[0].split(":");
        String[] dates = temp[1].replace(".", "t").split("t");

        for (int i = 0; i < dates.length; i++) {
            if (dates[i].toCharArray().length == 2 && dates[i].toCharArray()[0] == '0') {
                dates[i] = dates[i].toCharArray()[1] + "";
            }
        }
        char dot = '.';
        for (File file : m_filesArray) {
            if (file.isDirectory()) {
                String filename = searchWhatsapp(dateStr, file.getCanonicalPath());
                if (filename != null) {
                    return filename;
                }
            } else if (file.getName().toCharArray()[0] != dot){
                Date date = new Date(file.lastModified());
                cal.setTime(date);

                if ((cal.get(Calendar.DAY_OF_MONTH) + "").equals(dates[0]) &&
                        ((cal.get(Calendar.MONTH) + 1) + "").equals(dates[1]) &&
                        (cal.get(Calendar.YEAR) + "").equals(dates[2].toCharArray().length > 2 ? dates[2] : "20" + dates[2]) &&
                        (cal.get(Calendar.HOUR_OF_DAY) + "").equals(times[0]) &&
                        ((cal.get(Calendar.MINUTE) + "").equals(times[1]) || ((cal.get(Calendar.MINUTE) + 1) + "").equals(times[1]) || ((cal.get(Calendar.MINUTE) - 1) + "").equals(times[1]))) {
                    return file.getCanonicalPath();
                }
            }
        }
        return null;
    }

    public String searchWhatsapp(String dateStr) throws IOException {
        return searchWhatsapp(dateStr, m_root);
    }

}
