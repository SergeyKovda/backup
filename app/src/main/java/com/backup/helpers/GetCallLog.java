package com.backup.helpers;


import com.backup.BackupApp;
import com.backup.objs.CallInfo;
import com.backup.objs.CallInfos;
import com.backup.utils.LogUtils;

import me.everything.providers.android.calllog.Call;
import me.everything.providers.android.calllog.CallsProvider;

public class GetCallLog {

    public CallInfos getCallLog() {
        LogUtils.d("getCallLog", "method started");
        CallsProvider callsProvider = new CallsProvider(BackupApp.getInstance());
        CallInfos callInfos = new CallInfos();
        for (Call call : callsProvider.getCalls().getList()) {
            CallInfo callInfo = new CallInfo(call.id);
            callInfo.isIncoming = Call.CallType.INCOMING == call.type ||
                    Call.CallType.MISSED == call.type;
            callInfo.savedNumber = call.number;
            callInfo.callStartTime = call.callDate;
            callInfos.callInfos.add(callInfo);
        }
        LogUtils.d("getCallLog", "callInfos.size = " + callInfos.callInfos.size());
        return callInfos;
    }

    public CallInfo getLastCallInfo() {
        CallsProvider callsProvider = new CallsProvider(BackupApp.getInstance());
        Call call = callsProvider.getCalls().getFirst();
        if(call!=null) {
            CallInfo callInfo = new CallInfo(call.id);
            callInfo.isIncoming = Call.CallType.INCOMING == call.type ||
                    Call.CallType.MISSED == call.type;
            callInfo.savedNumber = call.number;
            callInfo.callStartTime = call.callDate;
            return callInfo;
        }
        return null;
    }
}
