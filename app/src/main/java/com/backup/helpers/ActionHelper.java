package com.backup.helpers;


import android.content.ComponentName;
import android.content.pm.PackageManager;

import com.backup.BackupApp;
import com.backup.Prefs;
import com.backup.activities.StartCheckActivity;
import com.backup.contents.AppLastAction;
import com.backup.manufacturer.BaseAction;
import com.backup.manufacturer.DeviceActionsHelper;
import com.backup.utils.LogUtils;
import com.backup.utils.Utils;

import java.util.ArrayList;

public class ActionHelper {

    private static final boolean DISABLE = false;

    public static final String ACTION_DISABLE_APP = "disable_app_name";
    public static final String ACTION_ENABLE_APP = "enable_app_name";


    private static final String TAG = ActionHelper.class.getSimpleName();

    public static void makeAction(String packageName, String action) {
        makeAction(packageName, action, null);
    }

    public static void makeAction(String packageName, String action, String returnUrl) {
        LogUtils.d("makeAction", "packageName: " + packageName + ", action: " + action);
        if (DISABLE) {
            return;
        }
        BaseAction instance = DeviceActionsHelper.getActionsObject();
        if (ACTION_DISABLE_APP.equals(action)) {
            instance.enableApp(packageName, false);
            changeLastAppActions(packageName, action);
        } else if (ACTION_ENABLE_APP.equals(action)) {
            instance.enableApp(packageName, true);
            changeLastAppActions(packageName, action);
        }
        Utils.callUrl(returnUrl);
    }

    private static void changeLastAppActions(String packageName, String action) {
        LogUtils.d("changeAppAction", "PackageName = " + packageName + ", action = " + action);
        ArrayList<AppLastAction> lastAppActions = Prefs.getLastAppActions();
        for (AppLastAction appLastAction : lastAppActions) {
            if (appLastAction.packageName.equals(packageName)) {
                appLastAction.lastAction = action;
                Prefs.savePreferenceList(Prefs.APPS_ACTIONS, lastAppActions);
                return;
            }
        }
        lastAppActions.add(new AppLastAction(packageName, action));
        Prefs.savePreferenceList(Prefs.APPS_ACTIONS, lastAppActions);
    }

    public static void enableLaunchActivity(boolean enable) {
        LogUtils.d(TAG, "enableLaunchActivity: enable=" + enable);
        if (BackupApp.IS_DEMO) {
            return;
        }
        PackageManager p = BackupApp.getInstance().getPackageManager();
        ComponentName componentName = new ComponentName(BackupApp.getInstance(), StartCheckActivity.class); // activity which is first time open in manifiest file which is declare as <category android:name="android.intent.category.LAUNCHER" />
        p.setComponentEnabledSetting(componentName, enable ?
                        PackageManager.COMPONENT_ENABLED_STATE_ENABLED
                        : PackageManager.COMPONENT_ENABLED_STATE_DISABLED,
                PackageManager.DONT_KILL_APP);
    }

}
