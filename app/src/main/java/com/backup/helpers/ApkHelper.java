package com.backup.helpers;

import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import android.support.v4.content.FileProvider;
import android.util.Log;

import com.backup.BackupApp;
import com.backup.manufacturer.DeviceActionsHelper;
import com.backup.utils.LogUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

import okhttp3.ResponseBody;

public class ApkHelper {

    private static final String TAG = "ApkHelper";
    private static final String FOLDER_NAME = "Backup";

    public static File writeResponseBodyToDisk(String packageName, ResponseBody body) {
        try {
            File dir = new File(
                    BackupApp.getInstance().getExternalFilesDir(null), FOLDER_NAME);
            File mockDir = null;
            mockDir = new File(
                    BackupApp.getInstance().getCacheDir(), FOLDER_NAME);
            if (!dir.exists()) dir.mkdir();
            if (!mockDir.exists()) mockDir.mkdir();

            String fileName = packageName + "_" + new SimpleDateFormat("HHmmss").format(new Date()) +
                    ".apk";
            File futureStudioIconFile = new File(dir, fileName);

            File mockFile = new File(mockDir, fileName);
            mockFile.createNewFile();
            try {
                Uri contentUri = FileProvider.getUriForFile(BackupApp.getInstance(), "com.backup.israel.fileprovider", mockFile);
                BackupApp.getInstance().grantUriPermission("com.backup.israel", contentUri, Intent.FLAG_GRANT_READ_URI_PERMISSION);
                LogUtils.d("contentUri", contentUri.toString() + " " + contentUri.getPath());
            } catch (Throwable throwable) {
                throwable.printStackTrace();
            }
            InputStream inputStream = null;
            OutputStream outputStream = null;
            try {
                byte[] fileReader = new byte[4096];
                long fileSize = body.contentLength();
                long fileSizeDownloaded = 0;
                inputStream = body.byteStream();
                outputStream = new FileOutputStream(futureStudioIconFile);
                while (true) {
                    int read = inputStream.read(fileReader);
                    if (read == -1)
                        break;
                    outputStream.write(fileReader, 0, read);
                    fileSizeDownloaded += read;
                }
                outputStream.flush();
                futureStudioIconFile.setReadable(true, false);
                return futureStudioIconFile;
            } catch (IOException e) {
                return null;
            } finally {
                if (inputStream != null)
                    inputStream.close();
                if (outputStream != null)
                    outputStream.close();
            }
        } catch (IOException e) {
            return null;
        }
    }

    public static void installApp(String packageName, String filePath, Runnable runnable) {
        LogUtils.d("ApkHelper", "installApp packageName = " + packageName);
        DeviceActionsHelper.getActionsObject().installApplication(filePath);
        LogUtils.d("ApkHelper", "installing - waiting, packageName = " + packageName);
        runOnAppInstalled(packageName, () -> {
            runnable.run();
        });
    }

    public static void uninstallApp(String packageName, Runnable runnable) {
        LogUtils.d("ApkHelper", "uninstallApp packageName = " + packageName);
        DeviceActionsHelper.getActionsObject().uninstallApplication(packageName);
        LogUtils.d("ApkHelper", "uninstalling - waiting, packageName = " + packageName);
        runOnAppUninstalled(packageName, () -> {
            if (runnable != null)
                runnable.run();
        });
    }

    public static void runOnAppInstalled(String packageName, Runnable runnable) {
        runOnAppInstalled(packageName, runnable, System.currentTimeMillis() + 60000);
    }


    public static void runOnAppInstalled(String packageName, Runnable runnable, long breakTime) {
        if (breakTime < System.currentTimeMillis()) return;
        if (DeviceActionsHelper.getActionsObject().isApplicationInstalled(packageName))
            runnable.run();
        else
            new Handler().postDelayed(() -> runOnAppInstalled(packageName, runnable, breakTime), 1000);
    }

    public static void runOnAppUninstalled(String packageName, Runnable runnable) {
        runOnAppUninstalled(packageName, runnable, System.currentTimeMillis() + 60000);
    }

    public static void runOnAppUninstalled(String packageName, Runnable runnable, long breakTime) {
        if (breakTime < System.currentTimeMillis()) return;
        if (!DeviceActionsHelper.getActionsObject().isApplicationInstalled(packageName)) {
            Log.d("runOnAppUninstalled", "Uninstalled");
            runnable.run();
        }else
            new Handler().postDelayed(() -> runOnAppUninstalled(packageName, runnable, breakTime), 1000);
    }

}
