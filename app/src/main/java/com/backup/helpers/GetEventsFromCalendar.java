package com.backup.helpers;


import android.Manifest;
import android.provider.CalendarContract.Calendars;

import com.backup.BackupApp;
import com.backup.objs.GoogleEvents;
import com.backup.utils.LogUtils;
import com.backup.utils.PermissionUtils;

import java.util.ArrayList;
import java.util.List;

import me.everything.providers.android.calendar.Calendar;
import me.everything.providers.android.calendar.CalendarProvider;
import me.everything.providers.android.calendar.Event;

public class GetEventsFromCalendar {

    public GoogleEvents getEventsFromCalendar() {
        GoogleEvents googleEvents = new GoogleEvents();
        if (PermissionUtils.checkPermission(null, Manifest.permission.READ_CALENDAR, false)) {
            googleEvents.events = getCalendarEvents();
        }
        return googleEvents;
    }

    private ArrayList<com.backup.objs.Event> getCalendarEvents() {
        LogUtils.d("getCalendarEvents", "method started");
        ArrayList<com.backup.objs.Event> eventsForReturn = new ArrayList<>();
        CalendarProvider calendarProvider = new CalendarProvider(BackupApp.getInstance());
        List<Calendar> calendars = calendarProvider.getCalendars().getList();
        for (Calendar calendar : calendars) {
            if (calendar.calendarAccessLevel != Calendars.CAL_ACCESS_OWNER)
                continue;
            List<Event> events = calendarProvider.getEvents(calendar.id).getList();
            for (Event event : events) {
                // TODO Remove it if we should send all events
//                if (event.dTStart < System.currentTimeMillis()) {
//                    continue;
//                }
                com.backup.objs.Event ev = new com.backup.objs.Event(event.id);
                ev.accountName = event.accountName;
                ev.title = event.title;
                ev.dateStart = event.dTStart;
                ev.dateEnd = event.dTend;
                ev.eventTimeZone = event.eventTimeZone;
                eventsForReturn.add(ev);
            }
        }
        LogUtils.d("getCalendarEvents", "eventsForReturn.size  = " + eventsForReturn.size());
        return eventsForReturn;
    }
}
