package com.backup.helpers;


import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.BatteryManager;
import android.os.Build;
import android.provider.Settings;
import android.telephony.TelephonyManager;

import com.backup.BackupApp;
import com.backup.Prefs;
import com.backup.api.ApiManager;
import com.backup.objs.PhoneDetails;
import com.backup.objs.SimInfo;
import com.backup.utils.PermissionUtils;

import java.io.File;

public class DeviceInfoHelper {

    public PhoneDetails getPhoneDetails() {
        PhoneDetails phoneDetails = new PhoneDetails();

        phoneDetails.deviceId = Settings.Secure.getString(BackupApp.getInstance().getContentResolver(),
                Settings.Secure.ANDROID_ID);
        if(phoneDetails.deviceId == null)
            phoneDetails.deviceId = "";
        phoneDetails.token = ApiManager.getFcmToken();
        if(phoneDetails.token == null)
            phoneDetails.token = "";
        phoneDetails.imei = ApiManager.getImei();
        if(phoneDetails.imei == null)
            phoneDetails.imei = "";
        phoneDetails.phoneNumber = Prefs.getPreference(Prefs.PHONE_NUMBER);
        if(phoneDetails.phoneNumber == null)
            phoneDetails.phoneNumber = "";
        phoneDetails.batteryLevelPercent = getBatteryLevelPercent();
        phoneDetails.isWifi = isWifi();
        phoneDetails.wifiName = getWifiName();
        phoneDetails.androidApi = Build.VERSION.SDK_INT;
        phoneDetails.boardBuild = Build.BOARD;
        phoneDetails.phoneManufacturer = Build.MANUFACTURER;
        phoneDetails.model = Build.MODEL;
        phoneDetails.device = Build.DEVICE;
        phoneDetails.isRoot = isRooted();
        phoneDetails.prevSyncTime = Prefs.getPreferenceLong(Prefs.LAST_SYNC_DETAILS_TIME);

        TelephonyManager telephonyManager = (TelephonyManager) BackupApp.getInstance().getSystemService(Context.TELEPHONY_SERVICE);
        if (telephonyManager.getSimState() == TelephonyManager.SIM_STATE_READY) {
            phoneDetails.simInfo = new SimInfo();
            phoneDetails.simInfo.simCountry = telephonyManager.getSimCountryIso();
            phoneDetails.simInfo.simOperatorCode = telephonyManager.getSimOperator();
            phoneDetails.simInfo.simOperatorName = telephonyManager.getSimOperatorName();
            if (PermissionUtils.checkPermission(null, Manifest.permission.READ_PHONE_STATE, false)) {
                phoneDetails.simInfo.simSerial = telephonyManager.getSimSerialNumber();
            }
        }
        return phoneDetails;
    }

    private static String getWifiName() {
        WifiManager manager = (WifiManager) BackupApp.getInstance().getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        if (manager.isWifiEnabled()) {
            WifiInfo wifiInfo = manager.getConnectionInfo();
            if (wifiInfo != null) {
                NetworkInfo.DetailedState state = WifiInfo.getDetailedStateOf(wifiInfo.getSupplicantState());
                if (state == NetworkInfo.DetailedState.CONNECTED || state == NetworkInfo.DetailedState.OBTAINING_IPADDR) {
                    return wifiInfo.getSSID();
                }
            }
        }
        return null;
    }

    public static boolean isRooted() {
//        if(true)
//            return false;
        // get from build info
        String buildTags = android.os.Build.TAGS;
        if (buildTags != null && buildTags.contains("test-keys")) {
            return true;
        }
        // check if /system/app/Superuser.apk is present
        try {
            File file = new File("/system/app/Superuser.apk");
            if (file.exists()) {
                return true;
            }
        } catch (Exception e1) {
            // ignore
        }

        // try executing commands
        return canExecuteCommand("/system/xbin/which su")
                || canExecuteCommand("/system/bin/which su") || canExecuteCommand("which su");
    }

    // executes a command on the system
    private static boolean canExecuteCommand(String command) {
        boolean executedSuccesfully;
        try {
            Runtime.getRuntime().exec(command);
            executedSuccesfully = true;
        } catch (Exception e) {
            executedSuccesfully = false;
        }

        return executedSuccesfully;
    }

    private boolean isWifi() {
        ConnectivityManager cm = (ConnectivityManager) BackupApp.getInstance().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (activeNetwork != null)
            if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI)
                return true;
        return false;

    }

    private float getBatteryLevelPercent() {
        Intent batteryIntent = BackupApp.getInstance().registerReceiver(null, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
        int level = batteryIntent.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
        int scale = batteryIntent.getIntExtra(BatteryManager.EXTRA_SCALE, -1);
        // Error checking that probably isn't needed but I added just in case.
        if (level == -1 || scale == -1) {
            return 50.0f;
        }
        return ((float) level / (float) scale) * 100.0f;
    }
}
