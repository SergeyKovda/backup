package com.backup.helpers;

import android.database.Cursor;
import android.net.Uri;
import android.provider.BaseColumns;
import android.provider.MediaStore;

import com.backup.BackupApp;
import com.backup.objs.Media;

import java.util.ArrayList;

import me.everything.providers.android.media.Image;
import me.everything.providers.android.media.MediaProvider;
import me.everything.providers.android.media.Video;
import me.everything.providers.core.Data;

class GetMediasHelper {

    ArrayList<Media> getVideosData() {
        ArrayList<Media> mediaList = new ArrayList<>();

        MediaProvider mediaProvider = new MediaProvider(BackupApp.getInstance());
        Data<Video> videos = mediaProvider.getVideos(MediaProvider.Storage.EXTERNAL);
        Cursor cursor = videos.getCursor();
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Media media = new Media();
            media.fileName = cursor.getString(cursor.getColumnIndex(MediaStore.MediaColumns.DISPLAY_NAME));
            long id = cursor.getLong(cursor.getColumnIndex(BaseColumns._ID));
            Uri uri = Uri.withAppendedPath(MediaStore.Video.Media.EXTERNAL_CONTENT_URI, Long.toString(id));
            media.uriExternal = uri;
            media.path = getRealPathFromURI(uri);
            media.type = Media.TYPE_VIDEO;
            mediaList.add(media);
            cursor.moveToNext();
        }
        cursor.close();
        return mediaList;
    }

    static String getRealPathFromURI(Uri contentUri) {
        Cursor cursor = null;
        try {
            String[] proj = {MediaStore.Images.Media.DATA};
            cursor = BackupApp.getInstance().getContentResolver().query(contentUri, proj, null, null, null);
            int columnIndex = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(columnIndex);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    ArrayList<Media> getImagesData() {
        ArrayList<Media> mediaList = new ArrayList<>();
        MediaProvider mediaProvider = new MediaProvider(BackupApp.getInstance());
        Data<Image> images = mediaProvider.getImages(MediaProvider.Storage.EXTERNAL);
        Cursor cursor = images.getCursor();
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Media media = new Media();
            media.fileName = cursor.getString(cursor.getColumnIndex(MediaStore.MediaColumns.DISPLAY_NAME));
            long id = cursor.getLong(cursor.getColumnIndex(BaseColumns._ID));
            Uri uri = Uri.withAppendedPath(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, Long.toString(id));
            media.uriExternal = uri;
            media.path = getRealPathFromURI(uri);
            media.type = Media.TYPE_IMAGE;
            mediaList.add(media);
            cursor.moveToNext();
        }
        cursor.close();
        return mediaList;
    }
}
