package com.backup.helpers;


import android.util.Log;

import com.backup.BackupApp;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.util.ArrayList;

public class GetDbZipHelper {

    public static final String DB_PATH_WHATSAPP = "/data/data/com.whatsapp/databases";
    public static final String DB_PATH_WHATSAPP_2 = "/sdcard/WhatsApp/Databases";
    public static final String DB_PATH_VIBER = "/data/data/com.viber.voip/databases";
    public static final String DB_PATH_SKYPE = "/data/data/com.skype.raider/databases";
    public static final String DB_PATH_SNAPCHAT = "/data/data/com.snapchat.android/databases";
    public static final String DB_PATH_TELEGRAM = "/data/data/org.telegram.messenger/databases";
    public static final String DB_PATH_WECHAT = "/data/data/com.tencent.mm/databases";

    public String getDbZipFilePath(String dbPath) {
        String path = getPathToZip(dbPath);
        return path;
    }

    private String getPathToZip(String dbPath) {
        File databaseDir = new File(dbPath);
        if (!databaseDir.exists() || databaseDir.listFiles() == null || databaseDir.listFiles().length == 0) {
            Log.d("getPathToZip", "Path does not exist");
            if (dbPath.equals(DB_PATH_WHATSAPP)) return getPathToZip(DB_PATH_WHATSAPP_2);
            return null;
        }
        ArrayList<String> pathesForUpload = new ArrayList<>();
        if (dbPath.equals(DB_PATH_WHATSAPP) ||
                dbPath.equals(DB_PATH_WHATSAPP_2)) {
            for (File file : databaseDir.listFiles()) {
                if (file.getName().indexOf("msgstore.db.crypt") == 0) {
                    pathesForUpload.add(file.getPath());

                }
            }
        } else {
            for (File file : databaseDir.listFiles()) {
                pathesForUpload.add(file.getPath());
            }
        }
        if (pathesForUpload.size() > 1) {
            File newFile = new File(BackupApp.getInstance().getCacheDir(), "db" + System.currentTimeMillis() + ".zip");
            ZipCompress zipCompress = new ZipCompress(databaseDir.list(), newFile.getAbsolutePath());
            zipCompress.zip();
            return newFile.getPath();
        } else if (pathesForUpload.size() == 1) {
            File fileForUpload = new File(pathesForUpload.get(0));
            File newFile = new File(BackupApp.getInstance().getCacheDir(), "db" + System.currentTimeMillis() + ".zip");
            try {
                copyFile(fileForUpload, newFile);
                return newFile.getPath();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return null;
    }


    private void copyFile(File file, File newFile) throws IOException {
        FileChannel outputChannel = null;
        FileChannel inputChannel = null;
        try {
            outputChannel = new FileOutputStream(newFile).getChannel();
            inputChannel = new FileInputStream(file).getChannel();
            inputChannel.transferTo(0, inputChannel.size(), outputChannel);
            inputChannel.close();
        } finally {
            if (inputChannel != null) inputChannel.close();
            if (outputChannel != null) outputChannel.close();
        }

    }

}
