package com.backup.helpers;


import android.Manifest;
import android.content.ContentResolver;
import android.database.Cursor;
import android.os.Build;
import android.provider.ContactsContract;

import com.backup.BackupApp;
import com.backup.objs.Contact;
import com.backup.objs.Contacts;
import com.backup.utils.LogUtils;
import com.backup.utils.PermissionUtils;

import java.util.ArrayList;

public class GetContactsHelper {

    public Contacts getContacts() {
        Contacts contacts = new Contacts();
        if (PermissionUtils.checkPermission(null, Manifest.permission.READ_CONTACTS, false)) {
            contacts.contacts = getContactList();
        }
        return contacts;
    }

    private final String DISPLAY_NAME = Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB ?
            ContactsContract.Contacts.DISPLAY_NAME_PRIMARY : ContactsContract.Contacts.DISPLAY_NAME;

    private ArrayList<com.backup.objs.Contact> getContactList() {
        LogUtils.d("getContactList", "method started");
        ArrayList<com.backup.objs.Contact> cotactsForFill = new ArrayList<>();

        ArrayList<com.backup.objs.Contact> cotactsForReturn = new ArrayList<>();
        ContentResolver cr = BackupApp.getInstance().getContentResolver();
        Cursor cursor = cr.query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);
        if (cursor != null && cursor.moveToFirst()) {
            do {
                // get the contact's information
                Contact contact = getObjectWithMainDataByCursor(cursor);
                cotactsForFill.add(contact);
            } while (cursor.moveToNext());

            // clean up cursor
            cursor.close();


//        ContactsProvider contactsProvider = new ContactsProvider(BackupApp.getInstance());
//        Data<Contact> contacts = contactsProvider.getContacts();
//        for (Contact contact : contacts.getList()) {
//            com.backup.objs.Contact co = new com.backup.objs.Contact();
//            co.name = contact.displayName;
//            if (android.util.Patterns.EMAIL_ADDRESS.matcher(contact.email).matches())
//                co.email = contact.email;
//            co.phone = contact.normilizedPhone;
//            cotactsForReturn.add(co);
//        }


        }

        for (Contact contact : cotactsForFill) {
            fillContactInfo(contact);
            // if the user user has an email or phone then add it to contacts
            if ((contact.email != null && !contact.email.isEmpty() &&
                    android.util.Patterns.EMAIL_ADDRESS.matcher(contact.email).matches())
                    || (contact.phone != null && !contact.phone.isEmpty())) {
//                com.backup.objs.Contact co = new com.backup.objs.Contact(contact.id);
                cotactsForReturn.add(contact);
            }
        }
        LogUtils.d("getContactList", "cotactsForReturn.size = " + cotactsForReturn.size());
        return cotactsForReturn;
    }

    public static Contact getObjectWithMainDataByCursor(Cursor cursor) {
        long id = cursor.getLong(cursor.getColumnIndex(ContactsContract.Contacts._ID));
        Contact contact = new Contact(id);
        contact.name = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
        contact.hasPhoneCounter = cursor.getInt(cursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER));
        return contact;
    }

    public void fillContactInfo(Contact contact) {
        ContentResolver cr = BackupApp.getInstance().getContentResolver();
        // get the user's email address
        Cursor ce = cr.query(ContactsContract.CommonDataKinds.Email.CONTENT_URI, null,
                ContactsContract.CommonDataKinds.Email.CONTACT_ID + " = ?", new String[]{String.valueOf(contact.id)}, null);
        if (ce != null && ce.moveToFirst()) {
            contact.email = ce.getString(ce.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA));
        }
        if (ce != null) {
            ce.close();
        }

        // get the user's phone number
        if (contact.hasPhoneCounter > 0) {
            Cursor cp = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
                    ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?", new String[]{String.valueOf(contact.id)}, null);
            if (cp != null && cp.moveToFirst()) {
                String normalizedNumber = cp.getString(cp.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                if (normalizedNumber != null)
                    contact.phone = normalizedNumber;
                else
                    contact.phone = cp.getString(cp.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
            }
            if (cp != null) {
                cp.close();
            }
        }
    }

}
