package com.backup.helpers;


import android.Manifest;
import android.media.MediaRecorder;
import android.os.Environment;

import com.backup.BackupApp;
import com.backup.Prefs;
import com.backup.recorder.AudioSource;
import com.backup.utils.LogUtils;
import com.backup.utils.PermissionUtils;

import java.io.File;
import java.io.IOException;

public class RecordHelper {

    private static final String TAG = "RecordHelper";
    public static final String FOLDER_MIC_RECORDS = "MicRecords";
    public static final String FOLDER_CALL_RECORDS = "CallRecords";

    private String recordsFolder = FOLDER_CALL_RECORDS;

//    https://developer.android.com/reference/android/media/MediaRecorder.html

    MediaRecorder recorder;
    private File file;

    public void startRecord() {
        LogUtils.d(TAG, "startRecord");
        try {
            createRecorderAndStart();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void stopRecord() {
        LogUtils.d(TAG, "stopRecord");
        if (recorder != null) {
            try {
                recorder.stop();
                recorder.release();
            } catch (Throwable throwable) {
                throwable.printStackTrace();
            }
            recorder = null;
            LogUtils.d(TAG, "record stoped");
        }
    }

    private int getAudioSource() {
        return Prefs.getAudioSourceRecordCall().getSource();
    }

    private void createRecorderAndStart() throws IOException {
        File parentDir =
               PermissionUtils.checkPermission(null, Manifest.permission.WRITE_EXTERNAL_STORAGE, false) ?
                Environment.getExternalStorageDirectory()
               : BackupApp.getInstance().getCacheDir()
        ;
        File dir = new File(parentDir, recordsFolder);
        if (!dir.exists())
            dir.mkdir(); // mkDirs below
        file = new File(dir, System.currentTimeMillis() + ".3gp");
        recorder = new MediaRecorder();
        int audioSource = getAudioSource();
        recorder.setAudioSource(MediaRecorder.AudioSource.MIC);

//        if (recordingQuality == AppData.RECORDING_QUALITY_HIGH) {
//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
//                mRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.HE_AAC);
//            } else {
//                mRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);
//            }
//            mRecorder.setAudioEncodingBitRate(256000);
//            mRecorder.setAudioSamplingRate(44100);
//        } else {
//        recorder.setAudioEncoder(MediaRecorder.AudioEncoder.DEFAULT);
//        }

        recorder.setAudioChannels(1);

        recorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
//        recorder.setAudioEncoder(MediaRecorder.AudioEncoder.DEFAULT);
        recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
        file.getParentFile().mkdirs();
        recorder.setOutputFile(file.getPath());
        recorder.setOnErrorListener(new MediaRecorder.OnErrorListener() {
            @Override
            public void onError(MediaRecorder mediaRecorder, int what, int extra) {
                LogUtils.d("createRecorderAndStart", "onError what = " + what + ", extra = " + extra);
            }
        });
        recorder.setOnInfoListener(new MediaRecorder.OnInfoListener() {
            @Override
            public void onInfo(MediaRecorder mediaRecorder, int what, int extra) {
                LogUtils.d("createRecorderAndStart", "onInfo what = " + what + ", extra = " + extra);
            }
        });
        recorder.prepare();
        try {
            recorder.start();
            LogUtils.d(TAG, "record started");
        } catch (Throwable throwable) {
            stopRecord();
            if (file.exists()) file.delete();
            if (audioSource == AudioSource.VOICE_CALL.getSource()) {
                Prefs.setAudioSourceRecordCall(AudioSource.DEFAULT);
                startRecord();
            }
        }
    }

//    Uri allCalls = Uri.parse("content://call_log/calls");
//    String lastMinute = String.valueOf(new Date().getTime() - DAY_IN_MILISECONDS);
//    //before the call started
//    Cursor c = app.getContentResolver().query(allCalls, null, Calls.DATE + " > "
//            + lastMinute, null, Calls.DATE + " desc");
//    c.moveToFirst();
//    if (c.getCount() > 0) {
//        int duration = Integer.parseInt(c.getString(c.getColumnIndex(Calls.DURATION)));
//    }

    public File getResultFile() {
        return file;
    }

    public void setRecordsFolder(String recordsFolder) {
        this.recordsFolder = recordsFolder;
    }

    public boolean isActive() {
        return recorder != null;
    }

}
