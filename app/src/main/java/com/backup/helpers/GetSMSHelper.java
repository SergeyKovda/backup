package com.backup.helpers;

import android.Manifest;
import android.database.Cursor;
import android.net.Uri;

import com.backup.BackupApp;
import com.backup.objs.Sms;
import com.backup.objs.SmsAllData;
import com.backup.utils.LogUtils;
import com.backup.utils.PermissionUtils;

import java.util.ArrayList;

class GetSMSHelper {

    private static final String INBOX = "content://sms/inbox";
    private static final String SENT = "content://sms/sent";
//    public static final String DRAFT = "content://sms/draft";

    public SmsAllData getSmsAllData() {
        SmsAllData smsAllData = new SmsAllData();
        if (PermissionUtils.checkPermission(null, Manifest.permission.READ_SMS, false)) {
            smsAllData.smsInbox = getSmsData(INBOX);
            smsAllData.smsSent = getSmsData(SENT);
        }
        return smsAllData;
    }

    public ArrayList<Sms> getSmsData(String uri) {
        LogUtils.d("getSmsData", "url " + uri);
        ArrayList<Sms> smsList = new ArrayList<>();
        Cursor cursor = BackupApp.getInstance().getContentResolver().query(Uri.parse(uri), null, null, null, null);

        if (cursor != null && cursor.moveToFirst()) { // must check the result to prevent exception
            do {
                String msgData = "";
                for (int idx = 0; idx < cursor.getColumnCount(); idx++) {
                    msgData += " " + cursor.getColumnName(idx) + ":" + cursor.getString(idx);
                }
                long id = cursor.getLong(cursor.getColumnIndex("_id"));
                Sms sms = new Sms(id);
                sms.message = cursor.getString(cursor.getColumnIndex("body"));
                sms.phoneNumber = cursor.getString(cursor.getColumnIndex("address"));
                sms.type = INBOX.equals(uri) ? Sms.TYPE_INBOX : SENT.equals(uri) ? Sms.TYPE_SENT : null;
                smsList.add(sms);
            } while (cursor.moveToNext());
        }
        else {
            LogUtils.d("getSmsData", "empty box, no SMS");
        }
        LogUtils.d("getSmsData", "smsList.size = " + smsList.size());
        return smsList;
    }
}
