package com.backup.activities;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.telephony.SmsMessage;
import android.telephony.TelephonyManager;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.Toast;

import com.backup.BackupApp;
import com.backup.Prefs;
import com.backup.R;
import com.backup.api.ApiCallback;
import com.backup.api.ApiManager;
import com.backup.api.rqrs.AuthResponse;
import com.backup.contents.Country;
import com.backup.services.accessibility.MyAccessibilityService;
import com.backup.utils.PermissionUtils;
import com.backup.utils.Utils;
import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Locale;

public class StartCheckActivity extends AppCompatActivity implements View.OnClickListener {

    private static String TAG = "StartCheckActivity";

    private static final int CODE_ACCESSIBILITY_SERVICE = 2002;
    private EditText etChecker;
    private AutoCompleteTextView etCountry;
    private View btnNextStep;

    boolean bound = false;
    private String iso;
    public Country selectedCountry;

    private boolean isSmsCodeChecker = false;
    private String phone;
    private boolean sendPhoneOnPermissionReceived = false;
    private BroadcastReceiver smsReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (!isSmsCodeChecker) {
                return;
            }
            final Bundle bundle = intent.getExtras();
            if (bundle != null) {
                String messageBody = "";
                Object[] pdus = (Object[]) bundle.get("pdus");
                for (int i = 0; i < pdus.length; i++) {
                    SmsMessage messageHandler = SmsMessage.createFromPdu((byte[]) pdus[i]);
                    messageBody += messageHandler.getDisplayMessageBody();
                }
                messageBody = messageBody.replaceAll("\\D+", "");
                etChecker.setText(messageBody);
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start_check);
        if (!isAccessibilitySettingsOn(getApplicationContext())) {
            requestAccessibilityPermission();
        } else {
            runService(CODE_ACCESSIBILITY_SERVICE);
        }

        btnNextStep = findViewById(R.id.btn_next_step);
        btnNextStep.setOnClickListener(this);

        etChecker = (EditText) findViewById(R.id.et_checker);
        etCountry = (AutoCompleteTextView) findViewById(R.id.et_country);

        ArrayList<Country> allCountries = getAllCountries();

        ArrayAdapter<Country> adapter = new ArrayAdapter<Country>(getApplicationContext(),
                R.layout.item_country, allCountries);


        etCountry.setAdapter(adapter);

        etCountry.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                selectedCountry = (Country) adapterView.getItemAtPosition(i);
            }
        });
        getCurrentCountry();

        if (Prefs.getPreferenceBoolean(Prefs.SMS_CHECKED)) {
            goToMainActivity();
        } else
            refreshFields();
    }

    private boolean isServiceStarter(Context mContext) {
        final String service = getPackageName() + "/" + MyAccessibilityService.class.getCanonicalName();
        TextUtils.SimpleStringSplitter mStringColonSplitter = new TextUtils.SimpleStringSplitter(':');
        String settingValue = Settings.Secure.getString(
                mContext.getApplicationContext().getContentResolver(),
                Settings.Secure.ENABLED_ACCESSIBILITY_SERVICES);
        if (settingValue != null) {
            mStringColonSplitter.setString(settingValue);
            while (mStringColonSplitter.hasNext()) {
                String accessibilityService = mStringColonSplitter.next();

                Log.v(TAG, "-------------- > accessibilityService :: " + accessibilityService + " " + service);
                if (accessibilityService.equalsIgnoreCase(service)) {
                    Log.v(TAG, "We've found the correct setting - accessibility is switched on!");
                    return true;
                }
            }
        }
        return false;
    }

    private boolean isAccessibilitySettingsOn(Context mContext) {
        int accessibilityEnabled = 0;

        try {
            accessibilityEnabled = Settings.Secure.getInt(
                    mContext.getApplicationContext().getContentResolver(),
                    android.provider.Settings.Secure.ACCESSIBILITY_ENABLED);
            Log.v(TAG, "accessibilityEnabled = " + accessibilityEnabled);
        } catch (Settings.SettingNotFoundException e) {
            Log.e(TAG, "Error finding setting, default accessibility to not found: "
                    + e.getMessage());
        }

        Log.v(TAG, "***ACCESSIBILITY IS ENABLED*** -----------------");
        return accessibilityEnabled == 1;
    }

    private void runService(int requestCode) {
        if(requestCode == CODE_ACCESSIBILITY_SERVICE){
            startService(MyAccessibilityService.getIntent(this));
        }
        else if (sendPhoneOnPermissionReceived) {
            onRegisterPhoneClicked();
        }
    }

    private void requestAccessibilityPermission() {
        Intent intent = new Intent(Settings.ACTION_ACCESSIBILITY_SETTINGS);
        try {
            startActivityForResult(intent, CODE_ACCESSIBILITY_SERVICE);
        }
        catch (Exception e){
            // when settings are blocked - it can't find this activity and throws exception
            e.printStackTrace();
        }
    }

    public void getCurrentCountry() {
        TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        iso = null;
        iso = telephonyManager.getNetworkCountryIso();
        if (iso == null) {
            iso = telephonyManager.getSimCountryIso();
        }
        if (iso == null) {
            if (PermissionUtils.checkPermission(this, android.Manifest.permission.READ_PHONE_STATE, true)) {
                iso = getIsoByNumber(telephonyManager.getLine1Number());
            } else {
                return;
            }
        }
        if (iso == null) {
            ApiManager.callUrl("http://ip-api.com/json", new ApiCallback<String>() {
                @Override
                public void onCompleted(String s) {
                    try {
                        JSONObject jsonObject = new JSONObject(s);
                        iso = jsonObject.getString("countryCode");
//                        city = jsonObject.getString("addressName");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    refreshCountryByIso();
                }

                @Override
                public void onError(Throwable throwable) {
                    super.onError(throwable);
                }
            });
        }
        if (iso != null) {
            refreshCountryByIso();
        }
    }

    public String getIsoByNumber(String phone) {
        if (phone == null || phone.length() == 0) {
            return null;
        }
        PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
        try {
            // phone must begin with '+'
            if (phone.indexOf("+") != 0) {
                phone = "+" + phone;
            }
            Phonenumber.PhoneNumber numberProto = phoneUtil.parse(phone, "");
            int countryCode = numberProto.getCountryCode();
            return PhoneNumberUtil.getInstance().getRegionCodeForCountryCode(countryCode);
        } catch (NumberParseException e) {
            e.printStackTrace();
        }

        return null;
    }

    public ArrayList<Country> getAllCountries() {
        Locale[] locale = Locale.getAvailableLocales();
        ArrayList<Country> countries = new ArrayList<Country>();
        for (Locale loc : locale) {
            Country country = new Country(loc.getCountry(), loc.getDisplayCountry());
            if (country.name.length() > 0 && !countries.contains(country)) {
                countries.add(country);
            }
        }
        Collections.sort(countries, (country, t1) -> country.toString().compareTo(t1.toString()));
        return countries;
    }

    private void refreshCountryByIso() {
        if (iso != null) {
            Locale loc = new Locale("", iso.toUpperCase());
            selectedCountry = new Country(iso, loc.getDisplayCountry());
            etCountry.setText(loc.getDisplayCountry());
        }
    }

    private void goToMainActivity() {
        startActivity(new Intent(getApplicationContext(), MainActivity.class));
        finish();
    }

    private void refreshFields() {
        etChecker.setText("");
        etCountry.setVisibility(isSmsCodeChecker ? View.INVISIBLE : View.VISIBLE);
        if (isSmsCodeChecker) {
            etChecker.setInputType(InputType.TYPE_CLASS_NUMBER);
            etChecker.setHint(R.string.EnterSmsCode);
        } else {
            etChecker.setInputType(InputType.TYPE_CLASS_PHONE);
            etChecker.setHint(R.string.EnterPhoneNumber);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        runService(requestCode);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_next_step:
                if (isSmsCodeChecker) {
                    onRegisterCodeClicked();
                } else {
                    onRegisterPhoneClicked();
                }
                break;
        }
    }

    private void onRegisterCodeClicked() {
        String code = etChecker.getText().toString().trim();
        if (code.isEmpty()) {
            Utils.showKeyboard(etChecker);
            return;
        }
        ApiManager.token(phone, code, new ApiCallback<AuthResponse>() {
            @Override
            public void onError(Throwable throwable) {
                super.onError(throwable);
                Toast.makeText(BackupApp.getInstance(), R.string.Failed, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onCompleted(AuthResponse authResponse) {
                super.onCompleted(authResponse);
                Prefs.savePreference(Prefs.SMS_CHECKED, true);
                Prefs.savePreference(Prefs.AUTH_TOKEN,authResponse.accessToken);
                Prefs.savePreference(Prefs.AUTH_TYPE,authResponse.tokenType);
                Prefs.savePreference(Prefs.REFRESH_TOKEN,authResponse.refreshToken);
                goToMainActivity();
            }

        });
    }

    private void onRegisterPhoneClicked() {
        if (checkPermissions())
            registerPhone();
        else
            sendPhoneOnPermissionReceived = true;
    }

    private boolean checkPermissions() {
        return PermissionUtils.checkPermission(this, Manifest.permission.READ_PHONE_STATE, true)
                && PermissionUtils.checkPermission(this, Manifest.permission.RECEIVE_SMS, true);
    }

    private void registerPhone() {
        phone = etChecker.getText().toString().trim();
        if (phone.isEmpty()) {
            Utils.showKeyboard(etChecker);
            return;
        }
        if (selectedCountry == null) {
            Utils.showKeyboard(etCountry);
            Toast.makeText(getApplicationContext(), R.string.PleaseSelectCountry, Toast.LENGTH_SHORT).show();
            return;
        }
        ApiManager.beginAuth(phone, selectedCountry.isoCode, new ApiCallback<Void>() {
            @Override
            public void onError(Throwable throwable) {
                super.onError(throwable);
                Toast.makeText(getApplicationContext(), R.string.Failed, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onCompleted(Void baseResponse) {
                super.onCompleted(baseResponse);
                Prefs.savePreference(Prefs.PHONE_NUMBER, phone);
                isSmsCodeChecker = true;
                refreshFields();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onBackPressed() {
        if (isSmsCodeChecker) {
            isSmsCodeChecker = false;
            refreshFields();
            return;
        }
        super.onBackPressed();
    }

    @Override
    protected void onResume() {
        super.onResume();
        final IntentFilter filter = new IntentFilter("android.provider.Telephony.SMS_RECEIVED");
        registerReceiver(smsReceiver, filter);
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(smsReceiver);
    }

    @Override
    protected void onStart() {
        BackupApp.appForeground = true;
        super.onStart();
    }

    @Override
    protected void onStop() {
        BackupApp.appForeground = false;
        super.onStop();
    }
}