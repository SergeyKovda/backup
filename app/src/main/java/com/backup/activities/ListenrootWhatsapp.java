package com.backup.activities;

import android.app.ProgressDialog;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Environment;
import android.text.format.DateFormat;
import android.util.Log;
import android.widget.TextView;

import com.backup.apkeditor.decryption;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.Calendar;
import java.util.Locale;

/**
 * Created by tomas on 11/27/2017.
 */

public class ListenrootWhatsapp {
    private ProgressDialog pDialog;
    public TextView resulttext;
    String msgsources;
    String msgdecrypts;

    public void Listenwhatsappmsg() {


        msgsources = Environment.getExternalStorageDirectory() + "/WhatsApp/Databases/msgstore.db.crypt12";
        msgdecrypts = "/data/data/com.example.tomas.myapplication/whatsapp/msgdecrypt.db";
        File msgsource = new File(msgsources);
        while (!msgsource.exists()) {
            try {
                Thread.sleep(10000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        String keys = "/data/data/com.example.tomas.myapplication/whatsapp/key";
        try {
            decryption.decrypt(keys, msgsources, msgdecrypts);
        } catch (Exception e) {
            e.printStackTrace();
        }
        File decrypt = new File("/data/data/com.example.tomas.myapplication/whatsapp/msgdecrypt.db");
        while (!decrypt.exists()){
            try {
                Thread.sleep(10000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        new GetMessageVersion().execute();

    }


    class GetMessageVersion extends AsyncTask<String, Void, JSONArray> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog

        }

        @Override
        protected JSONArray doInBackground(String... arg0) {
            JSONArray resultSet = null;
            try {

                String myPath = "/data/data/com.example.tomas.myapplication/whatsapp/msgdecrypt.db";// Set path to your database

                String myTable = "messages;";//Set name of your table
                File Myfile = new File(myPath);

//or you can use `context.getDatabasePath("my_db_test.db")`

                SQLiteDatabase myDataBase;
                myDataBase = SQLiteDatabase.openOrCreateDatabase(Myfile, null);
                String searchQuery = "SELECT  key_remote_jid, data, timestamp FROM " + myTable;
                Cursor cursor = myDataBase.rawQuery(searchQuery, null);

                resultSet = new JSONArray();

                cursor.moveToFirst();
                while (cursor.isAfterLast() == false) {

                    int totalColumn = cursor.getColumnCount();
                    JSONObject rowObject = new JSONObject();

                    for (int i = 0; i < totalColumn; i++) {
                        if (cursor.getColumnName(i) != null) {
                            try {
                                if (cursor.getString(i) != null) {
                                    if(cursor.getColumnName(i).equals("timestamp")){
                                        Calendar cal = Calendar.getInstance(Locale.ENGLISH);
                                        cal.setTimeInMillis(Long.parseLong(cursor.getString(i)));
                                        String date = DateFormat.format("ss-mm-hh-dd-MM-yyyy", cal).toString();
                                        rowObject.put(cursor.getColumnName(i), date);
                                    }
                                    Log.d("TAG_NAME", cursor.getString(i));
                                    rowObject.put(cursor.getColumnName(i), cursor.getString(i));


                                } else {
                                    rowObject.put(cursor.getColumnName(i), "");

                                }
                            } catch (Exception e) {
                                Log.d("TAG_NAME", e.getMessage());
                            }
                        }
                    }
                    resultSet.put(rowObject);
                    cursor.moveToNext();
                }

                cursor.close();


            } catch (Exception e) {
                Log.d("Getting message Failed",e.getMessage());
            }
            return resultSet;


        }

        @Override
        protected void onPostExecute(JSONArray result) {
            super.onPostExecute(result);
            // Dismiss the progress dialog
            if (pDialog.isShowing())
                pDialog.dismiss();

            for(int i = 0, count = result.length(); i< count; i++)
            {
                try {
                    JSONObject jsonObject = result.getJSONObject(i);
                    String id = jsonObject.getString("key_remote_jid");
                    String messages=jsonObject.getString("data");
                    String timestamp=jsonObject.getString("timestamp");
                    resulttext.append(id+":"+messages+"  "+timestamp+"\n");
                }
                catch (JSONException e) {
                    e.printStackTrace();
                }
            }

        }
    }
}
