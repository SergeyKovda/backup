package com.backup.utils;


import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.backup.BackupApp;

public class NetworkUtils {

    public static boolean isWifi() {
        ConnectivityManager connManager = (ConnectivityManager) BackupApp.getInstance().
                getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo mWifi = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        if (mWifi != null && mWifi.isConnected()) {
            return true;
        }
        return false;
    }

}
