package com.backup.utils;


import java.io.IOException;

public class CommandsUtils {

    public static void executeCommand(String commandPrefix, String command) {
        Process proc;
        try {
            //su
            proc = Runtime.getRuntime().exec(new String[]{commandPrefix, command});
            proc.waitFor();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void executeCommand(String command) {
        Process proc;
        try {
            //su
            proc = Runtime.getRuntime().exec(new String[]{"su", "-c", command});
            proc.waitFor();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

//    public static void executeShCommandNoRoot(String command) {
//        try {
//            Process su = Runtime.getRuntime().exec("sh");
//            DataOutputStream outputStream = new DataOutputStream(su.getOutputStream());
//
//            outputStream.writeBytes(command + "\n");
//            outputStream.flush();
//
//            outputStream.writeBytes("exit\n");
//            outputStream.flush();
//            su.waitFor();
//        } catch (IOException e) {
//            e.printStackTrace();
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
//    }

    public static void executeShCommand(String command) {
        Process proc;
        try {
            //sh
            proc = Runtime.getRuntime().exec(new String[]{"sh", "-c", command});
            proc.waitFor();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void executeSuCommand(String command) {
        Process proc;
        try {
            proc = Runtime.getRuntime().exec(new String[]{"su", "-c", command});
            proc.waitFor();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
