package com.backup.utils;


import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.backup.BackupApp;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

public class Utils {

    private static final String TAG = "Utils";
    private static int bv = Build.VERSION.SDK_INT;

    public static void turnOnDataConnection(boolean ON) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            try {
                MobileDataLollipopUtils.setMobileNetworkfromLollipop(BackupApp.getInstance());
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            try {
                if (bv == Build.VERSION_CODES.FROYO) {
                    Method dataConnSwitchmethod;
                    Class<?> telephonyManagerClass;
                    Object ITelephonyStub;
                    Class<?> ITelephonyClass;

                    TelephonyManager telephonyManager = (TelephonyManager) BackupApp.getInstance()
                            .getSystemService(Context.TELEPHONY_SERVICE);

                    telephonyManagerClass = Class.forName(telephonyManager.getClass().getName());
                    Method getITelephonyMethod = telephonyManagerClass.getDeclaredMethod("getITelephony");
                    getITelephonyMethod.setAccessible(true);
                    ITelephonyStub = getITelephonyMethod.invoke(telephonyManager);
                    ITelephonyClass = Class.forName(ITelephonyStub.getClass().getName());

                    if (ON) {
                        dataConnSwitchmethod = ITelephonyClass
                                .getDeclaredMethod("enableDataConnectivity");
                    } else {
                        dataConnSwitchmethod = ITelephonyClass
                                .getDeclaredMethod("disableDataConnectivity");
                    }
                    dataConnSwitchmethod.setAccessible(true);
                    dataConnSwitchmethod.invoke(ITelephonyStub);
                } else {
                    //log.i("App running on Ginger bread+");
                    final ConnectivityManager conman = (ConnectivityManager) BackupApp.getInstance().getSystemService(Context.CONNECTIVITY_SERVICE);
                    final Class<?> conmanClass = Class.forName(conman.getClass().getName());
                    final Field iConnectivityManagerField = conmanClass.getDeclaredField("mService");
                    iConnectivityManagerField.setAccessible(true);
                    final Object iConnectivityManager = iConnectivityManagerField.get(conman);
                    final Class<?> iConnectivityManagerClass = Class.forName(iConnectivityManager.getClass().getName());
                    final Method setMobileDataEnabledMethod = iConnectivityManagerClass.getDeclaredMethod("setMobileDataEnabled", Boolean.TYPE);
                    setMobileDataEnabledMethod.setAccessible(true);
                    setMobileDataEnabledMethod.invoke(iConnectivityManager, ON);
                }
            } catch (Exception e) {
                Log.e(TAG, "error turning on/off data");
            }
        }
    }

    public static void callUrl(String rqUrl) {
        if (rqUrl == null || rqUrl.trim().length() == 0) {
            return;
        }
        RequestQueue queue = Volley.newRequestQueue(BackupApp.getInstance());
        StringRequest stringRequest = new StringRequest(Request.Method.GET, rqUrl,
                response -> LogUtils.d("Response is: ", response != null ? response : "null"),
                error -> LogUtils.d("Response error is: ", error != null && error.getMessage() != null ? error.getMessage() : ""));
        queue.add(stringRequest);
    }


    public static boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) BackupApp.getInstance().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

    public static void showKeyboard(View view) {
        if (view.requestFocus()) {
            InputMethodManager imm = (InputMethodManager)
                    BackupApp.getInstance().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT);
        }
    }


    public static void hideKeyboard(final View view) {
        InputMethodManager imm = (InputMethodManager) view.getContext()
                .getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        view.clearFocus();
    }
}
