package com.backup.utils;


import android.util.Log;

import com.backup.BackupApp;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class LogUtils {

    private static SimpleDateFormat notificationDateFormat = new SimpleDateFormat("MMdd:HH:mm:ss");

    public static void d(String tag, String text) {
        Log.d(tag, text);
        File logFile = getLogFile();
        if (!logFile.exists()) {
            try {
                logFile.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        try {
            //BufferedWriter for performance, true to set append to file flag
            BufferedWriter buf = new BufferedWriter(new FileWriter(logFile, true));
            buf.append(notificationDateFormat.format(new Date()) + " | " + tag + ": " + text);
            buf.newLine();
            buf.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static File getLogFile() {
        File dir = new File(BackupApp.getInstance().getCacheDir() + "/Backup");
        if (!dir.exists()) {
            dir.mkdir();
        }
        return new File(dir, "log.txt");
    }

}
