package com.backup.utils;

import com.backup.Prefs;
import com.backup.extra.Package;
import com.backup.extra.PackageInfo;
import com.backup.extra.Packages;

public class PackageUtils {

    public static boolean isFeatureAvailable(PackageInfo.Feature feature) {
        String currentPaymentType = Prefs.getCurrentPaymentType();
        if (Package.PACKAGE_PREMIUM.equals(currentPaymentType)) {
            return isFeatureAvailable(Packages.premiumFeatures, feature);
        } else if (Package.PACKAGE_PLUS.equals(currentPaymentType)) {
            return isFeatureAvailable(Packages.plusFeatures, feature);
        } else if (Package.PACKAGE_BASIC.equals(currentPaymentType)) {
            return isFeatureAvailable(Packages.basicFeatures, feature);
        }
        return false;
    }

    private static boolean isFeatureAvailable(PackageInfo.Feature[] features, PackageInfo.Feature feature) {
        for (PackageInfo.Feature feature1 : features)
            if (feature1 == feature) return true;
        return false;
    }

}
