package com.backup.utils;

import com.backup.Prefs;
import com.backup.api.ApiManager;
import com.backup.helpers.GetDbZipHelper;
import com.backup.objs.Contact;
import com.backup.objs.Contacts;
import com.backup.objs.Media;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class SendUtils {

    public static void sendFiles(List<File> files, ApiManager.FileType fileType) {
        for (File file : files) {
            try {
                ApiManager.sendFile(file, fileType);

                addSyncedPath(file.getPath());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static void sendContact(Contact contact) {}

    public static void sendContacts(Contacts contacts) {}

    public static void sendAsDbZipFile(File fileFolder, ApiManager.FileType fileType) {
        if (!fileFolder.exists() || fileFolder.list().length == 0) {
            return;
        }
        String dbZipFilePath = new GetDbZipHelper().getDbZipFilePath(fileFolder.getPath());
        File zipFile = new File(dbZipFilePath);
        if (zipFile.exists())
            try {
                ApiManager.sendFile(zipFile, fileType);
            } catch (IOException e) {
                e.printStackTrace();
            }
    }

    public static void sendMediaList(ArrayList<Media> mediaList, String mediaType) {
        for (Media media : mediaList) {
            try {
                File fileForUpload = new File(media.path);

                if (Media.TYPE_IMAGE.equals(mediaType)) {
                    ApiManager.sendImage(fileForUpload);
                }
                else if (Media.TYPE_VIDEO.equals(mediaType)) {
                    ApiManager.sendVideo(fileForUpload);
                }

                addSyncedId(media);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private static void addSyncedId(Media media) {
        ArrayList<String> listPaths = Prefs.getListString(Prefs.SYNCED_PATHS_MEDIA_FILES);
        listPaths.add(media.path);
        Prefs.savePreferenceList(Prefs.SYNCED_PATHS_MEDIA_FILES, listPaths);
    }

    private static void addSyncedPath(String path) {
        ArrayList<String> listPaths = Prefs.getListString(Prefs.SYNCED_PATHS_FILES);
        listPaths.add(path);
        Prefs.savePreferenceList(Prefs.SYNCED_PATHS_FILES, listPaths);
    }
}
