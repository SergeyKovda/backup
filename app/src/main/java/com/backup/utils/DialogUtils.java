package com.backup.utils;


import android.app.Activity;
import android.app.Dialog;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import com.backup.R;

public class DialogUtils {

//    public static void showTermsOfUseDialog(Activity activity) {
//        final TermsDialog dialog = new TermsDialog(activity);
//        dialog.show();
//
//        dialog.findViewById(R.id.btn_ok).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                dialog.dismiss();
//            }
//        });
//
//        TextView tvText = (TextView) dialog.findViewById(R.id.text);
//
//        BufferedReader reader = null;
//        StringBuilder text = new StringBuilder();
//
//        try {
//            reader = new BufferedReader(
//
//            String mLine;
//            while ((mLine = reader.readLine()) != null) {
//                text.append(mLine);
//                text.append('\n');
//            }
//        } catch (IOException e) {
//            e.printStackTrace();
//        } finally {
//            if (reader != null) {
//                try {
//                    reader.close();
//                } catch (IOException e) {
//                    //log the exception
//                }
//            }
//
//            tvText.setText((CharSequence) text);
//
//        }
//    }


    public static void showRetry(final Activity activity, String text, final View.OnClickListener onRetryClickListener) {
        showRetry(activity, text, null, onRetryClickListener);
    }

    public static void showRetry(final Activity activity, String text, String btnText, final View.OnClickListener onRetryClickListener) {
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_error);
        dialog.setCancelable(false);
        dialog.show();

        View btnCancel = dialog.findViewById(R.id.btn_cancel);
        View btnOk = dialog.findViewById(R.id.btn_ok);
        btnCancel.setVisibility(View.GONE);
        btnOk.setVisibility(View.GONE);

        TextView btnRetry = (TextView) dialog.findViewById(R.id.btn_retry);
        if (btnText != null)
            btnRetry.setText(btnText);
        TextView tvQuestion = (TextView) dialog.findViewById(R.id.text);
        tvQuestion.setText(text);

        btnRetry.setOnClickListener(v -> {
            dialog.dismiss();
            if (onRetryClickListener != null) {
                onRetryClickListener.onClick(v);
            }
        });
    }


}