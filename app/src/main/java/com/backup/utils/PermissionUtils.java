package com.backup.utils;

import android.support.v4.app.ActivityCompat;
import android.support.v4.content.PermissionChecker;
import android.support.v7.app.AppCompatActivity;

import com.backup.BackupApp;

public class PermissionUtils {

    public static final int MY_PERMISSIONS_REQUEST = 1211;

    public static boolean checkPermission(AppCompatActivity activity, String permission, boolean request) {
        LogUtils.d("checkPermission", permission);
        int permissionCheck = android.support.v4.content.PermissionChecker.checkSelfPermission(
                activity != null ? activity : BackupApp.getInstance(), permission);
        if (PermissionChecker.PERMISSION_GRANTED == permissionCheck) {
            LogUtils.d("checkPermission", "" + true);
            return true;
        } else if (request && (PermissionChecker.PERMISSION_DENIED == permissionCheck ||
                PermissionChecker.PERMISSION_DENIED_APP_OP == permissionCheck)) {
            ActivityCompat.requestPermissions(activity,
                    new String[]{permission},
                    MY_PERMISSIONS_REQUEST);
            LogUtils.d("checkPermission", "" + false);
            return false;
        }
        LogUtils.d("checkPermission", "" + false);
        return false;
    }

}
