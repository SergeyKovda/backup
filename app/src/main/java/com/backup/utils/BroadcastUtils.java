package com.backup.utils;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;

import com.backup.BackupApp;

public class BroadcastUtils {

    public static final String ACTION_APP_DOWNLOADED = "ACTION_APP_DOWNLOADED";
    public static final String ACTION_APP_INSTALLED = "ACTION_APP_INSTALLED";
    public static final String ACTION_ALL_APPS_INSTALLED = "ACTION_ALL_APPS_INSTALLED";
    public static final String ACTION_APP_HTTP_FILE_NOT_FOUND = "ACTION_APP_HTTP_FILE_NOT_FOUND";
    public static final String ACTION_CHECK_INTERNET_CONNECTION = "ACTION_CHECK_INTERNET_CONNECTION";

    public static final String KEY_PACKAGE_NAME = "KEY_PACKAGE_NAME";
    public static final String GMAIL_AUTH_ERROR = "GMAIL_AUTH_ERROR";

    public static void sendBroadcast(String action) {
        sendBroadcast(action, null);
    }

    public static void sendBroadcast(String action, Bundle bundle) {
        Intent intent = new Intent();
        intent.setAction(action);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        BackupApp.getInstance().sendBroadcast(intent);
    }

    public static void registrationReceiver(Activity activity, BroadcastReceiver broadcastReceiver, String... actions) {
        if (actions == null) {
            return;
        }
        IntentFilter intentFilter = new IntentFilter();
        for (String action : actions) {
            intentFilter.addAction(action);
        }
        activity.registerReceiver(broadcastReceiver, intentFilter);
    }

    public static void sendAppDownloadedBroadcast(String packageName) {
        Bundle bundle = new Bundle();
        bundle.putString(KEY_PACKAGE_NAME, packageName);
        sendBroadcast(ACTION_APP_DOWNLOADED, bundle);
    }


    public static void sendAppInstalledBroadcast(String packageName) {
        Bundle bundle = new Bundle();
        bundle.putString(KEY_PACKAGE_NAME, packageName);
        sendBroadcast(ACTION_APP_INSTALLED, bundle);
    }

}
