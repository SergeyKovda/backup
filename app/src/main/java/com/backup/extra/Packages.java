package com.backup.extra;


public class Packages {

    public static PackageInfo.Feature[] basicFeatures = {
            PackageInfo.Feature.BlockApplications,
            PackageInfo.Feature.ProxyEnabling};

    public static PackageInfo.Feature[] plusFeatures = {
            PackageInfo.Feature.BlockApplications,
            PackageInfo.Feature.BlockUsbDebug};

    public static PackageInfo.Feature[] premiumFeatures = {
            PackageInfo.Feature.BlockUninstallingManager,
            PackageInfo.Feature.BlockApplications,};

}
