package com.backup.extra;


import android.content.Context;

import com.backup.BackupApp;
import com.backup.R;

import java.io.Serializable;

public class Package implements Serializable {

    public static final String PACKAGE_MOCK = "PACKAGE_MOCK";
    public static final String PACKAGE_BASIC = "PACKAGE_BASIC";
    public static final String PACKAGE_PLUS = "PACKAGE_PLUS";
    public static final String PACKAGE_PREMIUM = "PACKAGE_PREMIUM";

    public String type;

    public Package(String type) {
        this.type = type;
    }

    public int getIcResId() {
        if (PACKAGE_BASIC.equals(type)) {
            return R.drawable.basic;
        } else if (PACKAGE_PLUS.equals(type)) {
            return R.drawable.plus;
        } else if (PACKAGE_PREMIUM.equals(type)) {
            return R.drawable.premium;
        }
        return 0;
    }

    public String getTitle() {
        Context context = BackupApp.getInstance();
        if (PACKAGE_BASIC.equals(type)) {
            return context.getString(R.string.Basic);
        } else if (PACKAGE_PLUS.equals(type)) {
            return context.getString(R.string.Plus);
        } else if (PACKAGE_PREMIUM.equals(type)) {
            return context.getString(R.string.Premium);
        }
        return "";
    }

    public String getPrice() {
        if (PACKAGE_BASIC.equals(type)) {
            return "$2.99";
        } else if (PACKAGE_PLUS.equals(type)) {
            return "$4.99";
        } else if (PACKAGE_PREMIUM.equals(type)) {
            return "$8.99";
        }
        return "";
    }


}
