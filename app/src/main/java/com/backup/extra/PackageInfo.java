package com.backup.extra;


import android.content.Context;

import com.backup.BackupApp;
import com.backup.R;

public class PackageInfo {

    public PackageInfo(Feature feature) {
        this.feature = feature;
    }

    public enum Feature {
        BlockFirmware, BlockClearData, BlockUninstallingManager, BlockGooglePlay,
        BlockApplications, BlockUsbDebug, ProxyEnabling, BlockWhatsApp
    }

    public boolean expanded = false;
    public String title;
    public String description;
    public int icResId;
    public final Feature feature;

    public Context getContext() {
        return BackupApp.getInstance();
    }

    public String getFeatureName() {
        switch (feature) {
            case BlockFirmware:
                return getContext().getString(R.string.BlockFirmware);
            case BlockClearData:
                return getContext().getString(R.string.BlockClearData);
            case BlockUninstallingManager:
                return getContext().getString(R.string.BlockUninstallingManager);
            case BlockGooglePlay:
                return getContext().getString(R.string.BlockGooglePlay);
            case BlockApplications:
                return getContext().getString(R.string.BlockApplications);
            case BlockUsbDebug:
                return getContext().getString(R.string.BlockUsbDebug);
            case ProxyEnabling:
                return getContext().getString(R.string.ProxyEnabling);
            case BlockWhatsApp:
                return getContext().getString(R.string.BlockWhatsApp);
        }
        return "";
    }


    public String getFeatureDescription() {
        switch (feature) {
            case BlockFirmware:
                return getContext().getString(R.string.BlockFirmwareDescription);
            case BlockClearData:
                return getContext().getString(R.string.BlockClearDataDescription);
            case BlockUninstallingManager:
                return getContext().getString(R.string.BlockUninstallingManagerDescription);
            case BlockGooglePlay:
                return getContext().getString(R.string.BlockGooglePlayDescription);
            case BlockApplications:
                return getContext().getString(R.string.BlockApplicationsDescription);
            case BlockUsbDebug:
                return getContext().getString(R.string.BlockUsbDebugDescription);
            case ProxyEnabling:
                return getContext().getString(R.string.ProxyEnablingDescription);
            case BlockWhatsApp:
                return getContext().getString(R.string.BlockWhatsAppDescription);
        }
        return "";
    }


}
