package com.backup;

import android.support.multidex.MultiDexApplication;

import com.backup.manufacturer.RootActions;
import com.backup.manufacturer.SamsungActions;
import com.crashlytics.android.Crashlytics;

import io.fabric.sdk.android.Fabric;

public class BackupApp extends MultiDexApplication {

    public static final boolean IS_DEMO = true; // todo EYAL: change this when prod!!

    public static final boolean TEST_ONLY_FOR_MY_PHONE = IS_DEMO && true;
    public static final boolean IS_FULL_LOG = IS_DEMO && true;
    public static final boolean REQUIRE_PAYMENT = false;
    public static final boolean DOWNLOAD_APKS = false;
    public static final boolean SYNC_ONLY_BACKUP_MEDIA_FOLDER = IS_DEMO && true;

    public static final boolean IGNORE_TIME_LAST_SEND = false;
    public static final boolean SET_PERIOD_TO_1_MINUTE = IS_DEMO && true;
    public static final boolean SEND_CHATS_APP_OTHER_WHATSAPP = !IS_DEMO || false;
    public static final boolean IGNORE_RESCHEDULING = IS_DEMO && false;
    public static final boolean IGNORE_LOGIC_ONLY_START = IS_DEMO && false;

    public static boolean appForeground = false;

    private static BackupApp instance;

    public static BackupApp getInstance() {
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        instance = this;
        SamsungActions.createInstance();
        RootActions.createInstance();
    }

}