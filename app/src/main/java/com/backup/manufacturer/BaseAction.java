package com.backup.manufacturer;


import android.content.ComponentName;
import android.content.Intent;

import com.backup.activities.MainActivity;

public interface BaseAction {

    boolean onStartApp(MainActivity activity);

    void onActivityResult(int requestCode, int resultCode, Intent data);

    void onResume();

    void setAppCanBeUninstalled(String packageName, boolean state);

    boolean isAppCanBeUninstalled(String packageName);

    void enableApp(String packageName, boolean enable);

    boolean isAppEnabled(String packageName);

    boolean isApplicationInstalled(String packageName);

    void installApplication(String apkFilePath);

    void uninstallApplication(String packageName);

    public void turnProxyOn();

    public void turnProxyOff();

    public boolean isProxyOn();

    public void removePermission(String permission, String packageName);

    void enableComponent(ComponentName componentName, boolean enable);

    boolean makeAction(Runnable runnable);

    void updateBlockedUrls();
}
