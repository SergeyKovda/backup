package com.backup.manufacturer;


public interface IAdminResult {
    
    public void onAdminModeReceived(boolean alreadyWas);

    public void onAdminModeCanceled();
}
