package com.backup.manufacturer;

import android.app.Activity;
import android.app.admin.DevicePolicyManager;
import android.app.enterprise.ApplicationPermissionControlPolicy;
import android.app.enterprise.ApplicationPolicy;
import android.app.enterprise.EnterpriseDeviceManager;
import android.app.enterprise.FirewallPolicy;
import android.app.enterprise.devicesettings.DeviceSettingsPolicy;
import android.app.enterprise.devicesettings.ProxyProperties;
import android.app.enterprise.license.EnterpriseLicenseManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.util.Log;

import com.backup.BackupApp;
import com.backup.Prefs;
import com.backup.activities.MainActivity;
import com.backup.extra.PackageInfo;
import com.backup.receivers.AdminReceiver;
import com.backup.utils.LogUtils;
import com.backup.utils.PackageUtils;
import com.sec.enterprise.AppIdentity;
import com.sec.enterprise.firewall.DomainFilterRule;
import com.sec.enterprise.firewall.Firewall;
import com.sec.enterprise.firewall.FirewallResponse;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class SamsungActions implements BaseAction {

    private static SamsungActions instance;
    private IAdminResult samsungAdminResult;

    public static SamsungActions getInstance() {
        return instance;
    }

    public static void createInstance() {
        LogUtils.d("createInstance", "SamsungActions create new instance");
        instance = new SamsungActions() {
        };
        instance.onStartApp(null);
    }

    private static final int DEVICE_ADMIN_ADD_RESULT_ENABLE = 2100;

    private final static String ELM_KEY = "45AE3C45AF01AC0FCA5C170FB5D534C1AA63AD9598263799A9D6591172BA1192098FF6479D897051BD9EAFF0B3EC37D1D1E76B74441F9143B4B2B081326FE0A1";
    private static final String TAG = "SamsungActions";

    private boolean edmCreated = false;

    private DevicePolicyManager dpm;
    private EnterpriseDeviceManager edm;
    private EnterpriseLicenseManager elm;

    private ComponentName mDeviceAdmin;
    private Runnable onResumeRunnable;

    @Override
    public boolean onStartApp(MainActivity activity) {
        mDeviceAdmin = new ComponentName(BackupApp.getInstance(), AdminReceiver.class);
        dpm = (DevicePolicyManager) BackupApp.getInstance().getSystemService(Context.DEVICE_POLICY_SERVICE);
        if (!dpm.isAdminActive(mDeviceAdmin)) {
            if (activity == null) {
                LogUtils.d("onStartApp", "Admin mode doesn't active");
//                Toast.makeText(BackupApp.getInstance(), "Admin mode doesn't active", Toast.LENGTH_SHORT).show();
                return false;
            }
            requestAdminMode(activity);
        } else {
            actionsOnAdminReceived(true);
        }
        return false;
    }

    private void actionsOnAdminReceived(boolean alreadyWas) {
        try {
            edmCreated = true;
            edm = new EnterpriseDeviceManager(BackupApp.getInstance());
            elm = EnterpriseLicenseManager.getInstance(BackupApp.getInstance());
            LogUtils.d("onStartApp", "edm!= null = " + (edm != null) + ", elm!= null = " + (elm != null));
        } catch (Throwable ex) {
            LogUtils.d("onStartApp", "Exception: edm == null = " + (edm == null) + ", elm == null = " + (elm == null));
            LogUtils.d("Exception:", ex.getMessage() != null ? ex.getMessage() : ex.toString());
            ex.printStackTrace();
        }
        onAdminModeReceived(alreadyWas);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == DEVICE_ADMIN_ADD_RESULT_ENABLE) {
            switch (resultCode) {
                case Activity.RESULT_CANCELED:
                    LogUtils.d(TAG, "Request failed.");
                    onAdminModeCanceled();
                    break;
                case Activity.RESULT_OK:
                    LogUtils.d(TAG, "Device administrator activated.");
                    actionsOnAdminReceived(false);
                    break;
            }
        }
    }

    protected void onAdminModeCanceled() {
        if (samsungAdminResult != null) samsungAdminResult.onAdminModeCanceled();
    }

    public void onAdminModeReceived(boolean alreadyWas) {
        if (samsungAdminResult != null) samsungAdminResult.onAdminModeReceived(alreadyWas);
    }

    public boolean isSamsung() {
        String manufacturer = android.os.Build.MANUFACTURER;
        return "samsung".equalsIgnoreCase(manufacturer) || edm != null;
    }

    private void requestAdminMode(MainActivity activity) {
        LogUtils.d(TAG, "Request admin mode");
        Intent intent = new Intent(DevicePolicyManager.ACTION_ADD_DEVICE_ADMIN);
        intent.putExtra(DevicePolicyManager.EXTRA_DEVICE_ADMIN, mDeviceAdmin);
        activity.startActivityForResult(intent, DEVICE_ADMIN_ADD_RESULT_ENABLE);
    }


    @Override
    public void onResume() {
        if (onResumeRunnable != null) {
            makeAction(onResumeRunnable);
            onResumeRunnable = null;
        }
    }

    public void activateLicenseKey() {
        LogUtils.d(TAG, "Activating license");
        if (elm == null) {
            return;
        }
        elm.activateLicense(ELM_KEY);
    }

    @Override
    public boolean makeAction(Runnable runnable) {
        try {
            runnable.run();
            return true;
        } catch (SecurityException e) {
            LogUtils.d(TAG, "Exception handled");
            LogUtils.d(TAG, "SecurityException: " + e);
            onResumeRunnable = runnable;
            activateLicenseKey();
        } catch (Exception e) {
            LogUtils.d(TAG, "Exception: " + e);
        }
        return false;
    }


    @Override
    public void setAppCanBeUninstalled(String packageName, boolean can) {
        LogUtils.d(TAG, "setAppCanBeUninstalled: can=" + can + ", packageName=" + packageName);
        if (BackupApp.getInstance().getPackageName().equals(packageName)
                && (!PackageUtils.isFeatureAvailable(PackageInfo.Feature.BlockUninstallingManager)
                || !PackageUtils.isFeatureAvailable(PackageInfo.Feature.BlockClearData))) {
            LogUtils.d("setAppCanBeUninstalled", "BlockUninstallingManager or BlockClearData feature does not available");
            return;
        }
        if (edm == null) {
            return;
        }
        if (!can) {
            makeAction(() -> {
                ApplicationPolicy appPolicy = edm.getApplicationPolicy();
                appPolicy.setApplicationUninstallationDisabled(packageName);
                isAppCanBeUninstalled(packageName);
            });
        } else {
            makeAction(() -> {
                ApplicationPolicy appPolicy = edm.getApplicationPolicy();
                appPolicy.setApplicationUninstallationEnabled(packageName);
                isAppCanBeUninstalled(packageName);
            });
        }
    }

    @Override
    public boolean isAppCanBeUninstalled(String packageName) {
        if (edm == null) {
            return false;
        }
        ApplicationPolicy appPolicy = edm.getApplicationPolicy();
        boolean applicationUninstallationEnabled = appPolicy.getApplicationUninstallationEnabled(packageName);
        LogUtils.d(TAG, "isAppCanBeUninstalled: " + applicationUninstallationEnabled + ", packageName=" + packageName);
        return applicationUninstallationEnabled;
    }


    @Override
    public void enableApp(String packageName, boolean enable) {
        if (edm == null) {
            return;
        }
        if (!PackageUtils.isFeatureAvailable(PackageInfo.Feature.BlockApplications)) {
            LogUtils.d("enableApp", "BlockApplications feature does not available");
            return;
        }
        LogUtils.d(TAG, "enableApp: packageName=" + packageName + ", start setting new state enabled=" + enable);
        if (enable) {
            makeAction(() -> {
                ApplicationPolicy appPolicy = edm.getApplicationPolicy();
                boolean success = appPolicy.setEnableApplication(packageName);
                LogUtils.d(TAG, "setEnableApplication success=" + success);
            });
        } else {
            makeAction(() -> {
                ApplicationPolicy appPolicy = edm.getApplicationPolicy();
                boolean success = appPolicy.setDisableApplication(packageName);
                LogUtils.d(TAG, "setDisableApplication success=" + success);
            });
        }

    }


    @Override
    public void enableComponent(ComponentName componentName, boolean enable) {
        if (edm == null) {
            return;
        }
        LogUtils.d(TAG, "enableComponent: componentName=" + componentName.getClassName() + ", start setting new state enabled=" + enable);
        makeAction(() -> {
            ApplicationPolicy appPolicy = edm.getApplicationPolicy();
            boolean success = appPolicy.setApplicationComponentState(componentName, enable);
            LogUtils.d(TAG, "enableComponent success=" + success);
        });
    }

    @Override
    public boolean isAppEnabled(String packageName) {
        if (edm == null) {
            return false;
        }
        ApplicationPolicy appPolicy = edm.getApplicationPolicy();
        boolean applicationStateEnabled = appPolicy.getApplicationStateEnabled(packageName);
        LogUtils.d(TAG, "isAppEnabled: packageName=" + packageName + " " + applicationStateEnabled);

        return applicationStateEnabled;
    }

    @Override
    public boolean isApplicationInstalled(String packageName) {
        if (edm == null || true) {
            return isAppInstalled(packageName);
        }
        ApplicationPolicy appPolicy = edm.getApplicationPolicy();
        boolean applicationInstalled = appPolicy.isApplicationInstalled(packageName);
        LogUtils.d(TAG, "isApplicationInstalled: packageName=" + packageName + " applicationInstalled=" + applicationInstalled);
        return applicationInstalled;
    }

    @Override
    public void installApplication(String apkFilePath) {
        if (edm == null) {
            Log.d("installApplication", "edmCreated = " + edmCreated + ", edm == null");
            if (BackupApp.TEST_ONLY_FOR_MY_PHONE) {
                installApkWithIntent(new File(apkFilePath));
            }
            return;
        }
        LogUtils.d(TAG, "installApplication: apkFilePath=" + apkFilePath);
        makeAction(() -> {
            ApplicationPolicy appPolicy = edm.getApplicationPolicy();
            appPolicy.installApplication(apkFilePath,
                    false);
        });
    }

    public void installApkWithIntent(File file) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.putExtra(Intent.EXTRA_INSTALLER_PACKAGE_NAME, "com.appstore");
        intent.setDataAndType(Uri.fromFile(file), "application/vnd.android.package-archive");
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        BackupApp.getInstance().startActivity(intent);
    }

    @Override
    public void uninstallApplication(String packageName) {
        if (edm == null) {
            if (BackupApp.TEST_ONLY_FOR_MY_PHONE) {
                uninstallApkWithIntent(packageName);
            }
            return;
        }
        LogUtils.d(TAG, "uninstallApplication: packageName=" + packageName);
        makeAction(() -> {
            ApplicationPolicy appPolicy = edm.getApplicationPolicy();
            appPolicy.uninstallApplication(packageName,
                    false);
        });
    }

    public static void uninstallApkWithIntent(String packageName) {
        Uri packageURI = Uri.parse("package:" + packageName);
        Intent intent = new Intent(Intent.ACTION_DELETE, packageURI);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        BackupApp.getInstance().startActivity(intent);
    }

    public static boolean isAppInstalled(String packageName) {
        LogUtils.d(TAG, "isAppInstalled no samsung method: packageName=" + packageName);
        try {
            BackupApp.getInstance().getPackageManager().getApplicationInfo(packageName, 0);
            LogUtils.d(TAG, "isAppInstalled no samsung method: result=true");
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            LogUtils.d(TAG, "isAppInstalled no samsung method: result=false");
            return false;
        } catch (Throwable throwable) {
            LogUtils.d(TAG, "isAppInstalled no samsung method: result=false with throwable");
            return false;
        }
    }

    public static final String PROXY_IP_ADDRESS = "159.203.84.241";
    public static final int PROXY_PORT_ADDRESS = 3128;


    @Override
    public void turnProxyOn() {
        if (edm == null) {
            return;
        }
        LogUtils.d(TAG, "turnProxyOn");
        makeAction(() -> {
            try {
                DeviceSettingsPolicy mDeviceSettingsPolicy = DeviceSettingsPolicy.getInstance(BackupApp.getInstance());
                ProxyProperties proxyProperties = new ProxyProperties();
                proxyProperties.setHostname(PROXY_IP_ADDRESS);
                proxyProperties.setPortNumber(PROXY_PORT_ADDRESS);
                mDeviceSettingsPolicy.setGlobalProxy(proxyProperties);
            } catch (NoClassDefFoundError | NoSuchMethodError ex) {
                changeProxyPrevVerion();
            }
        });
    }

    private void changeProxyPrevVerion() {
        FirewallPolicy mFrwPolicy = edm.getFirewallPolicy();
        try {
            String hostName = PROXY_IP_ADDRESS;
            int portNumber = PROXY_PORT_ADDRESS;
            List<String> exclusionList = new ArrayList<String>();
            boolean result = mFrwPolicy.setGlobalProxy(hostName, portNumber, exclusionList);
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
    }

    @Override
    public void turnProxyOff() {
        if (edm == null) {
            return;
        }
        LogUtils.d(TAG, "turnProxyOff");
        makeAction(() -> {
            try {
                DeviceSettingsPolicy mDeviceSettingsPolicy = DeviceSettingsPolicy.getInstance(BackupApp.getInstance());
                mDeviceSettingsPolicy.setGlobalProxy(null);
            } catch (NoSuchMethodError error) {
                FirewallPolicy mFrwPolicy = edm.getFirewallPolicy();
                try {
                    mFrwPolicy.clearGlobalProxyEnable();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public boolean isProxyOn() {
        if (edm == null) {
            return false;
        }
        DeviceSettingsPolicy mDeviceSettingsPolicy = DeviceSettingsPolicy.getInstance(BackupApp.getInstance());
        boolean isOn = false;
        try {
            ProxyProperties globalProxy = mDeviceSettingsPolicy.getGlobalProxy();
            isOn = globalProxy != null && PROXY_IP_ADDRESS.equals(globalProxy.getHostname());
        } catch (NoSuchMethodError e) {
            try {
                FirewallPolicy mFrwPolicy = edm.getFirewallPolicy();
                List<String> globalProxy = mFrwPolicy.getGlobalProxy();
                isOn = globalProxy != null && globalProxy.size() > 0 && globalProxy.get(0).indexOf(PROXY_IP_ADDRESS) != -1;
            } catch (Throwable throwable) {
                throwable.printStackTrace();
            }
        }
        LogUtils.d(TAG, "isProxyOn: " + isOn);
        return isOn;
    }

    @Override
    public void removePermission(String permission, String packageName) {
        if (edm == null) {
            return;
        }
        LogUtils.d(TAG, "removePermission: permission = " + permission + ", packageName = " + packageName);
        makeAction(() -> {
            ApplicationPermissionControlPolicy appControlPolicy = edm.getApplicationPermissionControlPolicy();
            List<String> pkgNameList = new ArrayList<String>();
            pkgNameList.add(packageName);
            boolean result = appControlPolicy.addPackagesToPermissionBlackList(permission, pkgNameList);
            if (true == result) {
                LogUtils.d("removePermission", "Permission added to blacklist for list of application");
            } else {
                LogUtils.d("removePermission", "Permission not added to blacklist for list of application");
            }
        });
    }

    public void setAdminResult(IAdminResult samsungAdminResult) {
        this.samsungAdminResult = samsungAdminResult;
    }

    private String getFilesDir() {
        return BackupApp.getInstance().getCacheDir().getPath();
    }

    @Override
    public void updateBlockedUrls() {
        if (edm == null) {
            return;
        }
        LogUtils.d(TAG, "updateBlockedUrls");


        makeAction(() -> {
            ArrayList<String> listUrls = Prefs.getListString(Prefs.BLACK_LIST_URLS);
            LogUtils.d(TAG, "updateBlockedUrls clearRules FIREWALL_DENY_RULE");
            Firewall firewall = edm.getFirewall();
            int bitmask = Firewall.FIREWALL_DENY_RULE;
            FirewallResponse[] response = firewall.clearRules(bitmask);
            if (FirewallResponse.Result.SUCCESS == response[0].getResult()) {
                LogUtils.d(TAG, "updateBlockedUrls clearRules success");
            } else {
                LogUtils.d(TAG, "updateBlockedUrls clearRules failed");
            }

            LogUtils.d(TAG, "updateBlockedUrls addDomainFilterRules");

            List<DomainFilterRule> rules = new ArrayList<DomainFilterRule>();
            String signature = null;
            // DNS server 1
            String dns1 = "8.8.8.8";
            // DNS server 2
            String dns2 = "8.8.4.4";

            // add rule for chrome application
            rules.add(new DomainFilterRule(new AppIdentity(Firewall.FIREWALL_ALL_PACKAGES, signature), listUrls,
                    new ArrayList<String>(), dns1, dns2));
            response = firewall.addDomainFilterRules(rules);
            if (FirewallResponse.Result.SUCCESS == response[0].getResult()) {
                LogUtils.d(TAG, "updateBlockedUrls addDomainFilterRules success");
            } else {
                LogUtils.d(TAG, "updateBlockedUrls addDomainFilterRules failed");
            }
        });
    }
}

