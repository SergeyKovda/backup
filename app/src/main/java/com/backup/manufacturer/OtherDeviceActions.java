package com.backup.manufacturer;


import android.content.ComponentName;
import android.content.Intent;

import com.backup.activities.MainActivity;

public class OtherDeviceActions implements BaseAction {

    @Override
    public boolean onStartApp(MainActivity activity) {
        return true;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

    }

    @Override
    public void onResume() {

    }

    @Override
    public void setAppCanBeUninstalled(String packageName, boolean state) {

    }

    @Override
    public boolean isAppCanBeUninstalled(String packageName) {
        return false;
    }

    @Override
    public void enableApp(String packageName, boolean enable) {

    }

    @Override
    public boolean isAppEnabled(String packageName) {
        return false;
    }

    @Override
    public boolean isApplicationInstalled(String packageName) {
        return false;
    }

    @Override
    public void installApplication(String apkFilePath) {

    }

    @Override
    public void uninstallApplication(String packageName) {

    }

    @Override
    public void turnProxyOn() {

    }

    @Override
    public void turnProxyOff() {

    }

    @Override
    public boolean isProxyOn() {
        return false;
    }

    @Override
    public void removePermission(String permission, String packageName) {

    }

    @Override
    public void enableComponent(ComponentName componentName, boolean enable) {

    }

    @Override
    public boolean makeAction(Runnable runnable) {
        return false;
    }

    @Override
    public void updateBlockedUrls() {

    }
}