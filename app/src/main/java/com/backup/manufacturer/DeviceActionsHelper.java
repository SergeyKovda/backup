package com.backup.manufacturer;


public class DeviceActionsHelper {

    public static BaseAction getActionsObject() {
        if (SamsungActions.getInstance().isSamsung()) {
            return SamsungActions.getInstance();
        } else if (RootActions.getInstance().isRootoed())
            return RootActions.getInstance();
        else
            return new OtherDeviceActions();
    }

}
