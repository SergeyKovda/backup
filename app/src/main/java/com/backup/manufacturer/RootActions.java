package com.backup.manufacturer;


import android.app.Activity;
import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.widget.Toast;

import com.backup.BackupApp;
import com.backup.Prefs;
import com.backup.R;
import com.backup.activities.MainActivity;
import com.backup.receivers.AdminReceiver;
import com.backup.utils.LogUtils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;

public class RootActions implements BaseAction {

    private static final String TAG = RootActions.class.getSimpleName();
    private static final int DEVICE_ADMIN_ADD_RESULT_ENABLE = 2100;
    private static RootActions instance;
    private static boolean rootReceived;
    private IRootResult iRootResult;
    private Runnable onRootedResumeRunnable;
    private ComponentName mDeviceAdmin;
    private DevicePolicyManager dpm;
    private IAdminResult onAdminResult = null;

    public static RootActions getInstance() {
        return instance;
    }

    public static void createInstance() {
        LogUtils.d("createInstance", "RootActions create new instance");
        instance = new RootActions() {
        };
        instance.onStartApp(null);
    }

    @Override
    public boolean onStartApp(MainActivity activity) {
        try {
            boolean rooted = isRootoed();
            mDeviceAdmin = new ComponentName(BackupApp.getInstance(), AdminReceiver.class);
            dpm = (DevicePolicyManager) BackupApp.getInstance().getSystemService(Context.DEVICE_POLICY_SERVICE);
            if (!dpm.isAdminActive(mDeviceAdmin)) {
                if (activity == null) {
                    LogUtils.d("onStartApp", "Admin mode doesn't active");
//                Toast.makeText(BackupApp.getInstance(), "Admin mode doesn't active", Toast.LENGTH_SHORT).show();
                    return false;
                }
                requestAdminMode(activity);
                return false;
            }
//            if (!rooted && onRootedResumeRunnable == null && activity != null)
//                onRootedResumeRunnable = new Runnable() {
//                    @Override
//                    public void run() {
//                        if (iRootResult != null && isRootoed())
//                            iRootResult.onRootReceivedAndResumed();
//                    }
//                };
            return rooted;
        } catch (Throwable e) {
            e.printStackTrace();
        }
        return false;
    }

    private void requestAdminMode(MainActivity activity) {
        LogUtils.d(TAG, "Request admin mode");
        Intent intent = new Intent(DevicePolicyManager.ACTION_ADD_DEVICE_ADMIN);
        intent.putExtra(DevicePolicyManager.EXTRA_DEVICE_ADMIN, mDeviceAdmin);
        activity.startActivityForResult(intent, DEVICE_ADMIN_ADD_RESULT_ENABLE);
    }

    public boolean isRootoed() {
//        new Thread(new Runnable() {
//            @Override
//            public void run() {
        checkRoot();
        if (!rootReceived)
            Toast.makeText(BackupApp.getInstance(),
                    R.string.DeviceNotSamsungAndNotRoot, Toast.LENGTH_LONG).show();
//            }
//        }).start();
        return rootReceived;
    }

    private void checkRoot() {
        Process proc;
        try {
            LogUtils.d("checkRoot", "Starting");

            Intent mainIntent = new Intent(Intent.ACTION_MAIN, null);
            mainIntent.addCategory(Intent.CATEGORY_LAUNCHER);
            List<ResolveInfo> pkgAppsList = BackupApp.getInstance().getPackageManager().queryIntentActivities(mainIntent, 0);

            String packageName = pkgAppsList.get(0).activityInfo.packageName;
            proc = Runtime.getRuntime().exec(new String[]{"su", "-c", "pm enable " + packageName
//                    BackupApp.getInstance().getPackageName()
            });
            proc.waitFor();
            int exitValue = proc.exitValue();
            if (exitValue == 0) {
                LogUtils.d("checkRoot", "Finished. Root received, exitValue = " + exitValue);
                rootReceived = true;
            } else if (exitValue == 1) {
                LogUtils.d("checkRoot", "Finished. Root failed, exitValue = " + exitValue);
                rootReceived = false;
            }
        } catch (IOException e) {
            e.printStackTrace();
            LogUtils.d("checkRoot", "Finished. Root failed");
            rootReceived = false;
        } catch (InterruptedException e) {
            e.printStackTrace();
            LogUtils.d("checkRoot", "Finished. Root failed");
            rootReceived = false;
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == DEVICE_ADMIN_ADD_RESULT_ENABLE) {
            switch (resultCode) {
                case Activity.RESULT_CANCELED:
                    LogUtils.d(TAG, "Request failed.");
                    onAdminModeCanceled();
                    break;
                case Activity.RESULT_OK:
                    LogUtils.d(TAG, "Device administrator activated.");
                    onAdminModeReceived(false);
                    break;
            }
        }
    }

    public void setAdminResult(IAdminResult samsungAdminResult) {
        this.onAdminResult = samsungAdminResult;
    }

    protected void onAdminModeCanceled() {
        if (onAdminResult != null) onAdminResult.onAdminModeCanceled();
    }

    public void onAdminModeReceived(boolean alreadyWas) {
        if (onAdminResult != null) onAdminResult.onAdminModeReceived(alreadyWas);
    }

    @Override
    public void onResume() {
        if (onRootedResumeRunnable != null && isRootoed()) {
            makeAction(onRootedResumeRunnable);
            onRootedResumeRunnable = null;
        }
    }

    @Override
    public void setAppCanBeUninstalled(String packageName, boolean state) {

    }

    @Override
    public boolean isAppCanBeUninstalled(String packageName) {
        return false;
    }

    @Override
    public void enableApp(String packageName, boolean enable) {
        String command;
        if (enable)
            command = "pm enable " + packageName;
        else
            command = "pm disable " + packageName;
        executeCommand(command);
    }

    @Override
    public boolean isAppEnabled(String packageName) {
        return false;
    }

    @Override
    public boolean isApplicationInstalled(String packageName) {
        return false;
    }

    @Override
    public void installApplication(String apkFilePath) {

    }

    @Override
    public void uninstallApplication(String packageName) {
        try {
            String command;
            command = "pm uninstall " + packageName;
            executeCommand(command);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void turnProxyOn() {

    }

    @Override
    public void turnProxyOff() {

    }

    @Override
    public boolean isProxyOn() {
        return false;
    }

    @Override
    public void removePermission(String permission, String packageName) {

    }

    @Override
    public void enableComponent(ComponentName componentName, boolean enable) {

    }

    @Override
    public boolean makeAction(Runnable runnable) {
        runnable.run();
        return true;
    }

    @Override
    public void updateBlockedUrls() {
        executeCommand("mount -o remount,rw /system");

        File file = new File(getFileDirPath(), "/hosts");
        ArrayList<String> listUrls = Prefs.getListString(Prefs.BLACK_LIST_URLS);
        FileOutputStream fOut = null;
        BufferedWriter bw = null;
        try {
            fOut = new FileOutputStream(file);
            bw = new BufferedWriter(new OutputStreamWriter(fOut));
            for (int i = 0; i < listUrls.size(); i++) {
                bw.write("127.0.0.1 " + listUrls.get(i));
                bw.newLine();
            }
            bw.close();
            fOut.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        File defFile = new File("/data/data/com.backup.israel/hosts_def");
        if (listUrls.size() == 0) {
            if (defFile.exists())
                executeCommand("cp /data/data/com.backup.israel/hosts_def /system/etc/hosts");
//            CommandsUtils.executeShCommand(getFileDirPath() + "/scripts/copyhosts.sh stop");
        } else {
            if (!defFile.exists())
                executeCommand("cp /system/etc/hosts /data/data/com.backup.israel/hosts_def");
            executeCommand("cp /data/data/com.backup.israel/hosts /system/etc/hosts");
            executeCommand("chmod 0644 /system/etc/hosts");
//            CommandsUtils.executeShCommand(getFileDirPath() + "/scripts/copyhosts.sh start");
        }
    }

    public static String getFileDirPath() {
        return "/data/data/" + BackupApp.getInstance().getPackageName();
    }

    public static void executeCommand(String command) {
        Process proc;
        try {
            //su
            proc = Runtime.getRuntime().exec(new String[]{"su", "-c", command});
            proc.waitFor();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void setRootResult(IRootResult iRootResult) {
        this.iRootResult = iRootResult;
    }

}
