package com.backup.manufacturer;


public interface IRootResult {
    
    public void onRootReceivedAndResumed();
}
